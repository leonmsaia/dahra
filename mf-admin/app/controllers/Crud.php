<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->library('image_CRUD');
	}
	// Render Initialization
	public function renderCrud($output = null)
	{
		if ($this->ion_auth->logged_in()) {
			$this->load->view('example.php',$output);
		}else{
			redirect('/auth/login');
		}
	}
	public function index()
	{
		$this->renderCrud((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}
	// Render Initialization End
	// General Site Configuration
	public function SiteConfView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'site_conf';
			$data['title'] = 'Configuracion del Sitio';
			$data['desc'] = 'Configuracion general de Informacion del Sitio';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function site_conf()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('site_conf');
			$crud->set_subject('Configuracion de Sitio');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->display_as('site_name','Nombre de Sitio')
			     ->display_as('site_author','Autor del Sitio')
			     ->display_as('site_desc','Descripcion del Sitio')
			     ->display_as('site_keywords','Keywords')
			     ->display_as('site_favicon','Favicon')
					 ->display_as('site_appleicon','Apple Icon')
					 ->display_as('site_logo','Logo Header')
					 ->display_as('site_logofoot','Logo Footer')
					 ->display_as('site_charset','Charset')
					 ->display_as('site_lang','Lenguaje')
					 ->display_as('site_mail','E-Mail')
					 ->display_as('cooming_soon','Modo Mantenimiento');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function SMTPConfView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'smtp_conf';
			$data['title'] = 'Configuracion de SMTP';
			$data['desc'] = 'Configuracion general de Protoclo SMTP';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function smtp_conf()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('smtp_mail_conf');
			$crud->set_subject('Configuracion de SMTP');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->display_as('smtp_host','SMTP: Host')
			     ->display_as('smtp_port','SMTP: Puerto')
			     ->display_as('smtp_timeout','SMTP: Timeout')
					 ->display_as('smtp_user','SMTP: Usuario')
			     ->display_as('smtp_pass','SMTP: Contraseña')
					 ->display_as('smtp_charset','SMTP: Charset')
					 ->display_as('smtp_newline','SMTP: Newline')
					 ->display_as('smtp_mailtype','SMTP: Tipo de Mail')
					 ->display_as('smtp_validation','SMTP: Validacion')
					 ->display_as('from','SMTP: Remitente');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// General Site Configuration End
	// Premiun User
	public function PremiunUserView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'premiun_user';
			$data['title'] = 'Informacion de Usuarios Premiun';
			$data['desc'] = 'ABM de Informacion de Usuarios Premiun';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function premiun_user()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('artist_information');
			$crud->set_subject('Usuario Premiun');
			$crud->set_relation('user_id','users','username');
			$crud->set_relation('artist_country','loc_pais','nombre');
			$crud->set_relation('artist_province','loc_provincia','nombre');
			$crud->set_relation('artist_party','loc_partido','nombre');
			$crud->set_relation('artist_location','loc_localidad','nombre');
			$crud->set_relation('artist_neighbour','loc_barrio','nombre');
			$crud->set_relation('artist_subneighbour','loc_subbarrio','nombre');
			$crud->display_as('user_id','Usuario Asociado a Cuenta')
			     ->display_as('artist_name','Nombre Premiun')
			     ->display_as('artist_lastname','Apellido Premiun')
					 ->display_as('artist_slug','Perfil Premiun Slug (URL)')
			     ->display_as('artist_short_desc','Descripcion Corta')
					 ->display_as('artist_bio','Descripcion Larga')
					 ->display_as('artist_actual_studies','Marco Referencial 1')
					 ->display_as('artist_past_studies','Marco Referencial 2')
					 ->display_as('artist_picture','Foto de Perfil')
					 ->display_as('artist_birth','Fecha de Nacimiento/Fundacion')
					 ->display_as('artist_mail','E-Mail')
					 ->display_as('artist_phone','Telefono')
					 ->display_as('artist_adress','Direccion')
					 ->display_as('artist_adress_number','Numeracion')
					 ->display_as('artist_zipcode','Codigo Postal')
					 ->display_as('artist_country','Pais')
					 ->display_as('artist_province','Provincia')
					 ->display_as('artist_party','Partido')
					 ->display_as('artist_location','Localidad')
					 ->display_as('artist_neighbour','Barrio')
					 ->display_as('artist_subneighbour','Sub Barrio');
			$crud->callback_after_insert(array($this, 'gen_premiun_user_slug'));
			$crud->callback_after_update(array($this, 'gen_premiun_user_slug'));
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function gen_premiun_user_slug($post_array, $primary_key){
	  $this->load->helper('url');
	  $name = normaliza($post_array['artist_name'] . ' ' . $post_array['artist_lastname']);
	  $url_title = url_title($name, '-', TRUE);
	  $data = array(
	  	'artist_slug' => 'PRM_' . generateID() . '_' . $url_title
	  );
	  $this->db->where('artist_id', $primary_key);
	  $this->db->update('artist_information',$data);
	  return true;
	}
	public function FavPremiunUserView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'fav_premiun_user';
			$data['title'] = 'Usuarios Premiun Favoritos';
			$data['desc'] = 'ABM de Usuarios Premiun Favoritos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function fav_premiun_user()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_fav_artist_register');
			$crud->set_subject('Usuarios Premiun Favoritos');
			$crud->set_relation('artist_id','users','username');
			$crud->set_relation('faver_user','users','username');
			$crud->display_as('artist_id','Usuario Premiun Favorito')
			     ->display_as('faver_user','Usuario General Fan');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// Premiun User End
	// Catalogue
	// Categories, Subcategories Methods
	public function CategorieView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'categorie';
			$data['title'] = 'Categorias';
			$data['desc'] = 'ABM de Categorias';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function categorie()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_category');
			$crud->set_subject('Categoria');
			$crud->display_as('category_name','Nombre de Categoria')
			     ->display_as('category_slug','Slug (URL)')
			     ->display_as('category_desc','Descripcion de Categoria')
			     ->display_as('category_img','Imagen de Categoria');
			$crud->callback_after_insert(array($this, 'gen_categorie_slug'));
			$crud->callback_after_update(array($this, 'gen_categorie_slug'));
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function gen_categorie_slug($post_array, $primary_key){
		$this->load->helper('url');
		$name = normaliza($post_array['category_name']);
		$url_title = url_title($name, '-', TRUE);
		$data = array(
			'category_slug' => $url_title
		);
		$this->db->where('category_id', $primary_key);
		$this->db->update('mrk_category', $data);
		return true;
	}
	public function SubCategorieView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'subcategorie';
			$data['title'] = 'Subcategorias';
			$data['desc'] = 'ABM de Subcategorias';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function subcategorie()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_subcategory');
			$crud->set_subject('Subcategoria');
			$crud->display_as('subcategory_name','Nombre de Subcategoria')
			     ->display_as('subcategory_slug','Slug (URL)')
			     ->display_as('subcategory_desc','Descripcion de Subcategoria')
					 ->display_as('subcategory_category','Categoria Padre')
			     ->display_as('subcategory_img','Imagen de Subcategoria');
			$crud->set_relation('subcategory_category','mrk_category','category_name');
			$crud->callback_after_insert(array($this, 'gen_subcategorie_slug'));
			$crud->callback_after_update(array($this, 'gen_subcategorie_slug'));
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function gen_subcategorie_slug($post_array, $primary_key){
		$this->load->helper('url');
		$name = normaliza($post_array['subcategory_name']);
		$url_title = url_title($name, '-', TRUE);
		$data = array(
			'subcategory_slug' => $url_title
		);
		$this->db->where('category_id', $primary_key);
		$this->db->update('mrk_category', $data);
		return true;
	}
	// Categories, Subcategories Methods End
	// Products and Assets Methods
	public function ProductView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product';
			$data['title'] = 'Productos';
			$data['desc'] = 'ABM de Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_product');
			$crud->set_subject('Producto');
			$crud->set_relation('product_subcategory','mrk_subcategory','subcategory_name');
			$crud->set_relation('product_category','mrk_category','category_name');
			$crud->set_relation('product_author','users','username');
			$crud->display_as('product_name','Nombre de Producto')
			     ->display_as('product_code','Codigo de Producto')
			     ->display_as('product_slug','Slug (URL)')
			     ->display_as('product_desc','Descripcion de Producto')
			     ->display_as('product_qty','Cantidad disponible')
					 ->display_as('product_text','Texto General de Producto')
					 ->display_as('product_keyword','Keyword de Producto')
					 ->display_as('product_tags','Etiquetas de Producto')
					 ->display_as('product_materials','Marco Referencial 1')
					 ->display_as('product_subcategory','Subcategoria de Producto')
					 ->display_as('product_category','Categoria de Producto')
					 ->display_as('product_price','Precio de Venta de Producto')
					 ->display_as('product_status','Estado de Producto')
					 ->display_as('product_img','Imagen de Producto')
					 ->display_as('product_author','Autor de Producto')
					 ->display_as('product_exposition','Nivel de Exposicion de Producto')
					 ->display_as('product_pay_method','Metodo de Pago de Producto');
			$crud->callback_after_insert(array($this, 'gen_product_slug'));
			$crud->callback_after_update(array($this, 'gen_product_slug'));
			$crud->field_type(
			  'product_exposition',
			  'dropdown',
			  array(
			    '1' => 'Baja',
			    '2' => 'Media',
					'3' => 'Alta',
					'4' => 'Premiun'
			  )
			);
			$crud->field_type(
			  'product_status',
			  'dropdown',
			  array(
			    '0' => 'Finalizada',
			    '1' => 'Activada',
					'2' => 'Pausado',
					'8' => 'Sin Activar'
			  )
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function gen_product_slug($post_array, $primary_key){
		$this->load->helper('url');
		$name = normaliza($post_array['product_name']);
		$product_code = normaliza($post_array['product_code']);
		$url_title = url_title($name, '-', TRUE);
		$data = array(
			'product_slug' => 'PROD_' . $url_title . '_' . $product_code
		);
		$this->db->where('product_id', $primary_key);
		$this->db->update('mrk_product', $data);
		return true;
	}
	public function ProductAssetsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'products_assets';
			$data['title'] = 'Assets de Productos';
			$data['desc'] = 'ABM de Assets de Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function products_assets()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mkt_product_assets');
			$crud->set_subject('Asset de Producto');
			$crud->display_as('product_id','Producto Asociado')
			     ->display_as('product_image_path','Ruta de Imagen');
			$crud->set_relation('product_id','mrk_product','product_name');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductDetailsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_details';
			$data['title'] = 'Detalles de Productos';
			$data['desc'] = 'ABM de Detalles de Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_details()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_product_details');
			$crud->set_subject('Detalle de Producto');
			$crud->display_as('product_id','Producto Asociado')
			     ->display_as('mrk_product_detail_title','Titulo de Detalle')
					 ->display_as('mrk_product_detail_desc','Descripcion de Detalle');
			$crud->set_relation('product_id','mrk_product','product_name');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductImageSupportView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_image_support';
			$data['title'] = 'Soporte en Imagenes de Productos';
			$data['desc'] = 'ABM de Soporte en Imagenes de Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_image_support()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_product_img_support');
			$crud->set_subject('Soporte en Imagen de Producto');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('product_code','Codigo de Producto')
					 ->display_as('image_path_one','Imagen Soporte 1')
					 ->display_as('image_path_two','Imagen Soporte 2')
					 ->display_as('image_path_three','Imagen Soporte 3')
					 ->display_as('image_path_four','Imagen Soporte 4');
			$crud->set_relation('product_id','mrk_product','product_name');
			$crud->set_relation('product_code','mrk_product','product_code');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductTermsSaleView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_terms_sale';
			$data['title'] = 'Terminos de Contratacion para Productos';
			$data['desc'] = 'ABM de Terminos de Contratacion para Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_terms_sale()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_product_sale_terms');
			$crud->set_subject('Terminos de Contratacion para Producto');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('product_code','Codigo de Producto')
					 ->display_as('initial_date','Fecha de Inicio')
					 ->display_as('expiration_date','Fecha de Expiracion')
					 ->display_as('exposition','Nivel de Exposicion')
					 ->display_as('status','Estado');
			$crud->field_type(
 				'exposition',
 				'dropdown',
 				array(
 					'1' => 'Baja',
 					'2' => 'Media',
 					'3' => 'Alta',
 					'4' => 'Premiun'
 				)
 			);
			$crud->field_type(
				'status',
				'dropdown',
				array(
					'0' => 'Inactivo',
					'1' => 'Activo'
				)
			);
			$crud->set_relation('product_id','mrk_product','product_name');
			$crud->set_relation('product_code','mrk_product','product_code');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductShippingView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_shipping';
			$data['title'] = 'Terminos de Condiciones de Envio';
			$data['desc'] = 'ABM de Terminos de Condiciones de Envio';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_shipping()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_product_shipping');
			$crud->set_subject('Terminos de Condiciones de Envio');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('product_code','Codigo de Producto')
					 ->display_as('know_transport','Transporte Conocido')
					 ->display_as('owner_pay','Abona Vendedor')
					 ->display_as('buyer_pay','Abona Comprador')
					 ->display_as('personal_withdrawal','Retiro por Comprador');
			$crud->field_type(
 				'know_transport',
 				'dropdown',
 				array(
 					'0' => 'Inactivo',
 					'1' => 'Activo'
 				)
 			);
			$crud->field_type(
 				'owner_pay',
 				'dropdown',
 				array(
 					'0' => 'Inactivo',
 					'1' => 'Activo'
 				)
 			);
			$crud->field_type(
 				'buyer_pay',
 				'dropdown',
 				array(
 					'0' => 'Inactivo',
 					'1' => 'Activo'
 				)
 			);
			$crud->field_type(
 				'personal_withdrawal',
 				'dropdown',
 				array(
 					'0' => 'Inactivo',
 					'1' => 'Activo'
 				)
 			);
 			$crud->set_relation('product_id','mrk_product','product_name');
 			$crud->set_relation('product_code','mrk_product','product_code');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductSizesView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_sizes';
			$data['title'] = 'Terminos de Tamaños de Producto';
			$data['desc'] = 'ABM de Terminos de Tamaños de Producto';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_sizes()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_product_size');
			$crud->set_subject('Terminos de Terminos de Tamaños de Producto');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('product_code','Codigo de Producto')
					 ->display_as('weight','Peso (gr.)')
					 ->display_as('size_z','Alto (cm.)')
					 ->display_as('size_x','Ancho (cm.)')
					 ->display_as('size_y','Largo (cm.)')
					 ->display_as('size_container','Tipo de Dimension');
			$crud->set_relation('product_id','mrk_product','product_name');
 			$crud->set_relation('product_code','mrk_product','product_code');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function FavProductsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'fav_products';
			$data['title'] = 'Productos Favoritos';
			$data['desc'] = 'ABM de Productos Favoritos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function fav_products()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_fav_prod_register');
			$crud->set_subject('Producto Favorito');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('faver_user','Largo (cm.)')
					 ->display_as('faved_user','Tipo de Dimension');
			$crud->set_relation('product_id','mrk_product','product_name');
			$crud->set_relation('faver_user','use','username');
			$crud->set_relation('faved_user','use','username');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductQuestionsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_questions';
			$data['title'] = 'Preguntas de Productos';
			$data['desc'] = 'ABM de Preguntas de Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_questions()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mkt_question');
			$crud->set_subject('Pregunta de Producto');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('user_id','Autor de Producto')
					 ->display_as('question_author','Autor de Pregunta')
					 ->display_as('question_time','Fecha y Hora de Pregunta')
					 ->display_as('question_status','Estado de Pregunta')
					 ->display_as('question_text','Texto de Pregunta');
			$crud->set_relation('product_id','mrk_product','product_name');
			$crud->set_relation('user_id','use','username');
			$crud->set_relation('question_author','use','username');
			$crud->field_type(
 				'question_status',
 				'dropdown',
 				array(
 					'0' => 'No Respondida',
 					'1' => 'Respondida'
 				)
 			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ProductAnswersView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'product_answers';
			$data['title'] = 'Respuestas de Productos';
			$data['desc'] = 'ABM de Respuestas de Productos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function product_answers()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mkt_question_answer');
			$crud->set_subject('Respuesta de Producto');
			$crud->display_as('product_id','Producto Asociado')
					 ->display_as('question_id','Pregunta Original')
					 ->display_as('answer_author','Autor de Respuesta')
					 ->display_as('answer_time','Fecha y Hora de Respuesta')
					 ->display_as('answer_text','Texto de Respuesta');
			$crud->set_relation('question_id','mkt_question','question_text');
			$crud->set_relation('answer_author','use','username');
			$crud->set_relation('product_id','mrk_product','product_name');
			$crud->field_type(
 				'answer_status',
 				'dropdown',
 				array(
 					'0' => 'No Respondida',
 					'1' => 'Respondida'
 				)
 			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// Products and Assets Methods End
	// Catalogue End
	// Commercial Methods
	public function TokenValueView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'token_value';
			$data['title'] = 'Valor de Token';
			$data['desc'] = 'Modificador de Valor de Token';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function token_value()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_configuration');
			$crud->set_subject('Valor de Token');
			$crud->display_as('mrk_configuration_credit_cost','Valor de Token');
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function TokensPlanView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'tokens_plan';
			$data['title'] = 'Planes de Tokens';
			$data['desc'] = 'ABM de Planes de Tokens';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function tokens_plan()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_point_plans');
			$crud->set_subject('Plan de Tokens');
			$crud->display_as('mrk_point_plans_title','Titulo de Plan')
			     ->display_as('mrk_point_plans_desc','Descripcion de Plan')
			     ->display_as('mrk_point_plans_qty','Valor en Tokens de Plan')
			     ->display_as('mrk_point_plans_discount','Descuento Aplicado en Plan')
			     ->display_as('mrk_point_plans_high','Plan Destacado')
					 ->display_as('mrk_point_plans_date','Fecha de Creacion de Plan');
			$crud->field_type(
			  'mrk_point_plans_high',
			  'dropdown',
			  array(
			    '1' => 'Habilitar',
			    '0' => 'Desabilitar'
			  )
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function PaymentMethodsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'payment_methods';
			$data['title'] = 'Metodos de Pago';
			$data['desc'] = 'ABM de Metodos de Pago';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function payment_methods()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_pay_method');
			$crud->set_subject('Metodo de Pago');
			$crud->display_as('pay_method_name','Nombre de Metodo de Pago')
			     ->display_as('pay_method_url','URL de Metodo de Pago')
			     ->display_as('pay_method_ico','Icono de Metodo de Pago')
			     ->display_as('pay_method_comission','Comision de Metodo de Pago')
			     ->display_as('pay_method_status','Estado de Metodo de Pago');
			$crud->field_type(
			  'pay_method_status',
			  'dropdown',
			  array(
			    '1' => 'Activo',
			    '0' => 'Inactivo'
			  )
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function DiscountCodeView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'discount_code';
			$data['title'] = 'Codigos de Descuento';
			$data['desc'] = 'ABM de Codigos de Descuento';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function discount_code()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_discount_code');
			$crud->set_subject('Codigo de Descuento');
			$crud->display_as('mrk_discount_code_code','Codigo de Descuento')
			     ->display_as('mrk_discount_code_name','Nombre de Descuento')
					 ->display_as('mrk_discount_code_date_init','Fecha de Inicio de Descuento')
			     ->display_as('mrk_discount_code_date_end','Fecha de Finalizacion de Descuento')
			     ->display_as('mrk_discount_code_status','Estado del Descuento')
			     ->display_as('mrk_discount_code_discount_type','Tipo de Descuento')
					 ->display_as('mrk_discount_code_discount_value','Valor de Descuento');
			$crud->field_type(
			  'mrk_discount_code_discount_type',
			  'dropdown',
			  array(
			    '1' => 'Valor Fijo',
			    '0' => 'Valor Porcentual'
			  )
			);
			$crud->field_type(
			  'mrk_discount_code_status',
			  'dropdown',
			  array(
			    '1' => 'Habilitado',
			    '0' => 'Inhabilitado'
			  )
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function CreditOperationView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'credit_operation';
			$data['title'] = 'Operaciones Crediticias';
			$data['desc'] = 'ABM de Operaciones Crediticias';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function credit_operation()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mrk_credit_operation');
			$crud->set_subject('Operacion Crediticia');
			$crud->set_relation('mrk_credit_operation_user_id','users','username');
			$crud->display_as('mrk_credit_operation_user_id','Usuario General Objetivo')
			     ->display_as('mrk_credit_operation_qty','Monto de Operacion (Tokens)')
			     ->display_as('mrk_credit_operation_type','Tipo de Operacion')
			     ->display_as('mrk_credit_operation_date','Fecha de Operacion')
			     ->display_as('mrk_credit_operation_operation_id','Codigo de Operacion');
			$crud->field_type(
 			  'mrk_credit_operation_type',
 			  'dropdown',
 			  array(
 			    '1' => 'Ingreso',
 			    '2' => 'Egreso'
 			  )
 			);
			$crud->callback_after_insert(array($this, 'gen_credit_operation_code'));
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function gen_credit_operation_code($post_array, $primary_key){
	  $operation_code = 'CRED_OP_' . generateID();
	  $data = array(
	  	'mrk_credit_operation_operation_id' => $operation_code
	  );
	  $this->db->where('mrk_credit_operation_id', $primary_key);
	  $this->db->update('mrk_credit_operation', $data);
	  return true;
	}
	public function BillingRegisterView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'billing_register';
			$data['title'] = 'Registros de Facturacion';
			$data['desc'] = 'ABM de Registros de Facturacion';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function billing_register()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('site_recipe_entry_register');
			$crud->set_subject('Registro de Facturacion');
			$crud->set_relation('code','mrk_credit_operation','mrk_credit_operation_operation_id');
			$crud->set_relation('user_id','users','username');
			$crud->display_as('user_id','Usuario General Objetivo')
			     ->display_as('date','Fecha de la Operacion')
			     ->display_as('amount','Monto de la Operacion')
			     ->display_as('code','Codigo de Operacion')
			     ->display_as('state','Estado de Operacion');
			$crud->field_type(
 			  'state',
 			  'dropdown',
 			  array(
 			    '1' => 'Activo',
 			    '0' => 'Inactivo'
 			  )
 			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// Commercial Methods End
	// Ads & Banner Methods
	public function AdsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'ads';
			$data['title'] = 'Avisos Publicitarios';
			$data['desc'] = 'ABM de Avisos Publicitarios';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function ads()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mkt_ads');
			$crud->set_subject('Avisos Publicitarios');
			$crud->set_relation('mkt_ads_user_author','users','username');
			$crud->display_as('mkt_ads_title','Titulo de Aviso Publicitario')
			     ->display_as('mkt_ads_adscode','Codigo de Aviso Publicitario')
			     ->display_as('mkt_ads_desc','Descripcion de Aviso Publicitario')
			     ->display_as('mkt_ads_print_initial','Impresiones de Aviso Publicitario')
			     ->display_as('mkt_ads_date','Fecha de Publicacion de Aviso Publicitario')
					 ->display_as('mkt_ads_url','URL Objetiva de Aviso Publicitario')
					 ->display_as('mkt_ads_clickpointer','Clicks de Aviso Publicitario')
					 ->display_as('mkt_ads_image','Imagen de Aviso Publicitario')
					 ->display_as('mkt_ads_user_author','Autor de Aviso Publicitario')
					 ->display_as('mkt_ads_relevance','Relevancia de Aviso Publicitario')
					 ->display_as('mkt_status','Estado de Aviso Publicitario');
			$crud->field_type(
 			  'mkt_status',
 			  'dropdown',
 			  array(
 			    '0' => 'Finalizado',
					'1' => 'Activado',
					'2' => 'Pausado',
 			    '8' => 'Sin Activar'
 			  )
 			);
			$crud->field_type(
 			  'mkt_ads_relevance',
 			  'dropdown',
 			  array(
 			    '1' => 'Baja',
					'2' => 'Media',
					'3' => 'Alta'
 			  )
 			);
			$crud->callback_after_insert(array($this, 'gen_ads_publication_code'));
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function gen_ads_publication_code($post_array, $primary_key){
		$operation_code = 'ADS_' . generateID();
		$data = array(
			'mkt_ads_adscode' => $operation_code
		);
		$this->db->where('mkt_ads_id', $primary_key);
		$this->db->update('mkt_ads', $data);
		return true;
	}
	public function AdsPlansView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'adsplans';
			$data['title'] = 'Planes de Avisos Publicitarios';
			$data['desc'] = 'ABM de Planes de Avisos Publicitarios';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function adsplans()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mkt_ads_plans');
			$crud->set_subject('Plan de Avisos Publicitarios');
			$crud->display_as('mkt_ads_plans_title','Titulo de Plan')
			     ->display_as('mkt_ads_plans_desc','Descripcion de Plan')
			     ->display_as('mkt_ads_plans_qty_cost','Valor en Tokens de Plan')
			     ->display_as('mkt_ads_plans_prints','Impresiones Aplicadas en Plan')
					 ->display_as('mkt_ads_plans_clics','Clicks Aplicados en Plan')
			     ->display_as('mkt_ads_plans_high','Plan Destacado')
					 ->display_as('mkt_ads_plans_date','Fecha de Creacion de Plan');
			$crud->field_type(
			  'mkt_ads_plans_high',
			  'dropdown',
			  array(
			    '1' => 'Habilitar',
			    '0' => 'Desabilitar'
			  )
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// Ads & Banner Methods End

	// General Users Methods
	public function GeneralUsersView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'general_users';
			$data['title'] = 'Usuarios Generales';
			$data['desc'] = 'ABM de Usuarios Generales';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function general_users()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('users');
			$crud->set_subject('Usuario');
			$crud->set_relation('pais','loc_pais','nombre');
			$crud->set_relation('provincia','loc_provincia','nombre');
			$crud->set_relation('partido','loc_partido','nombre');
			$crud->set_relation('localidad','loc_localidad','nombre');
			$crud->set_relation('barrio','loc_barrio','nombre');
			$crud->set_relation('subbarrio','loc_subbarrio','nombre');
			$crud->display_as('ip_address','Direccion IP')
					 ->display_as('username','Nombre de Usuario')
					 ->display_as('password','Contraseña')
					 ->display_as('salt','SALT')
					 ->display_as('activation_code','Codigo de Activacion')
					 ->display_as('forgotten_password_code','Codigo de Reestablecimiento de Contraseña')
					 ->display_as('forgotten_password_time','Tiempo de Reestablecimiento de Contraseña')
					 ->display_as('remember_code','Codigo de Recuerdo')
					 ->display_as('created_on','Fecha de Creacion')
					 ->display_as('last_login','Ultimo Login')
					 ->display_as('active','Estado de Activacion')
					 ->display_as('first_name','Nombre')
					 ->display_as('last_name','Apellido')
					 ->display_as('dni','Numero de Identidad')
					 ->display_as('adress','Direccion')
					 ->display_as('company','Compañia/Empresa')
					 ->display_as('zipcode','Codigo Postal')
					 ->display_as('cuit','Codigo Fiscal')
					 ->display_as('phone','Telefono')
					 ->display_as('email','E-Mail')
					 ->display_as('facebook','Cuenta de Facebook')
					 ->display_as('twitter','Cuenta de Twitter')
					 ->display_as('oauth_provider','Proveedor OAuth')
					 ->display_as('oauth_uid','UID OAuth')
					 ->display_as('gender','Genero')
					 ->display_as('locale','Local')
					 ->display_as('image_path','Imagen')
					 ->display_as('cover','Imagen Cover')
					 ->display_as('link','Link OAuth')
					 ->display_as('picture','Fotografia')
					 ->display_as('pais','Pais')
					 ->display_as('provincia','Provincia')
					 ->display_as('partido','Partido')
					 ->display_as('localidad','Localidad')
					 ->display_as('barrio','Barrio')
					 ->display_as('subbarrio','Sub Barrio')
					 ->display_as('zip','Codigo Postal')
					 ->display_as('about','Breve Reseña de Usuario');
			$crud->field_type(
 			  'active',
 			  'dropdown',
 			  array(
 			    '1' => 'Activo',
 			    '0' => 'Inactivo'
 			  )
 			);
			$crud->unset_columns(array('zip'));
			$crud->unset_edit_fields(array('zip'));
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function GroupsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'groups';
			$data['title'] = 'Grupos de Usuarios';
			$data['desc'] = 'ABM de Grupos de Usuarios';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function groups()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('groups');
			$crud->set_subject('Grupo de Usuario');
			$crud->display_as('name','Nombre del Grupo')
			     ->display_as('description','Descripcion del Grupo');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function UserInGroupView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'users_in_group';
			$data['title'] = 'Usuarios en Grupos';
			$data['desc'] = 'ABM de Usuarios en Grupos';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function users_in_group()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('users_groups');
			$crud->set_subject('Usuarios en Grupos');
			$crud->set_relation('user_id','users','username');
			$crud->set_relation('group_id','groups','name');
			$crud->display_as('user_id','Usuario General Objetivo')
			     ->display_as('group_id','Grupo Objetivo');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function UserNotificationsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'user_notifications';
			$data['title'] = 'Notificaciones de Usuarios';
			$data['desc'] = 'ABM de Notificaciones de Usuarios';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function user_notifications()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('users_notification_setting');
			$crud->set_subject('Notificaciones de Usuarios');
			$crud->set_relation('user_id','users','username');
			$crud->display_as('user_id','Usuario General Objetivo')
					 ->display_as('sale_work','Contacto Realizado por el Usuario General')
					 ->display_as('question_work','Pregunta Realizada en Producto del Usuario General')
					 ->display_as('finish_work','Finalizacion de Aviso del Usuario General')
					 ->display_as('repub_work','Republicacion de Aviso del Usuario General')
					 ->display_as('buy_work','Consulta Realizada por el Usuario General')
					 ->display_as('almost_finish_work','Aviso a Punto de Finalizar del Usuario General')
					 ->display_as('write_message','Mensaje Escrito al Usuario General')
					 ->display_as('oferts_promotions','Envio de Ofertas y Promociones del Site')
					 ->display_as('terms_cond','Actualizacion de Terminos y Condiciones');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function UserMessagesView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'user_messages';
			$data['title'] = 'Mensajes de Usuario';
			$data['desc'] = 'ABM de Mensajes de Usuario';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function user_messages()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('user_message');
			$crud->set_subject('Mensaje de Usuario');
			$crud->set_relation('message_author','users','username');
			$crud->set_relation('message_receptor','users','username');
			$crud->set_relation('message_prod_id','mrk_product','product_name');
			$crud->set_relation('message_original_thread','user_message','message_text');
			$crud->display_as('message_author','Autor del Mensaje')
					 ->display_as('message_prod_id','Producto Relacionado')
					 ->display_as('message_receptor','Usuario General Receptor de Mensaje')
					 ->display_as('message_read','Lectura y Respuesta de Mensaje')
					 ->display_as('message_date','Fecha de Envio del Mensaje')
					 ->display_as('message_text','Cuerpo del Mensaje')
					 ->display_as('message_original_thread','Mensaje Original Relacionado');
			$crud->field_type(
 			  'message_read',
 			  'dropdown',
 			  array(
 			    '1' => 'Leído',
 			    '0' => 'No Leído'
 			  )
 			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	// General Users Methods End


	// Support Methods
	public function CoomingSoon()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'storeconf';
			$data['title'] = 'Proximamente';
			$data['desc'] = 'El contenido estara disponible en la proxima actualizacion.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/coomingsoon', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function StatsView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'stats';
			$data['title'] = 'Estadisticas';
			$data['desc'] = 'Disponible en proximas actualizaciones.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/stats', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function TechSupport()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'storeconf';
			$data['title'] = 'Soporte Técnico';
			$data['desc'] = 'Por favor, rellena el formulario para solicitar asistencia, el mismo sera enviado al área de soporte ténico. La respuesta tardara entre 48 y 72 hs. en dias habiles..';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/support', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function NewsAndUpdates()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'storeconf';
			$data['title'] = 'Noticias y Novedades';
			$data['desc'] = 'Noticias y Novedades oficiales desde el Blog de FrikiCode.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			$this->load->view('v2/pages/news_updates', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function UserManualView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'storeconf';
			$data['title'] = 'Manual de Uso';
			$data['desc'] = 'Manual de Uso de Meifter CMS v2.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/user_manual', $data);
		}else{
			redirect('/auth/login');
		}
	}
	// Support Methods End

























	// General Site
	public function TextView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'general_text_crud';
			$data['title'] = 'Textos Generales del Sitio';
			$data['desc'] = 'Modificacion de Textos y Fotografias generales del Sitio';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function general_text_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('txt_esc');
			$crud->set_subject('Textos Generales');
			$crud->set_field_upload('txt_esc_contact_image','../assets/uploads/img/');
			$crud->display_as('txt_esc_contact_services_title','Titulo para Servicios')
					 ->display_as('txt_esc_contact_title','Titulo de Contacto')
					 ->display_as('txt_esc_services_title','Titulo de Servicios')
					 ->display_as('txt_esc_crew_title','Titulo de Equipo')
					 ->display_as('txt_esc_contact_image','Imagen de Contacto (Fondo)');
		 $crud->unset_add();
		 $crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function SlideCrudView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'slider_home_crud';
			$data['title'] = 'Carousel de Imagenes en Home';
			$data['desc'] = 'Alta, Baja y Modificacion de imagenes en Slider en Home.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function slider_home_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('slide_home');
			$crud->set_subject('Slide de Home');
			$crud->set_field_upload('path','../assets/uploads/slide/');
			$crud->display_as('text_princ','Titulo')
					 ->display_as('text_second','Descripcion')
					 ->display_as('link','Link')
					 ->display_as('link_disable','Habilitar Link')
					 ->display_as('path','Fotografia')
					 ->display_as('order','Orden');
		 $crud->field_type(
				'link_disable',
				'dropdown',
				array(
					'0' => 'Activo',
					'1' => 'Inactivo'
				)
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function ServiceCrudView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'service_crud';
			$data['title'] = 'Servicios';
			$data['desc'] = 'Alta, Baja y Modificacion de Servicios ofrecidos. Ver <a href="https://iconsmind.com/view_icons/" target="_blank">Iconos disponibles</a>. Nota: Al escribir el nombre del icono, reemplazar los espacios en blanco por guiones (-).';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function service_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('services');
			$crud->set_subject('Servicio');
			$crud->display_as('title','Titulo')
					 ->display_as('icon','Icono')
					 ->display_as('desc','Descripcion');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function CrewCrudView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'crew_crud';
			$data['title'] = 'Equipo';
			$data['desc'] = 'Alta, Baja y Modificacion de Equipo.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function crew_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('crew');
			$crud->set_subject('Equipo');
			$crud->set_field_upload('pic','../assets/uploads/crew/');
			$crud->display_as('name','Nombre')
					 ->display_as('lastname','Apellido')
					 ->display_as('position','Posicion')
					 ->display_as('bio','Breve Reseña')
					 ->display_as('pic','Fotografia')
					 ->display_as('facebook','Facebook')
					 ->display_as('twitter','Twitter')
					 ->display_as('instagram','Instagram')
					 ->display_as('linkedin','Linkedin');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	// Common Services
	public function ContactMSG()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'contact_msg';
			$data['title'] = 'Mensajes de Contacto';
			$data['desc'] = 'Contactos enviados por los usuarios';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function contact_msg()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('com_contact');
			$crud->set_subject('Mensaje de Contacto');
			$crud->display_as('com_contact_name','Nombre')
					 ->display_as('com_contact_lastname','Apellido')
					 ->display_as('com_contact_email','E-Mail')
					 ->display_as('com_contact_phone','Telefono')
					 ->display_as('com_contact_consult','Consulta');
			$crud->unset_add();
			$crud->unset_edit();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	// Contact Nfo
	public function contactNfo()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('contact_data');
			$crud->set_subject('Informacion de Contacto');
			$crud->display_as('contact_nfo_address','Direccion')
					 ->display_as('contact_nfo_phone','Telefono')
					 ->display_as('contact_nfo_mail','E-Mail')
					 ->display_as('contact_nfo_fb','Facebook')
					 ->display_as('contact_nfo_tw','Twitter')
					 ->display_as('contact_nfo_insta','Instagram')
					 ->display_as('contact_nfo_tw_user','Usuario de Twitter');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function InfoContactConf()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'contactNfo';
			$data['title'] = 'Informacion de Contacto';
			$data['desc'] = 'Configuracion de Parametros de Informacion de Contacto';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}

	// Site Functions
	public function SiteConfViews()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'siteconf';
			$data['title'] = 'Configuracion de Sitio';
			$data['desc'] = 'Configuracion de Factores Basicos del Site.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function siteconf()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('site_conf');
			$crud->set_subject('Configuracion de Sitio');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->set_field_upload('site_favicon','../assets/uploads/');
			$crud->set_field_upload('site_logo','../assets/uploads/');
			$crud->set_field_upload('site_logofoot','../assets/uploads/');
			$crud->set_field_upload('site_appleicon','../assets/uploads/');
			$crud->display_as('site_name','Nombre de Sitio')
					 ->display_as('site_author','Autor de Sitio')
					 ->display_as('site_desc','Descripcion de Sitio')
					 ->display_as('site_favicon','Favicon')
					 ->display_as('site_logo','Logo')
					 ->display_as('site_logofoot','Logo de Footer')
					 ->display_as('site_charset','Charset')
					 ->display_as('site_lang','Lenguaje de Sitio')
					 ->display_as('cooming_soon','Modo Mantenimiento')
					 ->display_as('site_appleicon','Imagen Social (2MB Max.)');
			$crud->field_type(
				'cooming_soon',
				'dropdown',
				array(
					'1' => 'Habilitar',
					'0' => 'Desabilitar'
				)
			);
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}



	// User
	public function UserListView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'userlist';
			$data['title'] = 'Lista de Usuarios';
			$data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Usuarios.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function userlist()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('users');
			$crud->set_subject('Usuario');
			$crud->unset_add();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	// About Module
	public function AboutView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'about';
			$data['title'] = 'Quienes Somos';
			$data['desc'] = 'Texto e Imagenes para modulo "Quienes Somos"';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function about()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('about');
			$crud->set_subject('Quienes Somos');
			$crud->set_field_upload('pic','../assets/uploads/img/');
			$crud->required_fields(
					 'title',
					 'subtitle',
					 'text',
					 'pic'
			);
			$crud->display_as('title','Titulo')
					 ->display_as('subtitle','Subtitulo')
					 ->display_as('pic','Fotografia')
					 ->display_as('text','Texto');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function ContactCrudView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'contact_crud';
			$data['title'] = 'Informacion de Contacto';
			$data['desc'] = 'Informacion de Contacto del Site.';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function contact_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('contact_data');
			$crud->set_subject('Informacion de Contacto');
			$crud->display_as('contact_nfo_address','Direccion')
					 ->display_as('contact_nfo_phone','Telefono')
					 ->display_as('contact_nfo_mail','Mail')
					 ->display_as('contact_nfo_fb','Facebook')
					 ->display_as('contact_nfo_tw','Twitter')
					 ->display_as('contact_nfo_linkedin','Linkedin');
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function LegalTextView()
	{
		if ($this->ion_auth->logged_in()) {
			$data['url'] = 'legal_text_crud';
			$data['title'] = 'Textos Legales';
			$data['desc'] = 'Textos Legales del Sitio. Adicional Codigo Afip F960';
			$data['secondCrud'] = FALSE;
			$data['url_second'] = '';
			$this->load->view('v2/pages/crud-holder', $data);
		}else{
			redirect('/auth/login');
		}
	}
	public function legal_text_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('txt_esc_legal');
			$crud->set_subject('Textos Legales');
			$crud->display_as('terms_cond_title','Terminos y Condiciones. Titulo.')
					 ->display_as('terms_cond_txt','Terminos y Condiciones. Texto.')
					 ->display_as('privacity_title','Ética Profesional. Titulo.')
					 ->display_as('privacity_txt','Ética Profesional. Texto.')
					 ->display_as('afip_form_f960','Codigo AFIP F960');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_columns(array('terms_cond_title','terms_cond_txt'));
			$crud->unset_edit_fields('terms_cond_title','terms_cond_txt');
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}
