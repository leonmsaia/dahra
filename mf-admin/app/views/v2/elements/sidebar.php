<div class="sidemenu">
  <div class="sidemenu-inner scroll">
    <?php $this->load->view('v2/elements/adminScreen');?>
    <nav class="admin-nav">
      <h6>Tools Principales</h6>
      <ul>
        <li>
          <a class="waves-effect" href="<?php echo base_url();?>" title=""><i class="ti-home red lighten-1"></i> Dashboard</a>
        </li>
        <li><a class="waves-effect" href="#" title=""><i class="ti-star orange lighten-1"></i> Usuarios Premiun</a>
          <ul>
            <li><a href="<?php echo base_url();?>crud/PremiunUserView" title="">Informacion de Usuarios Premiun</a></li>
            <li><a href="<?php echo base_url();?>crud/FavPremiunUserView" title="">Usuarios Premiun Favoritos</a></li>
          </ul>
        </li>
        <li><a class="waves-effect" href="#" title=""><i class="ti-layout-tab purple lighten-1"></i> Catalogo</a>
          <ul>
            <li><a href="<?php echo base_url();?>crud/CategorieView" title="">Categorias</a></li>
            <li><a href="<?php echo base_url();?>crud/SubCategorieView" title="">Subcategorias</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductView" title="">Productos</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductAssetsView" title="">Productos: Assets</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductDetailsView" title="">Productos: Detalles</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductImageSupportView" title="">Productos: Imagenes Soporte</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductTermsSaleView" title="">Productos: Terminos de Contratacion</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductShippingView" title="">Productos: Terminos de Envios</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductSizesView" title="">Productos: Terminos de Tamaño</a></li>
            <li><a href="<?php echo base_url();?>crud/FavProductsView" title="">Productos: Favoritos</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductQuestionsView" title="">Productos: Preguntas</a></li>
            <li><a href="<?php echo base_url();?>crud/ProductAnswersView" title="">Productos: Respuestas</a></li>
          </ul>
        </li>
        <li><a class="waves-effect" href="#" title=""><i class="ti-layout-tab red lighten-1"></i> Sitio</a>
          <ul>
            <li><a href="<?php echo base_url();?>crud/SiteConfView" title="">Configuracion de Sitio</a></li>
            <li><a href="<?php echo base_url();?>crud/SMTPConfView" title="">Configuracion de SMTP</a></li>
          </ul>
        </li>
        <li><a class="waves-effect" href="#" title=""><i class="ti-briefcase blue lighten-1"></i> Comerciales</a>
          <ul>
            <li><a href="<?php echo base_url();?>crud/TokenValueView" title="">Valor de Token</a></li>
            <li><a href="<?php echo base_url();?>crud/TokensPlanView" title="">Planes de Tokens</a></li>
            <li><a href="<?php echo base_url();?>crud/PaymentMethodsView" title="">Metodos de Pago</a></li>
            <li><a href="<?php echo base_url();?>crud/DiscountCodeView" title="">Codigos de Descuento</a></li>
            <li><a href="<?php echo base_url();?>crud/CreditOperationView" title="">Operaciones Crediticias</a></li>
            <li><a href="<?php echo base_url();?>crud/BillingRegisterView" title="">Registros de Facturacion</a></li>
          </ul>
        </li>
        <li><a class="waves-effect" href="#" title=""><i class="ti-comments brown lighten-1"></i> Publicidad</a>
          <ul>
            <li><a href="<?php echo base_url();?>crud/AdsView" title="">Avisos Publicitarios</a></li>
            <li><a href="<?php echo base_url();?>crud/AdsPlansView" title="">Planes de Avisos Publicitarios</a></li>
          </ul>
        </li>
        <li><a class="waves-effect" href="#" title=""><i class="ti-user green lighten-1"></i> Usuarios Generales</a>
          <ul>
            <li><a href="<?php echo base_url();?>crud/GeneralUsersView" title="">Usuarios Generales</a></li>
            <li><a href="<?php echo base_url();?>crud/GroupsView" title="">Grupos</a></li>
            <li><a href="<?php echo base_url();?>crud/UserInGroupView" title="">Relacion Usuario/Grupo</a></li>
            <li><a href="<?php echo base_url();?>crud/UserNotificationsView" title="">Notificaciones de Usuario</a></li>
            <li><a href="<?php echo base_url();?>crud/UserMessagesView" title="">Mensajes de Usuario</a></li>
          </ul>
        </li>
      </ul>
      <h6>Tools de Soporte</h6>
      <ul>
        <li><a class="waves-effect" href="<?php echo base_url();?>crud/SiteConfView" title=""><i class="ti-panel purple lighten-1"></i> Configuracion</a></li>
        <li><a class="waves-effect" href="<?php echo base_url();?>crud/TechSupport" title=""><i class="ti-support pink lighten-2"></i> Soporte Tecnico</a></li>
        <li><a class="waves-effect" href="<?php echo base_url();?>crud/UserManualView" title=""><i class="ti-help orange lighten-1"></i> Manual de Usuario</a></li>
        <li><a class="waves-effect" href="<?php echo base_url();?>crud/NewsAndUpdates" title=""><i class="ti-rss-alt yellow lighten-1"></i> Noticias y Novedades</a></li>
      </ul>
      <h6 class="copyright">
        Meifter CMS v2.
        <br>
        <small>Desarrollado por <a class="purple-text" href="https://friki-code.com" target="_blank">FrikiCode</a></small>
        <br><br>
        <small>&copy; <?php echo date('Y');?> Todos los Derechos Reservados</small>
      </h6>
    </nav>
  </div>
</div>
