<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area" style="height: 90vh;">
    <?php $this->load->view('v2/elements/breadcrum');?>

		<div class="widgets-wrapper">
			<div class="row">
				<div class="masonary">
				  <div class="col s12">
				    <div class="widget z-depth-1">
				      <div class="loader"></div>
				      <div class="widget-title">
				        <h3><?php echo $title;?></h3>
				        <p><?php echo $desc;?></p>
				      </div>
				      <div class="widget-crud">

								<div id="embed-api-auth-container"></div>
								<div id="chart-container"></div>
								<div id="view-selector-container"></div>

							</div>
				    </div>
				  </div>
				</div>
			</div>
		</div>

	</div>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<?php $this->load->view('v2/elements/scripts');?>
<script>
gapi.analytics.ready(function() {
  gapi.analytics.auth.authorize({
    container: 'embed-api-auth-container',
    clientid: 'REPLACE WITH YOUR CLIENT ID'
  });
  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector-container'
  });
  viewSelector.execute();
  var dataChart = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:date',
      'start-date': '30daysAgo',
      'end-date': 'yesterday'
    },
    chart: {
      container: 'chart-container',
      type: 'LINE',
      options: {
        width: '100%'
      }
    }
  });
  viewSelector.on('change', function(ids) {
    dataChart.set({query: {ids: ids}}).execute();
  });
});
</script>
