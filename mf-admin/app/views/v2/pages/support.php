<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area" style="height: 90vh;">
    <?php $this->load->view('v2/elements/breadcrum');?>

		<div class="widgets-wrapper">
			<div class="row">
				<div class="masonary">
				  <div class="col s12">
				    <div class="widget z-depth-1">
				      <div class="loader"></div>
				      <div class="widget-title">
				        <h3><?php echo $title;?></h3>
				        <p><?php echo $desc;?></p>
				      </div>
				      <div class="widget-crud">
                <script>
                  hbspt.forms.create({
                	portalId: "4536551",
                	formId: "378a6c37-2f54-48f3-b9b5-7fd3c8efa1a9"
                });
                </script>
							</div>
				    </div>
				  </div>
				</div>
			</div>
		</div>

	</div>
<script>
$( document ).ready(function() {
    $('.loader-container').remove();
});

</script>
