<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area" style="height: 90vh;">
    <?php $this->load->view('v2/elements/breadcrum');?>

		<div class="widgets-wrapper">
			<div class="row">
				<div class="masonary">
				  <div class="col s12">
				    <div class="widget z-depth-1">
				      <div class="loader"></div>
				      <div class="widget-title">
				        <h3><?php echo $title;?></h3>
				        <p><?php echo $desc;?></p>
				      </div>
				      <div class="widget-crud">
								<div class="row">
									<?php foreach ($rss as $rs): ?>
										<div class="col s4">
											<div class="revive-post">
												<a href="<?php echo $rs['link'];?>" target="_blank" title="" class="post-img">
													<img src="<?php echo $rs['thumbnail'];?>" alt="<?php echo $rs['title'];?>" />
												</a>
												<div class="post-detail">
													<h3><a href="<?php echo $rs['link'];?>" target="_blank" title="<?php echo $rs['title'];?>"><?php echo $rs['title'];?></a></h3>
													<ul class="meta">
														<li><a href="<?php echo $blogUrl . '/category/' . $rs['categoryslug'];?>" target="_blank" title="<?php echo $rs['title'];?>"><?php echo $rs['categoryname'];?></a></li>
														<li><?php echo $rs['pubDate'];?></li>
														<li><a href="<?php echo $rs['link'];?>" target="_blank" title="<?php echo $rs['title'];?>"><?php echo $rs['comments'];?> Comentarios</a></li>
													</ul>
													<p>
														<?php echo $rs['description'];?>
													</p>
													<a class="readmore" href="<?php echo $rs['link'];?>" target="_blank" title="<?php echo $rs['title'];?>">Leer Mas</a>
												</div>
											</div>
										</div>
									<?php endforeach ?>
								</div>
							</div>
				    </div>
				  </div>
				</div>
			</div>
		</div>

	</div>

<?php $this->load->view('v2/elements/scripts');?>
