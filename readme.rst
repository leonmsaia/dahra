###################
Dahra: Marketplace Artistico
###################

Descripcion de Proyecto

*******************
Informacion de Release
*******************

Actualmente se esta trabajando en la version 1.0 de Dahra, utilizando motor Codeigniter PHP v.3.
Base de datos MySQLi integrado en Motor Apache.

**************************
Backlog y New Features
**************************
El proceso del Proyecto en cuanto al proceso de Bugs, Issues y nuevas Features se maneja y gestiona mediante Trello.
https://trello.com/b/As3Ct422/proyecto-dahra

*******************
Requisitos de Servidor
*******************

PHP version 5.6 o versiones posteriores.

*******
Licencia
*******
El proyecto tiene una licencia cerrada y privativa para su implementacion.
Los recursos externos poseen cada uno su propia licencia.
