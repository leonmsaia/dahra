<section class="height-80" style="padding-top: 0;">
  <div class="slider" data-arrows="true" data-paging="true">
      <ul class="slides">
        <li>
          <section class="cover height-80 imagebg text-center" data-overlay="3">
              <div class="background-image-holder">
                <img alt="background" src="<?php echo base_url();?>assets/img/work-2.jpg">
              </div>
              <div class="container pos-vertical-center">
                  <div class="row">
                      <div class="col-md-12">
                          <h1>Encontra esos Accesorios que van con vos</h1>
                          <a class="btn btn--primary type--uppercase" href="#">
                            <span class="btn__text">
                              Encontrar
                            </span>
                          </a>
                      </div>
                  </div>
              </div>
          </section>
        </li>
        <li>
          <section class="cover height-80 imagebg text-center" data-overlay="3">
              <div class="background-image-holder">
                <img alt="background" src="<?php echo base_url();?>assets/img/work-1.jpg">
              </div>
              <div class="container pos-vertical-center">
                  <div class="row">
                      <div class="col-md-12">
                          <h1>Descubri esas obras que van con vos</h1>
                          <a class="btn btn--primary type--uppercase" href="#">
                            <span class="btn__text">
                              Descubri
                            </span>
                          </a>
                      </div>
                  </div>
              </div>
          </section>
        </li>
        <li>
          <section class="cover height-80 imagebg text-center" data-overlay="3">
              <div class="background-image-holder">
                <img alt="background" src="<?php echo base_url();?>assets/img/work-3.jpg">
              </div>
              <div class="container pos-vertical-center">
                  <div class="row">
                      <div class="col-md-12">
                          <h1>Descubri esos Productos Unicos en Porcelana</h1>
                          <a class="btn btn--primary type--uppercase" href="#">
                            <span class="btn__text">
                              Descubri
                            </span>
                          </a>
                      </div>
                  </div>
              </div>
          </section>
        </li>
      </ul>
  </div>
</section>

<section style="margin-top:95px;">
    <div class="container">
      <div class="row main_home_block">
        <div class="col-md-10">
          <div class="row text-block">
            <div class="col-md-12">
              <h3>Descubre</h3>
            </div>
          </div>
          <div class="masonry">
            <div class="masonry__container row masonry--active">
              <div class="masonry__item col-md-4"></div>
              <?php foreach ($product_list->result() as $pd): ?>
                <div class="masonry__item col-md-4 filter-audio" data-masonry-filter="Audio">
                  <div class="product">
                    <a href="<?php echo base_url() . 'marketplace/product/' . $pd->product_slug;?>">
                      <img alt="Image" src="<?php echo $pd->product_img;?>">
                    </a>
                    <a class="block" href="<?php echo base_url() . 'marketplace/product/' . $pd->product_slug;?>">
                      <div>
                        <h5><?php echo $pd->product_name;?></h5>
                      </div>
                      <div>
                        <span><?php echo $pd->product_desc;?></span>
                      </div>
                      <div>
                        <span class="h4 inline-block"><?php echo $pd->product_desc;?></span>
                      </div>
                    </a>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <?php $this->load->view('layout/components/vertical_ads_module');?>
      </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row text-block">
            <div class="col-md-12">
                <h3>Conoce a tu Artista</h3>
            </div>
        </div>
        <div class="masonry">
            <div class="masonry__container row masonry--active">
              <?php foreach ($artist_list->result() as $arts): ?>
                <div class="col-md-4 masonry__item">
                    <div class="card card-1 boxed boxed--sm boxed--border">
                        <div class="card__top">
                          <div class="card__avatar">
                            <span><strong><?php echo $arts->artist_name . ' ' . $arts->artist_lastname ?></strong></span>
                          </div>
                        </div>
                        <div class="card__body">
                            <a href="<?php echo base_url() . 'artist/' . $arts->artist_slug;?>">
                              <img src="<?php echo $arts->artist_picture;?>" alt="<?php echo $arts->artist_name;?>" class="border--round">
                            </a>
                        </div>
                    </div>
                </div>
              <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row text-block">
            <div class="col-md-12">
                <h3>Los mas Populares</h3>
            </div>
        </div>
        <div class="masonry">
            <div class="masonry-filter-container text-center row justify-content-center align-items-center">
                <div class="masonry-filter-holder">
                    <div class="masonry__filters" data-filter-all-text="Ver Todas las Categorias">
                        <ul>
                            <li class="active" data-masonry-filter="*">Ver Todas las Categorias</li>
                            <?php foreach ($category_list->result() as $cat_list_filter): ?>
                              <li data-masonry-filter="<?php echo $cat_list_filter->category_name;?>">
                                <?php echo $cat_list_filter->category_name;?>
                              </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="masonry__container row masonry--active">
                <div class="masonry__item col-md-4"></div>
                <?php foreach ($products_by_category->result() as $prods_cat): ?>
                  <div class="masonry__item col-md-4 filter-audio" data-masonry-filter="<?php echo $prods_cat->category_name;?>">
                      <div class="product">
                          <a href="<?php echo base_url() . 'marketplace/product/' . $prods_cat->product_slug;?>">
                            <img alt="Image" src="<?php echo $prods_cat->product_img;?>">
                          </a>
                          <a class="block" href="<?php echo base_url() . 'marketplace/product/' . $prods_cat->product_slug;?>">
                              <div>
                                  <h5><?php echo $prods_cat->product_name;?></h5>
                                  <br>
                                  <span><?php echo $prods_cat->product_desc;?></span>
                              </div>
                              <div>
                                <span class="h4 inline-block">$<?php echo $prods_cat->product_price;?></span>
                              </div>
                          </a>
                      </div>
                  </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="text-center imagebg" data-overlay="4">
    <div class="background-image-holder"> <img alt="background" src="<?php echo base_url();?>assets/img/hero-1.jpg"> </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="text-block"> <i class="icon icon--lg icon-Credit-Card color--dark"></i>
                    <h4>Compra de la forma mas segura</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-block"> <i class="icon icon--lg icon-Truck color--dark"></i>
                    <h4>Cotizá el envío o retiralo por tu cuenta</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-block"> <i class="icon icon--lg icon-Panorama color--dark"></i>
                    <h4>Disfrutá la obra en tu casa</h4>
                </div>
            </div>
        </div>
    </div>
</section>
