<section class="imageblock switchable feature-large height-100">
    <div class="imageblock__content col-lg-6 col-md-4 pos-right">
        <div class="background-image-holder">
            <img alt="background" src="<?php echo base_url();?>assets/img/bg/register_bg.jpg" />
        </div>
    </div>
    <div class="container pos-vertical-center">
        <div class="row">
            <div class="col-lg-5 col-md-7">
                <h2>Crea tu cuenta Gubia y comenza a disfrutar del Arte</h2>
                <p class="lead">Hacete miembro hoy y descubri importantes beneficios.</p>

                <?php // $this->load->view('page/User/Register_social');?>

                <?php echo form_open("User/createUserAction");?>
                  <div class="row">
                      <div class="col-12">
                          <input type="text" name="first_name" id="first_name" placeholder="Nombre" />
                      </div>
                      <div class="col-12">
                          <input type="text" name="last_name" id="last_name" placeholder="Apellido" />
                      </div>
                      <div class="col-12">
                          <input type="email" name="email" id="email" placeholder="E-Mail" />
                      </div>
                      <div class="col-12">
                          <input type="password" name="password" id="password" placeholder="Contraseña" />
                      </div>
                      <div class="col-12">
                          <input type="password" name="password_confirm" id="password_confirm" placeholder="Confirmar Contraseña" />
                      </div>
                      <div class="col-12">
                          <button type="submit" class="btn btn--primary type--uppercase">Crear Cuenta</button>
                      </div>
                      <div class="col-12">
                          <span class="type--fine-print">Al crear esta cuenta, esta aceptando los
                              <a href="<?php echo base_url();?>terms">Terminos y Condiciones</a>
                          </span>
                      </div>
                  </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</section>
