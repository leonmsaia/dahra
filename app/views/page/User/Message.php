<section class="space--sm">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="boxed boxed--border boxed--lg bg--secondary">
                    <?php $this->load->view('page/User/dashboard/sidebar');?>
                </div>
            </div>
            <div class="col-md-9">
              <div class="row justify-content-center">
                <div class="col-md-12">
                  <h5><?php echo $module_title;?></h5>
                  <hr>
                </div>
                <div class="col-md-12">
                  <?php $this->load->view('page/User/dashboard/' . $module);?>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>
