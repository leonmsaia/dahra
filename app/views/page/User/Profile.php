<section class="bg--secondary space--sm">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="boxed boxed--lg boxed--border">
                    <div class="text-block text-center">
                        <img alt="<?php echo $artist_object->artist_name;?>" src="<?php echo $artist_object->artist_picture?>" class="image--md" />
                        <span class="h5"><?php echo $artist_object->artist_name . ' ' . $artist_object->artist_lastname;?></span>
                        <span>"<?php echo $artist_object->artist_short_desc;?>"</span>
                    </div>
                    <div class="text-block clearfix text-center">
                        <ul class="row row--list">
                          <li class="col-md-6">
                              <span class="type--fine-print block">Nacionalidad:</span>
                              <span><?php echo $artist_object->artist_country;?></span>
                          </li>
                          <li class="col-md-6">
                              <span class="type--fine-print block">Edad:</span>
                              <span><?php echo calculateAge($artist_object->artist_birth);?></span>
                          </li>
                          <li class="col-md-12" style="margin-top:20px;">
                              <span class="type--fine-print block">Estudios:</span>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <span class="type--fine-print block">Actuales</span>
                                    </div>
                                  </div>
                                  <?php $actual_studies = explode(',', $artist_object->artist_actual_studies);?>
                                  <ul class="row">
                                    <?php foreach ($actual_studies as $study): ?>
                                      <li class="col-md-6">
                                        <?php echo $study;?>
                                      </li>
                                    <?php endforeach; ?>
                                  </ul>
                                </div>
                                <div class="col-md-6">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <span class="type--fine-print block">Cursados</span>
                                    </div>
                                  </div>
                                  <?php $past_studies = explode(',', $artist_object->artist_past_studies);?>
                                  <ul class="row">
                                    <?php foreach ($past_studies as $pst_study): ?>
                                      <li class="col-md-6">
                                        <?php echo $pst_study;?>
                                      </li>
                                    <?php endforeach; ?>
                                  </ul>
                                </div>
                              </div>
                          </li>
                          <li class="col-md-12" style="margin-top:20px;">
                              <span class="type--fine-print block"><b>Biografia</b></span>
                              <span>
                                <?php echo $artist_object->artist_bio;?>
                              </span>
                          </li>
                        </ul>
                    </div>
                </div>
                <div class="boxed boxed--border">
                    <ul class="row row--list clearfix text-center">
                        <li class="col-md-4">
                            <span class="h6 type--uppercase type--fade">Likes</span>
                            <span class="h3"><?php echo $artist_favs;?></span>
                        </li>
                        <li class="col-md-4">
                            <span class="h6 type--uppercase type--fade">Obras</span>
                            <span class="h3"><?php echo $number_works;?></span>
                        </li>
                        <li class="col-md-4">
                            <span class="h6 type--uppercase type--fade">Exposiciones</span>
                            <span class="h3">2,129</span>
                        </li>
                    </ul>
                </div>
                <div class="boxed boxed--border">
                    <h4>Obras Publicadas</h4>
                    <section class="sidebar boxed boxed--lg">
                        <div class="container">
                            <div class="masonry">
                                <div class="masonry__container row masonry--active">
                                    <div class="masonry__item col-md-4"></div>
                                    <?php foreach ($product_list->result() as $prd_lst): ?>
                                      <div class="masonry__item col-md-4">
                                          <div class="product">
                                              <a href="<?php echo base_url() . 'marketplace/product/' . $prd_lst->product_slug;?>">
                                                <img alt="Image" src="<?php echo $prd_lst->product_img;?>">
                                              </a>
                                              <a class="block" href="<?php echo base_url() . 'marketplace/product/' . $prd_lst->product_slug;?>">
                                                  <div>
                                                      <h5><?php echo $prd_lst->product_name;?></h5>
                                                      <br>
                                                      <span><?php echo $prd_lst->product_desc;?></span>
                                                  </div>
                                                  <div>
                                                    <span class="h4 inline-block">$<?php echo $prd_lst->product_price;?></span>
                                                  </div>
                                              </a>
                                          </div>
                                      </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <a class="btn btn--primary-2" href="<?php echo base_url() . 'artist/' . $artist_object->artist_slug . '/store';?>">
                    	<span class="btn__text">Ver Todas</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
