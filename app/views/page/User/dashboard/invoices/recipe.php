<?php
  $site_nfo = $site_information->result()[0];
  $site_ctc = $site_contact->result()[0];
?>
<!doctype html>
<html>
  <head>
      <meta charset="utf-8">
      <title><?php echo $title;?></title>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <style>
      .recipe_main {
          max-width: 800px;
          margin: auto;
          padding: 30px;
          font-size: 16px;
          line-height: 24px;
          font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
          color: #555;
      }
      .recipe_main table {
          width: 100%;
          line-height: inherit;
          text-align: left;
      }
      .recipe_main table td {
          padding: 5px;
          vertical-align: top;
      }
      .recipe_main table tr td:nth-child(2) {
          text-align: right;
      }
      .recipe_main table tr.top table td {
          padding-bottom: 20px;
      }
      .recipe_main table tr.top table td.title {
          font-size: 45px;
          line-height: 45px;
          color: #333;
      }
      .recipe_main table tr.information table td {
          padding-bottom: 40px;
      }
      .recipe_main table tr.heading td {
          background: #eee;
          border-bottom: 1px solid #ddd;
          font-weight: bold;
      }
      .recipe_main table tr.details td {
          padding-bottom: 20px;
      }
      .recipe_main table tr.item td{
          border-bottom: 1px solid #eee;
      }
      .recipe_main table tr.item.last td {
          border-bottom: none;
      }
      .recipe_main table tr.total td:nth-child(2) {
          border-top: 2px solid #eee;
          font-weight: bold;
      }
      @media only screen and (max-width: 600px) {
          .recipe_main table tr.top table td {
              width: 100%;
              display: block;
              text-align: center;
          }
          .recipe_main table tr.information table td {
              width: 100%;
              display: block;
              text-align: center;
          }
      }
      .print_btn button {
        background-color: transparent;
        border: none;
        color: #555;
      }
      .print_btn i {
        float: left;
        width: 35px;
      }
      .print_btn .text {
        float: left;
        font-weight: bold;
        margin-top: 5px;
      }
      @media print {
        .print_btn {
          display: none;
        }
        @page {
          margin: 0;
        }
      }
      .rtl {
          direction: rtl;
          font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      }
      .rtl table {
          text-align: right;
      }
      .rtl table tr td:nth-child(2) {
          text-align: left;
      }
      </style>
  </head>
  <body>
      <?php foreach ($recipe_information->result() as $rcp): ?>
        <div class="recipe_main">
            <table class="print_btn">
              <tr>
                <td>
                  <button onClick="window.print()">
                    <i class="material-icons">printer</i> <span class="text">Imprimir</span>
                  </button>
                </td>
              </tr>
              <tr>
                <td><br><br></td>
              </tr>
            </table>
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                  <td colspan="3">
                    <table>
                      <tr>
                        <td class="title">
                          <img src="<?php echo base_url();?>assets/img/logo-light.png" style="width:100%; max-width:180px;">
                        </td>
                        <td>
                          Recibo N°: <?php echo $rcp->site_recipe_id;?><br>
                          Código de Operacion: <?php echo strtoupper($rcp->code);?><br>
                          Fecha de Operacion: <?php echo date("d/m/Y", strtotime($rcp->date));?>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr class="information">
                  <td colspan="3">
                    <table>
                      <tr>
                        <td>
                          <?php echo $site_nfo->site_name;?> de <?php echo $site_nfo->site_author;?><br>
                          CUIT: <?php echo $site_ctc->cuit;?><br>
                          <?php echo str_replace(',', '<br>', $site_ctc->contact_adress);?><br>
                          Tel.: <?php echo $site_ctc->contact_phone;?>
                        </td>
                        <td>
                          <?php echo $rcp->first_name . ' ' . $rcp->last_name;?><br>
                          CUIT/CUIL: <?php echo $rcp->cuit;?><br>
                          <?php echo str_replace(',', '<br>', $rcp->adress);?><br>
                          <?php echo $rcp->email;?>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <table>
                      <tr class="heading">
                        <td>
                          Metodo de Pago
                        </td>
                      </tr>
                      <tr class="details">
                        <td>
                          MercadoPago
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <table>
                      <tr class="heading">
                        <td>
                          Item
                        </td>
                        <td style="text-align: center;">
                          Descripcion
                        </td>
                        <td style="text-align: right;">
                          Subtotal
                        </td>
                      </tr>
                      <tr class="item">
                        <td>Plan Tipo "<?php echo $rcp->mrk_point_plans_title;?>"</td>
                        <td style="text-align: center;"><?php echo $rcp->mrk_credit_operation_qty;?> Creditos de Website <?php echo $site_nfo->site_name;?></td>
                        <td style="text-align: right;">$<?php echo number_format((float)$rcp->amount, 2, '.', '');?></td>
                      </tr>
                      <tr class="total">
                        <td></td>
                        <td style="text-align: right;">Total:</td>
                        <td style="text-align: right;">$<?php echo number_format((float)$rcp->amount, 2, '.', '');?></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <table>
                      <tr>
                        <td><br></td>
                      </tr>
                      <tr class="heading">
                        <td>Titulo</td>
                      </tr>
                      <tr>
                        <td style="text-align: justify;">
                          <p style="font-size: 14px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
            </table>
        </div>
      <?php endforeach; ?>
  </body>
</html>
