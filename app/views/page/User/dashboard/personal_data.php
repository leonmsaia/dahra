<?php foreach ($user_information->result() as $usr_nfo): ?>
  <span id="pathData" path="<?php echo base_url();?>" style="display:none;"></span>
  <?php if (checkIFHaveArtistHelper() == TRUE): ?>
    <span id="actualData" status="<?php echo $statusData;?>" style="display:none;"></span>
  <?php endif; ?>
  <?php echo form_open("Dashboard/save_Personal_Data");?>
    <div class="row">
        <div class="col-md-12">
          <h5>Informacion Basica</h5>
        </div>
        <div class="col-md-6">
            <input type="text" name="first_name" id="first_name" placeholder="Nombre" value="<?php echo $usr_nfo->first_name;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="last_name" id="last_name" placeholder="Apellido" value="<?php echo $usr_nfo->last_name;?>"/>
        </div>
        <div class="col-md-4">
            <input type="text" name="dni" id="dni" placeholder="DNI" value="<?php echo $usr_nfo->dni;?>"/>
        </div>
        <div class="col-md-4">
            <input type="text" name="cuit" id="cuit" placeholder="CUIL/CUIT" value="<?php echo $usr_nfo->cuit;?>"/>
        </div>
        <?php if (checkIFHaveArtistHelper() == TRUE): ?>
          <div class="col-md-4">
            <input type="text" name="artist_birth" id="artist_birth" class="datepicker" placeholder="Fecha de Nacimiento"value="<?php echo $usr_nfo->artist_birth;?>" />
          </div>
        <?php endif; ?>
    </div>
    <?php if (checkIFHaveArtistHelper() == TRUE): ?>
      <div class="row">
          <div class="col-md-12">
            <br>
          </div>
          <div class="col-md-12">
            <h5>Informacion de Contacto</h5>
          </div>
            <div class="col-md-6">
                <input type="email" name="artist_mail" id="artist_mail" placeholder="E-Mail" value="<?php echo $usr_nfo->artist_mail;?>"/>
            </div>
          <div class="col-md-6">
              <input type="text" name="artist_phone" id="artist_phone" placeholder="Telefono" value="<?php echo $usr_nfo->artist_phone;?>"/>
          </div>
      </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12">
          <br>
        </div>
        <div class="col-md-12">
          <h5>Redes Sociales</h5>
        </div>
        <div class="col-md-6">
            <input type="text" name="facebook" id="facebook" placeholder="Facebook" value="<?php echo $usr_nfo->facebook;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="twitter" id="twitter" placeholder="Twitter" value="<?php echo $usr_nfo->twitter;?>"/>
        </div>
    </div>
    <?php if (checkIFHaveArtistHelper() == TRUE): ?>
      <div class="row">
        <div class="col-md-12">
          <br>
        </div>
        <div class="col-md-12">
          <h5>Direccion</h5>
        </div>
        <div class="col-4">
            <input type="text" name="artist_adress" id="artist_adress" placeholder="Calle" value="<?php echo $usr_nfo->artist_adress;?>"/>
        </div>
        <div class="col-4">
            <input type="text" name="artist_adress_number" id="artist_adress_number" placeholder="Altura" value="<?php echo $usr_nfo->artist_adress_number;?>"/>
        </div>
        <div class="col-4">
            <input type="text" name="artist_zipcode" id="artist_zipcode" placeholder="Codigo Postal" value="<?php echo $usr_nfo->artist_zipcode;?>"/>
        </div>
        <div class="col-4">
          <div class="input-select">
            <select name="country_selector" id="country_selector">
              <?php if ($usr_nfo->artist_country != 0): ?>
                <?php foreach ($artist_country->result() as $ctry): ?>
                  <?php if ($usr_nfo->artist_country == $ctry->id): ?>
                    <option value="<?php echo $ctry->id;?>" selected>
                  <?php else: ?>
                    <option value="<?php echo $ctry->id;?>">
                  <?php endif; ?>
                    <?php echo $ctry->nombre;?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
          </div>
        </div>
        <div class="col-4">
          <div class="input-select">
            <select name="province_selector" id="province_selector">
              <?php if ($usr_nfo->artist_province != 0): ?>
                <?php foreach ($artist_province->result() as $prv): ?>
                  <?php if ($usr_nfo->artist_province == $prv->id): ?>
                    <option value="<?php echo $prv->id;?>" selected>
                  <?php else: ?>
                    <option value="<?php echo $prv->id;?>">
                  <?php endif; ?>
                    <?php echo $prv->nombre;?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
          </div>
        </div>
        <div class="col-4">
          <div class="input-select">
            <select name="party_selector" id="party_selector">
              <?php if ($usr_nfo->artist_party != 0): ?>
                <?php foreach ($artist_party->result() as $prty): ?>
                  <?php if ($usr_nfo->artist_party == $prty->id): ?>
                    <option value="<?php echo $prty->id;?>" selected>
                  <?php else: ?>
                    <option value="<?php echo $prty->id;?>">
                  <?php endif; ?>
                    <?php echo $prty->nombre;?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
          </div>
        </div>
        <div class="col-4">
          <div class="input-select">
            <select name="locality_selector" id="locality_selector">
              <?php if ($usr_nfo->artist_location != 0): ?>
                <?php foreach ($artist_location->result() as $locl): ?>
                  <?php if ($usr_nfo->artist_location == $locl->id): ?>
                    <option value="<?php echo $locl->id;?>" selected>
                  <?php else: ?>
                    <option value="<?php echo $locl->id;?>">
                  <?php endif; ?>
                    <?php echo $locl->nombre;?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
          </div>
        </div>
        <div class="col-4">
          <div class="input-select">
            <select name="suburb_selector" id="suburb_selector">
              <?php if ($usr_nfo->artist_neighbour != 0): ?>
                <?php foreach ($artist_neighbour->result() as $surb): ?>
                  <?php if ($usr_nfo->artist_neighbour == $surb->id): ?>
                    <option value="<?php echo $surb->id;?>" selected>
                  <?php else: ?>
                    <option value="<?php echo $surb->id;?>">
                  <?php endif; ?>
                    <?php echo $surb->nombre;?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
          </div>
        </div>
        <div class="col-4">
          <div class="input-select">
            <select name="subsuburb_selector" id="subsuburb_selector">
              <?php if ($usr_nfo->artist_country != 0): ?>
                <?php foreach ($artist_subneighbour->result() as $ssurb): ?>
                  <?php if ($usr_nfo->artist_subneighbour == $ssurb->id): ?>
                    <option value="<?php echo $ssurb->id;?>" selected>
                  <?php else: ?>
                    <option value="<?php echo $ssurb->id;?>">
                  <?php endif; ?>
                    <?php echo $ssurb->nombre;?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
          </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="row">
      <div class="col-md-12">
        <br>
      </div>
      <div class="col-md-4">
        <button type="submit" class="btn btn--primary type--uppercase">Guardar</button>
      </div>
    </div>
  <?php echo form_close();?>
<?php endforeach; ?>
