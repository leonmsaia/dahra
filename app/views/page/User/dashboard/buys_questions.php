<ul>
  <?php foreach ($user_question_list->result() as $usr_qstns): ?>
    <li class="row" style="margin-bottom:25px;">
      <div class="col-md-2">
        <img class="img-responsive img-thumbnail" src="<?php echo $usr_qstns->product_img;?>" alt="<?php echo $usr_qstns->product_name;?>">
      </div>
      <div class="col-md-10">
        <span>
          <a href="#"><?php echo $usr_qstns->product_name;?></a>
        </span>
        <br>
        <span>$<?php echo $usr_qstns->product_price;?></span>
        <br>
        <span>x <?php echo $usr_qstns->product_qty;?> disponibles</span>
      </div>
      <ul class="col-md-12" style="margin-top: 20px; padding-top:15px; border-top:1px solid #cdcdcd;">
        <li class="row">
          <span class="col-md-12">
            <span class="row">
              <span class="col-md-10"><i class="icon-Speach-Bubble" style="margin-right:10px;"></i><?php echo $usr_qstns->question_text;?></span>
              <span class="col-md-2"><small><?php echo $usr_qstns->question_time;?></small></span>
            </span>
          </span>
        </li>
        <?php if (questionHaveAnswer($usr_qstns->question_status > 0)): ?>
          <?php $anserData = answersDetails($usr_qstns->question_id);?>
          <?php foreach ($anserData->result() as $answ): ?>
            <li class="row">
              <span class="col-md-12">
                <span class="row">
                  <span class="col-md-10"><i class="icon-Speach-Bubble" style="margin-right:10px;"></i>
                    <b>Respuesta:</b> <?php echo $answ->answer_text;?>
                  </span>
                  <span class="col-md-2"><small><?php echo $answ->answer_time;?></small></span>
                </span>
              </span>
            </li>
          <?php endforeach; ?>
        <?php else: ?>
          <li class="row">
            <span class="col-md-12">
              <b>Aun no respondio.</b>
            </span>
          </li>
        <?php endif; ?>
      </ul>
      <div class="col-md-12" style="margin-top: 20px; padding-top:15px; border-top:1px solid #cdcdcd;">
        <div class="modal-instance">
          <a class="btn btn--primary modal-trigger" href="#">
            <span class="btn__text">
              Enviar Consulta
            </span>
          </a>
          <div class="modal-container">
            <div class="modal-content">
              <div class="boxed boxed--lg">
                  <h2>Enviar consulta al Artista:</h2>
                  <hr class="short">
                  <?php echo form_open("Marketplace/makeConsultDashboard", 'class="row"');?>
                    <div class="col-md-12" style="padding-left:15px; margin-top:45px;">
                      <input type="hidden" name="message_receptor" value="<?php echo $usr_qstns->product_author;?>">
                      <input type="hidden" name="message_prod_id" value="<?php echo $usr_qstns->product_id;?>">
                      <textarea rows="8" cols="80" id="message_text" name="message_text" style="margin-bottom:20px;" placeholder="Escribir Consulta"></textarea>
                      <div class="row">
                        <div class="col-md-4">
                          <button type="submit" class="btn btn--primary">Consultar</button>
                        </div>
                      </div>
                    </div>
                  <?php echo form_close();?>
              </div>
              <div class="modal-close modal-close-cross"></div>
            </div>
          </div>
        </div>
        <div class="modal-instance">
          <a class="btn btn--sm modal-trigger" href="#">
            <span class="btn__text">
              Hacer otra Pregunta
            </span>
          </a>
          <div class="modal-container">
            <div class="modal-content">
              <div class="boxed boxed--lg">
                  <h2>Hacer otra pregunta:</h2>
                  <hr class="short">
                  <?php echo form_open('Marketplace/makeQuestionModal');?>
                    <input type="hidden" id="user_owner_id" name="user_owner_id" value="<?php echo $usr_qstns->product_author;?>" readonly>
                    <input type="hidden" id="product_slug" name="product_slug" value="<?php echo $usr_qstns->product_slug;?>" readonly>
                    <input type="hidden" id="product_id" name="product_id" value="<?php echo $usr_qstns->product_id;?>" readonly>
                    <input type="hidden" id="question_status" name="question_status" value="0" readonly>
                    <textarea rows="8" cols="80" id="question_text" name="question_text" style="margin-bottom:20px;" placeholder="Escribir Pregunta"></textarea>
                    <div class="row">
                      <div class="col-md-4">
                        <button type="submit" class="btn btn--primary">Preguntar</button>
                      </div>
                    </div>
                  <?php echo form_close();?>
              </div>
              <div class="modal-close modal-close-cross"></div>
            </div>
          </div>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>
