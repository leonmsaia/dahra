<div class="tabs-container" data-content-align="left">
	<ul class="tabs">

    <li class="active">
			<div class="tab__title">
				<span class="h5">Activas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
          <tbody>
						<?php foreach ($sales_actives->result() as $sls_actv): ?>
							<tr class="row">
	              <td class="col-md-2">
	                <img class="img-responsive img-thumbnail" src="<?php echo $sls_actv->product_img;?>" alt="<?php echo $sls_actv->product_name;?>">
	              </td>
	              <td class="col-md-2">
									<h5><?php echo $sls_actv->product_name;?></h5>
					        <h6>$<?php echo $sls_actv->price_transaction;?></h6>
	              </td>
	              <td class="col-md-3">
									<h5 style="margin-bottom:0px;"><?php echo $sls_actv->first_name . ' ' . $sls_actv->last_name;?></h5>
					        <ul>
					          <li><?php echo $sls_actv->phone;?></li>
					          <li><?php echo $sls_actv->email;?></li>
					          <li>
					            <a href="#">Enviar Mensaje</a>
					          </li>
					        </ul>
	              </td>
	              <td class="col-md-3">
									<?php if($sls_actv->sale_status == 0): ?>
					          Pendiente de Pago
					        <?php elseif($sls_actv->sale_status == 1): ?>
					          Pagado
					        <?php elseif($sls_actv->sale_status == 2): ?>
					          Procesando
					        <?php elseif($sls_actv->sale_status == 3): ?>
					          Entregado
					        <?php endif; ?>
	              </td>
	              <td class="col-md-2">
									<div class="modal-instance">
					        	<a class="modal-trigger" href="#">
					        		<span class="btn__text">
					        			Ver Detalle
					        		</span>
					        	</a>
					        	<div class="modal-container">
					            <div class="modal-content">
					              <div class="boxed boxed--lg">
					                  <h5 style="margin-bottom: 0;">Detalle de Compra: </h5>
					                  <h2><?php echo $sls_actv->product_name;?></h2>
					                  <hr class="short">
					                  <ul>
					                    <li>Dueño de Producto: <?php echo $sls_actv->first_name . ' ' . $sls_actv->last_name;?></li>
					                    <li>Nombre de Producto: <?php echo $sls_actv->product_name;?></li>
					                    <li>Precio Final: <?php echo $sls_actv->price_transaction;?></li>
					                    <li>Estado de Transacción:
					                      <?php if($sls_actv->sale_status == 0): ?>
					                        Pendiente de Pago
					                      <?php elseif($sls_actv->sale_status == 1): ?>
					                        Pagado
					                      <?php elseif($sls_actv->sale_status == 2): ?>
					                        Procesando
					                      <?php elseif($sls_actv->sale_status == 3): ?>
					                        Entregado
					                      <?php endif; ?>
					                    </li>
					                    <li>Fecha de Transacción: <?php echo $sls_actv->date;?></li>
					                    <li>Método de Pago: <?php echo $sls_actv->payment_method;?></li>
					                  </ul>
					              </div>
					              <div class="modal-close modal-close-cross"></div>
					            </div>
					        	</div>
					        </div>
	              </td>
	            </tr>
						<?php endforeach; ?>
          </tbody>
        </table>
			</div>
		</li>

		<li>
			<div class="tab__title">
				<span class="h5">Pagadas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
          <tbody>
						<?php foreach ($sales_payed->result() as $sls_pyd): ?>
							<tr class="row">
	              <td class="col-md-2">
	                <img class="img-responsive img-thumbnail" src="<?php echo $sls_pyd->product_img;?>" alt="<?php echo $sls_pyd->product_name;?>">
	              </td>
	              <td class="col-md-2">
									<h5><?php echo $sls_pyd->product_name;?></h5>
					        <h6>$<?php echo $sls_pyd->price_transaction;?></h6>
	              </td>
	              <td class="col-md-3">
									<h5 style="margin-bottom:0px;"><?php echo $sls_pyd->first_name . ' ' . $sls_pyd->last_name;?></h5>
					        <ul>
					          <li><?php echo $sls_pyd->phone;?></li>
					          <li><?php echo $sls_pyd->email;?></li>
					          <li>
					            <a href="#">Enviar Mensaje</a>
					          </li>
					        </ul>
	              </td>
	              <td class="col-md-3">
									<?php if($sls_pyd->sale_status == 0): ?>
					          Pendiente de Pago
					        <?php elseif($sls_pyd->sale_status == 1): ?>
					          Pagado
					        <?php elseif($sls_pyd->sale_status == 2): ?>
					          Procesando
					        <?php elseif($sls_pyd->sale_status == 3): ?>
					          Entregado
					        <?php endif; ?>
	              </td>
	              <td class="col-md-2">
									<div class="modal-instance">
					        	<a class="modal-trigger" href="#">
					        		<span class="btn__text">
					        			Ver Detalle
					        		</span>
					        	</a>
					        	<div class="modal-container">
					            <div class="modal-content">
					              <div class="boxed boxed--lg">
					                  <h5 style="margin-bottom: 0;">Detalle de Compra: </h5>
					                  <h2><?php echo $sls_pyd->product_name;?></h2>
					                  <hr class="short">
					                  <ul>
					                    <li>Dueño de Producto: <?php echo $sls_pyd->first_name . ' ' . $sls_pyd->last_name;?></li>
					                    <li>Nombre de Producto: <?php echo $sls_pyd->product_name;?></li>
					                    <li>Precio Final: <?php echo $sls_pyd->price_transaction;?></li>
					                    <li>Estado de Transacción:
					                      <?php if($sls_pyd->sale_status == 0): ?>
					                        Pendiente de Pago
					                      <?php elseif($sls_pyd->sale_status == 1): ?>
					                        Pagado
					                      <?php elseif($sls_pyd->sale_status == 2): ?>
					                        Procesando
					                      <?php elseif($sls_pyd->sale_status == 3): ?>
					                        Entregado
					                      <?php endif; ?>
					                    </li>
					                    <li>Fecha de Transacción: <?php echo $sls_pyd->date;?></li>
					                    <li>Método de Pago: <?php echo $sls_pyd->payment_method;?></li>
					                  </ul>
					              </div>
					              <div class="modal-close modal-close-cross"></div>
					            </div>
					        	</div>
					        </div>
	              </td>
	            </tr>
						<?php endforeach; ?>
          </tbody>
        </table>
			</div>
		</li>

		<li>
			<div class="tab__title">
				<span class="h5">En Proceso</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
          <tbody>
						<?php foreach ($sales_in_process->result() as $sls_in_prcss): ?>
							<tr class="row">
	              <td class="col-md-2">
	                <img class="img-responsive img-thumbnail" src="<?php echo $sls_in_prcss->product_img;?>" alt="<?php echo $sls_in_prcss->product_name;?>">
	              </td>
	              <td class="col-md-2">
									<h5><?php echo $sls_in_prcss->product_name;?></h5>
					        <h6>$<?php echo $sls_in_prcss->price_transaction;?></h6>
	              </td>
	              <td class="col-md-3">
									<h5 style="margin-bottom:0px;"><?php echo $sls_in_prcss->first_name . ' ' . $sls_in_prcss->last_name;?></h5>
					        <ul>
					          <li><?php echo $sls_in_prcss->phone;?></li>
					          <li><?php echo $sls_in_prcss->email;?></li>
					          <li>
					            <a href="#">Enviar Mensaje</a>
					          </li>
					        </ul>
	              </td>
	              <td class="col-md-3">
									<?php if($sls_in_prcss->sale_status == 0): ?>
					          Pendiente de Pago
					        <?php elseif($sls_in_prcss->sale_status == 1): ?>
					          Pagado
					        <?php elseif($sls_in_prcss->sale_status == 2): ?>
					          Procesando
					        <?php elseif($sls_in_prcss->sale_status == 3): ?>
					          Entregado
					        <?php endif; ?>
	              </td>
	              <td class="col-md-2">
									<div class="modal-instance">
					        	<a class="modal-trigger" href="#">
					        		<span class="btn__text">
					        			Ver Detalle
					        		</span>
					        	</a>
					        	<div class="modal-container">
					            <div class="modal-content">
					              <div class="boxed boxed--lg">
					                  <h5 style="margin-bottom: 0;">Detalle de Compra: </h5>
					                  <h2><?php echo $sls_in_prcss->product_name;?></h2>
					                  <hr class="short">
					                  <ul>
					                    <li>Dueño de Producto: <?php echo $sls_in_prcss->first_name . ' ' . $sls_in_prcss->last_name;?></li>
					                    <li>Nombre de Producto: <?php echo $sls_in_prcss->product_name;?></li>
					                    <li>Precio Final: <?php echo $sls_in_prcss->price_transaction;?></li>
					                    <li>Estado de Transacción:
					                      <?php if($sls_in_prcss->sale_status == 0): ?>
					                        Pendiente de Pago
					                      <?php elseif($sls_in_prcss->sale_status == 1): ?>
					                        Pagado
					                      <?php elseif($sls_in_prcss->sale_status == 2): ?>
					                        Procesando
					                      <?php elseif($sls_in_prcss->sale_status == 3): ?>
					                        Entregado
					                      <?php endif; ?>
					                    </li>
					                    <li>Fecha de Transacción: <?php echo $sls_in_prcss->date;?></li>
					                    <li>Método de Pago: <?php echo $sls_in_prcss->payment_method;?></li>
					                  </ul>
					              </div>
					              <div class="modal-close modal-close-cross"></div>
					            </div>
					        	</div>
					        </div>
	              </td>
	            </tr>
						<?php endforeach; ?>
          </tbody>
        </table>
			</div>
		</li>

		<li>
			<div class="tab__title">
				<span class="h5">Cerradas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
          <tbody>
						<?php foreach ($sales_closed->result() as $sls_clsd): ?>
							<tr class="row">
	              <td class="col-md-2">
	                <img class="img-responsive img-thumbnail" src="<?php echo $sls_clsd->product_img;?>" alt="<?php echo $sls_clsd->product_name;?>">
	              </td>
	              <td class="col-md-2">
									<h5><?php echo $sls_clsd->product_name;?></h5>
					        <h6>$<?php echo $sls_clsd->price_transaction;?></h6>
	              </td>
	              <td class="col-md-3">
									<h5 style="margin-bottom:0px;"><?php echo $sls_clsd->first_name . ' ' . $sls_clsd->last_name;?></h5>
					        <ul>
					          <li><?php echo $sls_clsd->phone;?></li>
					          <li><?php echo $sls_clsd->email;?></li>
					          <li>
					            <a href="#">Enviar Mensaje</a>
					          </li>
					        </ul>
	              </td>
	              <td class="col-md-3">
									<?php if($sls_clsd->sale_status == 0): ?>
					          Pendiente de Pago
					        <?php elseif($sls_clsd->sale_status == 1): ?>
					          Pagado
					        <?php elseif($sls_clsd->sale_status == 2): ?>
					          Procesando
					        <?php elseif($sls_clsd->sale_status == 3): ?>
					          Entregado
					        <?php endif; ?>
	              </td>
	              <td class="col-md-2">
									<div class="modal-instance">
					        	<a class="modal-trigger" href="#">
					        		<span class="btn__text">
					        			Ver Detalle
					        		</span>
					        	</a>
					        	<div class="modal-container">
					            <div class="modal-content">
					              <div class="boxed boxed--lg">
					                  <h5 style="margin-bottom: 0;">Detalle de Compra: </h5>
					                  <h2><?php echo $sls_clsd->product_name;?></h2>
					                  <hr class="short">
					                  <ul>
					                    <li>Dueño de Producto: <?php echo $sls_clsd->first_name . ' ' . $sls_clsd->last_name;?></li>
					                    <li>Nombre de Producto: <?php echo $sls_clsd->product_name;?></li>
					                    <li>Precio Final: <?php echo $sls_clsd->price_transaction;?></li>
					                    <li>Estado de Transacción:
					                      <?php if($sls_clsd->sale_status == 0): ?>
					                        Pendiente de Pago
					                      <?php elseif($sls_clsd->sale_status == 1): ?>
					                        Pagado
					                      <?php elseif($sls_clsd->sale_status == 2): ?>
					                        Procesando
					                      <?php elseif($sls_clsd->sale_status == 3): ?>
					                        Entregado
					                      <?php endif; ?>
					                    </li>
					                    <li>Fecha de Transacción: <?php echo $sls_clsd->date;?></li>
					                    <li>Método de Pago: <?php echo $sls_clsd->payment_method;?></li>
					                  </ul>
					              </div>
					              <div class="modal-close modal-close-cross"></div>
					            </div>
					        	</div>
					        </div>
	              </td>
	            </tr>
						<?php endforeach; ?>
          </tbody>
        </table>
			</div>
		</li>

	</ul>
</div>
