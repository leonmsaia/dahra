<?php if (checkIFHaveArtistHelper()): ?>
  <ul>
    <li>
      <h5>Creditos: <?php echo $credit_qty;?></h5>
      <a href="<?php echo base_url();?>user/buy_credits">Comprar Planes</a>
    </li>
    <li><br></li>
    <li><h5>Visitas</h5></li>
    <li>
      Totales: <?php echo $num_of_visits;?> visitas.
    </li>
    <li><br></li>
    <li><h5>Preguntas</h5></li>
    <li>
      <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'dashboard/sales/questions';?>">
      	<span class="btn__text">
          <i><?php echo $questions_actives;?></i> Preguntas sin responder
        </span>
      </a>
    </li>
    <li><br></li>
    <li><h5>Publicaciones</h5></li>
    <li>
      <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'dashboard/sales/publications';?>">
      	<span class="btn__text">
          <i><?php echo $publications_active;?></i> Activas
        </span>
      </a>
      <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'dashboard/sales/publications';?>">
      	<span class="btn__text">
          <i><?php echo $publications_paused;?></i> Pausadas
        </span>
      </a>
      <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'dashboard/sales/publications';?>">
      	<span class="btn__text">
          <i><?php echo $publications_ended;?></i> Finalizadas
        </span>
      </a>
      <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'dashboard/sales/publications';?>">
      	<span class="btn__text">
          <i><?php echo $publications_not_actived;?></i> Sin Activar
        </span>
      </a>
    </li>
  </ul>
<?php endif; ?>

<script src="<?php echo base_url();?>assets/js/jquery.canvasjs.min.js"></script>
<script>
  var mostVisitedProds = <?php echo $most_visited_prods;?>;
  window.onload = function () {
    var options = {
    title: {
      text: "Tus 5 Productos Mas Visitados"
    },
    animationEnabled: true,
    data: [{
      type: "pie",
      startAngle: 40,
      toolTipContent: "<b>{label}</b>: {y}%. {value} visitas.",
      showInLegend: "true",
      legendText: "{label}",
      indexLabelFontSize: 16,
      indexLabel: "{label} | {value} visitas",
      dataPoints: mostVisitedProds
    }]
    };
    $("#chartContainer").CanvasJSChart(options);
  }
</script>
