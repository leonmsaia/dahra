<div class="container">
	<div class="row">
	  <div class="col-md-12" style="margin-bottom:35px;">
			<h3>Creditos Actuales: <?php echo $credit_qty;?></h3>
	  	<h4>Suma Creditos hoy</h4>
			<p>Aprovecha nuestras promociones y descuentos comprando creditos, y comenza a publicar hoy mismo.</p>
			<a href="<?php echo base_url();?>user/buy_credits">Ver Planes</a>
	  </div>
	</div>
</div>

<div class="container">
	<div class="row">
	  <div class="col-md-12">
			<table class="border--round">
				<thead>
					<tr>
						<th>N° Fac.</th>
			      <th>Cod. Op.</th>
						<th>Fecha</th>
			      <th>Creditos</th>
			      <th>Operacion</th>
						<th>Importe</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
			    <?php foreach ($credit_balance->result() as $sts): ?>
			      <?php if ($sts->mrk_credit_operation_type == 1): ?>
			        <tr class="alert bg--success">
			      <?php elseif($sts->mrk_credit_operation_type == 2): ?>
			        <tr class="alert bg--error">
			      <?php endif; ?>
			          <td><?php echo $sts->mrk_credit_operation_id;?></td>
			    			<td><?php echo $sts->mrk_credit_operation_operation_id;?></td>
			    			<td><?php echo date("d/m/Y", strtotime($sts->mrk_credit_operation_date));?></td>
			          <td><?php echo $sts->mrk_credit_operation_qty;?> Creditos</td>
			          <td>
			            <?php if ($sts->mrk_credit_operation_type == 1): ?>
			              Credito
			            <?php elseif($sts->mrk_credit_operation_type == 2): ?>
			              Debito
			            <?php endif; ?>
			          </td>
								<?php if ($sts->mrk_credit_operation_type == 1): ?>
			    				<td>$<?php echo getValueOfCreditOperation($sts->mrk_credit_operation_operation_id);?></td>
									<td style="text-align:center;">
				            <a href="<?php echo base_url() . 'dashboard/view_recipe/' . getRecipeIDByReferenceCode($sts->mrk_credit_operation_operation_plane_code);?>" target="_blank">
											<i class="material-icons">description</i>
				            </a>
				          </td>
								<?php elseif($sts->mrk_credit_operation_type == 2): ?>
									<td style="text-align:center;">---</td>
									<td style="text-align:center;">---</td>
								<?php endif; ?>
			  		</tr>
			    <?php endforeach; ?>
				</tbody>
			</table>
			<a href="<?php echo base_url();?>dashboard/subscription/history">Ver Historial</a>
		</div>
</div>
