<?php foreach ($notification_config->result() as $not_config): ?>
	<?php echo form_open('User/updateMailConf');?>
		<table class="border--round">
			<thead>
				<tr>
					<th>Tipo de Notificacion</th>
					<th>Si</th>
					<th>No</th>
				</tr>
			</thead>
			<tbody>
				<?php if (checkIFHaveArtistHelper()): ?>
		    <tr>
					<td>Hicieron una pregunta en tu Exposicion</td>
					<?php if ($not_config->question_work == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="question_advise_positive" type="radio" name="question_advise" value="1" checked/>
			        	<label for="question_advise_positive"></label>
			        </div>
			      </td>
						<td>
			        <div class="input-radio">
			        	<input id="question_advise_negative" type="radio" name="question_advise" value="0"/>
			        	<label for="question_advise_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="question_advise_positive" type="radio" name="question_advise" value="1"/>
			        	<label for="question_advise_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="question_advise_negative" type="radio" name="question_advise" value="0" checked/>
			        	<label for="question_advise_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
				<tr>
					<td>Publicacion Exitosa</td>
					<?php if ($not_config->publish_work == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="exposition_makeit_positive" type="radio" name="publish_work" value="1" checked/>
			        	<label for="exposition_makeit_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="exposition_makeit_negative" type="radio" name="publish_work" value="0" />
			        	<label for="exposition_makeit_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="exposition_makeit_positive" type="radio" name="publish_work" value="1" />
			        	<label for="exposition_makeit_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="exposition_makeit_negative" type="radio" name="publish_work" value="0" checked/>
			        	<label for="exposition_makeit_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
				<tr>
					<td>Finalizo tu Publicacion</td>
					<?php if ($not_config->finish_work == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="exposition_finished_positive" type="radio" name="exposition_finished" value="1" checked/>
			        	<label for="exposition_finished_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="exposition_finished_negative" type="radio" name="exposition_finished" value="0" />
			        	<label for="exposition_finished_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="exposition_finished_positive" type="radio" name="exposition_finished" value="1" />
			        	<label for="exposition_finished_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="exposition_finished_negative" type="radio" name="exposition_finished" value="0" checked/>
			        	<label for="exposition_finished_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
		    <tr>
					<td>Republicacion Exitosa</td>
					<?php if ($not_config->repub_work == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="exposition_republished_positive" type="radio" name="exposition_republished" value="1" checked/>
			        	<label for="exposition_republished_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="exposition_republished_negative" type="radio" name="exposition_republished" value="0" />
			        	<label for="exposition_republished_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="exposition_republished_positive" type="radio" name="exposition_republished" value="1" />
			        	<label for="exposition_republished_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="exposition_republished_negative" type="radio" name="exposition_republished" value="0" checked/>
			        	<label for="exposition_republished_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
				<tr>
					<td>Tu publicacion esta por finalizar</td>
					<?php if ($not_config->almost_finish_work == 1): ?>
						<td>
							<div class="input-radio">
								<input id="exposition_almost_finished_positive" type="radio" name="exposition_almost_finished" value="1" checked/>
								<label for="exposition_almost_finished_positive"></label>
							</div>
						</td>
						<td>
							<div class="input-radio">
								<input id="exposition_almost_finished_negative" type="radio" name="exposition_almost_finished" value="0" />
								<label for="exposition_almost_finished_negative"></label>
							</div>
						</td>
					<?php else: ?>
						<td>
							<div class="input-radio">
								<input id="exposition_almost_finished_positive" type="radio" name="exposition_almost_finished" value="1" />
								<label for="exposition_almost_finished_positive"></label>
							</div>
						</td>
						<td>
							<div class="input-radio">
								<input id="exposition_almost_finished_negative" type="radio" name="exposition_almost_finished" value="0" checked/>
								<label for="exposition_almost_finished_negative"></label>
							</div>
						</td>
					<?php endif; ?>
				</tr>
				<?php else: ?>
					<input type="hidden" name="question_advise" value="0"/>
					<input type="hidden" name="exposition_finished" value="0"/>
					<input type="hidden" name="exposition_republished" value="0"/>
					<input type="hidden" name="exposition_almost_finished" value="0"/>
				<?php endif; ?>
				<tr>
					<td>Te enviaron un Mensaje Privado</td>
					<?php if ($not_config->write_message == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="write_message_positive" type="radio" name="write_message" value="1" checked/>
			        	<label for="write_message_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="write_message_negative" type="radio" name="write_message" value="0" />
			        	<label for="write_message_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="write_message_positive" type="radio" name="write_message" value="1" />
			        	<label for="write_message_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="write_message_negative" type="radio" name="write_message" value="0" checked/>
			        	<label for="write_message_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
		    <tr>
					<td>Ofertas, Promociones y Novedades de Dahra</td>
					<?php if ($not_config->oferts_promotions == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="oferts_promotions_positive" type="radio" name="oferts_promotions" value="1" checked/>
			        	<label for="oferts_promotions_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="oferts_promotions_negative" type="radio" name="oferts_promotions" value="0" />
			        	<label for="oferts_promotions_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="oferts_promotions_positive" type="radio" name="oferts_promotions" value="1" />
			        	<label for="oferts_promotions_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="oferts_promotions_negative" type="radio" name="oferts_promotions" value="0" checked/>
			        	<label for="oferts_promotions_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
		    <tr>
					<td>Cambios en los Terminos y Condiciones</td>
					<?php if ($not_config->terms_cond == 1): ?>
						<td>
			        <div class="input-radio">
			        	<input id="terms_cond_positive" type="radio" name="terms_cond" value="1" checked/>
			        	<label for="terms_cond_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="terms_cond_negative" type="radio" name="terms_cond" value="0" />
			        	<label for="terms_cond_negative"></label>
			        </div>
			      </td>
					<?php else: ?>
						<td>
			        <div class="input-radio">
			        	<input id="terms_cond_positive" type="radio" name="terms_cond" value="1" />
			        	<label for="terms_cond_positive"></label>
			        </div>
			      </td>
			      <td>
			        <div class="input-radio">
			        	<input id="terms_cond_negative" type="radio" name="terms_cond" value="0" checked/>
			        	<label for="terms_cond_negative"></label>
			        </div>
			      </td>
					<?php endif; ?>
				</tr>
				</tr>
			</tbody>
		</table>
		<div class="row">
			<div class="col-md-4">
				<button type="submit" class="btn btn--primary">Guardar Preferencias</button>
			</div>
		</div>
	<?php echo form_close();?>
<?php endforeach; ?>
