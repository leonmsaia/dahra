<table class="table--alternate-row">
  <tbody>
    <?php foreach ($publications_active->result() as $pbs): ?>
    <tr class="row">
      <td class="col-md-2">
        <img class="img-responsive img-thumbnail" src="<?php echo $pbs->product_img;?>" alt="<?php echo $pbs->product_name;?>">
      </td>
      <td class="col-md-2">
        <h5><?php echo $pbs->product_name;?></h5>
        <h6>$<?php echo $pbs->price_transaction;?></h6>
      </td>
      <td class="col-md-3">
        <h5 style="margin-bottom:0px;"><?php echo $pbs->first_name . ' ' . $pbs->last_name;?></h5>
        <ul>
          <li><?php echo $pbs->phone;?></li>
          <li><?php echo $pbs->email;?></li>
          <li>
            <a href="#">Enviar Mensaje</a>
          </li>
        </ul>
      </td>
      <td class="col-md-1">
        <?php if($pbs->sale_status == 0): ?>
          Pendiente de Pago
        <?php elseif($pbs->sale_status == 1): ?>
          Pagado
        <?php elseif($pbs->sale_status == 2): ?>
          Procesando
        <?php elseif($pbs->sale_status == 3): ?>
          Entregado
        <?php endif; ?>
      </td>
      <td class="col-md-2">
        <div class="modal-instance">
        	<a class="modal-trigger" href="#">
        		<span class="btn__text">
        			Ver Detalle
        		</span>
        	</a>
        	<div class="modal-container">
            <div class="modal-content">
              <div class="boxed boxed--lg">
                  <h5 style="margin-bottom: 0;">Detalle de Compra: </h5>
                  <h2><?php echo $pbs->product_name;?></h2>
                  <hr class="short">
                  <ul>
                    <li>Dueño de Producto: <?php echo $pbs->first_name . ' ' . $pbs->last_name;?></li>
                    <li>Nombre de Producto: <?php echo $pbs->product_name;?></li>
                    <li>Precio Final: <?php echo $pbs->price_transaction;?></li>
                    <li>Estado de Transacción:
                      <?php if($pbs->sale_status == 0): ?>
                        Pendiente de Pago
                      <?php elseif($pbs->sale_status == 1): ?>
                        Pagado
                      <?php elseif($pbs->sale_status == 2): ?>
                        Procesando
                      <?php elseif($pbs->sale_status == 3): ?>
                        Entregado
                      <?php endif; ?>
                    </li>
                    <li>Fecha de Transacción: <?php echo $pbs->date;?></li>
                    <li>Método de Pago: <?php echo $pbs->payment_method;?></li>
                  </ul>
              </div>
              <div class="modal-close modal-close-cross"></div>
            </div>
        	</div>
        </div>
      </td>
      <td class="col-md-2">
        <div class="btn btn--primary btn--sm dropdown">
          <span class="btn__text">
            Accion
          </span>
          <span class="dropdown__trigger"></span>
          <div class="dropdown__container">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-lg-2 dropdown__content">
                  <ul class="menu-vertical">
                    <li>
                      Tengo un Problema
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
