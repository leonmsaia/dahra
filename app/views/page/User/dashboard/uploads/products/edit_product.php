<div class="tabs-container" data-content-align="left">
  <ul class="tabs">




    <li class="active">
      <div class="tab__title">
        <span class="h5">Datos Basicos</span>
      </div>
      <div class="tab__content">
        <?php foreach ($product_information->result() as $prod): ?>
          <span id="pathData" path="<?php echo base_url();?>" style="display:none;"></span>
          <div class="col-md-12" style="margin-bottom:35px;">
            <div class="row">
              <div class="col-md-12">
                <div class="row">

                  <div class="col-md-12">
                    <?php echo form_open("Marketplace/edit_basic_product_info", 'class="row"');?>
                      <input type="hidden" name="product_code" value="<?php echo $prod->product_code;?>" readonly>
                      <div class="col-md-12">
                        <label>Titulo</label>
                        <input type="text" name="product_name" id="product_name" value="<?php echo $prod->product_name;?>" class="validate-required">
                      </div>
                      <div class="col-md-6">
                        <label>Categoria</label>
                        <div class="input-select">
                          <select name="categorie_selector" id="categorie_selector">
                            <option selected="" value="Default">Seleccionar una Categoria</option>
                            <?php foreach ($category_list->result() as $cgt_lst): ?>
                              <?php if ($selected_cat == $cgt_lst->category_id): ?>
                                <option value="<?php echo $cgt_lst->category_id;?>" selected>
                              <?php else: ?>
                                <option value="<?php echo $cgt_lst->category_id;?>">
                              <?php endif; ?>
                                <?php echo $cgt_lst->category_name;?>
                              </option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label>SubCategoria</label>
                        <div class="input-select">
                          <select name="subcategorie_selector" id="subcategorie_selector">
                            <?php foreach ($subcategory->result() as $subcat): ?>
                              <option value="<?php echo $subcat->subcategory_id;?>">
                                <?php echo $subcat->subcategory_name;?>
                              </option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <label>Descripcion Breve</label>
                        <textarea class="validate-required" name="product_desc" id="product_desc" rows="4"><?php echo $prod->product_desc;?></textarea>
                      </div>
                      <div class="col-md-12">
                        <label>Cuerpo de Texto</label>
                        <textarea class="validate-required" name="product_text" id="product_text" rows="4"><?php echo $prod->product_text;?></textarea>
                      </div>
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-6" style="padding-left:15px;">
                            <label>Precio</label>
                            <div class="input-icon">
                            	<input type="number" name="product_price" id="product_price" value="<?php echo $prod->product_price;?>"/>
                            </div>
                          </div>
                          <div class="col-md-6" style="padding-right:15px;">
                            <label>Cantidad</label>
                            <div class="input-icon">
                            	<input type="number" name="product_qty" id="product_qty" value="<?php echo $prod->product_qty;?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <hr>
                      </div>
                      <div class="col-md-12">
                        <h4>Envios</h4>
                      </div>
                      <?php foreach ($shipping_detail->result() as $shipng): ?>
                        <div class="col-md-6">
                          <div class="input-checkbox">
                            <?php if ($shipng->know_transport == 1): ?>
                              <input name="condition_send[]" id="my_transport" type="checkbox" value="my_transport" checked/>
                            <?php else: ?>
                              <input name="condition_send[]" id="my_transport" type="checkbox" value="my_transport"/>
                            <?php endif; ?>
                            <label for="checkbox"></label>
                          </div>
                          <span>Transportes que conozco o sugeridos por Dahra</span>
                          <br>
                          <br>
                          <span>
                            <div class="input-radio input-radio--innerlabel" style="width:100%; margin-bottom:10px;">
                              <?php if ($shipng->buyer_pay == 1): ?>
                                <input id="condition_send_type_buyer" type="radio" name="knowed_transport" value="send_buyer" checked/>
                              <?php else: ?>
                                <input id="condition_send_type_buyer" type="radio" name="knowed_transport" value="send_buyer" />
                              <?php endif; ?>
                              <label for="condition_send_type">Envío a cargo del comprador</label>
                            </div>
                            <div class="input-radio input-radio--innerlabel" style="width:100%;">
                              <?php if ($shipng->owner_pay == 1): ?>
                                <input id="condition_send_type_free" type="radio" name="knowed_transport" value="send_free" checked/>
                              <?php else: ?>
                                <input id="condition_send_type_free" type="radio" name="knowed_transport" value="send_free" />
                              <?php endif; ?>
                              <label for="condition_send_type">Envío gratis (Pagas los costos de envío)</label>
                            </div>
                          </span>
                        </div>
                        <div class="col-md-6">
                          <div class="input-checkbox">
                            <?php if ($shipng->personal_withdrawal == 1): ?>
                              <input name="condition_send[]" id="i_pick_up" type="checkbox" value="i_pick_up" checked/>
                            <?php else: ?>
                              <input name="condition_send[]" id="i_pick_up" type="checkbox" value="i_pick_up"/>
                            <?php endif; ?>
                            <label for="checkbox"></label>
                          </div>
                          <span>También se puede retirar en persona</span>
                        </div>
                      <?php endforeach; ?>
                      <div class="col-md-12">
                        <div class="row" style="margin-top:35px;">
                          <div class="col-md-4">
                            <button type="submit" class="btn btn--primary">Guardar Datos Basicos</button>
                          </div>
                        </div>
                      </div>
                    <?php echo form_close();?>
                  </div>

                </div>
              </div>
            </div>
          </div>
      </div>
    </li>




    <li>
      <div class="tab__title">
        <span class="h5">Multimedia</span>
      </div>
      <div class="tab__content">
        <?php $generic_image = base_url() . 'assets/img/system/generic_image.jpg';?>
        <div class="col-md-12" style="padding-left: 15px;">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <h4>Fotografias de Productos</h4>
                    </div>
                    <?php foreach ($image_list->result() as $img): ?>
                      <div class="col-md-3">
                        <?php echo form_open("Upload/uploadToS3", 'enctype="multipart/form-data"');?>
                          <input type="file" name="userfile" id="prod_pic_one" value="" style="display:none;">
                          <input type="hidden" name="product_code" value="<?php echo $product_code;?>">
                          <input type="hidden" name="image_name" value="image_path_one">
                          <input type="hidden" name="redirect" value="marketplace/post/edit/<?php echo $product_code;?>">
                          <?php if ($img->image_path_one == NULL): ?>
                            <img id="prod_pic_one_trigger"src="<?php echo $generic_image;?>" alt="prod_image" class="img-thumbnail">
                          <?php else: ?>
                            <img id="prod_pic_one_trigger"src="<?php echo $img->image_path_one;?>" alt="prod_image" class="img-thumbnail">
                          <?php endif; ?>
                          <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
                        <?php echo form_close();?>
                      </div>
                      <div class="col-md-3">
                        <?php echo form_open("Upload/uploadToS3", 'enctype="multipart/form-data"');?>
                          <input type="file" name="userfile" id="prod_pic_two" value="" style="display:none;">
                          <input type="hidden" name="product_code" value="<?php echo $product_code;?>">
                          <input type="hidden" name="image_name" value="image_path_two">
                          <input type="hidden" name="redirect" value="marketplace/post/edit/<?php echo $product_code;?>">
                          <?php if ($img->image_path_two == NULL): ?>
                            <img id="prod_pic_two_trigger"src="<?php echo $generic_image;?>" alt="prod_image" class="img-thumbnail">
                          <?php else: ?>
                            <img id="prod_pic_two_trigger"src="<?php echo $img->image_path_two;?>" alt="prod_image" class="img-thumbnail">
                          <?php endif; ?>
                          <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
                        <?php echo form_close();?>
                      </div>
                      <div class="col-md-3">
                        <?php echo form_open("Upload/uploadToS3", 'enctype="multipart/form-data"');?>
                          <input type="file" name="userfile" id="prod_pic_three" value="" style="display:none;">
                          <input type="hidden" name="product_code" value="<?php echo $product_code;?>">
                          <input type="hidden" name="image_name" value="image_path_three">
                          <input type="hidden" name="redirect" value="marketplace/post/edit/<?php echo $product_code;?>">
                          <?php if ($img->image_path_three == NULL): ?>
                            <img id="prod_pic_three_trigger"src="<?php echo $generic_image;?>" alt="prod_image" class="img-thumbnail">
                          <?php else: ?>
                            <img id="prod_pic_three_trigger"src="<?php echo $img->image_path_three;?>" alt="prod_image" class="img-thumbnail">
                          <?php endif; ?>
                          <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
                        <?php echo form_close();?>
                      </div>
                      <div class="col-md-3">
                        <?php echo form_open("Upload/uploadToS3", 'enctype="multipart/form-data"');?>
                          <input type="file" name="userfile" id="prod_pic_four" value="" style="display:none;">
                          <input type="hidden" name="product_code" value="<?php echo $product_code;?>">
                          <input type="hidden" name="image_name" value="image_path_four">
                          <input type="hidden" name="redirect" value="marketplace/post/edit/<?php echo $product_code;?>">
                          <?php if ($img->image_path_four == NULL): ?>
                            <img id="prod_pic_four_trigger"src="<?php echo $generic_image;?>" alt="prod_image" class="img-thumbnail">
                          <?php else: ?>
                            <img id="prod_pic_four_trigger"src="<?php echo $img->image_path_four;?>" alt="prod_image" class="img-thumbnail">
                          <?php endif; ?>
                          <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
                        <?php echo form_close();?>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </li>










    <li>
      <div class="tab__title">
        <span class="h5">Detalles</span>
      </div>
      <div class="tab__content">
        <div class="col-md-12">
          <?php echo form_open("Marketplace/save_product_size", 'class="row"');?>
            <input type="hidden" name="state" value="marketplace/post/edit/" readonly>
            <input type="hidden" name="product_code" value="<?php echo $product_code;?>" readonly>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4>Dimensiones</h4>
                </div>
                <div class="col-md-12">
                  <label>Tipo de Dimension</label>
                  <div class="input-icon">
                    <input type="text" name="size_container" value=""/>
                  </div>
                </div>
                <div class="col-md-3">
                  <label>Alto (cm)</label>
                  <div class="input-icon">
                    <input type="number" name="size_z" value=""/>
                  </div>
                </div>
                <div class="col-md-3">
                  <label>Ancho (cm)</label>
                  <div class="input-icon">
                    <input type="number" name="size_y" value=""/>
                  </div>
                </div>
                <div class="col-md-3">
                  <label>Largo (cm)</label>
                  <div class="input-icon">
                    <input type="number" name="size_x" value=""/>
                  </div>
                </div>
                <div class="col-md-3">
                    <label>Peso (gr)</label>
                    <div class="input-icon">
                      <input type="number" name="weight" value=""/>
                    </div>
                </div>
                <div class="col-md-12">
                  <br>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn--primary">Guardar Dimension</button>
                </div>
              </div>
            </div>
          <?php echo form_close();?>
        </div>
        <div class="col-md-12" style="margin-top:25px;">
          <table class="border--round">
            <thead>
              <tr>
                <th>Titulo</th>
                <th>Alto</th>
                <th>Ancho</th>
                <th>Largo</th>
                <th>Peso</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($size_list->num_rows() > 0): ?>
                <?php foreach ($size_list->result() as $prod_size_lst): ?>
                    <tr>
                      <td><?php echo $prod_size_lst->size_container; ?></td>
                      <td><?php echo $prod_size_lst->size_z; ?></td>
                      <td><?php echo $prod_size_lst->size_y; ?></td>
                      <td><?php echo $prod_size_lst->size_x; ?></td>
                      <td><?php echo $prod_size_lst->weight; ?></td>
                      <td>
                        <?php echo form_open("Marketplace/delete_product_size", 'class="row"');?>
                          <input type="hidden" name="state" value="marketplace/post/edit/" readonly>
                          <input type="hidden" name="product_code" value="<?php echo $product_code;?>" readonly>
                          <input type="hidden" name="product_size_id" value="<?php echo $prod_size_lst->product_size_id; ?>" readonly>
                          <input class="btn btn--primary btn--sm" type="submit" name="" value="Eliminar">
                        <?php echo form_close();?>
                      </td>
                    </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td>
                    Aun no hay tamaños cargados.
                  </td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
        <div class="col-md-12">
          <br>
          <hr>
        </div>
        <div class="col-md-12">
          <?php echo form_open("Marketplace/save_product_details", 'class="row"');?>
            <input type="hidden" name="state" value="marketplace/post/edit/" readonly>
            <input type="hidden" name="product_code" value="<?php echo $product_code;?>" readonly>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4>Detalles</h4>
                </div>
                <div class="col-md-12">
                  <label>Titulo de Detalle</label>
                  <div class="input-icon">
                    <input type="text" name="mrk_product_detail_title" value=""/>
                  </div>
                </div>
                <div class="col-md-12">
                  <label>Descripcion de Detalle</label>
                  <div class="input-icon">
                    <input type="text" name="mrk_product_detail_desc" value=""/>
                  </div>
                </div>
                <div class="col-md-12">
                  <br>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn--primary">Guardar Detalle</button>
                </div>
              </div>
            </div>
          <?php echo form_close();?>
        </div>
        <div class="col-md-12" style="margin-top:25px;">
          <table class="border--round">
            <thead>
              <tr>
                <th>Titulo</th>
                <th>Descripcion</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($detail_list->num_rows() > 0): ?>
                <?php foreach ($detail_list->result() as $prod_dtl_lst): ?>
                    <tr>
                      <td><?php echo $prod_dtl_lst->mrk_product_detail_title; ?></td>
                      <td><?php echo $prod_dtl_lst->mrk_product_detail_desc; ?></td>
                      <td>
                        <?php echo form_open("Marketplace/delete_product_detail", 'class="row"');?>
                          <input type="hidden" name="state" value="marketplace/post/edit/" readonly>
                          <input type="hidden" name="product_code" value="<?php echo $product_code;?>" readonly>
                          <input type="hidden" name="mrk_product_details_id" value="<?php echo $prod_dtl_lst->mrk_product_details_id; ?>" readonly>
                          <input class="btn btn--primary btn--sm" type="submit" name="" value="Eliminar">
                        <?php echo form_close();?>
                      </td>
                    </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td>
                    Aun no hay detalles cargados.
                  </td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12">
          <?php echo form_open("Marketplace/save_tags_materials_data", 'class="row"');?>
            <?php foreach ($prod_nfo->result() as $prod_nfo): ?>
              <input type="hidden" name="state" value="marketplace/post/edit/" readonly>
              <input type="hidden" name="product_code" value="<?php echo $product_code;?>" readonly>
              <div class="col-md-12">
                <h4>Etiquetas de Busqueda</h4>
              </div>
              <div class="col-md-6">
                <label>Etiquetas</label>
                <input type="text" name="product_tags" value="<?php echo $prod_nfo->product_tags; ?>" class="validate-required">
              </div>
              <div class="col-md-6">
                <label>Materiales</label>
                <input type="text" name="product_materials" value="<?php echo $prod_nfo->product_materials; ?>" class="validate-required">
              </div>
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-4">
                <button type="submit" class="btn btn--primary">Guardar Etiquetas</button>
              </div>
            <?php endforeach; ?>
          <?php echo form_close();?>
        </div>
      </div>
    </li>







    <li>
      <div class="tab__title">
        <span class="h5">Medios de Pago</span>
      </div>
      <div class="tab__content">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              <br>
            </div>
            <div class="col-md-4">
              <?php echo form_open("Marketplace/process_step_two_to_payment");?>
                <input type="hidden" name="product_code" value="<?php echo $product_code;?>" readonly>
                <button type="submit" class="btn btn--primary">Ver Medios de Pago</button>
              <?php echo form_close();?>
            </div>
          </div>
        </div>
      </div>
    </li>




  </ul>
</div>


  <script>
    // Images Trigger
    $('#prod_pic_one_trigger').click(function() {
      $('#prod_pic_one').trigger('click');
    });
    $('#prod_pic_two_trigger').click(function() {
      $('#prod_pic_two').trigger('click');
    });
    $('#prod_pic_three_trigger').click(function() {
      $('#prod_pic_three').trigger('click');
    });
    $('#prod_pic_four_trigger').click(function() {
      $('#prod_pic_four').trigger('click');
    });
    // Ajax
    function getSubcategorieByCategoryComboAjax() {
      var categoryID = $('#categorie_selector').val();
      var datapath = $('#pathData').attr('path') + 'Marketplace/getSubcategorieByCategoryID/' + categoryID;
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#subcategorie_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Subcategoria</option>';
          $('#subcategorie_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#subcategorie_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['subcategory_id'] + '">' +
              res[i]['subcategory_name'] +
              '</option>';
              $('#subcategorie_selector').append(optionConstruct);
            }
          }else{
            $('#subcategorie_selector').attr('disabled', 'disabled');
          }
        }
      })
    };
    $('#categorie_selector').change(function() {
      getSubcategorieByCategoryComboAjax();
    });
  </script>
<?php endforeach; ?>
