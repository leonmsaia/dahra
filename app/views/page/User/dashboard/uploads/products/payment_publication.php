<div class="col-md-12" style="padding-left: 15px; margin-top:15px;">
    <?php echo form_open("Dashboard/save_product_terms", "class='row'");?>
    <input type="hidden" name="actual_credit_balance" id="actual_credit_balance" value="<?php echo $credit_qty;?>" readonly>
    <?php foreach ($prod_nfo->result() as $prod): ?>
      <input type="hidden" name="product_id" value="<?php echo $prod->product_id;?>">
      <input type="hidden" name="product_code" value="<?php echo $prod->product_code;?>">
    <?php endforeach; ?>
    <div class="col-m-12" style="padding-left: 15px;">
      <div class="row">
        <div class="col-md-6" style="padding-left: 15px;">
          <div class="row">
            <div class="col-m-12" style="padding-left: 15px; margin-bottom:35px;">
              <h4>Duración</h4>
              <p>
                Selecciona la duración para el Producto.
                <br>
                Luego de finalizado, deberas volver a publicar el mismo nuevamente.
              </p>
            </div>
            <div class="col-md-12">
              <label>Duracion (Meses): <span id="duration_val_marker"></span></label>
              <input id="range-duration" class="range-duration" type="range" name="range-duration" min="1" max="24" step="1" value="1" />
              <input type="hidden" id="duration_val" name="duration_val" value="1" readonly placeholder="1">
              <small>Creditos Requeridos Cantidad de Meses</small>
              <ul>
                <li>Creditos Requeridos por 1 Mes = 1</li>
                <li>Creditos Requeridos por 6 Meses = 5</li>
                <li>Creditos Requeridos por 12 Meses = 10</li>
                <li>Creditos Requeridos por 24 Meses = 20</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6" style="padding-left: 15px;">
          <div class="row">
            <div class="col-m-12" style="padding-left: 15px; margin-bottom:35px;">
              <h4>Alcance</h4>
              <p>
                Selecciona la duración para el Producto.
                <br>
                Luego de finalizado, deberas volver a publicar el mismo nuevamente.
              </p>
            </div>
            <div class="col-md-12">
              <label>Alcance de Exposicion: <span id="exposition_val_marker"></span></label>
              <input id="range-exposition" class="range-exposition" type="range" name="range-exposition" min="1" max="4" step="1" value="1" />
              <input type="hidden" id="exposition_val" name="exposition_val" value="Minima" readonly placeholder="Minima">
              <small>Creditos Requeridos por Tipo de Exposicion:</small>
              <ul>
                <li>Minima: x 1 Credito por Mes.</li>
                <li>Media: x 5 Creditos por Mes.</li>
                <li>Alta: x 10 Creditos por Mes.</li>
                <li>Premiun: x 20 Creditos por Mes.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12" style="margin-top:35px;">
      <table class="border--round">
        <thead>
          <tr>
            <th>Meses de Publicacion</th>
            <th>Exposicion de Publicacion</th>
            <th>Finalizacion de la Publicacion</th>
            <th>Costo en Creditos</th>
            <th>Accion</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><span class="month_counter"></span> Meses</td>
            <td><span id="expositionVal"></span></td>
            <td><span id="expirationDate_formatted"></span></td>
            <td>
              <span id="credit_cost"></span> Creditos
            </td>
            <td><button id="activate_publication" type="submit" class="btn btn--primary" disabled>Activar</button></td>
          </tr>
        </tbody>
      </table>
      <div id="alert_credit_enough" class="alert bg--success">
        <div class="alert__body">
          <span>Actualmente dispones de <span class="actual_credit_show"></span> Creditos, una vez finalizada la operacion tendras <span id="result_credits_for_operation"></span> creditos.</span>
        </div>
      </div>
      <div id="alert_credit_not_enough" class="alert bg--error">
        <div class="alert__body">
          <span>Actualmente dispones de <span class="actual_credit_show"></span> Creditos, no puedes pagar el costo de la actual operacion, necesitas <span id="needed_credits_for_operation"></span> creditos más, <a href="<?php echo base_url();?>user/buy_credits">compra un plan</a> ahora para seguir operando.</span>
        </div>
      </div>
    </div>

  <?php echo form_close();?>
</div>


<script>
  function calculateCreditCost() {
    // Calc Credits per Month
    var credit_cost_by_months;
    var slider_val_duration = $('#range-duration').val();
    credit_cost_by_months = slider_val_duration;
    if (slider_val_duration == 6) {
      credit_cost_by_months = slider_val_duration - 1;
    }else if (slider_val_duration == 12) {
      credit_cost_by_months = slider_val_duration - 2;
    }else if (slider_val_duration == 24) {
      credit_cost_by_months = slider_val_duration - 4;
    }
    // Calc Credits per Exposition
    var slider_val_exposition = $('#range-exposition').val();
    if (slider_val_exposition == '1') {
      var expo_amplification = 1;
    }else if (slider_val_exposition == '2') {
      var expo_amplification = 5;
    }else if (slider_val_exposition == '3') {
      var expo_amplification = 10;
    }else if (slider_val_exposition == '4') {
      var expo_amplification = 20;
    }
    credit_cost_by_months = credit_cost_by_months * expo_amplification;
    $('#credit_cost').text(credit_cost_by_months);
    // Check Credit Validation
    var credit_amount_total = $('#actual_credit_balance').val();
    $('.actual_credit_show').text(credit_amount_total);
    // Date for Publication
    var actualDate = new Date();
    var variationDate = slider_val_duration * 30;
    var expirationDate = actualDate.setDate(actualDate.getDate() + variationDate);
    // Formatting Date
    var expirationDateFormatted = new Date(expirationDate);
    var day = (expirationDateFormatted.getDate() < 10 ? '0' : '') + expirationDateFormatted.getDate();
    var month = (expirationDateFormatted.getMonth() < 9 ? '0' : '') + (expirationDateFormatted.getMonth() + 1);
    var year = expirationDateFormatted.getFullYear();
    var finalExpirationDateFormatted = day + '/' + month + '/' + year;
    $('#expirationDate_formatted').text(finalExpirationDateFormatted);
    // Check User Credits
    if (credit_amount_total >= credit_cost_by_months) {
      var result_credits = credit_amount_total - credit_cost_by_months;
      $('#activate_publication').prop('disabled', false);
      $('#alert_credit_not_enough').hide();
      $('#alert_credit_enough').show();
      $('#result_credits_for_operation').text(result_credits);
    }else{
      var needed_credits = credit_cost_by_months - credit_amount_total;
      $('#activate_publication').prop('disabled', true);
      $('#alert_credit_not_enough').show();
      $('#alert_credit_enough').hide();
      $('#needed_credits_for_operation').text(needed_credits);
    }
  }
  function getRangeExposition() {
    var slider_val_exposition = $('#range-exposition').val();
    if (slider_val_exposition == '1') {
      var expo_txt = 'Minima';
    }else if (slider_val_exposition == '2') {
      var expo_txt = 'Media';
    }else if (slider_val_exposition == '3') {
      var expo_txt = 'Alta';
    }else if (slider_val_exposition == '4') {
      var expo_txt = 'Premiun';
    }
    $('#exposition_val').val(expo_txt);
    $('#exposition_val_marker').text(expo_txt);
    // Set Exposition Value
    $('#expositionVal').text(expo_txt);
  }
  $('#range-exposition').change(function () {
    getRangeExposition();
    calculateCreditCost();
  });
  function getRangeDuration() {
    var slider_val_duration = $('#range-duration').val();
    $('#duration_val_marker').text(slider_val_duration);
    $('.month_counter').text(slider_val_duration);
  }
  $('#range-duration').change(function () {
    getRangeDuration();
    calculateCreditCost();
  });
  $(document).ready(function() {
    getRangeExposition();
    getRangeDuration();
    calculateCreditCost();
  });
</script>
