<span id="pathData" path="<?php echo base_url();?>" style="display:none;"></span>

<div class="col-md-12" style="margin-bottom:35px;">
  <div class="row">
    <div class="col-md-12">
      <div class="row">

        <div class="col-md-12">
          <?php echo form_open("Marketplace/create_new_product", 'class="row"');?>
            <div class="col-md-12">
              <h4>Publicar Nuevo Producto</h4>
            </div>
            <div class="col-md-12">
              <label>Titulo</label>
              <input type="text" name="product_name" id="product_name" value="" class="validate-required">
            </div>
            <div class="col-md-6">
              <label>Categoria</label>
              <div class="input-select">
                <select name="categorie_selector" id="categorie_selector">
                  <option selected="" value="Default">Seleccionar una Categoria</option>
                  <?php foreach ($category_list->result() as $cgt_lst): ?>
                    <?php if ($usr_nfo->artist_country == $cgt_lst->category_id): ?>
                      <option value="<?php echo $cgt_lst->category_id;?>" selected>
                    <?php else: ?>
                      <option value="<?php echo $cgt_lst->category_id;?>">
                    <?php endif; ?>
                      <?php echo $cgt_lst->category_name;?>
                    </option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <label>SubCategoria</label>
              <div class="input-select">
                <select name="subcategorie_selector" id="subcategorie_selector">
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <label>Descripcion Breve</label>
              <textarea class="validate-required" name="product_desc" id="product_desc" rows="4"></textarea>
            </div>
            <div class="col-md-12">
              <label>Cuerpo de Texto</label>
              <textarea class="validate-required" name="product_text" id="product_text" rows="4"></textarea>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-6" style="padding-left:15px;">
                  <label>Precio</label>
                  <div class="input-icon">
                  	<input type="number" name="product_price" id="product_price" value=""/>
                  </div>
                </div>
                <div class="col-md-6" style="padding-right:15px;">
                  <label>Cantidad</label>
                  <div class="input-icon">
                  	<input type="number" name="product_qty" id="product_qty" value=""/>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <hr>
            </div>
            <div class="col-md-12">
              <h4>Envios</h4>
            </div>
            <div class="col-md-6">
              <div class="input-checkbox">
                <input name="condition_send[]" id="my_transport" type="checkbox" value="my_transport"/>
                <label for="checkbox"></label>
              </div>
              <span>Transportes que conozco o sugeridos por Dahra</span>
              <br>
              <br>
              <span>
                <div class="input-radio input-radio--innerlabel" style="width:100%; margin-bottom:10px;">
                  <input id="condition_send_type_buyer" type="radio" name="knowed_transport" value="send_buyer" />
                  <label for="condition_send_type">Envío a cargo del comprador</label>
                </div>
                <div class="input-radio input-radio--innerlabel" style="width:100%;">
                  <input id="condition_send_type_free" type="radio" name="knowed_transport" value="send_free" />
                  <label for="condition_send_type">Envío gratis (Pagas los costos de envío)</label>
                </div>
              </span>
            </div>
            <div class="col-md-6">
              <div class="input-checkbox">
                <input name="condition_send[]" id="i_pick_up" type="checkbox" value="i_pick_up"/>
                <label for="checkbox"></label>
              </div>
              <span>También se puede retirar en persona</span>
            </div>
            <div class="col-md-12">
              <div class="row" style="margin-top:35px;">
                <div class="col-md-4">
                  <button type="submit" class="btn btn--primary">Guardar Producto</button>
                </div>
              </div>
            </div>
          <?php echo form_close();?>
        </div>


      </div>
    </div>
  </div>
</div>
<script>
  // Images Trigger
  $('#prod_pic_one_trigger').click(function() {
    $('#prod_pic_one').trigger('click');
  });
  $('#prod_pic_two_trigger').click(function() {
    $('#prod_pic_two').trigger('click');
  });
  $('#prod_pic_three_trigger').click(function() {
    $('#prod_pic_three').trigger('click');
  });
  $('#prod_pic_four_trigger').click(function() {
    $('#prod_pic_four').trigger('click');
  });
  // Ajax
  function getSubcategorieByCategoryComboAjax() {
    var categoryID = $('#categorie_selector').val();
    var datapath = $('#pathData').attr('path') + 'Marketplace/getSubcategorieByCategoryID/' + categoryID;
    $.ajax({
      url: datapath,
      type: 'GET',
      success:function(res){
        var res = JSON.parse(res);
        $('#subcategorie_selector').empty();
        var defaultSelect = '<option value="">Seleccione una Subcategoria</option>';
        $('#subcategorie_selector').append(defaultSelect);
        if (res.length > 0) {
          $('#subcategorie_selector').removeAttr('disabled');
          for (var i = 0; i < res.length; i++) {
            var optionConstruct = '<option value="' + res[i]['subcategory_id'] + '">' +
            res[i]['subcategory_name'] +
            '</option>';
            $('#subcategorie_selector').append(optionConstruct);
          }
        }else{
          $('#subcategorie_selector').attr('disabled', 'disabled');
        }
      }
    })
  };
  $('#categorie_selector').change(function() {
    getSubcategorieByCategoryComboAjax();
  });
</script>
