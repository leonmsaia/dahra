<div class="sidebar__widget">
    <h5>Panel de Control</h5>
    <br>
    <ul class="menu-vertical">
      <li>
        <a href="<?php echo base_url();?>dashboard">
          Resumen
        </a>
      </li>
      <?php if (checkIFHaveArtistHelper() == TRUE): ?>
        <li>
          <a href="<?php echo base_url();?>dashboard/subscription">
            Creditos
          </a>
        </li>
      <?php endif; ?>
      <li>
        <br>
      </li>
      <li>
        <a href="<?php echo base_url();?>dashboard/buys/questions">
          Intereses
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>dashboard/buys/favorites">
          Favoritos
        </a>
      </li>
      <?php if (checkIFHaveArtistHelper() == TRUE): ?>
        <li>
          <br>
        </li>
        <li>
          <a href="<?php echo base_url();?>dashboard/sales/publications">
            Exposiciones
          </a>
        </li>
        <li>
          <a href="<?php echo base_url();?>dashboard/sales/questions">
            Preguntas
          </a>
        </li>
        <li>
          <a href="<?php echo base_url();?>dashboard/messages">
            Mensajes
          </a>
        </li>
        <li>
          <a href="<?php echo base_url();?>dashboard/stats">
            Estadisticas
          </a>
        </li>
      <?php endif; ?>
      <li>
        <br>
      </li>
      <li>
        <a href="<?php echo base_url();?>dashboard/configuration/personal_data">
          Datos Personales
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>dashboard/configuration/security_data">
          Seguridad
        </a>
      </li>
      <?php if (checkIFHaveArtistHelper() == TRUE): ?>
        <li>
          <a href="<?php echo base_url();?>dashboard/configuration/my_profile">
            Mi Perfil
          </a>
        </li>
      <?php endif; ?>
      <li>
        <a href="<?php echo base_url();?>dashboard/configuration/mails_notifications">
          E-Mails y Notificaciones
        </a>
      </li>
    </ul>
</div>
