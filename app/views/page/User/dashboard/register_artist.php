<span id="pathData" path="<?php echo base_url();?>" style="display:none;"></span>
<?php if (checkIFHaveArtistHelper() == TRUE): ?>
  <span id="actualData" status="<?php echo $statusData;?>" style="display:none;"></span>
<?php endif; ?>
<?php echo form_open("Dashboard/save_Personal_Data");?>
    <div class="row">
        <div class="col-md-12">
          <h5>Informacion Basica</h5>
        </div>
        <div class="col-md-4">
            <input type="text" name="dni" id="dni" placeholder="DNI" value=""/>
        </div>
        <div class="col-md-4">
            <input type="text" name="cuit" id="cuit" placeholder="CUIL/CUIT" value=""/>
        </div>
        <div class="col-md-4">
          <input type="text" name="artist_birth" id="artist_birth" class="datepicker" placeholder="Fecha de Nacimiento"value="" />
        </div>
        <div class="col-md-12">
          <p>Estos datos seran necesarios para poder facturarle la compra de creditos.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <br>
        </div>
        <div class="col-md-12">
          <h5>Informacion de Contacto</h5>
        </div>
        <div class="col-md-6">
            <input type="text" name="artist_phone" id="artist_phone" placeholder="Telefono" value=""/>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <br>
      </div>
      <div class="col-md-12">
        <h5>Direccion</h5>
      </div>
      <div class="col-4">
          <input type="text" name="artist_adress" id="artist_adress" placeholder="Calle" value=""/>
      </div>
      <div class="col-4">
          <input type="text" name="artist_adress_number" id="artist_adress_number" placeholder="Altura" value=""/>
      </div>
      <div class="col-4">
          <input type="text" name="artist_zipcode" id="artist_zipcode" placeholder="Codigo Postal" value=""/>
      </div>
      <div class="col-4">
        <div class="input-select">
          <select name="country_selector" id="country_selector">
            <option value="" selected>Seleccione una Opcion</option>
            <?php foreach ($artist_country->result() as $ctry): ?>
                <option value="<?php echo $ctry->id;?>">
                <?php echo $ctry->nombre;?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-4">
        <div class="input-select">
          <select name="province_selector" id="province_selector">
            <option value="" selected>Seleccione una Opcion</option>
            <?php foreach ($artist_province->result() as $prv): ?>
                <option value="<?php echo $prv->id;?>">
                  <?php echo $prv->nombre;?>
                </option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-4">
        <div class="input-select">
          <select name="party_selector" id="party_selector">
            <option value="" selected>Seleccione una Opcion</option>
              <?php foreach ($artist_party->result() as $prty): ?>
                <option value="<?php echo $prty->id;?>">
                  <?php echo $prty->nombre;?>
                </option>
              <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-4">
        <div class="input-select">
          <select name="locality_selector" id="locality_selector">
            <option value="" selected>Seleccione una Opcion</option>
            <?php foreach ($artist_location->result() as $locl): ?>
              <option value="<?php echo $locl->id;?>">
                <?php echo $locl->nombre;?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-4">
        <div class="input-select">
          <select name="suburb_selector" id="suburb_selector">
            <option value="" selected>Seleccione una Opcion</option>
            <?php foreach ($artist_neighbour->result() as $surb): ?>
              <option value="<?php echo $surb->id;?>">
                <?php echo $surb->nombre;?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-4">
        <div class="input-select">
          <select name="subsuburb_selector" id="subsuburb_selector">
            <option value="" selected>Seleccione una Opcion</option>
            <?php foreach ($artist_subneighbour->result() as $ssurb): ?>
              <option value="<?php echo $ssurb->id;?>">
                <?php echo $ssurb->nombre;?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <p>Estos datos seran necesarios para poder facturarle la compra de creditos.</p>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <br>
    </div>
    <div class="col-md-4">
      <button type="submit" class="btn btn--primary type--uppercase">Crear Cuenta de Artista</button>
    </div>
  </div>
<?php echo form_close();?>
