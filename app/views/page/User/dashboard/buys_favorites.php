<div class="tabs-container" data-content-align="left">
	<ul class="tabs">
		<li class="active">
			<div class="tab__title">
				<span class="h5">Productos</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
        	<tbody>
						<?php foreach ($fav_products->result() as $prd_fvs): ?>
							<?php
							 	$expiration_date = getExpirationDate($prd_fvs->product_id);
							?>
	            <tr class="row">
	        			<td class="col-md-3">
	                <img class="img-responsive img-thumbnail" src="<?php echo $prd_fvs->product_img;?>" alt="<?php echo $prd_fvs->product_name;?>">
	              </td>
	        			<td class="col-md-4">
	                <h5>
										<a href="<?php echo base_url() . 'marketplace/product/' . $prd_fvs->product_slug;?>">
											<?php echo $prd_fvs->product_name;?>
										</a>
									</h5>
	                <h6>
										<a href="<?php echo base_url() . 'marketplace/artist/' . $prd_fvs->artist_slug;?>">
											<?php echo $prd_fvs->artist_name . ' ' . $prd_fvs->artist_lastname;?>
										</a>
									</h6>
	              </td>
	        			<td class="col-md-1 text-center">$<?php echo $prd_fvs->product_price;?></td>
	              <td class="col-md-1 text-center">Finaliza <?php echo $expiration_date;?></td>
	        			<td class="col-md-3 text-center">
	                <ul>
	                  <li>
											<div class="modal-instance">
							          <a class="btn btn--primary modal-trigger" href="#">
							            <span class="btn__text">
							              Enviar Consulta
							            </span>
							          </a>
							          <div class="modal-container">
							            <div class="modal-content">
							              <div class="boxed boxed--lg">
							                  <h2>Enviar consulta al Artista:</h2>
							                  <hr class="short">
							                  <?php echo form_open("Marketplace/makeConsultDashboardBuyFavorites", 'class="row"');?>
							                    <div class="col-md-12" style="padding-left:15px; margin-top:45px;">
							                      <input type="hidden" name="message_receptor" value="<?php echo $prd_fvs->product_author;?>">
							                      <input type="hidden" name="message_prod_id" value="<?php echo $prd_fvs->product_id;?>">
							                      <textarea rows="8" cols="80" id="message_text" name="message_text" style="margin-bottom:20px;" placeholder="Escribir Consulta"></textarea>
							                      <div class="row">
							                        <div class="col-md-4">
							                          <button type="submit" class="btn btn--primary">Consultar</button>
							                        </div>
							                      </div>
							                    </div>
							                  <?php echo form_close();?>
							              </div>
							              <div class="modal-close modal-close-cross"></div>
							            </div>
							          </div>
							        </div>
	                  </li>
	                  <li><br></li>
	                  <li>
	                    <a class="btn btn--primary btn--sm" href="#">
	                    	<span class="btn__text">
	                        Eliminar
	                      </span>
	                    </a>
	                  </li>
	                </ul>
	              </td>
	        		</tr>
						<?php endforeach; ?>
        	</tbody>
        </table>
			</div>
		</li>
		<li>
			<div class="tab__title">
				<span class="h5">Artistas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
        	<tbody>
						<?php foreach ($fav_artist->result() as $arts_fvs): ?>
	            <tr class="row">
	        			<td class="col-md-3">
	                <img class="img-responsive img-thumbnail" src="<?php echo $arts_fvs->artist_picture;?>" alt="<?php echo $arts_fvs->artist_name . ' ' . $arts_fvs->artist_lastname;?>">
	              </td>
	        			<td class="col-md-4">
	                <h5>
											<?php echo $arts_fvs->artist_name . ' ' . $arts_fvs->artist_lastname;?>
									</h5>
	              </td>
	        			<td class="col-md-1 text-center"></td>
	              <td class="col-md-1 text-center"></td>
	        			<td class="col-md-3 text-center">
	                <ul>
	                  <li>
	                    <a class="btn btn--primary btn--sm" href="<?php echo base_url() . 'marketplace/artist/' . $arts_fvs->artist_slug;?>">
	                    	<span class="btn__text">
	                        Ver Coleccion
	                      </span>
	                    </a>
	                  </li>
	                </ul>
	              </td>
	        		</tr>
						<?php endforeach; ?>
        	</tbody>
        </table>
			</div>
		</li>
	</ul>
</div>
