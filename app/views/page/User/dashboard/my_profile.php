<?php foreach ($user_information->result() as $usr_nfo): ?>
    <div class="row">
      <div class="col-md-12">
        <h5>Perfil Publico</h5>
      </div>
    </div>
    <?php echo form_open("Upload/uploadProfilePicToS3", 'enctype="multipart/form-data" class="row"');?>
      <div class="col-md-6">
        <input type="file" name="userfile" id="profile_pic" value="" style="display:none;">
        <input type="hidden" name="artist_id" value="<?php echo $usr_nfo->artist_id;?>">
        <input type="hidden" name="artist_id_slug" value="<?php echo $usr_nfo->user_id . '-' . $usr_nfo->artist_slug;?>">
        <input type="hidden" name="image_name" value="profile_pic">
        <img id="profile_pic_trigger" src="<?php echo $usr_nfo->artist_picture;?>" alt="picture" class="img-thumbnail">
      </div>
      <div class="col-md-6">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
      </div>
    <?php echo form_close();?>
    <?php echo form_open("User/save_profile_data", 'class="row"');?>
        <div class="col-md-12">
            <input type="text" name="artist_short_desc" id="artist_short_desc" placeholder="Saludo" value="<?php echo $usr_nfo->artist_short_desc;?>"/>
        </div>
        <div class="col-md-6">
            <textarea class="validate-required" name="artist_past_studies" id="artist_past_studies" rows="4" placeholder="Estudios Cursados"><?php echo $usr_nfo->artist_past_studies;?></textarea>
            <small>Separar cada estudio mediante una coma (,).</small>
        </div>
        <div class="col-md-6">
            <textarea class="validate-required" name="artist_actual_studies" id="artist_actual_studies" rows="4" placeholder="Estudios Actuales"><?php echo $usr_nfo->artist_actual_studies;?></textarea>
            <small>Separar cada estudio mediante una coma (,).</small>
        </div>
        <div class="col-md-12">
          <textarea class="validate-required" name="artist_bio" id="artist_bio" rows="4" placeholder="Biografia"><?php echo $usr_nfo->artist_bio;?></textarea>
        </div>
        <div class="col-md-12">
          <br>
        </div>
        <div class="col-md-4">
          <button type="submit" class="btn btn--primary type--uppercase">Guardar</button>
        </div>
    <?php echo form_close();?>
<?php endforeach; ?>

<script>
  // Images Trigger
  $('#profile_pic_trigger').click(function() {
    $('#profile_pic').trigger('click');
  });
</script>
