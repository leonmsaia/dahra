<ul>
  <?php foreach ($questions_in_articles->result() as $qstns): ?>
    <li class="row" style="margin-bottom:35px;">
      <div class="col-md-2">
        <img class="img-responsive img-thumbnail" src="<?php echo $qstns->product_img;?>" alt="<?php echo $qstns->product_name;?>">
      </div>
      <div class="col-md-10">
        <span>
          <a href="<?php echo base_url() . 'marketplace/product/' . $qstns->product_slug;?>"><?php echo $qstns->product_name;?></a>
        </span>
        <br>
        <span>$<?php echo $qstns->product_price;?></span>
        <br>
        <span>x <?php echo $qstns->product_qty;?> disponibles</span>
      </div>
      <ul class="col-md-12" style="margin-top: 20px; padding-top:15px; padding-bottom:15px; border-top:1px solid #cdcdcd; border-bottom:1px solid #cdcdcd;">
        <li class="row">
          <span class="col-md-12">
            <span class="row">
              <span class="col-md-10"><i class="icon-Speach-Bubble" style="margin-right:10px;"></i>
                <b><?php echo $qstns->username;?>:</b> <?php echo $qstns->question_text;?>
              </span>
              <span class="col-md-2"><small><?php echo $qstns->question_time;?></small></span>
            </span>
          </span>
        </li>
        <?php if (questionHaveAnswer($qstns->question_status > 0)): ?>
          <?php $anserData = answersDetails($qstns->question_id);?>
          <?php foreach ($anserData->result() as $answ): ?>
            <li class="row">
              <span class="col-md-12">
                <span class="row">
                  <span class="col-md-10"><i class="icon-Speach-Bubble" style="margin-right:10px;"></i>
                    <b>Respuesta:</b> <?php echo $answ->answer_text;?>
                  </span>
                  <span class="col-md-2"><small><?php echo $answ->answer_time;?></small></span>
                </span>
              </span>
            </li>
          <?php endforeach; ?>
        <?php endif; ?>
      </ul>
      <?php if (questionHaveAnswer($qstns->question_status == 0)): ?>
        <span class="col-md-12" style="margin-top:15px;">
          <?php echo form_open('Marketplace/answerQuestion');?>
            <input type="hidden" id="question_id" name="question_id" value="<?php echo $qstns->question_id;?>" readonly>
            <input type="hidden" id="user_owner_id" name="user_owner_id" value="<?php echo $qstns->product_author;?>" readonly>
            <input type="hidden" id="product_id" name="product_id" value="<?php echo $qstns->product_id;?>" readonly>
            <input type="hidden" id="answer_status" name="answer_status" value="1" readonly>
            <textarea rows="3" cols="80" id="question_text" name="question_text" style="margin-bottom:20px;" placeholder="Escribir respuesta"></textarea>
            <div class="row">
              <div class="col-md-4">
                <button type="submit" class="btn btn--primary">Responder</button>
              </div>
            </div>
          <?php echo form_close();?>
        </span>
      <?php endif; ?>
    </li>
  <?php endforeach; ?>
</ul>
