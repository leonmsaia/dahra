<ul>
  <?php foreach ($messages_list->result() as $chat): ?>
    <li class="msg_list_item row" style="margin-bottom:25px;">
      <div class="col-md-10">
        <h4>Contacto por Obra</h4>
        <span>Producto: <?php echo $chat->product_name;?> <small><b>($ <?php echo $chat->product_price;?>)</b></small></span>
        <br>
        <span>Disponible: <?php echo $chat->product_qty;?></span>
        <br>
        <span>Usuario: <?php echo getUserDetails($chat->message_author)->username;?></span>
      </div>
      <ul class="col-md-12" style="margin-top: 20px; padding-top:15px; border-top:1px solid #cdcdcd;">
        <?php if (chatHaveAnswer($chat->message_original_thread > 0)): ?>
          <?php $chatData = chatDetails($chat->message_original_thread);?>
          <?php foreach ($chatData->result() as $chat_response): ?>
            <li class="row">
              <span class="col-md-12">
                <span class="row">
                  <span class="col-md-10"><i class="icon-Speach-Bubble" style="margin-right:10px;"></i>
                    <b><?php echo getUserDetails($chat_response->message_author)->username;?></b>: <?php echo $chat_response->message_text;?>
                  </span>
                  <span class="col-md-2"><small><?php echo $chat_response->message_date;?></small></span>
                </span>
              </span>
            </li>
					<?php
						$original_chat = $chat_response->message_id;
					?>
          <?php endforeach; ?>
        <?php else: ?>
          <li class="row">
            <span class="col-md-12">
              <b>Aun no respondio.</b>
            </span>
          </li>
        <?php endif; ?>
				<li class="row">
					<div class="col-md-12" style="margin-top: 20px; padding-top:15px; border-top:1px solid #cdcdcd;">
		        <div class="modal-instance">
		          <a class="btn btn--sm modal-trigger" href="#">
		            <span class="btn__text">
		              Responder
		            </span>
		          </a>
		          <div class="modal-container">
		            <div class="modal-content">
		              <div class="boxed boxed--lg">
		                  <h2>Responder:</h2>
		                  <hr class="short">
		                  <?php echo form_open('User/responseChat');?>
		                    <input type="hidden" id="message_original_thread" name="message_original_thread" value="<?php echo $chat->message_original_thread;?>" readonly>
												<input type="hidden" id="message_original" name="message_original" value="<?php echo $original_chat;?>" readonly>
												<input type="hidden" id="message_receptor" name="message_receptor" value="<?php echo $chat->message_author;?>" readonly>
		                    <textarea rows="8" cols="80" id="message_text" name="message_text" style="margin-bottom:20px;" placeholder="Escribir Respuesta"></textarea>
		                    <div class="row">
		                      <div class="col-md-4">
		                        <button type="submit" class="btn btn--primary">Responder</button>
		                      </div>
		                    </div>
		                  <?php echo form_close();?>
		              </div>
		              <div class="modal-close modal-close-cross"></div>
		            </div>
		          </div>
		        </div>
		      </div>
				</li>
			</ul>
    </li>
  <?php endforeach; ?>
</ul>
