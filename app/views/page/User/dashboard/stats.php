<?php if (checkIFHaveArtistHelper()):?>
<ul>
  <li>
    <h5>5 Productos mas Visitados</h5>
    <div class="chart_wrapp" id="most_visited_chart"></div>
  </li>
  <li><br></li>
  <li>
    <h5>Estadistica Diaria (Historico)</h5>
    <div class="chart_wrapp" id="daily_stats"></div>
  </li>
  <li><br></li>
  <li>
    <h5>Top 5 Productos Mas Consultados</h5>
    <div class="chart_wrapp" id="most_consult_prod"></div>
  </li>
</ul>
<?php $this->load->view('layout/assets/graph_scripts');?>
<script>
  var stats_most_visited = <?php echo $most_visited_prods;?>;
  var chart = AmCharts.makeChart("most_visited_chart", {
    "type": "serial",
    "theme": "light",
    "language": "es",
    "dataProvider": stats_most_visited,
    "valueAxes": [ {
      "gridColor": "#FFFFFF",
      "gridAlpha": 0.2,
      "dashLength": 0
    } ],
    "gridAboveGraphs": true,
    "startDuration": 1,
    "graphs": [ {
      "balloonText": "[[category]]: <b>[[value]]</b>",
      "fillAlphas": 0.8,
      "lineAlpha": 0.2,
      "type": "column",
      "valueField": "value"
    } ],
    "chartCursor": {
      "categoryBalloonEnabled": false,
      "cursorAlpha": 0,
      "zoomable": false
    },
    "categoryField": "label",
    "categoryAxis": {
      "gridPosition": "start",
      "gridAlpha": 0,
      "tickPosition": "start",
      "tickLength": 20
    }
  });
  var visitedByDay = <?php echo $visited_by_day;?>;
  var chart = AmCharts.makeChart("daily_stats", {
      "type": "serial",
      "theme": "light",
      "language": "es",
      "marginTop":0,
      "marginRight": 80,
      "dataProvider": visitedByDay,
      "valueAxes": [{
          "axisAlpha": 0,
          "position": "left"
      }],
      "graphs": [{
          "id":"g1",
          "balloonText": "[[category]]<br><b>Visitas: <span style='font-size:14px;'>[[value]]</span></b>",
          "bullet": "round",
          "bulletSize": 8,
          "lineColor": "#d1655d",
          "lineThickness": 2,
          "negativeLineColor": "#637bb6",
          "type": "smoothedLine",
          "valueField": "value"
      }],
      "chartScrollbar": {
        "graph":"g1",
        "gridAlpha":0,
        "color":"#888888",
        "scrollbarHeight":55,
        "backgroundAlpha":0,
        "selectedBackgroundAlpha":0.1,
        "selectedBackgroundColor":"#888888",
        "graphFillAlpha":0,
        "autoGridCount":true,
        "selectedGraphFillAlpha":0,
        "graphLineAlpha":0.2,
        "graphLineColor":"#c2c2c2",
        "selectedGraphLineColor":"#888888",
        "selectedGraphLineAlpha":1
      },
      "chartCursor": {
          "categoryBalloonDateFormat": "DD/MM/YYYY",
          "cursorAlpha": 0,
          "valueLineEnabled":true,
          "valueLineBalloonEnabled":true,
          "valueLineAlpha":0.5,
          "fullWidth":true
      },
      "dataDateFormat": "YYYY-MM-DD",
      "categoryField": "date",
      "categoryAxis": {
          "minPeriod": "DD",
          "parseDates": true,
          "minorGridAlpha": 0.1,
          "minorGridEnabled": true
      }
  });
  var most_consult_prod_data = <?php echo $most_consult_prod;?>;
  var chart = AmCharts.makeChart( "most_consult_prod", {
    "type": "pie",
    "theme": "light",
    "language": "es",
    "dataProvider": most_consult_prod_data,
    "valueField": "qty",
    "titleField": "prod_name",
    "startDuration": 0,
    "innerRadius": 80,
    "pullOutRadius": 20,
    "marginTop": 30,
    "titleField": "prod_name"
  } );
</script>
<?php endif; ?>
