<a class="btn block btn--icon bg--facebook type--uppercase" href="#">
    <span class="btn__text">
        <i class="socicon-facebook"></i>
        Registro con Facebook
    </span>
</a>
<a class="btn block btn--icon bg--googleplus type--uppercase" href="#">
    <span class="btn__text">
        <i class="socicon-google"></i>
        Registro con Google
    </span>
</a>
<hr data-title="O">
