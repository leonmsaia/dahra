<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="60"></td>
							</tr>
						</table>
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" align="center">

									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<?php if ($question_actual_user == true): ?>
											<tr>
												<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 24px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
													Te hicieron una pregunta
												</td>
											</tr>
											<tr>
												<?php foreach ($question_info->result() as $qst): ?>
													<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 16px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
														Hola, <?php echo $qst->artist_name;?> <?php echo $qst->artist_lastname;?>
													</td>
												<?php endforeach; ?>
											</tr>
										<?php else: ?>
											<tr>
												<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 24px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
													Respondieron tu pregunta
												</td>
											</tr>
											<tr>
												<?php foreach ($question_info->result() as $qst): ?>
													<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 16px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
														Hola, <?php echo $qst->artist_name;?> <?php echo $qst->artist_lastname;?>
													</td>
												<?php endforeach; ?>
											</tr>
										<?php endif; ?>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%">
												<table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td width="100" height="1" bgcolor="#f67b7c" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<?php if ($question_actual_user == true): ?>
											<?php foreach ($question_info->result() as $qst): ?>
												<tr>
													<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px;" class="fullCenter">
		                        Tienes una pregunta en:
														<a href="<?php echo base_url() . 'marketplace/product/' . $qst->product_slug;?>" target="_blank" style="color: #808080; font-size: 14px; text-decoration: none; line-height: 34px; width: 100%;">
															<?php echo $qst->product_name;?>
														</a>
		                      </td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
											<?php foreach ($question_info->result() as $qst): ?>
												<?php foreach ($answers_info->result() as $answ): ?>
													<tr>
														<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px;" class="fullCenter">
															<a href="<?php echo base_url() . 'marketplace/product/' . $answ->product_slug;?>" target="_blank" style="color: #808080; font-size: 14px; text-decoration: none; line-height: 34px; width: 100%;">
																Producto: <?php echo $answ->product_name;?>. ($ <?php echo $answ->product_price;?> x<?php echo $answ->product_qty;?> disponible.)
															</a>
															<br><br>
			                        <b>Tu Pregunta:</b> "<?php echo $qst->question_text;?>"
			                        <br>
			                        <b>Respuesta de Usuario:</b> "<?php echo $answ->answer_text;?>"
			                      </td>
													</tr>
												<?php endforeach; ?>
											<?php endforeach; ?>
										<?php endif; ?>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
										<tr>
											<td width="100%" class="buttonScale" width="auto" align="left">
												<table border="0" cellpadding="0" cellspacing="0" align="left" class="buttonScale">
													<tr>
														<td width="auto" align="center" height="37" bgcolor="#fa6f6f" style="border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: 600; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; color: #ffffff;">
															<a href="<?php echo $path_in_button;?>" target="_blank" style="color: #ffffff; font-size: 14px; text-decoration: none; line-height: 34px; width: 100%;">
																<?php echo $txt_in_button;?>
															</a>
														</td>
														<?php if ($question_actual_user == false): ?>
															<td width="20" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
															<?php foreach ($answers_info->result() as $answ): ?>
																<td width="auto" align="center" height="37" style="border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: 600; font-family: Helvetica, Arial, sans-serif, 'Open Sans';">
																	<a href="<?php echo base_url() . 'marketplace/product/' . $answ->product_slug;?>" target="_blank" style="color: 808080; font-size: 14px; text-decoration: none; line-height: 34px; width: 100%;">
																		Hacer otra Pregunta
																	</a>
																</td>
															<?php endforeach; ?>
														<?php endif; ?>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
