<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="60"></td>
							</tr>
						</table>
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" align="center">
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 24px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
												Designed to Convert
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%">
												<table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td width="100" height="1" bgcolor="#f67b7c" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px;" class="fullCenter">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque earum provident praesentium nobis fugiat quas unde non, maiores placeat vel et, asperiores sed illum. Harum quibusdam ipsam quae, quisquam velit?
												<br><br>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque earum provident praesentium nobis fugiat quas unde non, maiores placeat vel et, asperiores sed illum. Harum quibusdam ipsam quae, quisquam velit?
												<br><br>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque earum provident praesentium nobis fugiat quas unde non, maiores placeat vel et, asperiores sed illum. Harum quibusdam ipsam quae, quisquam velit?
												<br><br>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque earum provident praesentium nobis fugiat quas unde non, maiores placeat vel et, asperiores sed illum. Harum quibusdam ipsam quae, quisquam velit?
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
