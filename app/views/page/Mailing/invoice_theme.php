<?php
  $site_nfo = $site_information->result()[0];
  $site_ctc = $site_contact->result()[0];
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="60"></td>
							</tr>
						</table>
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" align="center">
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 24px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
												Recibo N° <?php echo $recipe_id;?>
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%">
												<table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td width="100" height="1" bgcolor="#f67b7c" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px;" class="fullCenter">
												<?php foreach ($recipe_information->result() as $recipe_nfo): ?>
													Código de Operacion: <?php echo strtoupper($recipe_nfo->code);?>
													<br>
                          Fecha de Operacion: <?php echo date("d/m/Y", strtotime($recipe_nfo->date));?>
													<br>
													Item: Plan <?php echo $recipe_nfo->mrk_point_plans_title;?>
													<br>
													Descripcion: <?php echo $recipe_nfo->mrk_credit_operation_qty;?> Creditos de Website <?php echo $site_nfo->site_name;?>
													<br>
													Total: $<?php echo number_format((float)$recipe_nfo->amount, 2, '.', '');?>
												<?php endforeach; ?>
												<br>
												<br>
												<hr>
												Ver Recibo
												<br>
												<br>
												<a href="<?php echo base_url() . 'dashboard/view_recipe/' . $recipe_id;?>" target="_blank">
													<img src="<?php echo base_url() . 'assets/img/icons/invoice.png';?>" alt="" style="width: 80px;">
												</a>
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
