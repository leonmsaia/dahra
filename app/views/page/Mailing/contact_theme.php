<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="60"></td>
							</tr>
						</table>
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" align="center">
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 24px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
												Te Contactaron
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%">
												<table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td width="100" height="1" bgcolor="#f67b7c" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<?php foreach ($contact_information->result() as $cntc): ?>
											<tr>
												<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px;" class="fullCenter">
													Hola <?php echo getUserDetails($cntc->message_receptor)->first_name;?>!
													<br>
													Te contactaron por el aviso que publicaste.
													<br>
													<a href="<?php echo base_url() . 'marketplace/product/' . $cntc->product_slug;?>" target="_blank" style="color: #808080; font-size: 14px; text-decoration: none; line-height: 34px; width: 100%;">
														<b><?php echo $cntc->product_name;?></b>. ($ <?php echo $cntc->product_price;?> x <?php echo $cntc->product_qty;?> disponible.)
													</a>
													<br><br>
													Contacto:
													<br>
													<?php echo getUserDetails($cntc->message_author)->first_name;?>: "<?php echo $cntc->message_text;?>"
	                      </td>
											</tr>
										<?php endforeach; ?>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
