<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" class="image191" align="center">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
										<tr>
											<td width="100%" height="60"></td>
										</tr>
									</table>
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 38px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
												<?php echo $mail_head;?>
											</td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%">
												<table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td width="100" height="1" bgcolor="#808080" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
									</table>
									<?php foreach ($advise_info->result() as $nfo): ?>
										<table width="191" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
											<tr>
												<td width="100%" class="fullCenter">
													<a href="<?php echo base_url() . 'marketplace/product/' . $nfo->product_slug;?>" style="text-decoration: none;">
														<img src="<?php echo $nfo->product_img;?>" width="191" height="auto" alt="" border="0" class="hover">
													</a>
												</td>
											</tr>
										</table>
										<table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
											<tr>
												<td width="100%" height="30"></td>
											</tr>
										</table>
										<table width="350" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
											<tr>
												<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 24px; color: #444444; line-height: 32px; font-weight: 700;" class="fullCenter">
													<?php echo $nfo->product_name;?>
												</td>
											</tr>
											<tr>
												<td width="100%" height="25"></td>
											</tr>
											<tr>
												<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px; font-weight: 400;" class="fullCenter">
													Cantidad: <?php echo $nfo->product_qty;?>
													<br>
													Precio: $ <?php echo $nfo->product_price;?>
													<br>
													Exposicion:
													<?php if ($nfo->product_exposition == 1): ?>
														Bajo
													<?php elseif($nfo->product_exposition == 2): ?>
														Medio
													<?php elseif($nfo->product_exposition == 3): ?>
														Alto
													<?php elseif($nfo->product_exposition == 4): ?>
														Premiun
													<?php endif; ?>
													<?php if ($republished_assets == FALSE): ?>
														<br>
														Duración: 12 Meses
														<br>
														Creditos: 48
													<?php endif; ?>
												</td>
											</tr>
											<?php if ($republished_assets == TRUE): ?>
												<tr>
													<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #808080; line-height: 22px; font-weight: 400;" class="fullCenter">
														<br>
														<b>¿Porque no intentas mejorar el tipo de publicacion?</b>
														<br>
														Asi tendras:
														<br>
														<ul>
															<li>Mejor exposicion en los listados.</li>
															<li>Mas visitas.</li>
															<li>Mas posibles interesados.</li>
														</ul>
													</td>
												</tr>
											<?php endif; ?>
											<tr>
												<td width="100%" height="30"></td>
											</tr>
											<tr>
												<td width="100%" class="buttonScale" width="auto" align="left">
													<table border="0" cellpadding="0" cellspacing="0" align="left" class="buttonScale">
														<tr>
															<td width="auto" align="center" height="37" bgcolor="#fa6f6f" style="border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: 600; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; color: #ffffff;">
																<a href="<?php echo $path_in_button ?>" target="_blank" style="color: #ffffff; font-size: 14px; text-decoration: none; line-height: 34px; width: 100%;">
																	<?php echo $txt_in_button;?>
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									<?php endforeach; ?>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
							<tr>
								<td width="100%" height="25"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
				<tr>
					<td width="100%" valign="middle" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
							<tr>
								<td width="100%" height="25">&nbsp;</td>
							</tr>
							<tr>
								<td width="100" height="1" bgcolor="#f8f8f8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" height="25">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
