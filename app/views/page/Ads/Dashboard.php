<section class="space--sm">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="boxed boxed--border boxed--lg bg--secondary">
                    <?php $this->load->view('page/Ads/Dashboard/sidebar');?>
                </div>
            </div>
            <div class="col-md-9">
              <div class="row justify-content-center">
                <div class="col-md-12">
                  <h5><?php echo $module_title;?></h5>
                  <hr>
                </div>
                <div class="col-md-12">
                  <?php $this->load->view('page/Ads/dashboard/' . $module);?>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>

<script>
  $(document).ready(function() {
    // Render Location Combos
    function getAllCountryComboAjax() {
      var datapath = $('#pathData').attr('path') + 'Location/getAllPaises/';
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#province_selector').empty();
          $('#party_selector').empty();
          $('#locality_selector').empty();
          $('#suburb_selector').empty();
          $('#subsuburb_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Opcion</option>';
          $('#country_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#country_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['id'] + '">' +
              res[i]['nombre'] +
              '</option>';
              $('#country_selector').append(optionConstruct);
            }
            console.log(optionConstruct);
          }else{
            $('#country_selector').attr('disabled', 'disabled');
          }
        }
      })
    };
    function getProvinceByCountryComboAjax() {
      var countryID = $('#country_selector').val();
      var datapath = $('#pathData').attr('path') + 'Location/getProvinceByCountryID/' + countryID;
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#province_selector').empty();
          $('#party_selector').empty();
          $('#locality_selector').empty();
          $('#suburb_selector').empty();
          $('#subsuburb_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Opcion</option>';
          $('#province_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#province_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['id'] + '">' +
              res[i]['nombre'] +
              '</option>';
              $('#province_selector').append(optionConstruct);
            }
          }else{
            $('#province_selector').attr('disabled', 'disabled');
          }
        }
      })
    };
    function getPartyByProvinceComboAjax() {
      var provinceID = $('#province_selector').val();
      var datapath = $('#pathData').attr('path') + 'Location/getPartyByProvinceID/' + provinceID;
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#party_selector').empty();
          $('#locality_selector').empty();
          $('#suburb_selector').empty();
          $('#subsuburb_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Opcion</option>';
          $('#party_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#party_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['id'] + '">' +
              res[i]['nombre'] +
              '</option>';
              $('#party_selector').append(optionConstruct);
            }
          }else{
            $('#party_selector').attr('disabled', 'disabled');
          }
        }
      })
    };
    function getLocalityByPartyComboAjax() {
      var partyID = $('#party_selector').val();
      var datapath = $('#pathData').attr('path') + 'Location/getLocaliltyByPartyID/' + partyID;
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#locality_selector').empty();
          $('#suburb_selector').empty();
          $('#subsuburb_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Opcion</option>';
          $('#locality_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#locality_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['id'] + '">' +
              res[i]['nombre'] +
              '</option>';
              $('#locality_selector').append(optionConstruct);
            }
          }else{
            $('#locality_selector').attr('disabled', 'disabled');
          }
        }
      })
    };
    function getSuburbByLocalityComboAjax() {
      var localityID = $('#locality_selector').val();
      var datapath = $('#pathData').attr('path') + 'Location/getSuburbByLocalityID/' + localityID;
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#suburb_selector').empty();
          $('#subsuburb_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Opcion</option>';
          $('#suburb_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#suburb_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['id'] + '">' +
              res[i]['nombre'] +
              '</option>';
              $('#suburb_selector').append(optionConstruct);
            }
          }else{
            $('#suburb_selector').attr('disabled', 'disabled');
          }
        }
      })
    };
    function getSubSuburbByLocalityComboAjax() {
      var localityID = $('#suburb_selector').val();
      var datapath = $('#pathData').attr('path') + 'Location/getSubSuburbByLocalityID/' + localityID;
      $.ajax({
        url: datapath,
        type: 'GET',
        success:function(res){
          var res = JSON.parse(res);
          $('#subsuburb_selector').empty();
          var defaultSelect = '<option value="">Seleccione una Opcion</option>';
          $('#subsuburb_selector').append(defaultSelect);
          if (res.length > 0) {
            $('#subsuburb_selector').removeAttr('disabled');
            for (var i = 0; i < res.length; i++) {
              var optionConstruct = '<option value="' + res[i]['id'] + '">' +
              res[i]['nombre'] +
              '</option>';
              $('#subsuburb_selector').append(optionConstruct);
            }
          }else{
            $('#subsuburb_selector').attr('disabled', 'disabled');
          }
        }
      })
    };

    function checkIFBoothRadioAreFree() {
      var radioexpo = parseFloat($('input[name=exposition_val]:checked').val());
      var radiodura = parseFloat($('input[name=duration_val]:checked').val());
      if (radioexpo == 0 && radiodura == 0) {
        alert('Ambos no pueden ser gratuitos');
        return false;
      }else{
        return true;
      }
    }

    $('#country_selector').change(function() {
      getAllCountryComboAjax();
      getProvinceByCountryComboAjax();
    });
    $('#country_selector').change(function() {
      getProvinceByCountryComboAjax();
    });
    $('#province_selector').change(function() {
      getPartyByProvinceComboAjax();
    });
    $('#party_selector').change(function() {
      getLocalityByPartyComboAjax();
    });
    $('#locality_selector').change(function() {
      getSuburbByLocalityComboAjax();
    });
    $('#suburb_selector').change(function() {
      getSubSuburbByLocalityComboAjax();
    });
    var statusDataInitial = $('#actualData').attr('status');
    if (statusDataInitial == 'FALSE') {
      getAllCountryComboAjax();
    }

    function getValuesFromRadio() {
      var radioCheck = checkIFBoothRadioAreFree();
      if (radioCheck == true) {
        var radioprovider = parseFloat($('input[name=payment_val]:checked').data('provider_comission'));
        var provider_name = $('input[name=payment_val]:checked').data('provider_name');
        var radioexpo = parseFloat($('input[name=exposition_val]:checked').val());
        var radiodura = parseFloat($('input[name=duration_val]:checked').val());
        var acumulate_percent = radioexpo + radiodura;
        var product_value = $('#price_product').text();
        $('#percent_dahra').text(acumulate_percent);
        var real_percent = 100 - acumulate_percent;
        var real_earn = (real_percent * product_value) / 100;
        var real_percent_calc = product_value - real_earn;
        $('#percent_dahra_calc').text(real_percent_calc);
        var payment_comission = parseFloat(radioprovider);
        var real_payment_comission = (payment_comission * product_value) / 100;
        var real_payment_comission_calc = product_value - real_payment_comission;
        $('#real_payment_commision').text(real_payment_comission);
        $('#total_earn').text(real_earn - real_payment_comission);
        $('#activate_publication').prop('disabled', false);
        $('.provider_name').text(provider_name);
        $('#percent_in_alert').text(radioprovider);
        $('#payment_commision').text(radioprovider);
        $('#payment_in_alert').text(provider_name);
        $('#input_site_comission').val(acumulate_percent);
        $('#input_site_comission_real').val(real_percent_calc);
        $('#input_payment_comission').val(payment_comission);
        $('#input_payment_comission_real').val(real_payment_comission_calc);
        $('#input_product_final_earn').val(real_earn - real_payment_comission);
      }else{
        $('#activate_publication').prop('disabled', true);
      }
    }

    $('.radio-exposition').change(function() {
      getValuesFromRadio()
    });

    $('.radio-duration').change(function() {
      getValuesFromRadio()
    });

    $('.radio-payment').change(function() {
      getValuesFromRadio()
    });

    getValuesFromRadio();

  });

</script>
