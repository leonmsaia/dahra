<?php
  // Start Payment Method Configuration
  $price_cost = $store_configuration->result()[0]->mrk_configuration_credit_cost;
  $test_mode = true;
  if ($test_mode == true) {
    $user_buyer_email = 'test_user_93195655@testuser.com';
    $client_id_mp = $this->config->item('mercadopago_id_test');
    $client_secret_mp = $this->config->item('mercadopago_secret_test');
    $user_name = 'APRO';
    $user_lastname = 'APRO';
    MercadoPago\SDK::setClientId($client_id_mp);
    MercadoPago\SDK::setClientSecret($client_secret_mp);
  }else{
    $user_buyer_email = $user_info->result()[0]->email;
    $client_id_mp = $this->config->item('mercadopago_id');
    $client_secret_mp = $this->config->item('mercadopago_secret');
    $user_name = $user_info->result()[0]->first_name;
    $user_lastname = $user_info->result()[0]->last_name;
    MercadoPago\SDK::setClientId($client_id_mp);
    MercadoPago\SDK::setClientSecret($client_secret_mp);
    // Start Payment Method Configuration End
  }
?>
<section>
    <div class="container">
      <div class="row">
        <?php foreach ($point_plans->result() as $plns): ?>
          <?php
            // Payment Information
            $cost_by_plan = $plns->mrk_point_plans_qty * $price_cost;
            $cost_by_plan_with_discount = ((100 - $plns->mrk_point_plans_discount) * $cost_by_plan) / 100;
            // Sale Collection Information
            $product_qty = 1;
            $user_information = $user_info->result()[0];
            # Create a preference object
            $preference = new MercadoPago\Preference();
            # Create an item object
            $item = new MercadoPago\Item();
            $item->title = 'Plan: ' . $plns->mrk_point_plans_title;
            $item->quantity = $product_qty;
            $item->currency_id = "ARS";
            $item->unit_price = $cost_by_plan_with_discount;
            # Create a payer object
            $payer = new MercadoPago\Payer();
            $payer->name = $user_name;
            $payer->surname = $user_lastname;
            $payer->email = $user_buyer_email;
            $payer->identification = array(
              "type" => "DNI",
              "number" => $user_info->result()[0]->dni
            );
            // Setting preference properties
            $userid_trans = $user_info->result()[0]->id;
            $actual_date =  strtotime(date('Y-m-d H:i:s'));
            $amount_trans = $cost_by_plan_with_discount;
            $plan_id = $plns->mrk_point_plans_id;
            $code_trans = generateID();
            $transactionID = 'TRANS_USER_' . $userid_trans . '_DATE_' . $actual_date . '_AMOUNT_' . $amount_trans . '_CODE_' . $code_trans . '_PLAN_' . $plan_id;
            $preference->external_reference = $transactionID;
            $preference->items = array($item);
            $preference->payer = $payer;
            $preference->back_urls = array(
              "success" => base_url() . 'ads/payment_process/success',
              "failure" => base_url() . 'ads/payment_process/success',
              "pending" => base_url() . 'ads/payment_process/success'
            );
            # Save and posting preference
            $preference->save();
          ?>

          <div class="col-md-3">
              <div class="pricing pricing-1 boxed boxed--lg boxed--border boxed--emphasis">
                  <h3><?php echo $plns->mrk_point_plans_title;?></h3>
                  <span class="h2">
                      <strong>$<?php echo number_format(($cost_by_plan_with_discount), 2);?></strong>
                  </span>
                  <span class="type--fine-print"><?php echo $plns->mrk_point_plans_desc;?></span>
                  <?php if ($plns->mrk_point_plans_high == 1): ?>
                    <span class="label" style="top: 1.785714em;">Recomendado</span>
                  <?php endif; ?>
                  <hr>
                  <ul>
                    <li>
                        <span class="checkmark bg--primary-1"></span>
                        <span><?php echo $plns->mrk_point_plans_qty;?> Creditos</span>
                    </li>
                    <li>
                        <span class="checkmark bg--primary-1"></span>
                        <span><?php echo $plns->mrk_point_plans_discount;?>% de descuento</span>
                    </li>
                    <li>
                        <span class="checkmark bg--primary-1"></span>
                        <span>Ahorro de $<?php echo number_format(($cost_by_plan - $cost_by_plan_with_discount), 2);?></span>
                    </li>
                  </ul>
                  <?php if ($plns->mrk_point_plans_high == 1): ?>
                    <a class="btn btn--primary-1" href="<?php echo $preference->sandbox_init_point; ?>">
                      <span class="btn__text">Comprar Plan</span>
                    </a>
                  <?php else: ?>
                    <a class="btn btn--primary" href="<?php echo $preference->sandbox_init_point; ?>">
                      <span class="btn__text">Comprar Plan</span>
                    </a>
                  <?php endif; ?>
              </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
</section>
