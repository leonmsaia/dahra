<div class="col-md-12" style="margin-bottom:35px;">
	<div class="row">
	  <div class="col-md-4">
			<a class="btn btn--primary btn--sm" href="<?php echo base_url() . 'ads/publication/create_ads';?>">
				<span class="btn__text">
					Publicar Nuevo Aviso
				</span>
			</a>
	  </div>
	</div>
</div>

<div class="tabs-container" data-content-align="left">
	<ul class="tabs">

    <li class="active">
			<div class="tab__title">
				<span class="h5">Activas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
        	<tbody>
						<?php foreach ($sales_list->result() as $sls_lst): ?>
							<?php if ($sls_lst->mkt_status == 1): ?>
								<tr class="row">
		        			<td class="col-md-3">
		                <h5><?php echo $sls_lst->mkt_ads_title;?></h5>
		              </td>
									<td class="col-md-2 text-center">
										Imp. Rest.: <?php echo countAllPrintsByAdsAuthorAndMinusCurrent($sls_lst->mkt_ads_user_author,  $sls_lst->mkt_ads_adscode);?>
									</td>
									<td class="col-md-2 text-center">
										Clics Rest.: <?php echo countAllClicsByAdsAuthorAndMinusCurrent($sls_lst->mkt_ads_user_author,  $sls_lst->mkt_ads_adscode);?>
									</td>
		        			<td class="col-md-5 text-center">
		                <ul>
		                  <li>
		                    <a class="btn btn--primary btn--sm" href="<?php echo base_url() . 'ads/publication/edit_ads/' . $sls_lst->mkt_ads_adscode;?>">
		                    	<span class="btn__text">
		                        Editar
		                      </span>
		                    </a>
		                  </li>
		                  <li><br></li>
		                  <li>
		                    <div class="btn btn--primary btn--sm dropdown">
		                      <span class="btn__text">
		                        Estado
		                      </span>
		                    	<span class="dropdown__trigger"></span>
		                    	<div class="dropdown__container dropdown_fixed">
		                    		<div class="container">
		                    			<div class="row">
		                    				<div class="col-md-3 col-lg-2 dropdown__content">
		                    					<ul class="menu-vertical">
		                    						<li>
																			<?php echo form_open('Ads/status_publication');?>
																				<input type="hidden" name="mkt_status" value="2">
																				<input type="hidden" name="mkt_ads_id" value="<?php echo $sls_lst->mkt_ads_id;?>">
																				<button type="submit" class="type--bold input_plain">Pausar</button>
																			<?php echo form_close();?>
		                    						</li>
		                    						<li>
																			<?php echo form_open('Ads/status_publication');?>
																				<input type="hidden" name="mkt_status" value="0">
																				<input type="hidden" name="mkt_ads_id" value="<?php echo $sls_lst->mkt_ads_id;?>">
																				<button type="submit" class="type--bold input_plain">Finalizar</button>
																			<?php echo form_close();?>
		                    						</li>
		                    					</ul>
		                    				</div>
		                    			</div>
		                    		</div>
		                    	</div>
		                    </div>
		                  </li>
		                </ul>
		              </td>
		        		</tr>
							<?php endif; ?>
						<?php endforeach; ?>
        	</tbody>
        </table>
			</div>
		</li>

    <li>
			<div class="tab__title">
				<span class="h5">Pausadas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
        	<tbody>
						<?php foreach ($sales_list->result() as $sls_lst): ?>
							<?php if ($sls_lst->mkt_status == 2): ?>
								<tr class="row">
									<td class="col-md-3">
		                <h5><?php echo $sls_lst->mkt_ads_title;?></h5>
		              </td>
									<td class="col-md-2 text-center">
										Imp. Rest.: <?php echo countAllPrintsByAdsAuthorAndMinusCurrent($sls_lst->mkt_ads_user_author,  $sls_lst->mkt_ads_adscode);?>
									</td>
									<td class="col-md-2 text-center">
										Clics Rest.: <?php echo countAllClicsByAdsAuthorAndMinusCurrent($sls_lst->mkt_ads_user_author,  $sls_lst->mkt_ads_adscode);?>
									</td>
			      			<td class="col-md-5 text-center">
			              <ul>
			                <li>
			                  <a class="btn btn--primary btn--sm" href="<?php echo base_url() . 'ads/publication/edit_ads/' . $sls_lst->mkt_ads_adscode;?>">
			                  	<span class="btn__text">
			                      Editar
			                    </span>
			                  </a>
			                </li>
			                <li><br></li>
			                <li>
			                  <div class="btn btn--primary btn--sm dropdown">
			                    <span class="btn__text">
			                      Estado
			                    </span>
			                  	<span class="dropdown__trigger"></span>
			                  	<div class="dropdown__container dropdown_fixed">
			                  		<div class="container">
			                  			<div class="row">
			                  				<div class="col-md-3 col-lg-2 dropdown__content">
			                  					<ul class="menu-vertical">
																		<li>
																			<?php echo form_open('Ads/status_publication');?>
																				<input type="hidden" name="mkt_status" value="1">
																				<input type="hidden" name="mkt_ads_id" value="<?php echo $sls_lst->mkt_ads_id;?>">
																				<button type="submit" class="type--bold input_plain">Reanudar</button>
																			<?php echo form_close();?>
		                    						</li>
		                    						<li>
																			<?php echo form_open('Ads/status_publication');?>
																				<input type="hidden" name="mkt_status" value="0">
																				<input type="hidden" name="mkt_ads_id" value="<?php echo $sls_lst->mkt_ads_id;?>">
																				<button type="submit" class="type--bold input_plain">Finalizar</button>
																			<?php echo form_close();?>
		                    						</li>
			                  					</ul>
			                  				</div>
			                  			</div>
			                  		</div>
			                  	</div>
			                  </div>
			                </li>
			              </ul>
			            </td>
			      		</tr>
							<?php endif; ?>
						<?php endforeach; ?>
        	</tbody>
        </table>
			</div>
		</li>

		<li>
			<div class="tab__title">
				<span class="h5">Finalizadas</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
        	<tbody>
						<?php foreach ($sales_list->result() as $sls_lst): ?>
							<?php if ($sls_lst->mkt_status == 0): ?>
								<tr class="row">
									<td class="col-md-3">
		                <h5><?php echo $sls_lst->mkt_ads_title;?></h5>
		              </td>
		              <td class="col-md-1 text-center">
										Finalizó
									</td>
		        			<td class="col-md-3 text-center">
		                <ul>
		                  <li>
		                    <a class="btn btn--primary btn--sm" href="<?php echo base_url() . 'ads/publication/republish/' . $sls_lst->mkt_ads_adscode;?>">
		                    	<span class="btn__text">
		                        Republicar
		                      </span>
		                    </a>
		                  </li>
		                </ul>
		              </td>
		        		</tr>
							<?php endif; ?>
						<?php endforeach; ?>
        	</tbody>
        </table>
			</div>
		</li>

		<li>
			<div class="tab__title">
				<span class="h5">Sin Activar</span>
			</div>
			<div class="tab__content">
        <table class="table--alternate-row">
        	<tbody>
						<?php foreach ($sales_list->result() as $sls_lst): ?>
							<?php if ($sls_lst->mkt_status == 8): ?>
								<tr class="row">
									<td class="col-md-3">
		                <h5><?php echo $sls_lst->mkt_ads_title;?></h5>
		              </td>
		              <td class="col-md-1 text-center"></td>
		        			<td class="col-md-3 text-center">
		                <ul>
		                  <li>
		                    <a class="btn btn--primary btn--sm" href="<?php echo base_url() . 'ads/publication/payment_publication/' . $sls_lst->mkt_ads_adscode;?>">
		                    	<span class="btn__text">
		                        Activar
		                      </span>
		                    </a>
		                  </li>
		                </ul>
		              </td>
		        		</tr>
							<?php endif; ?>
						<?php endforeach; ?>
        	</tbody>
        </table>
			</div>
		</li>

	</ul>
</div>
