<div class="row">
    <div class="col-md-12">
			<table class="border--round table--alternate-row">
				<thead>
					<tr>
						<th>N°</th>
						<th>Codigo de Aviso</th>
						<th>Nombre de Aviso</th>
						<th>Prints</th>
						<th>Clicks</th>
						<th>Fecha Alta</th>
						<th>Detalles</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($ads_list->result() as $ads_lst): ?>
					<?php
						// Prepare ADS Info Collections
						// Click Collection
						$click_counted = countAllClicsByAdsCode($ads_lst->mkt_ads_adscode);
						$click_initial = $ads_lst->mkt_ads_clickpointer;
						$click_percent = ($click_counted * 100) / $click_initial;
						// Print Collection
						$print_counted = countAllPrintsByAdsCode($ads_lst->mkt_ads_adscode);
						$print_initial = $ads_lst->mkt_ads_print_initial;
						$print_percent = ($print_counted * 100) / $print_initial;
					?>
						<tr>
							<td><?php echo $ads_lst->mkt_ads_id;?></td>
							<td><?php echo strtoupper($ads_lst->mkt_ads_adscode);?></td>
							<td><?php echo $ads_lst->mkt_ads_title;?></td>
							<td>
								<div class="progress-horizontal">
								  <div class="progress-horizontal__bar" data-value="<?php echo $print_percent;?>"></div>
								  <span class="progress-horizontal__label h5"><?php echo $print_counted;?>/<?php echo $print_initial;?></span>
								</div>
							</td>
							<td>
								<div class="progress-horizontal">
								  <div class="progress-horizontal__bar" data-value="<?php echo $click_percent;?>"></div>
								  <span class="progress-horizontal__label h5"><?php echo $click_counted;?>/<?php echo $click_initial;?></span>
								</div>
							</td>
							<td><?php echo date('d/m/Y', strtotime($ads_lst->mkt_ads_date));?></td>
							<td style="text-align:center;">
								<a href="<?php echo base_url() . 'publicist/stats/' . $ads_lst->mkt_ads_adscode;?>">
									<i class="material-icons">search</i>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
    </div>
</div>
