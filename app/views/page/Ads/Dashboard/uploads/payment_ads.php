<div id="pathData" path="<?php echo base_url();?>"></div>
<div class="col-md-12" style="padding-left: 15px; margin-top:15px;">
    <div class="row">
      <input type="hidden" name="actual_credit_balance" id="actual_credit_balance" value="<?php echo $credit_qty;?>" readonly>
      <div class="col-m-12" style="padding-left: 15px;">
        <div class="row">
          <div class="col-md-4">
            <div class="row ads_module">
              <div class="col-md-12"><h3 class="text-center">Exposicion Minima</h3></div>
              <?php foreach ($ads_nfo->result() as $ads): ?>
                <div id="plan_type_1" class="col-md-12 ads_op" data-adsCode="<?php echo $ads->mkt_ads_adscode;?>" style="font-size: 0.9em;">
                  <a href="#" target="_blank">
                    <span class="title_container"><?php echo $ads->mkt_ads_title;?></span>
                    <span class="text_url"><?php echo substr($ads->mkt_ads_url, 0, 20);?></span>
                    <span class="text_title"><?php echo $ads->mkt_ads_desc;?></span>
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row ads_module">
              <div class="col-md-12"><h3 class="text-center">Exposicion Media</h3></div>
              <?php foreach ($ads_nfo->result() as $ads): ?>
                <div id="plan_type_2" class="col-md-12 ads_op" data-adsCode="<?php echo $ads->mkt_ads_adscode;?>" style="font-size: 0.9em;">
                  <a href="#" target="_blank">
                    <?php if ($ads->mkt_ads_image != NULL): ?>
                      <span class="image_container">
                        <img src="<?php echo $ads->mkt_ads_image;?>" alt="<?php echo $ads->mkt_ads_title;?>">
                      </span>
                    <?php endif; ?>
                    <span class="title_container"><?php echo $ads->mkt_ads_title;?></span>
                    <span class="text_url"><?php echo substr($ads->mkt_ads_url, 0, 20);?></span>
                    <span class="text_title"><?php echo $ads->mkt_ads_desc;?></span>
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row ads_module">
              <div class="col-md-12"><h3 class="text-center">Exposicion Alta</h3></div>
              <?php foreach ($ads_nfo->result() as $ads): ?>
                <div id="plan_type_3" class="col-md-12 ads_op high_class" data-adsCode="<?php echo $ads->mkt_ads_adscode;?>">
                  <a href="#" target="_blank">
                    <?php if ($ads->mkt_ads_image != NULL): ?>
                      <span class="image_container">
                        <img src="<?php echo $ads->mkt_ads_image;?>" alt="<?php echo $ads->mkt_ads_title;?>">
                      </span>
                    <?php endif; ?>
                    <span class="title_container"><?php echo $ads->mkt_ads_title;?></span>
                    <span class="text_url"><?php echo substr($ads->mkt_ads_url, 0, 20);?></span>
                    <span class="text_title"><?php echo $ads->mkt_ads_desc;?></span>
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6" style="padding-left: 15px;">
            <div class="row">
              <div class="col-m-12" style="padding-left: 15px; margin-bottom:35px;">
                <h4>Plan</h4>
                <p>
                  Selecciona el plan de publicacion del aviso.
                  <br>
                  Luego de finalizado, deberas crear un nuevo aviso.
                </p>
              </div>
              <div class="col-md-12">
                <label>Plan: <span id="plan_val_marker"></span></label>
                <input id="range-plan" class="range-plan" type="range" name="range-plan" min="1" max="4" step="1" value="1" />
                <input type="hidden" id="plan_val" name="plan_val" value="1" readonly placeholder="1">
                <ul>
                  <?php foreach ($payment_methods->result() as $plan): ?>
                    <li><?php echo $plan->mkt_ads_plans_title;?> = Creditos Requeridos: <?php echo $plan->mkt_ads_plans_qty_cost;?></li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6" style="padding-left: 15px;">
            <div class="row">
              <div class="col-m-12" style="padding-left: 15px; margin-bottom:35px;">
                <h4>Alcance</h4>
                <p>
                  Selecciona el alcance de publicacion del aviso.
                  <br>
                  Luego de finalizado, deberas crear un nuevo aviso.
                </p>
              </div>
              <div class="col-md-12">
                <label>Alcance de Exposicion: <span id="exposition_val_marker"></span></label>
                <input id="range-exposition" class="range-exposition" type="range" name="range-exposition" min="1" max="3" step="1" value="1" />
                <input type="hidden" id="exposition_val" name="exposition_val" value="Minima" readonly placeholder="Minima">
                <ul>
                  <li>Minima: x 1 Credito.</li>
                  <li>Media: x 2 Creditos.</li>
                  <li>Alta: x 3 Creditos.</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12" style="margin-top:35px;">
        <?php echo form_open("ads/publication/pay_process");?>
          <?php foreach ($ads_nfo->result() as $ads): ?>
            <input type="hidden" name="mkt_ads_adscode" value="<?php echo $ads->mkt_ads_adscode;?>">
          <?php endforeach; ?>
          <input type="hidden" id="final_val_form_clics" name="final_val_form_clics" value="" readonly>
          <input type="hidden" id="final_val_form_prints" name="final_val_form_prints" value="" readonly>
          <input type="hidden" id="final_val_form_exposition" name="final_val_form_exposition" value="" readonly>
          <input type="hidden" id="final_token_cost" name="final_token_cost" value="" readonly>
          <table class="border--round">
            <thead>
              <tr>
                <th>Prints</th>
                <th>Clics</th>
                <th>Exposicion</th>
                <th>Costo en Creditos</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><span id="print_counter"></span></td>
                <td><span id="clic_counter"></span></td>
                <td><span id="plan_visualizer"></span></td>
                <td><span id="credit_cost"></span> Creditos</td>
                <td><button id="activate_publication" type="submit" class="btn btn--primary" disabled>Activar</button></td>
              </tr>
            </tbody>
          </table>
        <?php echo form_close();?>
        <div id="alert_credit_enough" class="alert bg--success">
          <div class="alert__body">
            <span>Actualmente dispones de <span class="actual_credit_show"></span> Creditos, una vez finalizada la operacion tendras <span id="result_credits_for_operation"></span> creditos.</span>
          </div>
        </div>
        <div id="alert_credit_not_enough" class="alert bg--error">
          <div class="alert__body">
            <span>Actualmente dispones de <span class="actual_credit_show"></span> Creditos, no puedes pagar el costo de la actual operacion, necesitas <span id="needed_credits_for_operation"></span> creditos más, <a href="<?php echo base_url();?>user/buy_credits">compra un plan</a> ahora para seguir operando.</span>
          </div>
        </div>
      </div>
    </div>
</div>
<script>
  function calculateCreditCostsarea() {
    // Calc Credits per Month
    var credit_cost_by_months;
    var slider_val_duration = $('#range-duration').val();
    credit_cost_by_months = slider_val_duration;
    if (slider_val_duration == 6) {
      credit_cost_by_months = slider_val_duration - 1;
    }else if (slider_val_duration == 12) {
      credit_cost_by_months = slider_val_duration - 2;
    }else if (slider_val_duration == 24) {
      credit_cost_by_months = slider_val_duration - 4;
    }
    // Calc Credits per Exposition
    var slider_val_exposition = $('#range-exposition').val();
    if (slider_val_exposition == '1') {
      var expo_amplification = 1;
    }else if (slider_val_exposition == '2') {
      var expo_amplification = 5;
    }else if (slider_val_exposition == '3') {
      var expo_amplification = 10;
    }else if (slider_val_exposition == '4') {
      var expo_amplification = 20;
    }
    credit_cost_by_months = credit_cost_by_months * expo_amplification;
    $('#credit_cost').text(credit_cost_by_months);
    // Check Credit Validation
    var credit_amount_total = $('#actual_credit_balance').val();
    $('.actual_credit_show').text(credit_amount_total);
    // Date for Publication
    var actualDate = new Date();
    var variationDate = slider_val_duration * 30;
    var expirationDate = actualDate.setDate(actualDate.getDate() + variationDate);
    // Formatting Date
    var expirationDateFormatted = new Date(expirationDate);
    var day = (expirationDateFormatted.getDate() < 10 ? '0' : '') + expirationDateFormatted.getDate();
    var month = (expirationDateFormatted.getMonth() < 9 ? '0' : '') + (expirationDateFormatted.getMonth() + 1);
    var year = expirationDateFormatted.getFullYear();
    var finalExpirationDateFormatted = day + '/' + month + '/' + year;
    $('#expirationDate_formatted').text(finalExpirationDateFormatted);
    // Check User Credits
    if (credit_amount_total >= credit_cost_by_months) {
      var result_credits = credit_amount_total - credit_cost_by_months;
      $('#activate_publication').prop('disabled', false);
      $('#alert_credit_not_enough').hide();
      $('#alert_credit_enough').show();
      $('#result_credits_for_operation').text(result_credits);
    }else{
      var needed_credits = credit_cost_by_months - credit_amount_total;
      $('#activate_publication').prop('disabled', true);
      $('#alert_credit_not_enough').show();
      $('#alert_credit_enough').hide();
      $('#needed_credits_for_operation').text(needed_credits);
    }
  }
  function calculateCreditCost() {
    var slider_val_plan = $('#range-plan').val();
    var slider_val_exposition = $('#range-exposition').val();
    // Get Information From Plan
    var datapath = $('#pathData').attr('path') + 'Ads/getPlansInformation/';
    $.ajax({
      url: datapath,
      type: 'GET',
      success:function(res){
        var res = JSON.parse(res);
        console.log(res);
        for (var i = 0; i < res.length; i++) {
          if (res[i].mkt_ads_plans_id == slider_val_plan) {
            var credit_cost_total = res[i].mkt_ads_plans_qty_cost * slider_val_exposition;
            $('#print_counter').text(res[i].mkt_ads_plans_prints);
            $('#clic_counter').text(res[i].mkt_ads_plans_clics);
            $('#plan_visualizer').text(res[i].mkt_ads_plans_title);
            $('#credit_cost').text(credit_cost_total);
            // Check Credit Validation
            var credit_amount_total = $('#actual_credit_balance').val();
            $('.actual_credit_show').text(credit_amount_total);
            // Check User Credits
            if (credit_amount_total >= credit_cost_total) {
              var result_credits = credit_amount_total - credit_cost_total;
              $('#activate_publication').prop('disabled', false);
              $('#alert_credit_not_enough').hide();
              $('#alert_credit_enough').show();
              $('#result_credits_for_operation').text(result_credits);
            }else{
              var needed_credits = credit_cost_total - credit_amount_total;
              $('#activate_publication').prop('disabled', true);
              $('#alert_credit_not_enough').show();
              $('#alert_credit_enough').hide();
              $('#needed_credits_for_operation').text(needed_credits);
            }
            // Set Values for Submit Form
            $('#final_val_form_clics').val(res[i].mkt_ads_plans_clics);
            $('#final_val_form_prints').val(res[i].mkt_ads_plans_prints);
            $('#final_val_form_exposition').val(slider_val_exposition);
            $('#final_token_cost').val(credit_cost_total);
          }
        }
      }
    })
  }
  function getRangeExposition() {
    var slider_val_exposition = $('#range-exposition').val();
    if (slider_val_exposition == '1') {
      var expo_txt = 'Minima';
    }else if (slider_val_exposition == '2') {
      var expo_txt = 'Media';
    }else if (slider_val_exposition == '3') {
      var expo_txt = 'Alta';
    }
    $('#exposition_val').val(expo_txt);
    $('#exposition_val_marker').text(expo_txt);
    // Set Exposition Value
    var datapath = $('#pathData').attr('path') + 'Ads/getPlansInformation/';
    $.ajax({
      url: datapath,
      type: 'GET',
      success:function(res){
        var res = JSON.parse(res);
        console.log(res);
        for (var i = 0; i < res.length; i++) {
          if (res[i].mkt_ads_plans_id == slider_val_exposition) {
            $('.ads_op').css('opacity','0.4');
            $('#plan_type_' + slider_val_exposition).css('opacity','1.0');
          }
        }
      }
    })
    $('#expositionVal').text(expo_txt);
  }
  $('#range-exposition').change(function () {
    getRangeExposition();
    calculateCreditCost();
  });
  function getRangeDuration() {
    var slider_val_plan = $('#range-plan').val();
    var datapath = $('#pathData').attr('path') + 'Ads/getPlansInformation/';
    $.ajax({
      url: datapath,
      type: 'GET',
      success:function(res){
        var res = JSON.parse(res);
        console.log(res);
        for (var i = 0; i < res.length; i++) {
          if (res[i].mkt_ads_plans_id == slider_val_plan) {
            $('#plan_val_marker').text(res[i].mkt_ads_plans_title);
          }
        }
      }
    })
  }
  $('#range-plan').change(function () {
    getRangeDuration();
    calculateCreditCost();
  });
  $(document).ready(function() {
    getRangeExposition();
    getRangeDuration();
    calculateCreditCost();
    getAllPlansByInJson();
  });
</script>
