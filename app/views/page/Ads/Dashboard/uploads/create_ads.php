<span id="pathData" path="<?php echo base_url();?>" style="display:none;"></span>

<div class="col-md-12" style="margin-bottom:35px;">
  <div class="row">
    <div class="col-md-12">
      <div class="row">

        <div class="col-md-12">
          <?php echo form_open("Ads/create_new_product", 'class="row"');?>
            <div class="col-md-12">
              <h4>Publicar Nuevo Aviso</h4>
            </div>
            <div class="col-md-6">
              <label>Titulo</label>
              <input type="text" name="ads_title" value="">
            </div>
            <div class="col-md-6">
              <label>Url Referenciada</label>
              <input type="text" name="ads_url" value="">
            </div>
            <div class="col-md-12">
              <label>Cuerpo de Texto</label>
              <input type="text" name="ads_text" value="">
            </div>
            <div class="col-md-12">
              <hr>
            </div>
            <div class="col-md-12">
              <div class="row" style="margin-top:35px;">
                <div class="col-md-4">
                  <button type="submit" class="btn btn--primary">Guardar Aviso</button>
                </div>
              </div>
            </div>
          <?php echo form_close();?>
        </div>


      </div>
    </div>
  </div>
</div>
