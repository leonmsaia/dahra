<?php $generic_image = base_url() . 'assets/img/system/generic_image.jpg';?>
<div class="col-md-12" style="padding-left: 15px;">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <h4>Fotografia de Aviso</h4>
            </div>
            <?php foreach ($new_ads_created->result() as $img): ?>
              <div class="col-md-3">
                <?php echo form_open("Upload/uploadAdsToS3", 'enctype="multipart/form-data"');?>
                  <input type="file" name="userfile" id="prod_pic_one" value="" style="display:none;">
                  <input type="hidden" name="product_code" value="<?php echo $ads_code;?>">
                  <input type="hidden" name="image_name" value="ads_picture">
                  <input type="hidden" name="redirect" value="ads/publication/create_ads_second_step/<?php echo $ads_code;?>">
                  <?php if ($img->mkt_ads_image == NULL): ?>
                    <img id="prod_pic_one_trigger"src="<?php echo $generic_image;?>" alt="prod_image" class="img-thumbnail">
                  <?php else: ?>
                    <img id="prod_pic_one_trigger"src="<?php echo $img->mkt_ads_image;?>" alt="prod_image" class="img-thumbnail">
                  <?php endif; ?>
                  <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
                <?php echo form_close();?>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <br>
  <hr>
</div>
<div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <br>
    </div>
    <div class="col-md-4">
      <?php echo form_open("Ads/process_step_two_to_payment");?>
        <input type="hidden" name="ads_code" value="<?php echo $ads_code;?>" readonly>
        <button type="submit" class="btn btn--primary">Ver Medios de Pago</button>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script>
  // Images Trigger
  $('#prod_pic_one_trigger').click(function() {
    $('#prod_pic_one').trigger('click');
  });
</script>
