<?php foreach ($new_ads_created->result() as $ads): ?>
  <div class="col-md-12" style="margin-bottom:35px;">
    <div class="row">
      <div class="col-md-12">
        <div class="row">

          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12">
                <h4>Editar Cuerpo de Aviso</h4>
              </div>
              <div class="col-md-12">
                <?php echo form_open("ads/publication/edit_basic_ads", 'class="row"');?>
                <input type="hidden" name="mkt_ads_adscode" value="<?php echo $ads->mkt_ads_adscode;?>" readonly>
                  <div class="col-md-12">
                    <label>Titulo</label>
                    <input type="text" name="ads_title" value="<?php echo $ads->mkt_ads_title;?>">
                  </div>
                  <div class="col-md-12">
                    <label>Url Referenciada</label>
                    <input type="text" name="ads_url" value="<?php echo $ads->mkt_ads_url;?>">
                  </div>
                  <div class="col-md-12">
                    <label>Cuerpo de Texto</label>
                    <input type="text" name="ads_text" value="<?php echo $ads->mkt_ads_desc;?>">
                  </div>
                  <div class="col-md-12">
                    <hr>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <button type="submit" class="btn btn--primary">Guardar Aviso</button>
                      </div>
                    </div>
                  </div>
                <?php echo form_close();?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12">
                <h4>Editar Imagen de Aviso</h4>
              </div>
              <?php foreach ($new_ads_created->result() as $img): ?>
                <div class="col-md-6">
                  <?php echo form_open("Upload/uploadAdsToS3", 'enctype="multipart/form-data"');?>
                  <input type="file" name="userfile" id="prod_pic_one" value="" style="display:none;">
                  <input type="hidden" name="product_code" value="<?php echo $ads_code;?>">
                  <input type="hidden" name="image_name" value="ads_picture">
                  <input type="hidden" name="redirect" value="ads/publication/edit_ads/<?php echo $ads_code;?>">
                  <?php if ($img->mkt_ads_image == NULL): ?>
                    <img id="prod_pic_one_trigger"src="<?php echo $generic_image;?>" alt="prod_image" class="img-thumbnail">
                  <?php else: ?>
                    <img id="prod_pic_one_trigger"src="<?php echo $img->mkt_ads_image;?>" alt="prod_image" class="img-thumbnail">
                  <?php endif; ?>
                  <input class="btn btn--primary btn--sm" type="submit" name="" value="Guardar Fotografia">
                  <?php echo form_close();?>
                </div>
              <?php endforeach; ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
<script>
  // Images Trigger
  $('#prod_pic_one_trigger').click(function() {
    $('#prod_pic_one').trigger('click');
  });
</script>
