<ul>
  <li>
    <h5>Creditos: <?php echo $credit_qty;?></h5>
    <a href="<?php echo base_url();?>publicist/buy_credits">Comprar Planes</a>
  </li>
  <li><br></li>
  <li><h5>Clicks</h5></li>
  <li>
    Totales: <?php echo $total_clic_count;?>.
  </li>
  <li>
    Ultimo Mes: <?php echo $total_clic_count_actual_month;?>.
  </li>
  <li><h5>Impresiones</h5></li>
  <li>
    Totales: <?php echo $total_print_count;?>.
  </li>
  <li>
    Ultimo Mes: <?php echo $total_print_count_actual_month;?>.
  </li>
  <li><br></li>
  <li><h5>Avisos</h5></li>
  <li>
    <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'publicist/ads';?>">
      <span class="btn__text">
        <i><?php echo $ads_active;?></i> Activos
      </span>
    </a>
    <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'publicist/ads';?>">
      <span class="btn__text">
        <i><?php echo $ads_paused;?></i> Pausados
      </span>
    </a>
    <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'publicist/ads';?>">
      <span class="btn__text">
        <i><?php echo $ads_ended;?></i> Finalizados
      </span>
    </a>
    <a class="btn btn--primary btn--icon" href="<?php echo base_url() . 'publicist/ads';?>">
      <span class="btn__text">
        <i><?php echo $ads_not_actived;?></i> Sin Activar
      </span>
    </a>
  </li>
</ul>
