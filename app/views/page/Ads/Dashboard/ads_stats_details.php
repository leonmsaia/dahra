<?php foreach ($ads_info->result() as $ads_nfo): ?>
<?php
	// Prepare ADS Info Collections
	// Click Collection
	$click_counted = countAllClicsByAdsCode($ads_nfo->mkt_ads_adscode);
	$click_initial = $ads_nfo->mkt_ads_clickpointer;
	$click_percent = ($click_counted * 100) / $click_initial;
	// Print Collection
	$print_counted = countAllPrintsByAdsCode($ads_nfo->mkt_ads_adscode);
	$print_initial = $ads_nfo->mkt_ads_print_initial;
	$print_percent = ($print_counted * 100) / $print_initial;
?>
<div class="row">
    <div class="col-md-12">
			<h4><?php echo $ads_nfo->mkt_ads_title;?></h4>
			<h4>Codigo de Aviso: <?php echo strtoupper($ads_nfo->mkt_ads_adscode);?></h4>
    </div>
		<div class="col-md-6" style="margin-bottom:15px;">
			<h5>Prints Restantes</h5>
			<div class="chart_wrapp" id="prints_burned_pie"></div>
			<div class="progress-horizontal" style="text-align:center;">
			  <div class="progress-horizontal__bar" data-value="<?php echo $print_percent;?>"></div>
			  <span class="progress-horizontal__label h5">Uso al <?php echo $print_percent;?>%</span>
			</div>
		</div>
		<div class="col-md-6">
			<h5>Clics Restantes</h5>
			<div class="chart_wrapp" id="clics_burned_pie"></div>
			<div class="progress-horizontal" style="text-align:center;">
			  <div class="progress-horizontal__bar" data-value="<?php echo $click_percent;?>"></div>
			  <span class="progress-horizontal__label h5">Uso al <?php echo $click_percent;?>%</span>
			</div>
		</div>
		<div class="col-md-12">
			<h5>N° de Prints Historico</h5>
			<div class="chart_wrapp" id="historical_print_graph"></div>
		</div>
		<div class="col-md-12">
			<h5>N° de Clics Historico</h5>
			<div class="chart_wrapp" id="historical_clic_graph"></div>
		</div>
</div>
<?php $this->load->view('layout/assets/graph_scripts');?>
<script>
var prints_pie_graph = <?php echo $info_pie_graph_prints;?>;
var chart = AmCharts.makeChart("prints_burned_pie", {
	"type": "pie",
	"theme": "light",
	"language": "es",
	"dataProvider": prints_pie_graph,
	"valueField": "value",
	"titleField": "title",
	"startDuration": 0,
	"innerRadius": 80,
	"titleField": "title",
	"labelsEnabled": false,
  "autoMargins": false,
  "marginTop": 0,
  "marginBottom": 0,
  "marginLeft": 0,
  "marginRight": 0,
  "pullOutRadius": 0,
	"legend": {
    "useGraphSettings": false
  },
});

var clics_pie_graph = <?php echo $info_pie_graph_clics;?>;
var chart = AmCharts.makeChart("clics_burned_pie", {
	"type": "pie",
	"theme": "light",
	"language": "es",
	"dataProvider": clics_pie_graph,
	"valueField": "value",
	"titleField": "title",
	"startDuration": 0,
	"innerRadius": 80,
	"titleField": "title",
	"labelsEnabled": false,
  "autoMargins": false,
  "marginTop": 0,
  "marginBottom": 0,
  "marginLeft": 0,
  "marginRight": 0,
  "pullOutRadius": 0,
	"legend": {
    "useGraphSettings": false
  },
});

var historicalclic = <?php echo $info_stats_historical_clics;?>;
var chart = AmCharts.makeChart("historical_clic_graph", {
		"type": "serial",
		"theme": "light",
		"language": "es",
		"marginTop":0,
		"marginRight": 80,
		"dataProvider": historicalclic,
		"valueAxes": [{
				"axisAlpha": 0,
				"position": "left"
		}],
		"graphs": [{
				"id":"g1",
				"balloonText": "[[category]]<br><b>Visitas: <span style='font-size:14px;'>[[value]]</span></b>",
				"bullet": "round",
				"bulletSize": 8,
				"lineColor": "#d1655d",
				"lineThickness": 2,
				"negativeLineColor": "#637bb6",
				"type": "smoothedLine",
				"valueField": "value"
		}],
		"chartScrollbar": {
			"graph":"g1",
			"gridAlpha":0,
			"color":"#888888",
			"scrollbarHeight":55,
			"backgroundAlpha":0,
			"selectedBackgroundAlpha":0.1,
			"selectedBackgroundColor":"#888888",
			"graphFillAlpha":0,
			"autoGridCount":true,
			"selectedGraphFillAlpha":0,
			"graphLineAlpha":0.2,
			"graphLineColor":"#c2c2c2",
			"selectedGraphLineColor":"#888888",
			"selectedGraphLineAlpha":1
		},
		"chartCursor": {
				"categoryBalloonDateFormat": "DD/MM/YYYY",
				"cursorAlpha": 0,
				"valueLineEnabled":true,
				"valueLineBalloonEnabled":true,
				"valueLineAlpha":0.5,
				"fullWidth":true
		},
		"dataDateFormat": "YYYY-MM-DD NN-SS-QQ",
		"categoryField": "date",
		"categoryAxis": {
				"minPeriod": "DD",
				"parseDates": true,
				"minorGridAlpha": 0.1,
				"minorGridEnabled": true
		}
});

var historicalprints = <?php echo $info_stats_historical_prints;?>;
var chart = AmCharts.makeChart("historical_print_graph", {
		"type": "serial",
		"theme": "light",
		"language": "es",
		"marginTop":0,
		"marginRight": 80,
		"dataProvider": historicalprints,
		"valueAxes": [{
				"axisAlpha": 0,
				"position": "left"
		}],
		"graphs": [{
				"id":"g1",
				"balloonText": "[[category]]<br><b>Visitas: <span style='font-size:14px;'>[[value]]</span></b>",
				"bullet": "round",
				"bulletSize": 8,
				"lineColor": "#d1655d",
				"lineThickness": 2,
				"negativeLineColor": "#637bb6",
				"type": "smoothedLine",
				"valueField": "value"
		}],
		"chartScrollbar": {
			"graph":"g1",
			"gridAlpha":0,
			"color":"#888888",
			"scrollbarHeight":55,
			"backgroundAlpha":0,
			"selectedBackgroundAlpha":0.1,
			"selectedBackgroundColor":"#888888",
			"graphFillAlpha":0,
			"autoGridCount":true,
			"selectedGraphFillAlpha":0,
			"graphLineAlpha":0.2,
			"graphLineColor":"#c2c2c2",
			"selectedGraphLineColor":"#888888",
			"selectedGraphLineAlpha":1
		},
		"chartCursor": {
				"categoryBalloonDateFormat": "DD/MM/YYYY",
				"cursorAlpha": 0,
				"valueLineEnabled":true,
				"valueLineBalloonEnabled":true,
				"valueLineAlpha":0.5,
				"fullWidth":true
		},
		"dataDateFormat": "YYYY-MM-DD NN-SS-QQ",
		"categoryField": "date",
		"categoryAxis": {
				"minPeriod": "DD",
				"parseDates": true,
				"minorGridAlpha": 0.1,
				"minorGridEnabled": true
		}
});

</script>
<?php endforeach; ?>
