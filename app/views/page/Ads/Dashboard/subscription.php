<div class="container">
	<div class="row">
	  <div class="col-md-12" style="margin-bottom:35px;">
			<h3>Creditos Actuales: <?php echo $credit_qty;?></h3>
	  	<h4>Suma Creditos hoy</h4>
			<p>Aprovecha nuestras promociones y descuentos comprando creditos, y comenza a publicar hoy mismo.</p>
			<a href="<?php echo base_url();?>publicist/buy_credits">Ver Planes</a>
	  </div>
	</div>
</div>

<div class="container">
	<div class="row">
	  <div class="col-md-12">
			<table class="border--round">
				<thead>
					<tr>
						<th>N° Fac.</th>
			      <th>Cod. Op.</th>
						<th>Fecha</th>
			      <th>Creditos</th>
			      <th>Operacion</th>
						<th>Importe</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
			    <?php foreach ($credit_balance->result() as $sts): ?>
			      <?php if ($sts->mrk_credit_operation_type == 1): ?>
			        <tr class="alert bg--success">
			      <?php elseif($sts->mrk_credit_operation_type == 2): ?>
			        <tr class="alert bg--error">
			      <?php endif; ?>
			          <td><?php echo $sts->mrk_credit_operation_id;?></td>
			    			<td><?php echo $sts->mrk_credit_operation_operation_id;?></td>
			    			<td><?php echo $sts->mrk_credit_operation_date;?></td>
			          <td><?php echo $sts->mrk_credit_operation_qty;?> Creditos</td>
			          <td>
			            <?php if ($sts->mrk_credit_operation_type == 1): ?>
			              Credito
			            <?php elseif($sts->mrk_credit_operation_type == 2): ?>
			              Debito
			            <?php endif; ?>
			          </td>
			    			<td>$530.20</td>
			    			<td>
			            <a href="#">
			              <i class="icon-File"></i> Ver
			            </a>
			          </td>
			  		</tr>
			    <?php endforeach; ?>
				</tbody>
			</table>
			<a href="<?php echo base_url();?>publicist/subscription/history">Ver Historial</a>
		</div>
</div>
