<div class="sidebar__widget">
    <h5>Panel de Control</h5>
    <br>
    <ul class="menu-vertical">
      <li>
        <a href="<?php echo base_url();?>publicist/dashboard">
          Resumen
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>publicist/subscription">
          Creditos
        </a>
      </li>
      <li>
        <br>
      </li>
      <li>
        <a href="<?php echo base_url();?>publicist/ads">
          Avisos
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>publicist/stats">
          Estadisticas
        </a>
      </li>
      <li>
        <br>
      </li>
      <li>
        <a href="<?php echo base_url();?>publicist/configuration/personal_data">
          Datos Personales
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>publicist/configuration/security_data">
          Seguridad
        </a>
      </li>
    </ul>
</div>
