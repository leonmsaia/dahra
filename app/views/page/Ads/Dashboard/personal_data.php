<?php foreach ($user_information->result() as $usr_nfo): ?>
  <span id="pathData" path="<?php echo base_url();?>" style="display:none;"></span>
  <?php echo form_open("Dashboard/save_Personal_Data");?>
    <div class="row">
        <div class="col-md-12">
          <h5>Informacion Basica</h5>
        </div>
        <div class="col-md-6">
            <input type="text" name="first_name" id="first_name" placeholder="Nombre" value="<?php echo $usr_nfo->first_name;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="last_name" id="last_name" placeholder="Apellido" value="<?php echo $usr_nfo->last_name;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="dni" id="dni" placeholder="DNI" value="<?php echo $usr_nfo->dni;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="cuit" id="cuit" placeholder="CUIL/CUIT" value="<?php echo $usr_nfo->cuit;?>"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <br>
        </div>
        <div class="col-md-12">
          <h5>Informacion de Contacto</h5>
        </div>
        <div class="col-md-6">
            <input type="email" name="artist_mail" id="artist_mail" placeholder="E-Mail" value="<?php echo $usr_nfo->email;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="artist_phone" id="artist_phone" placeholder="Telefono" value="<?php echo $usr_nfo->phone;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="artist_phone" id="artist_phone" placeholder="Direccion" value="<?php echo $usr_nfo->adress;?>"/>
        </div>
        <div class="col-md-6">
            <input type="text" name="artist_phone" id="artist_phone" placeholder="Zipcode" value="<?php echo $usr_nfo->zipcode;?>"/>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <br>
      </div>
      <div class="col-md-4">
        <button type="submit" class="btn btn--primary type--uppercase">Guardar</button>
      </div>
    </div>
  <?php echo form_close();?>
<?php endforeach; ?>
