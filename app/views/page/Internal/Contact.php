<section class="text-center height-50">
    <div class="container pos-vertical-center">
        <div class="row">
            <div class="col-md-8 col-lg-6">
                <h1>Contactate con Nostros</h1>
                <p class="lead">
                  TExto de Contacto. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="unpad ">
    <div class="map-container border--round" data-maps-api-key="AIzaSyCfo_V3gmpPm1WzJEC9p_sRbgvyVbiO83M" data-address="123 Rathdowne street, Carlton Victoria" data-marker-title="Stack" data-map-style="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FFBB00&quot;},{&quot;saturation&quot;:43.400000000000006},{&quot;lightness&quot;:37.599999999999994},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FFC200&quot;},{&quot;saturation&quot;:-61.8},{&quot;lightness&quot;:45.599999999999994},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FF0300&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:51.19999999999999},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FF0300&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:52},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#0078FF&quot;},{&quot;saturation&quot;:-13.200000000000003},{&quot;lightness&quot;:2.4000000000000057},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#00FF6A&quot;},{&quot;saturation&quot;:-1.0989010989011234},{&quot;lightness&quot;:11.200000000000017},{&quot;gamma&quot;:1}]}]" data-map-zoom="15"></div>
</section>


<section class="switchable ">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-5">
                <p class="lead">
                    Consultas a:
                    <a href="#">info@dahra.com</a>
                    <br /> Telefono: 1123747372
                </p>
                <p class="lead">
                    Horarios de Atencion L. a V. de 9:00 a 18:00
                </p>
                <p class="lead">
                  Lineas Rotativas
                </p>
            </div>
            <div class="col-md-6 col-12">
                <form class="form-email row" data-success="Thanks for your enquiry, we'll be in touch shortly." data-error="Please fill in all fields correctly." data-recaptcha-sitekey="6LewhCIUAAAAACSwFvBDhgtTbw6EnW6e9dip8o2u" data-recaptcha-theme="light">
                    <div class="col-md-6 col-12">
                        <label>Tu Nombre:</label>
                        <input type="text" name="Name" class="validate-required" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label>Tu Email:</label>
                        <input type="email" name="email" class="validate-required validate-email" />
                    </div>
                    <div class="col-md-12 col-12">
                        <label>Mensaje:</label>
                        <textarea rows="4" name="Message" class="validate-required"></textarea>
                    </div>
                    <div class="col-md-5 col-lg-4 col-6">
                        <button type="submit" class="btn btn--primary type--uppercase">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
