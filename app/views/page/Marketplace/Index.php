<section class="space--sm">
    <?php $this->load->view('layout/components/breadcrum');?>
    <div class="container">
        <div class="row main_home_block">
            <?php $this->load->view('layout/components/sidebar');?>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12">
                  <section class="sidebar boxed boxed--lg" style="padding-top: 0;">
                    <div class="container">
                      <div class="row">
                        <?php foreach ($products_list->result() as $filtered): ?>
                          <div class="col-md-4 prod-listing" data-ProdId="<?php echo $filtered->product_id;?>" data-ProdPrice="<?php echo $filtered->product_price;?>" data-ProdExposition="<?php echo $filtered->product_exposition;?>">
                            <div class="product">
                              <a href="<?php echo base_url() . 'marketplace/product/' . $filtered->product_slug;?>">
                                <img alt="Image" src="<?php echo $filtered->product_img;?>">
                              </a>
                              <a class="block" href="<?php echo base_url() . 'marketplace/product/' . $filtered->product_slug;?>">
                                <div>
                                  <h5><?php echo $filtered->product_name;?></h5>
                                  <br>
                                  <span><?php echo $filtered->product_desc;?></span>
                                </div>
                                <div>
                                  <span class="h4 inline-block">$<?php echo $filtered->product_price;?></span>
                                </div>
                              </a>
                            </div>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    </div>
                  </section>
                </div>
                <div class="col-md-12">
                  <div id="share"></div>
                </div>
              </div>
            </div>
            <?php $this->load->view('layout/components/vertical_ads_module');?>
        </div>
    </div>
</section>
