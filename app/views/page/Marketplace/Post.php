<section class="space--xs">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-md-12">
              <h2>Publicar</h2>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4>Fotografias</h4>
                </div>
                <div class="col-md-12">
                  <?php echo form_open("User/save_profile_data", 'class="row"');?>
                    <div class="col-md-3">
                      <input type="file" name="" id="prod_pic_one" value="" style="display:none;">
                      <img class="img-thumbnail" id="prod_pic_one_trigger" src="http://localhost/dahra/assets/uploads/products/80_ims.png" alt="">
                      <button class="btn" style="width:100%;">
                      	<span class="btn__text">Eliminar</span>
                      </button>
                    </div>
                    <div class="col-md-3">
                      <input type="file" name="" id="prod_pic_two" value="" style="display:none;">
                      <img class="img-thumbnail" id="prod_pic_two_trigger" src="http://localhost/dahra/assets/uploads/products/80_ims.png" alt="">
                      <button class="btn" style="width:100%;">
                      	<span class="btn__text">Eliminar</span>
                      </button>
                    </div>
                    <div class="col-md-3">
                      <input type="file" name="" id="prod_pic_three" value="" style="display:none;">
                      <img class="img-thumbnail" id="prod_pic_three_trigger" src="http://localhost/dahra/assets/uploads/products/80_ims.png" alt="">
                      <button class="btn" style="width:100%;">
                      	<span class="btn__text">Eliminar</span>
                      </button>
                    </div>
                    <div class="col-md-3">
                      <input type="file" name="" id="prod_pic_four" value="" style="display:none;">
                      <img class="img-thumbnail" id="prod_pic_four_trigger" src="http://localhost/dahra/assets/uploads/products/80_ims.png" alt="">
                      <button class="btn" style="width:100%;">
                      	<span class="btn__text">Eliminar</span>
                      </button>
                    </div>
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-3">
                          <button type="submit" class="btn btn--primary type--uppercase">Guardar Imagenes</button>
                        </div>
                      </div>
                    </div>
                  <?php echo form_close();?>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4>Fotografias</h4>
                </div>
                <div class="col-md-12">
                  <?php echo form_open("User/save_profile_data", 'class="row"');?>
                    <div class="col-md-3">
                      <input type="file" name="" id="prod_pic_one" value="" style="display:none;">
                      <img class="img-thumbnail" id="prod_pic_one_trigger" src="http://localhost/dahra/assets/uploads/products/80_ims.png" alt="">
                      <button class="btn" style="width:100%;">
                      	<span class="btn__text">Eliminar</span>
                      </button>
                    </div>
                  <?php echo form_close();?>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>


<script>
  // Images Trigger
  $('#prod_pic_one_trigger').click(function() {
    $('#prod_pic_one').trigger('click');
  });
  $('#prod_pic_two_trigger').click(function() {
    $('#prod_pic_two').trigger('click');
  });
  $('#prod_pic_three_trigger').click(function() {
    $('#prod_pic_three').trigger('click');
  });
  $('#prod_pic_four_trigger').click(function() {
    $('#prod_pic_four').trigger('click');
  });
</script>
