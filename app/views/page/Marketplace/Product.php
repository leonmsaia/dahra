<?php foreach ($prod_info->result() as $prod): ?>
  <?php $this->load->view('layout/components/breadcrum');?>
  <section class="space--xs">
      <div class="container">
          <div class="row justify-content-around">
              <div class="col-md-7 col-lg-6">
                  <div class="slider border--round boxed--border" data-paging="true" data-arrows="true">
                      <ul class="slides">
                        <?php foreach ($image_support->result() as $prod_images): ?>
                          <?php if($prod_images->image_path_one != NULL): ?>
                            <li>
                              <img alt="<?php echo $prod->product_name;?>" src="<?php echo $prod_images->image_path_one; ?>">
                            </li>
                          <?php endif; ?>
                          <?php if($prod_images->image_path_two != NULL): ?>
                            <li>
                              <img alt="<?php echo $prod->product_name;?>" src="<?php echo $prod_images->image_path_two; ?>">
                            </li>
                          <?php endif; ?>
                          <?php if($prod_images->image_path_three != NULL): ?>
                            <li>
                              <img alt="<?php echo $prod->product_name;?>" src="<?php echo $prod_images->image_path_three; ?>">
                            </li>
                          <?php endif; ?>
                          <?php if($prod_images->image_path_four != NULL): ?>
                            <li>
                              <img alt="<?php echo $prod->product_name;?>" src="<?php echo $prod_images->image_path_four; ?>">
                            </li>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </ul>
                  </div>
              </div>
              <div class="col-md-5 col-lg-4">
                  <h2><?php echo $prod->product_name;?></h2>
                  <?php if ($prod->product_status == 0): ?>
                    <div class="text-block">
                        <span class="h4 inline-block">LA PUBLICACION FINALIZO</span>
                    </div>
                  <?php endif; ?>
                  <div class="text-block">
                    <span class="h4 inline-block">$<?php echo $prod->product_price;?></span>
                  </div>
                  <p>
                    <?php echo $prod->product_desc;?>
                  </p>
                  <ul class="accordion accordion-2 accordion--oneopen">
                      <li>
                          <div class="accordion__title">
                              <span class="h5">Especificaciones</span>
                          </div>
                          <div class="accordion__content">
                              <ul class="bullets">
                                <?php foreach ($details_support->result() as $dtls): ?>
                                  <li>
                                      <span><?php echo $dtls->mrk_product_detail_title;?>: <?php echo $dtls->mrk_product_detail_desc;?></span>
                                  </li>
                                <?php endforeach; ?>
                              </ul>
                          </div>
                      </li>
                      <li>
                          <div class="accordion__title">
                              <span class="h5">Dimensiones</span>
                          </div>
                          <div class="accordion__content">
                              <ul>
                                <?php foreach ($size_support->result() as $size_sup): ?>
                                  <li>
                                      <span><?php echo $size_sup->size_container;?>: Z = <?php echo $size_sup->size_z;?>cm. X = <?php echo $size_sup->size_x;?>cm. Y = <?php echo $size_sup->size_y;?>cm.</span>
                                  </li>
                                <?php endforeach; ?>
                              </ul>
                          </div>
                      </li>
                      <li>
                          <div class="accordion__title">
                              <span class="h5">Envios</span>
                          </div>
                          <div class="accordion__content">
                            <ul>
                              <?php foreach ($shipping_support->result() as $shpn): ?>
                                <?php if ($shpn->personal_withdrawal != 0): ?>
                                  <li>* Acordar con el Artista</li>
                                <?php endif; ?>
                                <?php if ($shpn->owner_pay != 0): ?>
                                  <li>* Envio Gratuito</li>
                                <?php endif; ?>
                                <?php if ($shpn->buyer_pay != 0): ?>
                                  <li>* Envio a cargo del Comprador</li>
                                <?php endif; ?>
                              <?php endforeach; ?>
                            </ul>
                          </div>
                      </li>
                  </ul>
                  <?php if ($prod->product_status == 0): ?>
                    <div class="text-block">
                        <span class="h4 inline-block">LA PUBLICACION FINALIZO</span>
                    </div>
                  <?php endif; ?>
              </div>
              <?php if ($this->ion_auth->logged_in()): ?>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-5 offset-md-7">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="modal-instance" style="float:right;">
              		          <a class="btn btn--primary modal-trigger" href="#">
              		            <span class="btn__text">
              		              Enviar Consulta
              		            </span>
              		          </a>
              		          <div class="modal-container">
              		            <div class="modal-content">
              		              <div class="boxed boxed--lg">
              		                  <h2>Enviar consulta al Artista:</h2>
              		                  <hr class="short">
                                    <?php echo form_open("Marketplace/makeConsult", 'class="row"');?>
                                      <div class="col-md-12" style="padding-left:15px; margin-top:45px;">
                                        <input type="hidden" name="message_receptor" value="<?php echo $prod->product_author;?>">
                                        <input type="hidden" name="message_prod_id" value="<?php echo $prod->product_id;?>">
                                        <input type="hidden" name="product_slug" value="<?php echo $prod->product_slug;?>">
                		                    <textarea rows="8" cols="80" id="message_text" name="message_text" style="margin-bottom:20px;" placeholder="Escribir Consulta"></textarea>
                		                    <div class="row">
                		                      <div class="col-md-4">
                		                        <button type="submit" class="btn btn--primary">Consultar</button>
                		                      </div>
                		                    </div>
                                      </div>
                                    <?php echo form_close();?>
              		              </div>
              		              <div class="modal-close modal-close-cross"></div>
              		            </div>
              		          </div>
                          </div>
                        </div>
                        <?php if (checkIfProdIsFavedByUserID(getMyID(), $prod->product_id)): ?>
                          <div class="col-md-6">
                            <button type="submit" class="btn btn--primary btn--icon" style="margin-top: 0; height: 41px;" disabled>
                              <span class="btn__text fav-button">
                                <i class="icon-Heart"></i> Ya esta en tus Favoritos
                              </span>
                            </button>
                          </div>
                        <?php else: ?>
                          <div class="col-md-6">
                            <?php echo form_open('Marketplace/makeFavProd');?>
                              <input type="hidden" name="product_id" value="<?php echo $prod->product_id;?>">
                              <input type="hidden" name="faved_user" value="<?php echo $prod->product_author;?>">
                              <input type="hidden" name="product_slug" value="<?php echo $prod->product_slug;?>">
                              <button type="submit" class="btn btn--primary btn--icon" style="margin-top: 0; height: 41px;">
                                <span class="btn__text fav-button">
                                  <i class="icon-Heart"></i> Agregar a Favoritos
                                </span>
                              </button>
                            <?php echo form_close();?>
                          </div>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
    		        </div>
              <?php else: ?>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-5 offset-md-7" style="padding-left:15px; margin-top:45px;">
                      <p class="text-center">Debes estar logeado para poder enviar consultas o preguntas al Artista.</p>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <div id="share"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-12" style="margin:45px auto;">
                  <hr>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <?php foreach ($artist_information->result() as $arts): ?>
                      <div class="card card-2 text-center">
                          <div class="card__top">
                              <a href="#">
                                  <img alt="<?php echo $arts->artist_name;?>" src="<?php echo $arts->artist_picture;?>">
                              </a>
                          </div>
                          <div class="card__body">
                              <h4><?php echo $arts->artist_name;?> <?php echo $arts->artist_lastname;?></h4>
                              <span class="type--fade"><?php echo $arts->artist_country;?></span>
                          </div>
                          <div class="card__bottom text-center">
                            <a href="<?php echo base_url() . 'artist/' . $arts->artist_slug?>">Ver Perfil del Artista</a>
                            <br>
                            <a href="<?php echo base_url() . 'artist/' . $arts->artist_slug . '/store'?>">Ver mas Obras del Artista</a>
                          </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <?php $limit = 1; ?>
                      <?php foreach ($artist_works->result() as $works_arts): ?>
                        <?php if ($limit < 4): ?>
                          <div class="col-md-4">
                              <div class="product">
                                  <a href="<?php echo base_url() . 'marketplace/product/' . $works_arts->product_slug;?>">
                                      <img alt="<?php echo $works_arts->product_name;?>" src="<?php echo $works_arts->product_img;?>"/>
                                  </a>
                                  <a class="block" href="<?php echo base_url() . 'marketplace/product/' . $works_arts->product_slug;?>">
                                      <div>
                                          <h5><?php echo $works_arts->product_name;?></h5>
                                          <br>
                                          <span><?php echo $works_arts->product_desc;?></span>
                                      </div>
                                      <div>
                                          <span class="h4 inline-block">$<?php echo $works_arts->product_price;?></span>
                                      </div>
                                  </a>
                              </div>
                          </div>
                        <?php endif; ?>
                      <?php $limit++; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
  </section>
  <section>
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="text-block">
                      <h3>Preguntas</h3>
                  </div>
              </div>
              <div class="col-md-12">
                <ul>
                  <li class="row">
                    <?php if ($question_in_product->num_rows() != 0): ?>
                      <ul class="col-md-12" style="margin-top: 20px; padding-top:15px; border-top:1px solid #cdcdcd;">
                        <?php foreach ($question_in_product->result() as $qst): ?>
                            <li class="row">
                              <span class="col-md-12">
                                <span class="row">
                                  <span class="col-md-10"><i class="icon-Speach-Bubble" style="margin-right:10px;"></i><?php echo $qst->question_text;?></span>
                                  <span class="col-md-2"><small><?php echo $qst->question_time;?></small></span>
                                </span>
                              </span>
                            </li>
                            <?php if (questionHaveAnswer($qst->question_id > 0)): ?>
                              <?php $anserData = answersDetails($qst->question_id);?>
                              <?php foreach ($anserData->result() as $answ): ?>
                                <li class="row">
                                  <span class="col-md-12">
                                    <span class="row">
                                      <span class="col-md-10" style="padding-left: 30px;">
                                        <i class="icon-Speach-Bubble" style="margin-right:10px;"></i>
                                        <?php echo $answ->answer_text;?>
                                      </span>
                                      <span class="col-md-2"><small><?php echo $answ->answer_time;?></small></span>
                                    </span>
                                  </span>
                                </li>
                              <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                      </ul>
                    <?php else: ?>
                      <ul>
                        <li class="col-md-12">
                          Aun no hay preguntas en este producto. Sé el primero!
                        </li>
                      </ul>
                    <?php endif; ?>
                    <div class="col-md-12" style="margin-top: 20px; padding-top:15px; border-top:1px solid #cdcdcd;">
                      <?php if ($this->ion_auth->logged_in()): ?>
                        <?php echo form_open('Marketplace/makeQuestion');?>
                          <input type="hidden" id="user_owner_id" name="user_owner_id" value="<?php echo $prod->product_author;?>" readonly>
                          <input type="hidden" id="product_slug" name="product_slug" value="<?php echo $prod->product_slug;?>" readonly>
                          <input type="hidden" id="product_id" name="product_id" value="<?php echo $prod->product_id;?>" readonly>
                          <input type="hidden" id="question_status" name="question_status" value="0" readonly>
                          <textarea rows="8" cols="80" id="question_text" name="question_text" style="margin-bottom:20px;" placeholder="Escribir Pregunta"></textarea>
                          <div class="row">
                            <div class="col-md-4">
                              <button type="submit" class="btn btn--primary">Preguntar</button>
                            </div>
                          </div>
                        <?php echo form_close();?>
                      <?php else: ?>
                        Debes estar logeado para poder realizar preguntas.
                      <?php endif; ?>
                    </div>
                  </li>
                </ul>
              </div>
          </div>
      </div>
  </section>
  <section>
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="text-block">
                      <h3>Quizas pueda interesarte</h3>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="product">
                      <a href="#">
                          <img alt="Image" src="http://www.dahra.com.ar/arte/images/prods/58_nuq.jpg"/>
                      </a>
                      <a class="block" href="#">
                          <div>
                              <h5>Apple Keyboard</h5>
                              <span> Wireless Bluetooth</span>
                          </div>
                          <div>
                              <span class="h4 inline-block">$99</span>
                          </div>
                      </a>
                  </div>
              </div>
          </div>
      </div>
  </section>
<?php endforeach; ?>
