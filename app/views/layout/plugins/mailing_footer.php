<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td width="100%" bgcolor="#fa6f6f" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td class="icon54" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="60"></td>
							</tr>
						</table>
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" valign="top" class="icon34" align="center">
									<table width="175" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 30px; color: #ffffff; line-height: 24px; font-weight: 600;" class="fullCenter">
												<?php echo $total_advises;?>
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 18px; color: #ffffff; line-height: 24px; font-weight: 600;" class="fullCenter">
												Obras Publicadas
											</td>
										</tr>
										<tr>
											<td width="100%" height="10"></td>
										</tr>
									</table>
									<table width="35" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td width="100%" height="30"></td>
										</tr>
									</table>
									<table width="175" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 30px; color: #ffffff; line-height: 24px; font-weight: 600;" class="fullCenter">
												<?php echo $total_contacts;?>
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 18px; color: #ffffff; line-height: 24px; font-weight: 600;" class="fullCenter">
												Contactos Comerciales Realizados
											</td>
										</tr>
										<tr>
											<td width="100%" height="10"></td>
										</tr>
									</table>
									<table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td width="100%" height="30"></td>
										</tr>
									</table>
									<table width="175" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 30px; color: #ffffff; line-height: 24px; font-weight: 600;" class="fullCenter">
												$ <?php echo $total_amount_advises;?>
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 18px; color: #ffffff; line-height: 24px; font-weight: 600;" class="fullCenter">
												Valor de Obras Publicadas
											</td>
										</tr>
										<tr>
											<td width="100%" height="10"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="60"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td align="center" width="100%" valign="top" bgcolor="#344b61">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" class="icon30">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
										<tr>
											<td width="100%" height="50"></td>
										</tr>
									</table>

									<?php foreach ($site_info->result() as $site_nfo): ?>
										<table width="260" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
											<tr>
												<td width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 20px; color: #ffffff; line-height: 28px; font-weight: 700;" class="fullCenter">
													Contactanos
												</td>
											</tr>
											<tr>
												<td width="100%" height="25"></td>
											</tr>
											<tr>
												<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #ffffff; line-height: 22px; font-weight: 400;" class="fullCenter">
													<?php echo $site_nfo->contact_adress;?>
												</td>
											</tr>
										</table>
									<?php endforeach; ?>
									<table width="1" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
										<tr>
											<td width="100%" height="30"></td>
										</tr>
									</table>
									<table width="260" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
										<tr>
											<td width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 20px; color: #ffffff; line-height: 28px; font-weight: 700;" class="fullCenter">
                        Sobre Nosotros
											</td>
										</tr>
										<tr>
											<td width="100%" height="25"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 14px; color: #ffffff; line-height: 22px; font-weight: 400;" class="fullCenter">
												Lorem ipsum dolor sit amet consectetur adipiscing elit nteger.
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
							<tr>
								<td width="100%" height="25"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#344b61">
	<tr>
		<td align="center" width="100%" valign="top">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
				<tr>
					<td width="100%" valign="middle" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
							<tr>
								<td width="100%" height="25">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" height="1" style="line-height: 1px; font-size: 1px;" bgcolor="#3d556b">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" height="25">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
	<tr>
		<td align="center" width="100%" valign="top" bgcolor="#344b61">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="10"></td>
							</tr>
						</table>
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" align="center">
									<table width="140" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td height="60" valign="middle" width="100%" style="text-align: left;" class="fullCenter" id="logo_footer">
                        <a href="<?php echo base_url();?>">
                          <img width="125" src="<?php echo base_url();?>assets/img/logo-dark.png" alt="" border="0">
                        </a>
											</td>
										</tr>
									</table>
									<table width="20" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
										<tr>
											<td width="100%" height="1"></td>
										</tr>
									</table>
									<?php foreach ($site_info->result() as $site_nfo): ?>
										<table width="96" border="0" cellpadding="0" cellspacing="0" align="right" style="text-align: right; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="buttonScale">
											<tr>
												<td height="45" valign="middle" width="32" align="center" class="icons17" style="text-align: center;">
													<a href="<?php echo $site_nfo->contact_facebook;?>" style="text-decoration: none;">
	                          <img src="<?php echo base_url();?>assets/img/icons/social_icon_facebook.png" width="17" alt="" border="0" class="hover">
	                        </a>
												</td>
												<td height="45" valign="middle" width="32" align="center" class="icons17" style="text-align: center;">
													<a href="<?php echo $site_nfo->contact_twitter;?>" style="text-decoration: none;">
	                          <img src="<?php echo base_url();?>assets/img/icons/social_icon_twitter.png" width="17" alt="" border="0" class="hover">
	                        </a>
												</td>
												<td height="45" valign="middle" width="32" align="center" class="icons17" style="text-align: center;">
													<a href="<?php echo $site_nfo->contact_gplus;?>" style="text-decoration: none;">
	                          <img src="<?php echo base_url();?>assets/img/icons/social_icon_gplus.png" width="17" alt="" border="0" class="hover">
	                        </a>
												</td>
											</tr>
										</table>
									<?php endforeach; ?>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td width="100%" height="10"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#344b61">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
				<tr>
					<td width="100%" valign="middle" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
							<tr>
								<td width="100%" height="25">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" height="1"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
