<!doctype html>
<html lang="en">
    <head>
      <?php $this->load->view('layout/assets/head');?>
    </head>
    <body data-smooth-scroll-offset="77">
        <?php $this->load->view('layout/components/navbar');?>
        <div class="main-container">
          <?php $this->load->view($page);?>
          <?php $this->load->view('layout/components/footer');?>
        </div>
        <?php $this->load->view('layout/assets/scripts_bottom');?>
    </body>
</html>
