<script src="<?php echo base_url();?>assets/js/parallax.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.min.js"></script>
<script src="<?php echo base_url();?>assets/js/smooth-scroll.min.js"></script>
<script src="<?php echo base_url();?>assets/js/scripts.js"></script>
<script src="<?php echo base_url();?>assets/js/flickity.min.js"></script>
<script src="<?php echo base_url();?>assets/js/datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/rangeslider.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jssocials.min.js"></script>
<script src="<?php echo base_url();?>assets/js/fontawesome.min.js"></script>
<script>
    $("#share").jsSocials({
        shares: ["twitter", "facebook", "googleplus", "linkedin"]
    });
</script>
