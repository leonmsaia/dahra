<meta charset="utf-8">
<title><?php echo $title;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<?php echo base_url();?>assets/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo base_url();?>assets/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/rangeslider.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/jssocials.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/jssocials-theme-flat.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
