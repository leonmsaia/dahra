<div class="col-md-3">
    <div class="sidebar boxed boxed--border boxed--lg bg--secondary">
        <div class="sidebar__widget">
            <h5>Buscar en el Sitio</h5>
            <?php echo form_open('Marketplace/Search');?>
                <input type="text" name="query" placeholder="buscar..." />
            <?php echo form_close();?>
        </div>
        <div class="sidebar__widget">
          <?php echo form_open('Marketplace/Filter');?>
          <?php
            $id_identifier = NULL;
            $filter_type = NULL;
            $filter_query = NULL;
            $url = current_url();
            $url_parsed = parse_url($url)['path'];
            $output = array();
            $delimiters = explode('/', $url_parsed);
            $count_delimiters = count($delimiters);
            if ($count_delimiters == 2) {
              if ($delimiters[2] == 'marketplace') {
                $filter_type = 'marketplace';
              }
            }elseif ($count_delimiters == 5) {
              if ($delimiters[3] == 'subcategory') {
                $filter_type = 'subcategory';
                $id_identifier = $filter_info->result()[0]->subcategory_id;
              }elseif ($delimiters[3] == 'category') {
                $filter_type = 'category';
                $id_identifier = $filter_info->result()[0]->category_id;
              }
            }elseif ($count_delimiters == 4) {
              if ($delimiters[3] == 'Search') {
                $filter_query =  $query_string;
              }
            }
          ?>
          <input type="hidden" name="query_string" value="<?php echo $filter_query;?>">
          <input type="hidden" name="path_directory" value="<?php echo $filter_type;?>">
          <input type="hidden" name="filter_type_id" value="<?php echo $id_identifier;?>">
          <h5>Filtros</h5>
            <ul>
              <li>
                Precios (Hasta)
                <br>
                <div class="input-icon">
                	<i class="material-icons">attach_money</i>
                	<input  id="range-price-filter" class="range-price-filter" name="range-price-filter" value=""type="text" name="input" placeholder="Precios hasta" />
                </div>
              </li>
              <li>
                Relevancia: <span id="expositionVal"></span>
                <br>
                <input id="range-exposition-filter" class="range-exposition-filter" type="range" name="range-exposition-filter" min="1" max="4" step="1" value="1" />
              </li>
              <li>
                <button type="submit" class="btn btn--primary">Filtrar</button>
              </li>
            </ul>
          <?php echo form_close();?>
        </div>
        <div class="sidebar__widget">
            <h5>Texto</h5>
            <p>
              Texto Comercial
            </p>
        </div>
        <div class="sidebar__widget">
          <?php foreach ($category_information->result() as $cat): ?>
            <h5>
              <a href="<?php echo base_url() . 'marketplace/category/' . $cat->category_slug;?>"><?php echo $cat->category_name;?></a>
            </h5>
            <ul>
              <?php foreach ($subcategory_information->result() as $subcat): ?>
                <?php if ($cat->category_id == $subcat->subcategory_category): ?>
                  <li>
                    <a href="<?php echo base_url() . 'marketplace/subcategory/' . $subcat->subcategory_slug;?>"><?php echo $subcat->subcategory_name;?></a>
                  </li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          <?php endforeach; ?>
        </div>
    </div>
</div>

<script>
  function getRangeExposition() {
    var slider_val_exposition = $('#range-exposition-filter').val();
    if (slider_val_exposition == '1') {
      var expo_txt = 'Minima';
    }else if (slider_val_exposition == '2') {
      var expo_txt = 'Media';
    }else if (slider_val_exposition == '3') {
      var expo_txt = 'Alta';
    }else if (slider_val_exposition == '4') {
      var expo_txt = 'Premiun';
    }
    $('#expositionVal').text(expo_txt);
  }
  $('#range-exposition-filter').change(function () {
    getRangeExposition();
  });
  $(document).ready(function() {
    getRangeExposition();
  });
</script>
