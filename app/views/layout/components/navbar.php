
<section class="bar bar-3 bar--sm bg--secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="bar__module">
                    <span class="type--fade">El Arte se lleva dentro.</span>
                </div>
            </div>
            <div class="col-md-6 text-right text-left-xs text-left-sm">
                <div class="bar__module">
                  <?php if ($this->ion_auth->logged_in()): ?>
                      <?php if ($this->ion_auth->in_group(1) OR $this->ion_auth->in_group(2)): ?>
                        <ul class="menu-horizontal">
                          <?php if (checkIFHaveArtistHelper()): ?>
                            <li>
                              <a href="<?php echo base_url();?>user/buy_credits">
                                Creditos <span id="credit_container"><?php echo $credit_qty;?></span>
                              </a>
                            </li>
                            <li>
                              <a href="<?php echo base_url();?>dashboard/messages">
                                Mensajes [<?php echo $messages;?>]
                              </a>
                            </li>
                            <li>
                              <a href="<?php echo base_url();?>user/my_profile">
                                <?php echo $this->ion_auth->user()->row()->first_name;?>
                              </a>
                            </li>
                          <?php endif; ?>
                          <li>
                              <a href="<?php echo base_url();?>dashboard">
                                  Panel de Control
                              </a>
                          </li>
                          <li>
                              <a href="<?php echo base_url();?>User/logUserOut">
                                  Salir
                              </a>
                          </li>
                        </ul>
                      <?php elseif ($this->ion_auth->in_group(3)): ?>
                        <ul class="menu-horizontal">
                          <li>
                            <?php echo $this->ion_auth->user()->row()->first_name;?>
                          </li>
                          <li>
                              <a href="<?php echo base_url();?>publicist/buy_credits">
                                Creditos <span id="credit_container"><?php echo $credit_qty;?></span>
                              </a>
                          </li>
                          <li>
                              <a href="<?php echo base_url();?>publicist/dashboard">
                                  Panel de Control
                              </a>
                          </li>
                          <li>
                              <a href="<?php echo base_url();?>User/logUserOut">
                                  Salir
                              </a>
                          </li>
                        </ul>
                      <?php endif; ?>
                  <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="nav-container">
    <div>
        <div class="bar bar--sm visible-xs">
            <div class="container">
                <div class="row">
                    <div class="col-3 col-md-2">
                        <a href="<?php echo base_url();?>"> <img class="logo logo-dark" alt="logo" src="<?php echo base_url();?>assets/img/logo-dark.png"> <img class="logo logo-light" alt="logo" src="<?php echo base_url();?>assets/img/logo-light.png"> </a>
                    </div>
                    <div class="col-9 col-md-10 text-right">
                        <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="icon icon--sm stack-interface stack-menu"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <nav id="menu1" class="bar bar-1 hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 col-md-2 hidden-xs">
                        <div class="bar__module">
                            <a href="<?php echo base_url();?>"> <img class="logo logo-dark" alt="logo" src="<?php echo base_url();?>assets/img/logo-dark.png"> <img class="logo logo-light" alt="logo" src="<?php echo base_url();?>assets/img/logo-light.png"> </a>
                        </div>
                    </div>
                    <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                        <div class="bar__module">
                          <?php if ($this->ion_auth->logged_in()): ?>
                            <?php if (checkIFHaveArtistHelper()): ?>
                              <a class="btn btn--sm" href="<?php echo base_url();?>marketplace/post">
                                <span class="btn__text">
                                  Publicar
                                </span>
                              </a>
                            <?php endif; ?>
                          <?php else: ?>
                            <a class="btn btn--sm" href="<?php echo base_url();?>login">
                              <span class="btn__text">
                                Ingresar
                              </span>
                            </a>
                            <a class="btn btn--sm" href="<?php echo base_url();?>register">
                              <span class="btn__text">
                                Registrarse
                              </span>
                            </a>
                          <?php endif ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="bar__module">
                          <ul class="menu-horizontal">
                            <?php foreach ($category_list->result() as $cat_list): ?>
                              <li>
                                <a href="<?php echo base_url() . 'marketplace/category/' . $cat_list->category_slug;?>">
                                  <?php echo $cat_list->category_name;?>
                                </a>
                              </li>
                            <?php endforeach; ?>
                          </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
