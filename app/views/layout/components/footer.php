<footer class="footer-4 space--sm text-center-xs">
    <div class="container">

        <div class="row">
            <div class="col-sm-6">
              <img alt="Image" class="logo" src="assets/img/logo-dark.png">
            </div>

            <div class="col-sm-6">
                <form class="row" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your email address." style="margin-top:0px;">
                    <div class="col-sm-7"> <input class="validate-required validate-email" type="text" name="EMAIL" placeholder="Subscribite a nuestro newsletter"> </div>
                    <div class="col-sm-5"> <button type="submit" class="btn btn--primary type--uppercase">Subscribirme</button> </div>
                    <div style="position: absolute; left: -5000px" aria-hidden="true"> <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value=""> </div>
                </form>
            </div>

            <div class="col-sm-6 col-md-3 col-xs-6">
                <h6 class="type--uppercase">Acerca de</h6>
                <ul class="list--hover">
                    <li><a href="#">¿Que es Dahra?</a></li>
                    <li><a href="#">Terminos y Condiciones</a></li>
                    <li><a href="#">Politicas de Privacidad</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 col-xs-6">
                <h6 class="type--uppercase">Saber Mas</h6>
                <ul class="list--hover">
                    <li>
                      <a href="<?php echo base_url();?>contact">
                        Contacto
                      </a>
                    </li>
                    <li><a href="#">Centro de Atención</a></li>
                    <li><a href="#">Servicios que Ofrecemos</a></li>
                    <li><a href="#">Consejos para Mejorar</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 col-xs-6">
                <h6 class="type--uppercase">Saber Mas</h6>
                <ul class="social-list list-inline list--hover">
                  <?php foreach (getSocialMedia()->result() as $socialMd): ?>
                    <li><a href="<?php echo $socialMd->contact_gplus;?>" target="_blank"><i class="socicon socicon-google icon icon--xs"></i></a></li>
                    <li><a href="<?php echo $socialMd->contact_twitter;?>" target="_blank"><i class="socicon socicon-twitter icon icon--xs"></i></a></li>
                    <li><a href="<?php echo $socialMd->contact_facebook;?>" target="_blank"><i class="socicon socicon-facebook icon icon--xs"></i></a></li>
                    <li><a href="<?php echo $socialMd->contact_instagram;?>" target="_blank"><i class="socicon socicon-instagram icon icon--xs"></i></a></li>
                  <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
              <span class="type--fine-print">© <span class="update-year"></span>
                Dahra S.R.L.
              </span>
              <a class="type--fine-print" href="#">Politicas de Privacidad</a>
              <a class="type--fine-print" href="#">Legales</a>
            </div>
        </div>
    </div>
</footer>
