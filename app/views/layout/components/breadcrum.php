<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1><?php echo $filter_title;?></h1>
            <ol class="breadcrumbs">
                <li>
                    <a href="<?php echo base_url();?>">Home</a>
                </li>
                <?php if ($filter_breadcrum_point == 1): ?>
                  <li>
                    <a href="<?php echo base_url() . 'marketplace/';?>">Marketplace</a>
                  </li>
                  <?php if ($filter_breadcrum_type == 'category'): ?>
                    <li><?php echo $filter_title;?></li>
                  <?php elseif($filter_breadcrum_type == 'subcategory'): ?>
                    <?php foreach ($filter_breadcrum_father_info->result() as $nfo_father): ?>
                      <li>
                        <a href="<?php echo base_url() . 'marketplace/category/' . $nfo_father->category_slug;?>"><?php echo $nfo_father->category_name;?></a>
                      </li>
                    <?php endforeach; ?>
                    <li><?php echo $filter_title;?></li>
                  <?php elseif($filter_breadcrum_type == 'product'): ?>
                    <li>
                      <a href="<?php echo base_url() . 'marketplace/category/' . $catego_info->category_slug;?>">
                        <?php echo $catego_info->category_name; ?>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url() . 'marketplace/subcategory/' . $subcatego_info->subcategory_slug;?>">
                        <?php echo $subcatego_info->subcategory_name;?>
                      </a>
                    </li>
                    <li><?php echo $filter_title;?></li>
                  <?php elseif($filter_breadcrum_type == 'search'): ?>
                    <li>Buscar</li>
                  <?php endif; ?>
                <?php else: ?>
                  <li>
                    Marketplace
                  </li>
                <?php endif; ?>
            </ol>
            <hr>
        </div>
    </div>
</div>
