<div class="col-md-2 ads">
  <div class="row ads_module">
    <?php foreach ($ads_list->result() as $ads): ?>
      <?php if ($ads->mkt_ads_relevance == 3): ?>
        <div class="col-md-12 ads_op high_class" data-adsCode="<?php echo $ads->mkt_ads_adscode;?>">
      <?php elseif ($ads->mkt_ads_relevance == 1): ?>
        <div class="col-md-12 ads_op" data-adsCode="<?php echo $ads->mkt_ads_adscode;?>" style="font-size: 0.9em;">
      <?php else: ?>
        <div class="col-md-12 ads_op" data-adsCode="<?php echo $ads->mkt_ads_adscode;?>">
      <?php endif; ?>
        <a href="<?php echo base_url();?>ads/<?php echo $ads->mkt_ads_adscode;?>" target="_blank">
          <?php if ($ads->mkt_ads_relevance >= 2): ?>
            <span class="image_container">
              <img src="<?php echo $ads->mkt_ads_image;?>" alt="<?php echo $ads->mkt_ads_title;?>">
            </span>
          <?php endif; ?>
          <span class="title_container"><?php echo $ads->mkt_ads_title;?></span>
          <span class="text_url"><?php echo substr($ads->mkt_ads_url, 0, 20);?></span>
          <span class="text_title"><?php echo $ads->mkt_ads_desc;?></span>
        </a>
      </div>
      <?php insertPrint($ads->mkt_ads_adscode);?>
    <?php endforeach; ?>
  </div>
</div>
