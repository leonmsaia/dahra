<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketplace extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Marketplace_model');
		$this->load->model('User_model');
		$this->load->model('Ads_model');
	}

	public function index() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Index';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// SubCategory Info
		$data['filter_title'] = 'Marketplace';
		$data['filter_breadcrum_point'] = 0;
		$data['filter_breadcrum_father_info'] = 'Marketplace';
		$data['filter_breadcrum_type'] = 'subcategory';

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();

		// Basic Information
		$data['products_list'] = $this->Marketplace_model->getProductsAvailable();

		// Metrics
		$data['ads_list'] = $this->Ads_model->getAdsAvailables();

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function Category($slug_category) {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Basic Information
		$data['filter_info'] = $this->Marketplace_model->getCategoryBySlug($slug_category);

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Index';

		// Page Information
		$data['title'] = 'Dahra | ' . $data['filter_info']->result()[0]->category_name;
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Category Info
		$data['filter_title'] = $data['filter_info']->result()[0]->category_name;
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = $this->Marketplace_model->getCategoryByID($data['filter_info']->result()[0]->category_id);
		$data['filter_breadcrum_type'] = 'category';


		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();

		// Basic Information
		$data['products_list'] = $this->Marketplace_model->getProductsInCategoryAvailable($data['filter_info']->result()[0]->category_id);

		// Metrics
		$data['ads_list'] = $this->Ads_model->getAdsAvailables();

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function SubCategory($slug_subcategory) {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}
		// Basic Information
		$data['filter_info'] = $this->Marketplace_model->getSubCategoryBySlug($slug_subcategory);

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Index';

		// Page Information
		$data['title'] = 'Dahra | ' . $data['filter_info']->result()[0]->subcategory_name;
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// SubCategory Info
		$data['filter_title'] = $data['filter_info']->result()[0]->subcategory_name;
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = $this->Marketplace_model->getCategoryByID($data['filter_info']->result()[0]->subcategory_category);
		$data['filter_breadcrum_type'] = 'subcategory';

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();

		// Basic Information
		$data['products_list'] = $this->Marketplace_model->getProductsBySubcategoryAvailable($data['filter_info']->result()[0]->subcategory_id);

		// Metrics
		$data['ads_list'] = $this->Ads_model->getAdsAvailables();

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function creditMarket() {

		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Basic Information
		$data['filter_info'] = '';

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Credits';

		// Page Information
		$data['title'] = 'Dahra | Mercado de Creditos';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// SubCategory Info
		$data['filter_title'] = 'Mercado de Creditos';
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = '';
		$data['filter_breadcrum_type'] = 'subcategory';

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();
		$user_id = getMyID();
		$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

		// Store Configuration
		$data['user_info'] =  $this->User_model->getBuyerUserInformation($user_id);
		$data['store_configuration'] = $this->Marketplace_model->getStoreConfiguration();
		$data['point_plans'] = $this->Marketplace_model->getCreditPlans();

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function payment_process_success() {
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);

			// Basic Information
			$data['filter_info'] = '';

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Marketplace/Payment_result';

			// Page Information
			$data['title'] = 'Dahra | Mercado de Creditos';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();

			// SubCategory Info
			$data['filter_title'] = 'Mercado de Creditos';
			$data['filter_breadcrum_point'] = 1;
			$data['filter_breadcrum_father_info'] = '';
			$data['filter_breadcrum_type'] = 'subcategory';

			// Sidebar Information
			$data['category_information'] = $this->Marketplace_model->getCategories();
			$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();
			$user_id = getMyID();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Store Configuration
			$data['user_info'] =  $this->User_model->getBuyerUserInformation($user_id);
			$data['store_configuration'] = $this->Marketplace_model->getStoreConfiguration();
			$data['point_plans'] = $this->Marketplace_model->getCreditPlans();
			$external_reference = $_GET['external_reference'];
			$operation_payment_check = $this->Marketplace_model->checkIfMPIsStored($external_reference);
			$operation_mp_check = $this->Marketplace_model->checkIfPaymentIsStored($external_reference);
			if ($operation_payment_check == true AND $operation_payment_check == true) {
				redirect(base_url() . 'dashboard/subscription/','refresh');
			}else{
				// Prepare MercadoPago Operation Collection
				$collection_id = $_GET['collection_id'];
				$collection_status = $_GET['collection_status'];
				$preference_id = $_GET['preference_id'];
				$payment_type = $_GET['payment_type'];
				$merchant_order_id = $_GET['merchant_order_id'];
				$filtered_external_reference = explode('_', $external_reference);
				$mpOperationDataBasic = array(
					'external_reference' => $external_reference,
					'collection_id' => $collection_id,
					'collection_status' => $collection_status,
					'preference_id' => $preference_id,
					'payment_type' => $payment_type,
					'merchant_order_id' => $merchant_order_id,
					'plane_code' => $filtered_external_reference[8]
				);
				$this->db->insert('site_recipe_entry_register_mp', $mpOperationDataBasic);
				// Prepare MercadoPago Operation Collection End
				// Prepare Operation Collection
				$operationDataBasic = array(
					'user_id' => $filtered_external_reference[2],
					'date' => gmdate("Y-m-d H:i:s", $filtered_external_reference[4]),
					'amount' => $filtered_external_reference[6],
					'code' => $filtered_external_reference[8],
					'external_reference' => $external_reference,
					'state' => $collection_status,
					'plan_id' => $filtered_external_reference[10]
				);
				$this->db->insert('site_recipe_entry_register', $operationDataBasic);
				$recipe_id = $this->db->insert_id();
				// Prepare Operation Collection End
				// Prepare Credit Operation Collection
				$credit_operation_code = 'CRED_OP_'. $filtered_external_reference[8];
				$pointPlanDetails = $this->Marketplace_model->getPointPlanDetails($filtered_external_reference[10]);
				$credit_qty_buyed = $pointPlanDetails->result()[0]->mrk_point_plans_qty;
				$creditOperationDataBasic = array(
					'mrk_credit_operation_user_id' => $user_id,
					'mrk_credit_operation_qty' => $credit_qty_buyed,
					'mrk_credit_operation_type' => 1,
					'mrk_credit_operation_operation_id' => $credit_operation_code,
					'mrk_credit_operation_operation_plane_code' => $filtered_external_reference[8]
				);
				$this->db->insert('mrk_credit_operation', $creditOperationDataBasic);
				$this->load->helper('tusfacturas');
				$data['recipe_information'] = $this->Marketplace_model->getCompleteRecipesByRecipeID($recipe_id);
				$payment_billing_collection = $data['recipe_information']->result()[0];
				generateBillingTypeC($payment_billing_collection);
				// Prepare Credit Operation Collection End
				redirect(base_url() . 'mailing/send_invoice/' . $recipe_id, 'refresh');
			}
		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function Search() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Query Capture
		$query = $_POST['query'];
		$data['query_string'] = $query;

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Index';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// SubCategory Info
		$data['filter_title'] = 'Resultados de ' . $query . '...';
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = '';
		$data['filter_breadcrum_type'] = 'search';

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();

		// Basic Information
		$data['products_list'] = $this->Marketplace_model->getProductsCompletedDataByQuery($query);

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function Filter() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Query Capture
		$value_path = $_POST['path_directory'];
		$value_filter_type = $_POST['filter_type_id'];
		$value_query = $_POST['query_string'];
		$value_price = $_POST['range-price-filter'];
		$value_relevance = $_POST['range-exposition-filter'];

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Index';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// SubCategory Info
		$data['filter_title'] = 'Productos Filtrados';
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = '';
		$data['filter_breadcrum_type'] = 'search';

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();

		// Basic Information
		if ($value_path == 'marketplace') {
			$data['products_list'] = $this->Marketplace_model->getProductsByFiltersMain($value_relevance, $value_price);
		}elseif ($value_path == 'category'){
			$data['products_list'] = $this->Marketplace_model->getProductsByFiltersCategory($value_relevance, $value_price, $value_filter_type);
		}elseif ($value_path == 'subcategory') {
			$data['products_list'] = $this->Marketplace_model->getProductsByFiltersSubcategory($value_relevance, $value_price, $value_filter_type);
		}elseif ($value_path == NULL) {
			$data['products_list'] = $this->Marketplace_model->getProductsByFiltersMainNull($value_relevance, $value_price);
		}elseif ($value_path == 'search') {
			$data['products_list'] = $this->Marketplace_model->getProductsByFiltersQuery($value_relevance, $value_price, $value_query);
		}

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function Artist($artist_slug) {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Get Data
		$data['artist_info'] = $this->Marketplace_model->getArtistBySlug($artist_slug);
		$product_author = $data['artist_info']->result()[0]->user_id;

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Profile';

		// Page Information
		$artist_obj = $data['artist_info']->result()[0];
		$data['title'] = 'Dahra | ' . $artist_obj->artist_name . ' ' . $artist_obj->artist_lastname;
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();
		$data['product_list'] = $this->Marketplace_model->getProductsByArtist($product_author);
		$data['artist_favs'] = $this->Marketplace_model->countFavsByArtist($product_author);
		$data['artist_object'] = $artist_obj;
		// Stats
		$data['number_works'] = $this->Marketplace_model->countWorksByArtist($product_author);

		$this->load->view('layout/' . $data['layout'], $data);
	}
	public function Artist_Store() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Artist_Store';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		$this->load->view('layout/' . $data['layout'], $data);
	}
	public function Artist_Advertising_Me() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Advertising_Me';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		$this->load->view('layout/' . $data['layout'], $data);
	}

	// Post and Edit Products in Marketplace
	public function post_advise() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Dashboard';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Publicar Producto';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Active Module
		$data['module_title'] = 'Publicar Producto';
		$data['module'] = 'uploads/products/edit_product';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function advise($product_slug) {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Preparation of Data
		$data['prod_info'] = $this->Marketplace_model->getProductBySlug($product_slug);
		$product_id = $data['prod_info']->result()[0]->product_id;
		$subcategory_id = $data['prod_info']->result()[0]->product_subcategory;

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Product';

		// Page Information
		$data['title'] = 'Dahra | ' . $data['prod_info']->result()[0]->product_name;
		$data['description'] = $data['prod_info']->result()[0]->product_desc;
		$data['keywords'] = $data['prod_info']->result()[0]->product_keyword;
		$data['author'] = 'Match Trade SRL';
		$data['filter_title'] = $data['prod_info']->result()[0]->product_name;

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();
		$data['image_support'] = $this->Marketplace_model->getImageSupport($product_id);
		$data['size_support'] = $this->Marketplace_model->getSizeSupport($product_id);
		$data['shipping_support'] = $this->Marketplace_model->getShippingSupport($product_id);
		$data['details_support'] = $this->Marketplace_model->getDetailsSupport($product_id);
		$data['artist_information'] = $this->Marketplace_model->getArtistInformation($data['prod_info']->result()[0]->product_author);
		$data['artist_works'] = $this->Marketplace_model->getWorksByArtistID($data['prod_info']->result()[0]->product_author);
		$product_sale_terms = $this->Marketplace_model->getSaleTermsByProdID($product_id);
		$data['buy_collection'] = $this->Marketplace_model->getSaleTermsByProdID($product_id);
		$data['product_info'] = $this->Marketplace_model->getProductByID($product_id);
		$data['product_shipping'] = $this->Marketplace_model->getShippingSupport($product_id);

		// Breadcrum Info
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = 'Marketplace';
		$data['filter_breadcrum_type'] = 'product';

		$data['catego_info'] = $this->Marketplace_model->getCategoryBySubCategoryID($subcategory_id)->result()[0];
		$data['subcatego_info'] = $this->Marketplace_model->getSubCategoryByID($subcategory_id)->result()[0];

		// Product Questions
		$data['question_in_product'] = $this->Marketplace_model->getQuestionsByProduct($product_id);

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();

		// Update Visit Counter in Product
		$this->User_model->insertUpdateInCounterByProduct($product_id);

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function post_terms() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Advertising_Me';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		$this->load->view('layout/' . $data['layout'], $data);
	}
	public function post_shipping() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Marketplace/Advertising_Me';

		// Page Information
		$data['title'] = 'Dahra | Marketplace';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		$this->load->view('layout/' . $data['layout'], $data);
	}

	//
	//
	//
	//
	//
	//
	// Actions
	//
	//
	//
	//
	//

	public function makeFavProd()
	{
		if ($this->ion_auth->logged_in()) {
			$faver_user = getMyID();
			$product_slug = $_POST['product_slug'];
			$product_id = $_POST['product_id'];
			$faved_user = $_POST['faved_user'];
			// Prepare Favorite Collection
			$dataBasic = array(
				 'product_id' => $product_id,
				 'faver_user' => $faver_user,
				 'faved_user' => $faved_user
			);
			$this->db->insert('mrk_fav_prod_register', $dataBasic);
			redirect('marketplace/product/' . $product_slug, 'refresh');
		}
	}

	public function makeConsult()
	{
		if ($this->ion_auth->logged_in()) {
			$message_author = getMyID();
			$product_slug = $_POST['product_slug'];
			$message_prod_id = $_POST['message_prod_id'];
			$message_receptor = $_POST['message_receptor'];
			$message_text = $_POST['message_text'];
			// Prepare Message/Consult Collection
			$dataBasic = array(
				 'message_author' => $message_author,
				 'message_prod_id' => $message_prod_id,
				 'message_receptor' => $message_receptor,
				 'message_text' => $message_text
			);
			$this->db->insert('user_message', $dataBasic);
			redirect('marketplace/product/' . $product_slug, 'refresh');
		}
	}

	public function makeConsultDashboard()
	{
		if ($this->ion_auth->logged_in()) {
			$message_author = getMyID();
			$message_prod_id = $_POST['message_prod_id'];
			$message_receptor = $_POST['message_receptor'];
			$message_text = $_POST['message_text'];
			// Prepare Message/Consult Collection
			$dataBasic = array(
				 'message_author' => $message_author,
				 'message_prod_id' => $message_prod_id,
				 'message_receptor' => $message_receptor,
				 'message_text' => $message_text
			);
			$this->db->insert('user_message', $dataBasic);
			redirect('dashboard/buys/questions/', 'refresh');
		}
	}

	public function makeConsultDashboardBuyFavorites()
	{
		if ($this->ion_auth->logged_in()) {
			$message_author = getMyID();
			$message_prod_id = $_POST['message_prod_id'];
			$message_receptor = $_POST['message_receptor'];
			$message_text = $_POST['message_text'];
			// Prepare Message/Consult Collection
			$dataBasic = array(
				 'message_author' => $message_author,
				 'message_prod_id' => $message_prod_id,
				 'message_receptor' => $message_receptor,
				 'message_text' => $message_text
			);
			$this->db->insert('user_message', $dataBasic);
			$insert_id = $this->db->insert_id();
			// Redirect to Mail Sender
			// Get Current User Mailing Configuration
			$getUserConfigurationSettings = $this->User_model->getMailNotificationSettings($user_id);
			$flag_send = $getUserConfigurationSettings->write_message;
			if ($flag_send == 1) {
				redirect('Mailing/contact_user/' . $insert_id, 'refresh');
			}else{
				redirect('dashboard/buys/favorites/', 'refresh');
			}
		}
	}

	public function makePurchase()
	{
		if ($this->ion_auth->logged_in()) {
			// Credit Information
			if ($this->ion_auth->logged_in()) {
				$user_buyer = getMyID();
				// General Information
				$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
				$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
			}

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Marketplace/Checkout';

			// Page Information
			$data['title'] = 'Dahra | Checkout';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Info Data Products
			$buyer_id = getMyID();
			$product_id = $_POST['product_id'];
			$product_sale_terms = $this->Marketplace_model->getSaleTermsByProdID($product_id);
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['buy_collection'] = $this->Marketplace_model->getSaleTermsByProdID($product_id);
			$data['payment_info'] = $this->Marketplace_model->getPaymentMethodByID($product_sale_terms->result()[0]->payment_provider);
			$data['product_info'] = $this->Marketplace_model->getProductByID($product_id);
			$data['product_shipping'] = $this->Marketplace_model->getShippingSupport($product_id);
			$data['product_size'] = $this->Marketplace_model->getSizeSupport($product_id);
			$data['product_qty'] = intval($_POST['quantity']);
			$data['user_info'] =  $this->User_model->getBuyerUserInformation($buyer_id);
			$data['product_name'] = $data['product_info']->result()[0]->product_name;
			$data['product_unit_cost'] = floatval($data['buy_collection']->result()[0]->product_price);
			$data['user_zipcode'] = $data['user_info']->result()[0]->zipcode;
			$data['user_adress'] = $data['user_info']->result()[0]->adress;
			$product_size_high = intval($data['product_size']->result()[0]->size_z);
			$product_size_wide = intval($data['product_size']->result()[0]->size_x);
			$product_size_deep = intval($data['product_size']->result()[0]->size_y);
			$product_size_weight = intval($data['product_size']->result()[0]->weight);
			$product_dimension = $product_size_high . 'x' . $product_size_wide . 'x' . $product_size_deep . ',' . $product_size_weight;
			$data['product_dimension_composition'] = strval($product_dimension);
			$data['transaction_id'] = 'TRANS_' . generateID() . '_';

			$this->load->view('layout/' . $data['layout'], $data);
		}

	}

	public function makeQuestion() {
		// Credit Information
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		if ($this->ion_auth->logged_in()) {
			// Serialize Vars
			$question_author = getMyID();
			$question_text = $_POST['question_text'];
			$product_slug = $_POST['product_slug'];
			$user_id = $_POST['user_owner_id'];
			$product_id = $_POST['product_id'];
			$question_status = $_POST['question_status'];
			// Prepare Question Collection
			$dataBasic = array(
				 'question_author' => $question_author,
				 'question_text' => $question_text,
				 'user_id' => $user_id,
				 'product_id' => $product_id,
				 'question_status' => $question_status
			);
			$this->db->insert('mkt_question', $dataBasic);
			$insert_id = $this->db->insert_id();

			// Redirect to Mail Sender
			$getUserConfigurationSettings = $this->User_model->getMailNotificationSettings($user_id);
			$flag_send = $getUserConfigurationSettings->question_work;
			if ($flag_send == 1) {
				redirect('Mailing/question_to_user_product_view/' . $insert_id, 'refresh');
			}else{
				redirect('marketplace/product/' . $product_slug, 'refresh');
			}

		}
	}
	public function makeQuestionModal() {
		if ($this->ion_auth->logged_in()) {
			// Serialize Vars
			$question_author = getMyID();
			$question_text = $_POST['question_text'];
			$product_slug = $_POST['product_slug'];
			$user_id = $_POST['user_owner_id'];
			$product_id = $_POST['product_id'];
			$question_status = $_POST['question_status'];
			// Prepare Question Collection
			$dataBasic = array(
				 'question_author' => $question_author,
				 'question_text' => $question_text,
				 'user_id' => $user_id,
				 'product_id' => $product_id,
				 'question_status' => $question_status
			);
			$this->db->insert('mkt_question', $dataBasic);
			redirect('dashboard/buys/questions', 'refresh');
		}
	}
	public function answerQuestion() {
		if ($this->ion_auth->logged_in()) {
			// Serialize Vars
			$answer_author = getMyID();
			$user_owner_id = $_POST['user_owner_id'];
			$question_id = $_POST['question_id'];
			$product_id = $_POST['product_id'];
			$answer_status = $_POST['answer_status'];
			$answer_text = $_POST['question_text'];

			// Prepare Question Collection
			$dataBasic = array(
				 'answer_author' => $answer_author,
				 'question_id' => $question_id,
				 'answer_text' => $answer_text,
				 'product_id' => $product_id,
				 'answer_status' => $answer_status
			);
			$this->db->insert('mkt_question_answer', $dataBasic);
			$insert_id = $this->db->insert_id();

			$dataBasic = array(
			   'question_status' => 1
			);
			$this->db->where('question_id', $question_id);
			$this->db->update('mkt_question', $dataBasic);

			// Redirect to Mail Sender
			$getUserConfigurationSettings = $this->User_model->getMailNotificationSettings($user_id);
			$flag_send = $getUserConfigurationSettings->question_work;
			if ($flag_send == 1) {
				redirect('Mailing/question_to_user/' . $insert_id, 'refresh');
			}else{
				redirect('dashboard/buys/favorites/', 'refresh');
			}
		}
	}

	public function getSubcategorieByCategoryID($category_id)
	{
		$list_crude = $this->Marketplace_model->getSubCategoryByCategoryID($category_id);
		$list = array();
		foreach ($list_crude->result() as $bs_list) {
			$dataBasic = array(
			   'subcategory_id' => $bs_list->subcategory_id,
				 'subcategory_name' => $bs_list->subcategory_name
			);
			array_push($list, $dataBasic);
		}
		echo json_encode($list);
	}

	public function create_new_product()
	{
		$active_user = getMyID();
		// Product Code Generation
		$product_code = generateID();
		// Slug Generation
		$this->load->helper('url');
    $name = $_POST['product_name'];
    $url_title = url_title($name, '-', TRUE);
		$slug = 'PROD_' . $url_title . '_'. $product_code;
		// Var Serialize
		$product_author = $active_user;
		$product_name = '';
		$product_slug = $slug;
		$categorie_selector = '';
		$subcategorie_selector = '';
		$product_desc = '';
		$product_text = '';
		$product_price = '';
		$product_qty = '';
		$knowed_transport = '';
		$condition_send = '';
		// Generate Product Simple Collection
		if (!empty($_POST['product_name'])) {
			$product_name = $_POST['product_name'];
		}
		if (!empty($_POST['categorie_selector'])) {
			$categorie_selector = $_POST['categorie_selector'];
		}
		if (!empty($_POST['subcategorie_selector'])) {
			$subcategorie_selector = $_POST['subcategorie_selector'];
		}
		if (!empty($_POST['product_desc'])) {
			$product_desc = $_POST['product_desc'];
		}
		if (!empty($_POST['product_text'])) {
			$product_text = $_POST['product_text'];
		}
		if (!empty($_POST['product_price'])) {
			$product_price = $_POST['product_price'];
		}
		if (!empty($_POST['knowed_transport'])) {
			$knowed_transport = $_POST['knowed_transport'];
		}
		if (!empty($_POST['condition_send'])) {
			$condition_send = $_POST['condition_send'];
		}
		if (!empty($_POST['product_qty'])) {
			$product_qty = $_POST['product_qty'];
		}
		$dataBasic = array(
			 'product_author' => $product_author,
			 'product_name' => $product_name,
			 'product_category' => $categorie_selector,
			 'product_subcategory' => $subcategorie_selector,
			 'product_desc' => $product_desc,
			 'product_text' => $product_text,
			 'product_status' => 0,
			 'product_slug' => $slug,
			 'product_code' => $product_code,
			 'product_price' => $product_price,
			 'product_qty' => $product_qty
		);
		// Insert Product Simple Collection
		$this->db->insert('mrk_product', $dataBasic);
		// Get Fresh Product ID
		$data['new_product_created'] = $this->Marketplace_model->getProductByCode($product_code);
		$product_id = $data['new_product_created']->result()[0]->product_id;
		// Create Basic Shipping Context
		$dataBasicShippingCreation = array(
			 'product_id' => $product_id,
			 'product_code' => $product_code
		);
		$this->db->insert('mrk_product_shipping', $dataBasicShippingCreation);
		// Create Basic Image Support
		$dataBasicImageCreation = array(
			 'product_id' => $product_id,
			 'product_code' => $product_code
		);
		$this->db->insert('mrk_product_img_support', $dataBasicImageCreation);
		// Update Basic Shipping Context
		// Radio Options
		foreach ($condition_send as $cond) {
			if ($cond == 'my_transport') {
				$dataShipping = array(
					'know_transport' => 1
				);
				$this->db->where('product_code', $product_code);
				$this->db->update('mrk_product_shipping', $dataShipping);
			}
			if ($cond == 'i_pick_up') {
				$dataShipping = array(
					'personal_withdrawal' => 1
				);
				$this->db->where('product_code', $product_code);
				$this->db->update('mrk_product_shipping', $dataShipping);
			}
			else{
				$dataShipping = array(
					'know_transport' => 0,
					'personal_withdrawal' => 0
				);
				$this->db->where('product_code', $product_code);
				$this->db->update('mrk_product_shipping', $dataShipping);
			}
		}
		// For Knowed Transport
		// Checkbox Options
		if ($knowed_transport == 'send_free') {
			$dataShipping = array(
				'owner_pay' => 1
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_shipping', $dataShipping);
		}
		if ($knowed_transport == 'send_buyer') {
			$dataShipping = array(
				'buyer_pay' => 1
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_shipping', $dataShipping);
		}
		else{
			$dataShipping = array(
				'buyer_pay' => 0,
				'owner_pay' => 0
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_shipping', $dataShipping);
		}
		// Finish and Redirect to Step Two
		redirect('marketplace/post/second_step/' . $product_code, 'refresh');
	}

	public function save_product_size()
	{
		$state = $_POST['state'];
		$product_code = $_POST['product_code'];
		$data['new_product_created'] = $this->Marketplace_model->getProductByCode($product_code);
		$product_id = $data['new_product_created']->result()[0]->product_id;
		$size_container = $_POST['size_container'];
		$weight = $_POST['weight'];
		$size_z = $_POST['size_z'];
		$size_x = $_POST['size_x'];
		$size_y = $_POST['size_y'];
		// Create Basic Shipping Context
		$dataBasicSizeCreation = array(
			 'product_id' => $product_id,
			 'product_code' => $product_code,
			 'size_container' => $size_container,
			 'weight' => $weight,
			 'size_z' => $size_z,
			 'size_x' => $size_x,
			 'size_y' => $size_y
		);
		$this->db->insert('mrk_product_size', $dataBasicSizeCreation);
		// Finish and Redirect
		redirect($state . $product_code, 'refresh');
	}

	public function delete_product_size()
	{
		$state = $_POST['state'];
		$product_size_id = $_POST['product_size_id'];
		$product_code = $_POST['product_code'];
		$this->db->where('product_size_id', $product_size_id);
		$this->db->delete('mrk_product_size');
		// Finish and Redirect
		redirect($state . $product_code, 'refresh');
	}

	public function save_product_details()
	{
		$state = $_POST['state'];
		$product_code = $_POST['product_code'];
		$data['new_product_created'] = $this->Marketplace_model->getProductByCode($product_code);
		$product_id = $data['new_product_created']->result()[0]->product_id;

		$mrk_product_detail_title = $_POST['mrk_product_detail_title'];
		$mrk_product_detail_desc = $_POST['mrk_product_detail_desc'];

		// Create Basic Detail Context
		$dataBasicDetailsCreation = array(
			 'product_id' => $product_id,
			 'mrk_product_detail_title' => $mrk_product_detail_title,
			 'mrk_product_detail_desc' => $mrk_product_detail_desc
		);
		$this->db->insert('mrk_product_details', $dataBasicDetailsCreation);
		// Finish and Redirect
		redirect($state . $product_code, 'refresh');
	}

	public function delete_product_detail()
	{
		$state = $_POST['state'];
		$mrk_product_details_id = $_POST['mrk_product_details_id'];
		$product_code = $_POST['product_code'];
		$this->db->where('mrk_product_details_id', $mrk_product_details_id);
		$this->db->delete('mrk_product_details');
		// Finish and Redirect
		redirect($state . $product_code, 'refresh');
	}

	public function save_tags_materials_data()
	{
		$state = $_POST['state'];
		$product_code = $_POST['product_code'];
		$data['new_product_created'] = $this->Marketplace_model->getProductByCode($product_code);
		$product_id = $data['new_product_created']->result()[0]->product_id;
		$product_tags = $_POST['product_tags'];
		$product_materials = $_POST['product_materials'];
		$dataTagsMaterials = array(
			'product_tags' => $product_tags,
			'product_materials' => $product_materials
		);
		$this->db->where('product_code', $product_code);
		$this->db->update('mrk_product', $dataTagsMaterials);
		// Finish and Redirect
		redirect($state . $product_code, 'refresh');
	}

	public function process_step_two_to_payment()
	{
		$product_code = $_POST['product_code'];
		$dataUpdateStatus = array(
			'product_status' => 8
		);
		$this->db->where('product_code', $product_code);
		$this->db->update('mrk_product', $dataUpdateStatus);
		// Finish and Redirect
		redirect('marketplace/post/payment/' . $product_code, 'refresh');
	}

	public function edit_basic_product_info()
	{
		// Slug Generation
		$product_code = $_POST['product_code'];
		$active_user = getMyID();
		$this->load->helper('url');
    $name = $_POST['product_name'];
    $url_title = url_title($name, '-', TRUE);
		$slug = 'PROD_' . $url_title . '_'. $product_code;

		// Var Serialize
		$product_author = $active_user;
		$product_name = '';
		$product_slug = $slug;
		$categorie_selector = '';
		$subcategorie_selector = '';
		$product_desc = '';
		$product_text = '';
		$product_price = '';
		$product_qty = '';
		$knowed_transport = '';
		$condition_send = '';
		// Generate Product Simple Collection
		if (!empty($_POST['product_name'])) {
			$product_name = $_POST['product_name'];
		}
		if (!empty($_POST['categorie_selector'])) {
			$categorie_selector = $_POST['categorie_selector'];
		}
		if (!empty($_POST['subcategorie_selector'])) {
			$subcategorie_selector = $_POST['subcategorie_selector'];
		}
		if (!empty($_POST['product_desc'])) {
			$product_desc = $_POST['product_desc'];
		}
		if (!empty($_POST['product_text'])) {
			$product_text = $_POST['product_text'];
		}
		if (!empty($_POST['product_price'])) {
			$product_price = $_POST['product_price'];
		}
		if (!empty($_POST['knowed_transport'])) {
			$knowed_transport = $_POST['knowed_transport'];
		}
		if (!empty($_POST['condition_send'])) {
			$condition_send = $_POST['condition_send'];
		}
		if (!empty($_POST['product_qty'])) {
			$product_qty = $_POST['product_qty'];
		}
		$dataBasic = array(
			 'product_author' => $product_author,
			 'product_name' => $product_name,
			 'product_subcategory' => $subcategorie_selector,
			 'product_desc' => $product_desc,
			 'product_text' => $product_text,
			 'product_slug' => $slug,
			 'product_code' => $product_code,
			 'product_price' => $product_price,
			 'product_qty' => $product_qty
		);
		$this->db->where('product_code', $product_code);
		$this->db->update('mrk_product', $dataBasic);

		// Update Basic Shipping Context
		// Radio Options
		foreach ($condition_send as $cond) {
			if ($cond == 'my_transport') {
				$dataShipping = array(
					'know_transport' => 1
				);
				$this->db->where('product_code', $product_code);
				$this->db->update('mrk_product_shipping', $dataShipping);
			}
			if ($cond == 'i_pick_up') {
				$dataShipping = array(
					'personal_withdrawal' => 1
				);
				$this->db->where('product_code', $product_code);
				$this->db->update('mrk_product_shipping', $dataShipping);
			}
			else{
				$dataShipping = array(
					'know_transport' => 0,
					'personal_withdrawal' => 0
				);
				$this->db->where('product_code', $product_code);
				$this->db->update('mrk_product_shipping', $dataShipping);
			}
		}
		// For Knowed Transport
		// Checkbox Options
		if ($knowed_transport == 'send_free') {
			$dataShipping = array(
				'owner_pay' => 1
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_shipping', $dataShipping);
		}
		if ($knowed_transport == 'send_buyer') {
			$dataShipping = array(
				'buyer_pay' => 1
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_shipping', $dataShipping);
		}
		else{
			$dataShipping = array(
				'buyer_pay' => 0,
				'owner_pay' => 0
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_shipping', $dataShipping);
		}

		// Finish and Redirect to Step Two
		redirect('marketplace/post/edit/' . $product_code, 'refresh');
	}

}
