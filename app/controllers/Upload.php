<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//
class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function uploadMainPic()
	{
		if ($this->ion_auth->logged_in()) {
			// Config Upload
			$product_code = $_POST['product_code'];
      $image_name = $_POST['image_name'];
      $ext = '.' . pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
			$file = 'userfile';
			$name = $image_name;
			$path = 'pictures';
			$type = 'products';
			$deletable = true;

			// Create Path for DB
			$picture_path = $type . '/' . $product_code . '/' . $path . '/' . $name .  $ext;

			// Upload DB
      if ($image_name == 'image_path_one') {
        $dataBasic = array(
           'image_path_one' => $picture_path
        );
        // Update DB
        $aditionalDataBasic = array(
           'product_img' => $picture_path
        );
  			$this->db->where('product_code', $product_code);
  			$this->db->update('mrk_product', $aditionalDataBasic);
      }elseif($image_name == 'image_path_two'){
        $dataBasic = array(
  			   'image_path_two' => $picture_path
  			);
      }elseif($image_name == 'image_path_three'){
        $dataBasic = array(
  			   'image_path_three' => $picture_path
  			);
      }elseif($image_name == 'image_path_four'){
        $dataBasic = array(
  			   'image_path_four' => $picture_path
  			);
      }
			// Update DB
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product_img_support', $dataBasic);
			// Do Upload File
			uploadFile($product_code, $file, $name, $path, $type, $ext, $deletable);
      // Finish and Redirect to Step Two
  		redirect('marketplace/post/second_step/' . $product_code, 'refresh');
		}
	}

	public function uploadSecondaryPic()
	{
		if ($this->ion_auth->logged_in()) {
			// Config Upload
			$advise_code = $_POST['advise_code'];
			// $ext = '.png';
			$ext = '.' . pathinfo($_FILES['userfile_two']['name'], PATHINFO_EXTENSION);
			$file = 'userfile_two';
			$name = 'secondary_pic';
			$path = 'pictures';
			$type = 'advises';
			$deletable = true;

			// Get Advise ID
			$advise_id = getAdviseNfoByCode($advise_code)['advise_id'];

			// Create Path for DB
			$main_pic = $type . '/' . $advise_code . '/' . $path . '/' . $name .  $ext;
			// $main_pic = 'test';

			// Upload DB
			$dataBasic = array(
			   'second_pic' => $main_pic
			);

			// Update DB
			$this->db->where('advise_id', $advise_id);
			$this->db->update('list_advise_img_support', $dataBasic);

			// Do Upload File
			uploadFile($advise_code, $file, $name, $path, $type, $ext, $deletable);
		}
	}

	public function uploadSlidePic()
	{
		if ($this->ion_auth->logged_in()) {
			// Config Upload
			$advise_code = $_POST['advise_code'];
			$advise_order = $_POST['slide_order'];

			// $ext = '.png';
			$ext = '.' . pathinfo($_FILES['slide_pic']['name'], PATHINFO_EXTENSION);
			$file = 'slide_pic';
			$name = 'slide' . $advise_order;
			$path = 'pictures';
			$type = 'advises';
			$deletable = true;

			// Get Advise ID
			$advise_id = getAdviseNfoByCode($advise_code)['advise_id'];

			// Create Path for DB
			$main_pic = $type . '/' . $advise_code . '/' . $path . '/' . $name .  $ext;
			// $main_pic = 'test';

			// Upload DB

			$dataBasic = array(
				'advise_id' => $advise_id,
				'img_path' => $main_pic,
				'img_order' => $advise_order
			);

			// Update OR Insert DB
			$checkStatusOfSlidePosition = checkIfSlidePicExist($advise_id, $advise_order);
			if ($checkStatusOfSlidePosition == true) {
				$this->db->where('advise_id', $advise_id);
				$this->db->update('list_advise_img_support', $dataBasic);
			}else{
				$this->db->where('advise_id', $advise_id);
				$this->db->insert('list_advise_img_slide_support', $dataBasic);
			}

			// Do Upload File
			uploadFile($advise_code, $file, $name, $path, $type, $ext, $deletable);
		}
	}


	public function uploadToS3()
	{
		// Call S3 Library and Configuration File
		include 'src/Aws/Common/s3.php';

		// Create S3 Client
		$client = $aws->get('S3');

		// Config Upload
		$redirect = $_POST['redirect'];
		$product_code = $_POST['product_code'];
		$image_name = $_POST['image_name'];
		$ext = '.' . pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
		$file = 'userfile';
		$name = $image_name;
		$path = 'pictures';
		$type = 'products';
		$deletable = true;
		$bucket = 'dahra-assets';
		$uri = 'uploads/' . $type . '/' . $product_code . '/' . $path . '/';
		$pathToFile = $_FILES['userfile']['tmp_name'];

		// Create Upload Collection for S3
		$result = $client->putObject(array(
		    'Bucket' => $bucket,
		    'Key' => $uri . $image_name . $ext,
		    'SourceFile' => $pathToFile,
				'ContentType' =>'image/jpeg',
        'ACL' => 'public-read'
		));

		$picture_path = $result['ObjectURL'];

		// Upload DB
		if ($image_name == 'image_path_one') {
			$dataBasic = array(
				 'image_path_one' => $picture_path
			);
			// Update DB
			$aditionalDataBasic = array(
				 'product_img' => $picture_path
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product', $aditionalDataBasic);
		}elseif($image_name == 'image_path_two'){
			$dataBasic = array(
				 'image_path_two' => $picture_path
			);
		}elseif($image_name == 'image_path_three'){
			$dataBasic = array(
				 'image_path_three' => $picture_path
			);
		}elseif($image_name == 'image_path_four'){
			$dataBasic = array(
				 'image_path_four' => $picture_path
			);
		}

		// Update DB
		$this->db->where('product_code', $product_code);
		$this->db->update('mrk_product_img_support', $dataBasic);

		// Finish and Redirect to Step Two
		redirect($redirect, 'refresh');

	}



	public function uploadProfilePicToS3()
	{
		// Call S3 Library and Configuration File
		include 'src/Aws/Common/s3.php';

		// Create S3 Client
		$client = $aws->get('S3');

		// Config Upload
		$artist_id = $_POST['artist_id'];
		$artist_id_slug = $_POST['artist_id_slug'];
		$image_name = $_POST['image_name'];
		$ext = '.' . pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
		$file = 'userfile';
		$name = $image_name;
		$path = 'profile';
		$type = 'artists';
		$deletable = true;
		$bucket = 'dahra-assets';
		$uri = 'uploads/' . $type . '/' . $artist_id_slug . '/' . $path . '/';
		$pathToFile = $_FILES['userfile']['tmp_name'];

		// Create Upload Collection for S3
		$result = $client->putObject(array(
		    'Bucket' => $bucket,
		    'Key' => $uri . $image_name . $ext,
		    'SourceFile' => $pathToFile,
				'ContentType' =>'image/jpeg',
        'ACL' => 'public-read'
		));

		$picture_path = $result['ObjectURL'];
		$dataBasic = array(
			 'artist_picture' => $picture_path
		);

		// Update DB
		$this->db->where('artist_id', $artist_id);
		$this->db->update('artist_information', $dataBasic);

		// Finish and Redirect to Step Two
		redirect('dashboard/configuration/my_profile/', 'refresh');

	}

	public function uploadAdsToS3()
	{
		// Call S3 Library and Configuration File
		include 'src/Aws/Common/s3.php';
		// Create S3 Client
		$client = $aws->get('S3');
		// Config Upload
		$redirect = $_POST['redirect'];
		$product_code = $_POST['product_code'];
		$image_name = $_POST['image_name'];
		$ext = '.' . pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
		$file = 'userfile';
		$name = $image_name;
		$path = 'pictures';
		$type = 'ads';
		$deletable = true;
		$bucket = 'dahra-assets';
		$uri = 'uploads/' . $type . '/' . $product_code . '/' . $path . '/';
		$pathToFile = $_FILES['userfile']['tmp_name'];
		// Create Upload Collection for S3
		$result = $client->putObject(array(
		    'Bucket' => $bucket,
		    'Key' => $uri . $image_name . $ext,
		    'SourceFile' => $pathToFile,
				'ContentType' =>'image/jpeg',
        'ACL' => 'public-read'
		));
		$picture_path = $result['ObjectURL'];
		// Update DB
		$aditionalDataBasic = array(
			 'mkt_ads_image' => $picture_path
		);
		$this->db->where('mkt_ads_adscode', $product_code);
		$this->db->update('mkt_ads', $aditionalDataBasic);
		// Finish and Redirect to Step Two
		redirect($redirect, 'refresh');
	}

}
