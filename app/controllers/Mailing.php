<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ads_model');
		$this->load->model('Marketplace_model');
		$this->load->model('User_model');
		$this->load->model('Site_model');
	}

	public function published_advise($advise_code) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'advise_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['advise_info'] = $this->Marketplace_model->getProductByCode($advise_code);

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/' . $data['advise_info']->result()[0]->product_slug;

		// Mail Vars
		$data['mail_head'] = 'Publicacion Realizada';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Ver Publicacion';
		$data['path_in_button'] = $pathValue;

		// Addons Republished
		$data['republished_assets'] = FALSE;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
	  $this->Site_model->startMailProtocole($mailNfo);
	  // Mail Collection End
	}

	public function send_invoice($recipe_id) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'invoice_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Mail Vars
		$data['mail_head'] = 'Publicacion Realizada';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Ver Publicacion';

		// Addons Republished
		$data['republished_assets'] = FALSE;

		// Recipe Information
		$data['site_information'] = $this->Site_model->getSiteConfig();
		$data['site_contact'] = $this->Site_model->getSiteContact();
		$data['recipe_information'] = $this->Marketplace_model->getCompleteRecipesByRecipeID($recipe_id);
		$data['recipe_id'] = $recipe_id;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
	  $this->Site_model->startMailProtocole($mailNfo);
	  // Mail Collection End

		redirect(base_url() . 'dashboard/subscription/','refresh');
	}

	public function finished_advise($advise_code) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'advise_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['advise_info'] = $this->Marketplace_model->getProductByCode($advise_code);

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/' . $data['advise_info']->result()[0]->product_slug;

		// Mail Vars
		$data['mail_head'] = 'Publicacion Finalizada';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Republicar';
		$data['path_in_button'] = $pathValue;

		// Addons Republished
		$data['republished_assets'] = TRUE;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
	  $this->Site_model->startMailProtocole($mailNfo);
	  // Mail Collection End
	}

	public function republished_advise($advise_code) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'advise_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['advise_info'] = $this->Marketplace_model->getProductByCode($advise_code);

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/' . $data['advise_info']->result()[0]->product_slug;

		// Mail Vars
		$data['mail_head'] = 'Republicacion Realizada';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Ver Publicacion';
		$data['path_in_button'] = $pathValue;

		// Addons Republished
		$data['republished_assets'] = FALSE;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
	  $this->Site_model->startMailProtocole($mailNfo);
	  // Mail Collection End
	}

	public function question_to_user($question_id) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'question_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['question_info'] = $this->Marketplace_model->getQuestionsByQuestionIDWUserNfo($question_id);

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/';

		// Mail Vars
		$data['mail_head'] = '';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Ver Pregunta';
		$data['path_in_button'] = base_url() . 'dashboard/sales/questions';

		// Addons Republished
		$data['question_actual_user'] = true;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
		$this->Site_model->startMailProtocole($mailNfo);
		// Mail Collection End

		redirect('dashboard/sales/questions/', 'refresh');
	}

	public function question_to_user_product_view($question_id) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'question_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['question_info'] = $this->Marketplace_model->getQuestionsByQuestionIDWUserNfo($question_id);
		$product_id = $data['question_info']->result()[0]->product_id;
		$product_nfo = $this->Marketplace_model->getProductByID($product_id);
		$product_slug = $product_nfo->result()[0]->product_slug;

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/';

		// Mail Vars
		$data['mail_head'] = '';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Ver Pregunta';
		$data['path_in_button'] = base_url() . 'dashboard/sales/questions';

		// Addons Republished
		$data['question_actual_user'] = true;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
		$this->Site_model->startMailProtocole($mailNfo);
		// Mail Collection End

		redirect('marketplace/product/' . $product_slug, 'refresh');
	}

	public function answer_to_user($question_id) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'question_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['question_info'] = $this->Marketplace_model->getQuestionsByQuestionIDWUserNfo($question_id);
		$data['answers_info'] = $this->Marketplace_model->getAnswersToQuestion($question_id);

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/';

		// Mail Vars
		$data['mail_head'] = '';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Contactar';
		$data['path_in_button'] = base_url() . 'dashboard/sales/questions';

		// Addons Republished
		$data['question_actual_user'] = false;

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
	  $this->Site_model->startMailProtocole($mailNfo);
	  // Mail Collection End
	}

	public function contact_user ($contact_user) {

		// Template Information
		$data['layout'] = 'plugins/mailing';
		$data['page'] = 'contact_theme';

		// Page Information
		$data['title'] = 'Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Stats From Site
		$data['site_info'] = $this->Site_model->getSiteContact();
		$data['total_advises'] = $this->Marketplace_model->countTotalAdvisesLoad();
		$data['total_amount_advises'] = $this->Marketplace_model->countAllValuesFromAdvisesLoad();
		$data['total_contacts'] = $this->Marketplace_model->countTotalCommercialContacts();

		// Get Advise Information
		$data['contact_information'] = $this->Marketplace_model->getContactUserByContactID($contact_user);

		// Generate Values
		$pathValue = base_url() . 'marketplace/product/';

		// Mail Vars
		$data['mail_head'] = '';
		$data['mail_text_support'] = '';
		$data['txt_in_button'] = 'Contactar';
		$data['path_in_button'] = base_url() . 'dashboard/sales/questions';

		// Addons Republished
		$data['question_actual_user'] = false;

		$this->load->view('layout/' . $data['layout'], $data);

		// Mail Template Configuration
		$mail_template = $this->load->view('layout/' . $data['layout'], $data, true);
		// Mail Configuration
		$message =  $mail_template;
		$userMail = $this->Site_model->getSiteConfig()->result()[0]->site_mail;
		// Mail Information
		$mailNfo = array(
			'sendto' => $userMail,
			'message' =>  $mail_template,
			'subject' => $data['title']
		);
	  $this->Site_model->startMailProtocole($mailNfo);
	  // Mail Collection End

		// Redirect to Dashboard
		redirect('dashboard/buys/favorites/', 'refresh');
	}

}
