<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Marketplace_model');
		$this->load->model('User_model');
		$this->load->model('Ads_model');
		$this->load->model('Site_model');
	}

	public function index() {
		$user_id = getMyID();

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Home/Home';

		// Page Information
		$data['title'] = 'Dahra | Inicio';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['product_list'] = $this->Marketplace_model->getProductsAvailable();
		$data['category_list'] = $this->Marketplace_model->getCategories();
		$data['artist_list'] = $this->Marketplace_model->getArtists();
		$data['products_by_category'] = $this->Marketplace_model->getProductsWithCategories();

		// General Information
		$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
		$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

		// Metrics
		$data['ads_list'] = $this->Ads_model->getAdsAvailables();

		// $collection_transaction = array(
		// 	'name' => "Venta de 1000 tokens al Asuario Admin Istrator",
		// 	'amount' => 5000,
		// 	'reference' => "CRED_asdasfqweeqwe",
		// 	'description' => "Venta 1000 Tokens: Usuario Admin Istrator"
		// );
		// $this->Site_model->insertOperationInCashflowIngreseType($collection_transaction);

		$this->load->view('layout/' . $data['layout'], $data);
	}

}
