<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Marketplace_model');
	}

	public function contact(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/Contact';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Inicio';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function about(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/About';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Sobre Nosotros';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function terms(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/Terms';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Terminos y Condiciones';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function privacity(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/Privacity';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Politicas de Privacidad';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function atention(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/Atention';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Centro de Atencion';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function services(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/Services';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Servicios Dahra';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function advises(){
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Internal/Advises';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Consejos y Recomendaciones';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}
}
