<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ads extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ads_model');
		$this->load->model('Marketplace_model');
		$this->load->model('User_model');
	}

	public function adsRedirect($adsCode) {

		// Get Ads Information
		$data['ads_props'] = $this->Ads_model->getAdsByAdsCode($adsCode);
		foreach ($data['ads_props']->result() as $ads_nfo) {
			$ads_url = $ads_nfo->mkt_ads_url;
		}

		// Get IP Information
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		// Get Platform Information
		$navegador = get_browser(null, true);
		$browser = $navegador['browser'];
		$platform = $navegador['platform'];

		// Prepare Clic Count Collection
		$dateAdsCount = array(
			'mkt_ads_click_count_date' => $date = date('Y-m-d'),
			'mkt_ads_click_count_ads_code' => $adsCode,
			'mkt_ads_click_count_url' => $ads_url,
			'mkt_ads_click_count_ip' => $ip,
			'mkt_ads_click_count_browser' => $browser,
			'mkt_ads_click_count_platform' => $platform
		);
		$this->db->insert('mkt_ads_click_count', $dateAdsCount);

		redirect($ads_url,'refresh');
	}

	// Register Methods
	public function register(){
		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Ads/Register';
		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();
		// Page Information
		$data['title'] = 'Dahra | Crear Cuenta';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}
	public function createUserPublicistAction() {
		if ($this->ion_auth->logged_in()) {
			redirect('Home','refresh');
		}else{
			$regname = $_POST['first_name'];
			$reglastname = $_POST['last_name'];
			$regphone = $_POST['phone'];
			$regmail = $_POST['email'];
			$regdni = $_POST['dni'];
			$regcuit = $_POST['cuit'];
			$regpass = $_POST['password'];
			$additional_data = array(
				'first_name' => $regname,
				'last_name' => $reglastname,
				'username' => $regmail,
				'dni' => $regdni,
				'cuit' => $regcuit,
				'phone' => $regphone
			);
			$group = array('3');
			$this->ion_auth->register($regmail, $regpass, $regmail, $additional_data, $group);
			redirect('user/login', 'refresh');
		}
	}
	// Register Methods End

	// View and Holders
	public function dashboard(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Panel de Control';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Resumen de Cuenta';
			$data['module'] = 'resume';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Info for Dashboard
			$data['ads_active'] = $this->Ads_model->countAdsByPublicistStatus($user_id, 1);
			$data['ads_paused'] = $this->Ads_model->countAdsByPublicistStatus($user_id, 2);
			$data['ads_ended'] = $this->Ads_model->countAdsByPublicistStatus($user_id, 0);
			$data['ads_not_actived'] = $this->Ads_model->countAdsByPublicistStatus($user_id, 8);
			$data['total_clic_count'] = $this->Ads_model->countAllClicByAdsAuthor($user_id);
			$data['total_clic_count_actual_month'] = $this->Ads_model->countAllClicsByAdsAuthorByActualMonth($user_id);
			$data['total_print_count'] = $this->Ads_model->countAllPrintsByAdsAuthor($user_id);
			$data['total_print_count_actual_month'] = $this->Ads_model->countAllPrintsByAdsAuthorByActualMonth($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	// Stats Methods
	public function stats(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Dashboard';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Estadisticas';
			$data['module'] = 'stats';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['ads_list'] = $this->Ads_model->getAdviseByUserID($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function ads_stats_detail($ads_code) {
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();
			$property_validation = $this->Ads_model->checkIfUserOwnAds($user_id, $ads_code);
			if ($property_validation == true) {
				// Template Information
				$data['layout'] = 'Initial';
				$data['page'] = 'page/Ads/Dashboard';

				// Info Data Products
				$data['category_list'] = $this->Marketplace_model->getCategories();
				$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

				// Page Information
				$data['title'] = 'Dahra | Dashboard';
				$data['description'] = 'Consegui contactos comerciales de calidad hoy';
				$data['keywords'] = 'contactos; ventas, vendedores, bla';
				$data['author'] = 'Match Trade SRL';

				// Active Module
				$data['module_title'] = 'Estadisticas';
				$data['module'] = 'ads_stats_details';

				// General Information
				$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
				$data['ads_info'] = $this->Ads_model->getAdsByAdsCode($ads_code);
				$data['info_pie_graph_prints'] = $this->Ads_model->getCountPrintsDataFromAdsByAdsCodePieGraph($ads_code);
				$data['info_pie_graph_clics'] = $this->Ads_model->getCountClicsDataFromAdsByAdsCodePieGraph($ads_code);
				$data['info_stats_historical_clics'] = $this->Ads_model->getCountClicHistoricalByAdsCode($ads_code);
				$data['info_stats_historical_prints'] = $this->Ads_model->getCountPrintHistoricalByAdsCode($ads_code);

				$this->load->view('layout/' . $data['layout'], $data);
			}else{
				redirect(base_url() . 'publicist/stats/','refresh');
			}
		}else{
			redirect(base_url(),'refresh');
		}
	}

	// Stats Methods End
	// Subscription Methods
	public function subscription(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Registro de Creditos y Facturacion';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Active Module
			$data['module_title'] = 'Registro de Creditos y Facturacion';
			$data['module'] = 'subscription';

			// Balance Stats
			$data['credit_balance'] = $this->Marketplace_model->getCreditOperationsByUserID($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function subscription_history(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Facturacion Historica';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Facturacion: Historial';
			$data['module'] = 'subscription_history';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	// Subscription Methods End

	// Personal Data Methods
	public function personal_data(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$user_id = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Informacion Personal';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Information
			if (checkIFHaveArtistHelper() == TRUE) {
				$data['user_information'] = $this->User_model->getUserInformation($user_id);
				foreach ($data['user_information']->result() as $usr_nfo) {
					if ($usr_nfo->artist_country != 0) {
						$data['artist_country'] = $this->Location_model->getPais();
						$data['artist_province'] = $this->Location_model->getProvinciaByPais($usr_nfo->artist_country);
						$data['artist_party'] = $this->Location_model->getPartidoByProvincia($usr_nfo->artist_province);
						$data['artist_location'] = $this->Location_model->getLocalidadByPartido($usr_nfo->artist_party);
						$data['artist_neighbour'] = $this->Location_model->getBarrioByLocalidad($usr_nfo->artist_location);
						$data['artist_subneighbour'] = $this->Location_model->getSubBarrioByBarrio($usr_nfo->artist_neighbour);
						$data['statusData'] = TRUE;
					}else{
						$data['statusData'] = FALSE;
					}
				}
			}else{
				$data['user_information'] = $this->User_model->getUserInformationSimple($user_id);
			}

			// Active Module
			$data['module_title'] = 'Datos Personales';
			$data['module'] = 'personal_data';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function save_Personal_Data()
	{
		if ($this->ion_auth->logged_in()) {

			// User Basic Information
			$user_id = getMyID();
			$first_name = '';
			$last_name = '';
			$dni = '';
			$cuit = '';
			$facebook = '';
			$twitter = '';
			// User Basic Information Serialization
			if (!empty($_POST['first_name'])) {
				$first_name = $_POST['first_name'];
			}
			if (!empty($_POST['last_name'])) {
				$last_name = $_POST['last_name'];
			}
			if (!empty($_POST['dni'])) {
				$dni = $_POST['dni'];
			}
			if (!empty($_POST['cuit'])) {
				$cuit = $_POST['cuit'];
			}
			if (!empty($_POST['facebook'])) {
				$facebook = $_POST['facebook'];
			}
			if (!empty($_POST['twitter'])) {
				$twitter = $_POST['twitter'];
			}
			$dataBasic = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'dni' => $dni,
				'cuit' => $cuit,
				'facebook' => $facebook,
				'twitter' => $twitter
			);
			$this->db->where('id', $user_id);
			$this->db->update('users', $dataBasic);

			// User Artist Information
			$checkTax = $this->User_model->checkIfUserHaveArtist($user_id);
			$artist_birth = '';
			$artist_mail = '';
			$artist_phone = '';
			$artist_adress = '';
			$artist_adress_number = '';
			$artist_zipcode = '';
			$country_selector = '';
			$province_selector = '';
			$party_selector = '';
			$locality_selector = '';
			$suburb_selector = '';
			$subsuburb_selector = '';
			if (!empty($_POST['artist_birth'])) {
				$artist_birth = $_POST['artist_birth'];
			}
			if (!empty($_POST['artist_mail'])) {
				$artist_mail = $_POST['artist_mail'];
			}
			if (!empty($_POST['artist_phone'])) {
				$artist_phone = $_POST['artist_phone'];
			}
			if (!empty($_POST['artist_adress'])) {
				$artist_adress = $_POST['artist_adress'];
			}
			if (!empty($_POST['artist_adress_number'])) {
				$artist_adress_number = $_POST['artist_adress_number'];
			}
			if (!empty($_POST['artist_zipcode'])) {
				$artist_zipcode = $_POST['artist_zipcode'];
			}
			if (!empty($_POST['country_selector'])) {
				$country_selector = $_POST['country_selector'];
			}
			if (!empty($_POST['province_selector'])) {
				$province_selector = $_POST['province_selector'];
			}
			if (!empty($_POST['party_selector'])) {
				$party_selector = $_POST['party_selector'];
			}
			if (!empty($_POST['locality_selector'])) {
				$locality_selector = $_POST['locality_selector'];
			}
			if (!empty($_POST['suburb_selector'])) {
				$suburb_selector = $_POST['suburb_selector'];
			}
			if (!empty($_POST['subsuburb_selector'])) {
				$subsuburb_selector = $_POST['subsuburb_selector'];
			}
			$dataAdvance = array(
				'artist_name' => $first_name,
				'artist_lastname' => $last_name,
				'artist_birth' => $artist_birth,
				'artist_mail' => $artist_mail,
				'artist_phone' => $artist_phone,
				'artist_adress' => $artist_adress,
				'artist_adress_number' => $artist_adress_number,
				'artist_zipcode' => $artist_zipcode,
				'artist_country' => $country_selector,
				'artist_province' => $province_selector,
				'artist_party' => $party_selector,
				'artist_location' => $locality_selector,
				'artist_neighbour' => $suburb_selector,
				'artist_subneighbour' => $subsuburb_selector
			);
			if ($checkTax) {
				$this->db->where('user_id', $user_id);
				$this->db->update('artist_information', $dataAdvance);
			}else{
				$this->db->insert('artist_information', $dataAdvance);
			}
			redirect('dashboard/configuration/personal_data','refresh');

		}
	}
	public function security_data(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Iniciar Sesion';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Seguridad';
			$data['module'] = 'security_data';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	// Personal Data Methods End

	// Credit Market Methods
	public function creditMarket() {

		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);
		}

		// Basic Information
		$data['filter_info'] = '';

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/Ads/Credits';

		// Page Information
		$data['title'] = 'Dahra | Mercado de Creditos';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// SubCategory Info
		$data['filter_title'] = 'Mercado de Creditos';
		$data['filter_breadcrum_point'] = 1;
		$data['filter_breadcrum_father_info'] = '';
		$data['filter_breadcrum_type'] = 'subcategory';

		// Sidebar Information
		$data['category_information'] = $this->Marketplace_model->getCategories();
		$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();
		$user_id = getMyID();
		$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

		// Store Configuration
		$data['user_info'] =  $this->User_model->getBuyerUserInformation($user_id);
		$data['store_configuration'] = $this->Marketplace_model->getStoreConfiguration();
		$data['point_plans'] = $this->Marketplace_model->getCreditPlans();

		$this->load->view('layout/' . $data['layout'], $data);
	}
	// Credit Market Methods End

	public function ads(){
		if ($this->ion_auth->logged_in()) {
			// Template Information
			$user_id = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';
			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			// Page Information
			$data['title'] = 'Dahra | Avisos';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';
			// Active Module
			$data['module_title'] = 'Avisos';
			$data['module'] = 'ads_publications';
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			// Get Ads Information
			$data['sales_list'] = $this->Ads_model->getAdviseByUserID($user_id);

			$this->load->view('layout/' . $data['layout'], $data);
		}else{
			redirect(base_url(),'refresh');
		}
	}


	// Actions for Users
	public function status_publication(){
		if ($this->ion_auth->logged_in()) {

			$publication_status = $_POST['mkt_status'];
			$publication_id = $_POST['mkt_ads_id'];
			$dataBasic = array(
				 'mkt_status' => $publication_status
			);
			$this->db->where('mkt_ads_id', $publication_id);
			$this->db->update('mkt_ads', $dataBasic);
			redirect('publicist/ads','refresh');

		}else{
			redirect(base_url(),'refresh');
		}
	}
	// Actions for Users End















	// Publication and Payment Method

	public function create_ads(){

		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Publicar Producto';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Publicar Aviso: Paso 1';
			$data['module'] = 'uploads/create_ads';

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function create_new_second_step($ads_code){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			$data['ads_code'] = $ads_code;
			$data['new_ads_created'] = $this->Ads_model->getAdsByCode($ads_code);
			$ads_id = $data['new_ads_created']->result()[0]->mkt_ads_id;

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Publicar Producto Paso Dos';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Publicar Aviso: Paso 2';
			$data['module'] = 'uploads/create_ads_two';

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function edit_ads($ads_code){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			$data['product_information'] = $this->Marketplace_model->getProductByCode($ads_code);
			$data['product_code'] = $ads_code;

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Page Information
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['title'] = 'Dahra | Editar Producto';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Editar Aviso';
			$data['module'] = 'uploads/edit_ads';

			// Information
			$data['ads_code'] = $ads_code;
			$data['new_ads_created'] = $this->Ads_model->getAdsByCode($ads_code);
			$ads_id = $data['new_ads_created']->result()[0]->mkt_ads_id;

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function ads_publication_payment($Ads_code){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			$data['product_code'] = $Ads_code;

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Ads/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Publicar Aviso Acordar Pago';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Calcular Precio de Publicacion';
			$data['module'] = 'uploads/payment_ads';

			$data['ads_nfo'] = $this->Ads_model->getAdsByCode($Ads_code);
			$data['payment_methods'] = $this->Ads_model->getAdsPlansAll();

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}


	public function create_new_product()
	{
		$active_user = getMyID();
		// Product Code Generation
		$product_code = generateID();
		// Slug Generation
		$ads_code = 'ADS_'. $product_code;
		// Var Serialize
		$ads_author = $active_user;
		$ads_name = '';
		$ads_text = '';
		$ads_adscode = $ads_code;
		$ads_url = '';
		$ads_user_author = $active_user;
		// Generate Ads Simple Collection
		if (!empty($_POST['ads_title'])) {
			$ads_name = $_POST['ads_title'];
		}
		if (!empty($_POST['ads_url'])) {
			$ads_url = $_POST['ads_url'];
		}
		if (!empty($_POST['ads_text'])) {
			$ads_text = $_POST['ads_text'];
		}
		$dataBasic = array(
			 'mkt_ads_title' => $ads_name,
			 'mkt_ads_adscode' => $ads_code,
			 'mkt_ads_desc' => $ads_text,
			 'mkt_ads_url' => $ads_url,
			 'mkt_ads_user_author' => $ads_author,
			 'mkt_status' => 8
		);
		// Insert Ads Simple Collection
		$this->db->insert('mkt_ads', $dataBasic);

		// Finish and Redirect to Step Two
		redirect('ads/publication/create_ads_second_step/' . $ads_code, 'refresh');
	}

	public function process_step_two_to_payment()
	{
		$ads_code = $_POST['ads_code'];
		$dataUpdateStatus = array(
			'mkt_status' => 8
		);
		$this->db->where('mkt_ads_adscode', $ads_code);
		$this->db->update('mkt_ads', $dataUpdateStatus);
		// Finish and Redirect
		redirect('ads/publication/payment_publication/' . $ads_code, 'refresh');
	}

	public function edit_basic_ads()
	{
		$ads_code = $_POST['mkt_ads_adscode'];
		// Var Serialize
		$ads_name = '';
		$ads_text = '';
		$ads_url = '';
		// Generate Ads Simple Collection
		if (!empty($_POST['ads_title'])) {
			$ads_name = $_POST['ads_title'];
		}
		if (!empty($_POST['ads_url'])) {
			$ads_url = $_POST['ads_url'];
		}
		if (!empty($_POST['ads_text'])) {
			$ads_text = $_POST['ads_text'];
		}
		// Edit Ads Simple Collection
		$dataBasic = array(
			'mkt_ads_title' => $ads_name,
			'mkt_ads_desc' => $ads_text,
			'mkt_ads_url' => $ads_url
		);
		$this->db->where('mkt_ads_adscode', $ads_code);
		$this->db->update('mkt_ads', $dataBasic);

		// Finish and Redirect to Step Two
		redirect('ads/publication/edit_ads/' . $ads_code, 'refresh');
	}

	public function save_ads_terms()
	{
		if ($this->ion_auth->logged_in()) {
			// Sanitize Vars
			$user_id = getMyID();
			$mkt_ads_adscode = $_POST['mkt_ads_adscode'];
			$final_val_form_clics = $_POST['final_val_form_clics'];
			$final_val_form_prints = $_POST['final_val_form_prints'];
			$final_val_form_exposition = $_POST['final_val_form_exposition'];
			$final_token_cost = $_POST['final_token_cost'];
			// Activate ADvise and Apply Exposition, Clicks and Prints
			$dataBasic = array(
				'mkt_ads_clickpointer' => $final_val_form_clics,
				'mkt_ads_print_initial' => $final_val_form_prints,
				'mkt_ads_relevance' => $final_val_form_exposition,
				'mkt_status' => 1
			);
			$this->db->where('mkt_ads_adscode', $mkt_ads_adscode);
			$this->db->update('mkt_ads', $dataBasic);
			// Prepare Credit Operation Collection
			$mkt_ads_adscode = generateID();
			$mrk_credit_operation_operation_id = 'CRED_OP_'. $mkt_ads_adscode;
			$reduceCreditOperation = array(
				'mrk_credit_operation_user_id' => $user_id,
				'mrk_credit_operation_qty' => $final_token_cost,
				'mrk_credit_operation_type' => 2,
				'mrk_credit_operation_operation_id' => $mrk_credit_operation_operation_id
			);
			$this->db->insert('mrk_credit_operation', $reduceCreditOperation);
			// Prepare Credit Operation Collection End
			redirect('publicist/ads','refresh');
		}
	}

	public function getPlansInformation()
	{
		$list_crude = $this->Ads_model->getAdsPlansAll();
		$list = array();
		foreach ($list_crude->result() as $bs_list) {
			$dataBasic = array(
				 'mkt_ads_plans_id' => $bs_list->mkt_ads_plans_id,
			   'mkt_ads_plans_title' => $bs_list->mkt_ads_plans_title,
				 'mkt_ads_plans_prints' => $bs_list->mkt_ads_plans_prints,
				 'mkt_ads_plans_clics' => $bs_list->mkt_ads_plans_clics,
				 'mkt_ads_plans_qty_cost' => $bs_list->mkt_ads_plans_qty_cost
			);
			array_push($list, $dataBasic);
		}
		echo json_encode($list);
	}
	// Publication and Payment Method End

	// Payment Acreditation Method
	public function payment_process_success() {
		if ($this->ion_auth->logged_in()) {
			$user_buyer = getMyID();
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);

			// Basic Information
			$data['filter_info'] = '';

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/Marketplace/Payment_result';

			// Page Information
			$data['title'] = 'Dahra | Mercado de Creditos';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();

			// SubCategory Info
			$data['filter_title'] = 'Mercado de Creditos';
			$data['filter_breadcrum_point'] = 1;
			$data['filter_breadcrum_father_info'] = '';
			$data['filter_breadcrum_type'] = 'subcategory';

			// Sidebar Information
			$data['category_information'] = $this->Marketplace_model->getCategories();
			$data['subcategory_information'] = $this->Marketplace_model->getSubCategories();
			$user_id = getMyID();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Store Configuration
			$data['user_info'] =  $this->User_model->getBuyerUserInformation($user_id);
			$data['store_configuration'] = $this->Marketplace_model->getStoreConfiguration();
			$data['point_plans'] = $this->Marketplace_model->getCreditPlans();
			$external_reference = $_GET['external_reference'];
			$operation_payment_check = $this->Marketplace_model->checkIfMPIsStored($external_reference);
			$operation_mp_check = $this->Marketplace_model->checkIfPaymentIsStored($external_reference);
			if ($operation_payment_check == true AND $operation_payment_check == true) {
				redirect(base_url() . 'publicist/subscription/','refresh');
			}else{
				// Prepare MercadoPago Operation Collection
				$collection_id = $_GET['collection_id'];
				$collection_status = $_GET['collection_status'];
				$preference_id = $_GET['preference_id'];
				$payment_type = $_GET['payment_type'];
				$merchant_order_id = $_GET['merchant_order_id'];
				$filtered_external_reference = explode('_', $external_reference);
				$mpOperationDataBasic = array(
					'external_reference' => $external_reference,
					'collection_id' => $collection_id,
					'collection_status' => $collection_status,
					'preference_id' => $preference_id,
					'payment_type' => $payment_type,
					'merchant_order_id' => $merchant_order_id,
					'plane_code' => $filtered_external_reference[8]
				);
				$this->db->insert('site_recipe_entry_register_mp', $mpOperationDataBasic);
				// Prepare MercadoPago Operation Collection End
				// Prepare Operation Collection
				$operationDataBasic = array(
					'user_id' => $filtered_external_reference[2],
					'date' => gmdate("Y-m-d H:i:s", $filtered_external_reference[4]),
					'amount' => $filtered_external_reference[6],
					'code' => $filtered_external_reference[8],
					'external_reference' => $external_reference,
					'state' => $collection_status,
					'plan_id' => $filtered_external_reference[10]
				);
				$this->db->insert('site_recipe_entry_register', $operationDataBasic);
				$recipe_id = $this->db->insert_id();
				// Prepare Operation Collection End
				// Prepare Credit Operation Collection
				$credit_operation_code = 'CRED_OP_'. $filtered_external_reference[8];
				$pointPlanDetails = $this->Marketplace_model->getPointPlanDetails($filtered_external_reference[10]);
				$credit_qty_buyed = $pointPlanDetails->result()[0]->mrk_point_plans_qty;
				$creditOperationDataBasic = array(
					'mrk_credit_operation_user_id' => $user_id,
					'mrk_credit_operation_qty' => $credit_qty_buyed,
					'mrk_credit_operation_type' => 1,
					'mrk_credit_operation_operation_id' => $credit_operation_code,
					'mrk_credit_operation_operation_plane_code' => $filtered_external_reference[8]
				);
				$this->db->insert('mrk_credit_operation', $creditOperationDataBasic);

				$this->load->helper('tusfacturas');
				$data['recipe_information'] = $this->Marketplace_model->getCompleteRecipesByRecipeID($recipe_id);
				$payment_billing_collection = $data['recipe_information']->result()[0];
				generateBillingTypeC($payment_billing_collection);
				// Prepare Credit Operation Collection End
				redirect(base_url() . 'publicist/subscription/','refresh');
			}
		}else{
			redirect(base_url(),'refresh');
		}
	}

}
