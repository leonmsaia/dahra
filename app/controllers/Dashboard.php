<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Marketplace_model');
		$this->load->model('User_model');
		$this->load->model('Location_model');
		$this->load->model('Site_model');
	}

	// View and Holders
	public function dashboard(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			if (checkIFHaveArtistHelper()) {
				$data['artist_info'] = $this->Marketplace_model->getArtistByUserID(getMyID());
				$product_author = $data['artist_info']->result()[0]->user_id;
			}
			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Panel de Control';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Resumen';
			$data['module'] = 'resume';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Info for Dashboard
			if (checkIFHaveArtistHelper()) {
				$data['publications_active'] = $this->Marketplace_model->countProductsByArtistStatus($product_author, 1);
				$data['publications_paused'] = $this->Marketplace_model->countProductsByArtistStatus($product_author, 2);
				$data['publications_ended'] = $this->Marketplace_model->countProductsByArtistStatus($product_author, 0);
				$data['publications_not_actived'] = $this->Marketplace_model->countProductsByArtistStatus($product_author, 8);
				$data['questions_actives'] = $this->Marketplace_model->countQuestionsByArtistStatus($product_author, 0);
				$data['num_of_visits'] = $this->User_model->countAllVisitsByUserID($user_id);
			}

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function stats(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Dashboard';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Estadisticas';
			$data['module'] = 'stats';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Stats Information
			$data['most_visited_prods'] = $this->User_model->countVisitedProdsBySumByUserID($user_id);
			$data['visited_by_day'] = $this->User_model->countVisitedProdsBySumByUserIDByDate($user_id);
			$data['most_consult_prod'] = $this->User_model->countConsultProdsBySumByUser($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function subscription(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Registro de Creditos y Facturacion';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Active Module
			$data['module_title'] = 'Registro de Creditos y Facturacion';
			$data['module'] = 'subscription';

			// Balance Stats
			$data['credit_balance'] = $this->Marketplace_model->getCreditOperationsByUserID($user_id);
			$data['recipe_register'] = $this->Marketplace_model->getCompleteRecipesByUserID($user_id);
			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function recipe_gen($recipe_id) {
		if ($this->ion_auth->logged_in()) {
			$this->load->helper('tusfacturas');
			$user_id = getMyID();
			// Page Information
			$data['title'] = 'Dahra | Factura N° ' . $recipe_id;
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';
			$data['site_information'] = $this->Site_model->getSiteConfig();
			$data['site_contact'] = $this->Site_model->getSiteContact();
			$data['recipe_information'] = $this->Marketplace_model->getCompleteRecipesByRecipeID($recipe_id);
			$this->load->view('page/User/dashboard/invoices/recipe', $data);
		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function subscription_history(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Facturacion Historica';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Facturacion: Historial';
			$data['module'] = 'subscription_history';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function buys(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$user_id = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Compras';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Compras';
			$data['module'] = 'buys';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Info for Dashboard
			$data['publications_active'] = $this->Marketplace_model->getBuysByUser($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function buys_favorites(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$faver_user = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($faver_user);

			// Page Information
			$data['title'] = 'Dahra | Favoritos';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Favoritos';
			$data['module'] = 'buys_favorites';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($faver_user);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($faver_user);

			$data['fav_artist'] = $this->Marketplace_model->getUserFavoriteArtist($faver_user);
			$data['fav_products'] = $this->Marketplace_model->getUserFavoriteProducts($faver_user);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function buys_questions(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$question_author = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($question_author);

			// Page Information
			$data['title'] = 'Dahra | Intereses';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Intereses';
			$data['module'] = 'buys_questions';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($question_author);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($question_author);

			$data['user_question_list'] = $this->Marketplace_model->getQuestionsMadeItByUser($question_author);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function sales(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$user_buyer = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);

			// Page Information
			$data['title'] = 'Dahra | Ventas';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Ventas';
			$data['module'] = 'sales';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_buyer);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_buyer);


			$data['sales_actives'] = $this->Marketplace_model->getSalesByUserByState($user_buyer, 0);
			$data['sales_payed'] = $this->Marketplace_model->getSalesByUserByState($user_buyer, 1);
			$data['sales_in_process'] = $this->Marketplace_model->getSalesByUserByState($user_buyer, 2);
			$data['sales_closed'] = $this->Marketplace_model->getSalesByUserByState($user_buyer, 3);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function sales_publications(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Exposiciones';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Exposiciones';
			$data['module'] = 'sales_publications';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Information Uploads
			$data['sales_list'] = $this->Marketplace_model->getWorksByArtistID(1);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function sales_questions(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$user_id = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Preguntas';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Preguntas';
			$data['module'] = 'sales_questions';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Get Questions in Sales
			$data['questions_in_articles'] = $this->Marketplace_model->getQuestionsByUser($user_id);
			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function personal_data(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$user_id = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Informacion Personal';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Information
			if (checkIFHaveArtistHelper() == TRUE) {
				$data['user_information'] = $this->User_model->getUserInformation($user_id);
				foreach ($data['user_information']->result() as $usr_nfo) {
					if ($usr_nfo->artist_country != 0) {
						$data['artist_country'] = $this->Location_model->getPais();
						$data['artist_province'] = $this->Location_model->getProvinciaByPais($usr_nfo->artist_country);
						$data['artist_party'] = $this->Location_model->getPartidoByProvincia($usr_nfo->artist_province);
						$data['artist_location'] = $this->Location_model->getLocalidadByPartido($usr_nfo->artist_party);
						$data['artist_neighbour'] = $this->Location_model->getBarrioByLocalidad($usr_nfo->artist_location);
						$data['artist_subneighbour'] = $this->Location_model->getSubBarrioByBarrio($usr_nfo->artist_neighbour);
						$data['statusData'] = TRUE;
					}else{
						$data['statusData'] = FALSE;
					}
				}
			}else{
				$data['user_information'] = $this->User_model->getUserInformationSimple($user_id);
			}

			// Active Module
			if (checkIFHaveArtistHelper() == TRUE) {
				$data['module_title'] = 'Datos Personales: Artista';
				$data['module'] = 'personal_data';
			}else{
				$data['module_title'] = 'Datos Personales: Usuario Simple';
				$data['module'] = 'personal_data';
			}

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function save_Personal_Data()
	{
		if ($this->ion_auth->logged_in()) {

			// User Basic Information
			$user_id = getMyID();
			$first_name = '';
			$last_name = '';
			$dni = '';
			$cuit = '';
			$facebook = '';
			$twitter = '';
			// User Basic Information Serialization
			if (!empty($_POST['first_name'])) {
				$first_name = $_POST['first_name'];
			}
			if (!empty($_POST['last_name'])) {
				$last_name = $_POST['last_name'];
			}
			if (!empty($_POST['dni'])) {
				$dni = $_POST['dni'];
			}
			if (!empty($_POST['cuit'])) {
				$cuit = $_POST['cuit'];
			}
			if (!empty($_POST['facebook'])) {
				$facebook = $_POST['facebook'];
			}
			if (!empty($_POST['twitter'])) {
				$twitter = $_POST['twitter'];
			}
			$dataBasic = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'dni' => $dni,
				'cuit' => $cuit,
				'facebook' => $facebook,
				'twitter' => $twitter
			);
			$this->db->where('id', $user_id);
			$this->db->update('users', $dataBasic);

			// User Artist Information
			$checkTax = $this->User_model->checkIfUserHaveArtist($user_id);
			$artist_birth = '';
			$artist_mail = '';
			$artist_phone = '';
			$artist_adress = '';
			$artist_adress_number = '';
			$artist_zipcode = '';
			$country_selector = '';
			$province_selector = '';
			$party_selector = '';
			$locality_selector = '';
			$suburb_selector = '';
			$subsuburb_selector = '';
			if (!empty($_POST['artist_birth'])) {
				$artist_birth = $_POST['artist_birth'];
			}
			if (!empty($_POST['artist_mail'])) {
				$artist_mail = $_POST['artist_mail'];
			}
			if (!empty($_POST['artist_phone'])) {
				$artist_phone = $_POST['artist_phone'];
			}
			if (!empty($_POST['artist_adress'])) {
				$artist_adress = $_POST['artist_adress'];
			}
			if (!empty($_POST['artist_adress_number'])) {
				$artist_adress_number = $_POST['artist_adress_number'];
			}
			if (!empty($_POST['artist_zipcode'])) {
				$artist_zipcode = $_POST['artist_zipcode'];
			}
			if (!empty($_POST['country_selector'])) {
				$country_selector = $_POST['country_selector'];
			}
			if (!empty($_POST['province_selector'])) {
				$province_selector = $_POST['province_selector'];
			}
			if (!empty($_POST['party_selector'])) {
				$party_selector = $_POST['party_selector'];
			}
			if (!empty($_POST['locality_selector'])) {
				$locality_selector = $_POST['locality_selector'];
			}
			if (!empty($_POST['suburb_selector'])) {
				$suburb_selector = $_POST['suburb_selector'];
			}
			if (!empty($_POST['subsuburb_selector'])) {
				$subsuburb_selector = $_POST['subsuburb_selector'];
			}
			$dataAdvance = array(
				'artist_name' => $first_name,
				'artist_lastname' => $last_name,
				'artist_birth' => $artist_birth,
				'artist_mail' => $artist_mail,
				'artist_phone' => $artist_phone,
				'artist_adress' => $artist_adress,
				'artist_adress_number' => $artist_adress_number,
				'artist_zipcode' => $artist_zipcode,
				'artist_country' => $country_selector,
				'artist_province' => $province_selector,
				'artist_party' => $party_selector,
				'artist_location' => $locality_selector,
				'artist_neighbour' => $suburb_selector,
				'artist_subneighbour' => $subsuburb_selector
			);
			if ($checkTax) {
				$this->db->where('user_id', $user_id);
				$this->db->update('artist_information', $dataAdvance);
			}else{
				$this->db->insert('artist_information', $dataAdvance);
			}
			redirect('dashboard/configuration/personal_data','refresh');

		}
	}
	public function security_data(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Iniciar Sesion';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Seguridad';
			$data['module'] = 'security_data';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}
	public function my_profile(){
		if ($this->ion_auth->logged_in()) {

			// Template Information
			$user_id = getMyID();
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Iniciar Sesion';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Mi Perfil';
			$data['module'] = 'my_profile';

			$data['user_information'] = $this->User_model->getUserInformation($user_id);

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function save_my_profile(){
		if ($this->ion_auth->logged_in()) {

			$user_id = getMyID();

		}
	}

	public function mails_notifications(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Iniciar Sesion';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'E-Mails y Notificaciones';
			$data['module'] = 'mails_notifications';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			$data['notification_config'] = $this->User_model->getMailsNotification($user_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function create_publications(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();
			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Page Information
			$data['title'] = 'Dahra | Publicar Producto';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Publicar Producto';
			$data['module'] = 'uploads/products/create_product';

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function create_new_second_step($product_code){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			$data['product_code'] = $product_code;
			$data['new_product_created'] = $this->Marketplace_model->getProductByCode($product_code);
			$product_id = $data['new_product_created']->result()[0]->product_id;

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Page Information
			$data['title'] = 'Dahra | Publicar Producto Paso Dos';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Publicar Producto: Paso 2';
			$data['module'] = 'uploads/products/create_product_two';
			$data['prod_nfo'] = $this->Marketplace_model->getProductByID($product_id);
			$data['image_list'] = $this->Marketplace_model->getImageSupport($product_id);
			$data['size_list'] = $this->Marketplace_model->getSizeSupport($product_id);
			$data['detail_list'] = $this->Marketplace_model->getDetailsSupport($product_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function edit_publications($product_code){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			$data['product_information'] = $this->Marketplace_model->getProductByCode($product_code);
			$data['product_code'] = $product_code;

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$subcategory_id = $data['product_information']->result()[0]->product_subcategory;
			$data['subcategory'] = $this->Marketplace_model->getSubCategoryByID($subcategory_id);
			$data['selected_cat'] = $this->Marketplace_model->getCategoryBySubCategoryID($subcategory_id)->result()[0]->category_id;

			// Page Information
			$data['title'] = 'Dahra | Editar Producto';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Editar Producto';
			$data['module'] = 'uploads/products/edit_product';

			// Information
			$product_id = $data['product_information']->result()[0]->product_id;
			$data['prod_nfo'] = $this->Marketplace_model->getProductByID($product_id);
			$data['image_list'] = $this->Marketplace_model->getImageSupport($product_id);
			$data['size_list'] = $this->Marketplace_model->getSizeSupport($product_id);
			$data['detail_list'] = $this->Marketplace_model->getDetailsSupport($product_id);
			$data['shipping_detail'] = $this->Marketplace_model->getShippingSupport($product_id);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function product_publication_payment($product_code){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			$data['product_code'] = $product_code;
			$data['new_product_created'] = $this->Marketplace_model->getProductByCode($product_code);
			$product_id = $data['new_product_created']->result()[0]->product_id;

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Page Information
			$data['title'] = 'Dahra | Publicar Producto Acordar Pago';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Calcular Comision';
			$data['module'] = 'uploads/products/payment_publication';

			$data['prod_nfo'] = $this->Marketplace_model->getProductByID($product_id);
			$data['image_list'] = $this->Marketplace_model->getImageSupport($product_id);
			$data['size_list'] = $this->Marketplace_model->getSizeSupport($product_id);
			$data['payment_methods'] = $this->Marketplace_model->getPaymentMethods();

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}


	// Actions for Users
	public function status_publication(){
		if ($this->ion_auth->logged_in()) {

			$publication_status = $_POST['product_status'];
			$publication_id = $_POST['product_id'];
			$dataBasic = array(
				 'product_status' => $publication_status
			);
			$this->db->where('product_id', $publication_id);
			$this->db->update('mrk_product', $dataBasic);
			redirect('dashboard/sales/publications','refresh');

		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function status_republication(){
		if ($this->ion_auth->logged_in()) {

			// $publication_status = 1;
			// $publication_id = $_POST['product_id'];
			// $dataBasic = array(
			// 	 'product_status' => $publication_status
			// );
			// $this->db->where('product_id', $publication_id);
			// $this->db->update('mrk_product', $dataBasic);
			//
			//
			//
			//
			//
			//
			//
			// $exposition_value = $_POST['exposition_val'];
			// $duration_value = $_POST['duration_val'];
			//
			// // Calculate Expiration
			// $months = $duration_value;
			// $actual_date = date('Y-m-d');
			// $parsed_actual_date = strtotime($actual_date);
			// $fechaAgregada = '+' . $months . ' months';
			// $new_date = strtotime($fechaAgregada, $parsed_actual_date);
			//
			// // Serialize Values for Terms Collection
			// $product_id = $_POST['product_id'];
			// $product_code = $_POST['product_code'];
			// $initial_date = date('Y-m-d G:i:s', $parsed_actual_date);
			// $expiration_date = date('Y-m-d G:i:s', $new_date);
			// $product_price = $_POST['product_price'];
			// $site_comission = $_POST['site_comission'];
			// $site_comission_real = $_POST['site_comission_real'];
			// $payment_comission = $_POST['payment_comission'];
			// $payment_comission_real = $_POST['payment_comission_real'];
			// $product_final_earn = $_POST['product_final_earn'];
			//
			// // Prepare Terms Collection
			// $dataSaleTerm = array(
			// 	'product_id' => $product_id,
			// 	'product_code' => $product_code,
			// 	'initial_date' => $initial_date,
			// 	'expiration_date' => $expiration_date,
			// 	'product_price' => $product_price,
			// 	'site_comission' => $site_comission,
			// 	'site_comission_real' => $site_comission_real,
			// 	'payment_comission' => $payment_comission,
			// 	'payment_comission_real' => $payment_comission_real,
			// 	'product_final_earn' => $product_final_earn,
			// 	'exposition' => $exposition_value,
			// 	'status' => 1
			// );
			// $this->db->insert('mrk_product_sale_terms', $dataSaleTerm);
			//
			// // Activate Product and Apply Exposition
			// $dataBasic = array(
			// 	'product_status' => 1,
			// 	'product_exposition' => $exposition_value
			// );
			// $this->db->where('product_code', $product_code);
			// $this->db->update('mrk_product', $dataBasic);




			die;


			redirect('dashboard/sales/publications','refresh');

		}else{
			redirect(base_url(),'refresh');
		}
	}

	// Project Interactions
	public function testCounter()
	{
		$product_id = 1;
		$this->User_model->insertUpdateInCounterByProduct($product_id);
		// echo 'sarasa';
	}

	public function save_product_terms()
	{
		if ($this->ion_auth->logged_in()) {
			$exposition_value = $_POST['range-exposition'];
			$duration_value = $_POST['duration_val'];

			// Calculate Expiration
			$months = $duration_value;
			$actual_date = date('Y-m-d');
			$parsed_actual_date = strtotime($actual_date);
			$fechaAgregada = '+' . $months . ' months';
			$new_date = strtotime($fechaAgregada, $parsed_actual_date);

			// Serialize Values for Terms Collection
			$product_id = $_POST['product_id'];
			$product_code = $_POST['product_code'];
			$initial_date = date('Y-m-d G:i:s', $parsed_actual_date);
			$expiration_date = date('Y-m-d G:i:s', $new_date);

			// Prepare Terms Collection
			$dataSaleTerm = array(
				'product_id' => $product_id,
				'product_code' => $product_code,
				'initial_date' => $initial_date,
				'expiration_date' => $expiration_date,
				'exposition' => $exposition_value,
				'status' => 1
			);
			$this->db->insert('mrk_product_sale_terms', $dataSaleTerm);

			// Activate Product and Apply Exposition
			$dataBasic = array(
				'product_status' => 1,
				'product_exposition' => $exposition_value
			);
			$this->db->where('product_code', $product_code);
			$this->db->update('mrk_product', $dataBasic);

			if ($exposition_value == 4) {
				// Premium Exposition get Premium Ads
				$this->load->model('Ads_model');
				$product_information = $this->Marketplace_model->getProductByID($product_id);
				$product_nfo_prepared = $product_information->result()[0];
				$ads_for_premium_publications = $this->Ads_model->getAdsForPremiumProducts();
				$premium_settings = $ads_for_premium_publications->result()[0];

				$product_code = generateID();
				// Slug Generation
				$ads_code = 'ADS_'. $product_code;
				// Var Serialize
				$ads_author = $active_user;
				$ads_name = $product_nfo_prepared->product_name;
				$ads_text = $product_nfo_prepared->product_desc;
				$ads_adscode = $ads_code;
				$ads_url = base_url() . 'marketplace/product/' . $product_nfo_prepared->product_slug;
				$ads_user_author = $active_user;
				// Generate Ads Simple Collection
				if (!empty($_POST['ads_title'])) {
					$ads_name = $_POST['ads_title'];
				}
				if (!empty($_POST['ads_url'])) {
					$ads_url = $_POST['ads_url'];
				}
				if (!empty($_POST['ads_text'])) {
					$ads_text = $_POST['ads_text'];
				}
				// Prepare Premium Ads Collection
				$dataBasic = array(
					 'mkt_ads_title' => $ads_name,
					 'mkt_ads_adscode' => $ads_code,
					 'mkt_ads_desc' => $ads_text,
					 'mkt_ads_print_initial' => $premium_settings->mrk_product_premium_ads_print_initial,
					 'mkt_ads_url' => $ads_url,
					 'mkt_ads_clickpointer' => $premium_settings->mrk_product_premium_ads_clickpointer,
					 'mkt_ads_image' => $product_nfo_prepared->product_img,
					 'mkt_ads_user_author' => $ads_author,
					 'mkt_ads_relevance' => $premium_settings->mrk_product_premium_ads_relevance,
					 'mkt_status' => 1
				);
				// Insert Ads Simple Collection
				$this->db->insert('mkt_ads', $dataBasic);
				// Premium Exposition get Premium Ads End
			}
			// Redirect to Mail Sender
			// Get Current User Mailing Configuration
			$getUserConfigurationSettings = $this->User_model->getMailNotificationSettings($user_id);
			$flag_send = $getUserConfigurationSettings->publish_work;
			if ($flag_send == 1) {
				redirect('Mailing/published_advise/' . $product_code, 'refresh');
			}else{
				redirect('dashboard/sales/publications','refresh');
			}
		}
	}
}
