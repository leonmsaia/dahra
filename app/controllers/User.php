<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Marketplace_model');
		$this->load->model('User_model');
		$this->load->model('Location_model');
	}

	// View and Holders
	public function login(){

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Login';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Iniciar Sesion';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

  public function register(){

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Register';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Crear Cuenta';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function register_artist(){
		if ($this->ion_auth->logged_in()) {
			$user_id = getMyID();

			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Dashboard';

			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);

			// Page Information
			$data['title'] = 'Dahra | Crear cuenta de Artista';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';

			// Active Module
			$data['module_title'] = 'Crear Cuenta de Artista';
			$data['module'] = 'register_artist';

			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($user_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($user_id);

			// Information
			$data['artist_country'] = $this->Location_model->getPais();
			$data['artist_province'] = $this->Location_model->getProvinciaByPais(1);
			$data['artist_party'] = $this->Location_model->getPartidoByProvincia(1);
			$data['artist_location'] = $this->Location_model->getLocalidadByPartido(1);
			$data['artist_neighbour'] = $this->Location_model->getBarrioByLocalidad(1);
			$data['artist_subneighbour'] = $this->Location_model->getSubBarrioByBarrio(1);

			$this->load->view('layout/' . $data['layout'], $data);

		}else{
			redirect(base_url(),'refresh');
		}
	}

  public function recover(){

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Recover';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Recuperar Cuenta';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}
	public function dashboard(){

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Dashboard';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();

		// Page Information
		$data['title'] = 'Dahra | Iniciar Sesion';
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function my_profile(){
		// Get Data
		$product_author_id = getMyID();
		$data['artist_info'] = $data['artist_info'] = $this->Marketplace_model->getArtistByUserID($product_author_id);
		$product_author = $data['artist_info']->result()[0]->user_id;

		// Template Information
		$data['layout'] = 'Initial';
		$data['page'] = 'page/User/Profile';

		// Page Information
		$artist_obj = $data['artist_info']->result()[0];
		$data['title'] = 'Dahra | ' . $artist_obj->artist_name . ' ' . $artist_obj->artist_lastname;
		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
		$data['keywords'] = 'contactos; ventas, vendedores, bla';
		$data['author'] = 'Match Trade SRL';

		// Info Data Products
		$data['category_list'] = $this->Marketplace_model->getCategories();
		$data['product_list'] = $this->Marketplace_model->getProductsByArtist($product_author);
		$data['artist_favs'] = $this->Marketplace_model->countFavsByArtist($product_author);
		$data['artist_object'] = $artist_obj;
		// Stats

		// General Information
		$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($product_author_id);
		$data['messages'] = $this->User_model->getMessagesNotReadForUser($product_author_id);

		$data['number_works'] = $this->Marketplace_model->countWorksByArtist($product_author);

		$this->load->view('layout/' . $data['layout'], $data);
	}

	// Actions for Users
	public function loginAction(){
		$identity = $_POST['logmail'];
		$password = $_POST['logpass'];
		if(isset($_POST['logrem'])){
		  $remember = 1;
		}else{
		  $remember = 0;
		}
		$this->ion_auth->login($identity, $password, $remember);
		if ($this->ion_auth->login($identity, $password, $remember)){
			$user_id = getMyID();
			// Check If Mail Settings are Saved
			if ($this->User_model->checkIfUserHaveNotificationSetted($user_id) == false) {
				$mailSettings = array('user_id' => $user_id);
				$this->db->insert('users_notification_setting', $mailSettings);
				redirect('Home','refresh');
			}else{
				redirect('Home','refresh');
			}
		}
		else{
			redirect('User/login','refresh');
		}
	}
	public function logUserOut() {
		if ($this->ion_auth->logged_in()) {
			$this->ion_auth->logout();
			redirect('Home','refresh');
		}else{
			redirect('Home','refresh');
		}
	}

	public function createUserAction() {
		if ($this->ion_auth->logged_in()) {
			redirect('Home','refresh');
		}else{
			$regname = $_POST['first_name'];
			$reglastname = $_POST['last_name'];
			$regmail = $_POST['email'];
			$regpass = $_POST['password'];
			$additional_data = array(
				'first_name' => $regname,
				'last_name' => $reglastname,
				'username' => $regmail
			);
			$group = array('2');
			$this->ion_auth->register($regmail, $regpass, $regmail, $additional_data, $group);
			$this->save_contact_in_hubspot($regmail, $regname, $reglastname, $regphone);
			redirect('user/login', 'refresh');
		}
	}

	public function createUserArtistAction() {
		if ($this->ion_auth->logged_in()) {
			redirect('Dashboard','refresh');
		}else{

		}
	}

	public function save_profile_data()
	{
		$user_id = getMyID();
		// User Artist Profile Information
		$checkTax = $this->User_model->checkIfUserHaveArtist($user_id);
		$artist_short_desc = '';
		$artist_past_studies = '';
		$artist_actual_studies = '';
		$artist_bio = '';
		if (!empty($_POST['artist_short_desc'])) {
			$artist_short_desc = $_POST['artist_short_desc'];
		}
		if (!empty($_POST['artist_past_studies'])) {
			$artist_past_studies = $_POST['artist_past_studies'];
		}
		if (!empty($_POST['artist_actual_studies'])) {
			$artist_actual_studies = $_POST['artist_actual_studies'];
		}
		if (!empty($_POST['artist_bio'])) {
			$artist_bio = $_POST['artist_bio'];
		}
		$dataAdvance = array(
			'artist_short_desc' => $artist_short_desc,
			'artist_past_studies' => $artist_past_studies,
			'artist_actual_studies' => $artist_actual_studies,
			'artist_bio' => $artist_bio
		);
		if ($checkTax) {
			$this->db->where('user_id', $user_id);
			$this->db->update('artist_information', $dataAdvance);
		}else{
			$this->db->insert('artist_information', $dataAdvance);
		}
		redirect('dashboard/configuration/my_profile','refresh');
	}

	public function my_messages(){
		if ($this->ion_auth->logged_in()) {
			// Get Data
			$product_author_id = getMyID();
			$data['artist_info'] = $data['artist_info'] = $this->Marketplace_model->getArtistByUserID($product_author_id);
			$product_author = $data['artist_info']->result()[0]->user_id;
			// Template Information
			$data['layout'] = 'Initial';
			$data['page'] = 'page/User/Message';
			// Page Information
			$artist_obj = $data['artist_info']->result()[0];
			$data['title'] = 'Dahra | Mensajes';
			$data['description'] = 'Consegui contactos comerciales de calidad hoy';
			$data['keywords'] = 'contactos; ventas, vendedores, bla';
			$data['author'] = 'Match Trade SRL';
			// Info Data Products
			$data['category_list'] = $this->Marketplace_model->getCategories();
			// Active Module
			$data['module_title'] = 'Mensajes Recibidos';
			$data['module'] = 'message';
			// Stats
			$data['number_works'] = $this->Marketplace_model->countWorksByArtist($product_author);
			// General Information
			$data['credit_qty'] = $this->Marketplace_model->getNumberOfCreditsByUserID($product_author_id);
			$data['messages'] = $this->User_model->getMessagesNotReadForUser($product_author_id);
			// Messages Data
			$data['messages_list'] = $this->User_model->getMessagesForUser($product_author_id);
			$this->load->view('layout/' . $data['layout'], $data);
		}else{
			redirect('user/login', 'refresh');
		}
	}

	public function responseChat()
	{
		if ($this->ion_auth->logged_in()) {
			// Serialize Vars
			$question_author = getMyID();

			$message_original_thread = $_POST['message_original_thread'];
			$message_original = $_POST['message_original'];
			$message_text = $_POST['message_text'];
			$message_read = 1;
			$message_receptor = $_POST['message_receptor'];
			$message_author = $question_author;

			// Prepare Question Collection
			$dataBasic = array(
				 'message_original_thread' => $message_original_thread,
				 'message_text' => $message_text,
				 'message_receptor' => $message_receptor,
				 'message_author' => $message_author
			);
			$this->db->insert('user_message', $dataBasic);

			// Update Original Message
			$originalMessage = array(
				'message_read' => 1
			);
			$this->db->where('message_id', $message_original);
			$this->db->update('user_message', $originalMessage);

			redirect('dashboard/messages', 'refresh');

		}
	}

	public function updateMailConf()
	{
		if ($this->ion_auth->logged_in()) {
			// Serialize Vars
			$user_id = getMyID();
			$question_advise = $_POST['question_advise'];
			$exposition_finished = $_POST['exposition_finished'];
			$publish_work = $_POST['publish_work'];
			$exposition_republished = $_POST['exposition_republished'];
			$exposition_almost_finished = $_POST['exposition_almost_finished'];
			$write_message = $_POST['write_message'];
			$oferts_promotions = $_POST['oferts_promotions'];
			$terms_cond = $_POST['terms_cond'];

			// Update Original Message
			$originalMessage = array(
				'question_work' => $question_advise,
				'finish_work' => $exposition_finished,
				'repub_work' => $exposition_republished,
				'almost_finish_work' => $exposition_almost_finished,
				'publish_work' => $publish_work,
				'write_message' => $write_message,
				'oferts_promotions' => $oferts_promotions,
				'terms_cond' => $terms_cond
			);
			$this->db->where('user_id', $user_id);
			$this->db->update('users_notification_setting', $originalMessage);

			redirect('dashboard/configuration/mails_notifications', 'refresh');

		}
	}

	function save_contact_in_hubspot($email, $firstname, $lastname, $phone)
	{
	    $arr = array(
	        'properties' => array(
	            array(
	                'property' => 'email',
	                'value' => $email
	            ),
	            array(
	                'property' => 'firstname',
	                'value' => $firstname
	            ),
	            array(
	                'property' => 'lastname',
	                'value' => $lastname
	            ),
	            array(
	                'property' => 'phone',
	                'value' => $phone
	            )
	        )
	    );
	    $post_json = json_encode($arr);
	    $hapikey = $this->config->item('hubspot_hapi');
	    $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
	    $ch = @curl_init();
	    @curl_setopt($ch, CURLOPT_POST, true);
	    @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
	    @curl_setopt($ch, CURLOPT_URL, $endpoint);
	    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $response = @curl_exec($ch);
	    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $curl_errors = curl_error($ch);
	    @curl_close($ch);
	    echo "curl Errors: " . $curl_errors;
	    echo "\nStatus code: " . $status_code;
	    echo "\nResponse: " . $response;
	}

	// public function recover() {
	// 	if ($this->ion_auth->logged_in()) {
	// 		redirect('Home','refresh');
	// 	}else{
	// 		// Template Information
	// 		$data['layout'] = 'basic';
	// 		$data['page'] = 'user/recover';
	//
	// 		// Page Information
	// 		$data['title'] = 'Match and Trade | Recuperar Password';
	// 		$data['description'] = 'Consegui contactos comerciales de calidad hoy';
	// 		$data['keywords'] = 'contactos; ventas, vendedores, bla';
	// 		$data['author'] = 'Match Trade SRL';
	//
	// 		$this->load->view('layout/' . $data['layout'], $data);
	// 	}
	// }

}
