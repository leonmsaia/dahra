<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends CI_Model
{
	// Site Information
	public function getUserInformation($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->join('artist_information', 'users.id = artist_information.user_id');
		$result = $this->db->get('users');
		return $result;
	}
	public function getSiteContact()
	{
		$this->db->where('site_contact_id', 1);
		$result = $this->db->get('site_contact');
		return $result;
	}

	public function getSiteConfig()
	{
		$this->db->where('web_conf_id', 1);
		$result = $this->db->get('site_conf');
		return $result;
	}

	public function getSiteTaxConfig()
	{
		$this->db->where('site_tax_id', 1);
		$result = $this->db->get('site_tax');
		return $result;
	}

	public function startMailProtocole($mailNfo)
	{
		$siteConfiguration = $this->Site_model->getSiteConfig()->result()[0];
		$this->db->from('smtp_mail_conf');
		$this->db->where('smtp_conf_id', 1);
		$smtp = $this->db->get();
		$from = $smtp->result()[0]->from;
		$subject = $mailNfo['subject'];
		$sendto = $mailNfo['sendto'];
		$this->email->from($from, $siteConfiguration->site_name);
		$this->email->to($sendto);
		$this->email->reply_to($from, $siteConfiguration->site_name);
		$this->email->subject($subject);
		$this->email->message($mailNfo['message']);
		$this->email->send();
	}

	public function insertOperationInCashflowIngreseType($collection_transaction)
	{
		// Cast Cashflow DB
		$cashflow = $this->load->database('cashflow', TRUE);
		// Get Vars
		$date = date("y-m-d");
		// Prepare Cashflow Insert Ingrese Collection Vars
    $userid = 1;
    $categoryid = 1;
    $accountid = 1;
		$type = 1;
    $name = $collection_transaction['name'];
    $amount = number_format((float)$collection_transaction['amount'], 2, '.', '');
    $reference = $collection_transaction['reference'];
    $transactiondate = $date;
    $description = $collection_transaction['description'];
		// Prepare Cashflow Insert Ingrese Collection
		$dataBasic = array(
			 'userid' => $userid,
			 'categoryid' => $categoryid,
			 'accountid' => $accountid,
			 'name' => $name,
			 'amount' => $amount,
			 'reference' => $reference,
			 'transactiondate' => $transactiondate,
			 'type' => $type,
			 'description' => $description
		);
		$cashflow->insert('transaction', $dataBasic);
		// Prepare Cashflow Insert Ingrese Collection End
	}
}
