<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marketplace_model extends CI_Model
{

	// Marketplace Basic Modules
	public function getProducts()
	{
		$this->db->order_by('product_exposition', 'desc');
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductsAvailable()
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$result = $this->db->get('mrk_product');
		return $result;
	}

	// Marketplace Filter Modules
	public function getProductsByFiltersSubcategory($exposition, $price, $subcategory)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->where('product_exposition', $exposition);
		$this->db->where('product_price <=', $price);
		$this->db->where('product_subcategory', $subcategory);
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function getProductsByFiltersCategory($exposition, $price, $category)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->where('product_exposition', $exposition);
		$this->db->where('product_price <=', $price);
		$this->db->where('product_category', $category);
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function getProductsByFiltersMain($exposition, $price)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->where('product_exposition', $exposition);
		$this->db->where('product_price <=', $price);
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function getProductsByFiltersMainNull($exposition, $price)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->where('product_exposition', $exposition);
		$this->db->where('product_price <=', $price);
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function getProductsByFiltersQuery($exposition, $price, $query)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->where('product_exposition', $exposition);
		$this->db->where('product_price <=', $price);

		$this->db->like('product_name', $query);
		$this->db->or_like('product_desc', $query);
		$this->db->or_like('product_keyword', $query);
		$this->db->or_like('artist_lastname', $query);

		$result = $this->db->get('mrk_product');
		return $result;
	}
	// Marketplace Filter Modules End
	public function getCategories()
	{
		$result = $this->db->get('mrk_category');
		return $result;
	}

	public function getSubCategories()
	{
		$result = $this->db->get('mrk_subcategory');
		return $result;
	}

	public function getCategoryBySlug($category_slug)
	{
		$this->db->where('category_slug', $category_slug);
		$result = $this->db->get('mrk_category');
		return $result;
	}

	public function getCategoryByID($category_id)
	{
		$this->db->where('category_id', $category_id);
		$result = $this->db->get('mrk_category');
		return $result;
	}

	public function checkIfPaymentIsStored($operation_reference)
	{
		$this->db->from('site_recipe_entry_register');
		$this->db->where('code', $operation_reference);
		$haveArtist = $this->db->get();
		if ($haveArtist->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function checkIfMPIsStored($operation_reference)
	{
		$this->db->from('site_recipe_entry_register_mp');
		$this->db->where('external_reference', $operation_reference);
		$haveArtist = $this->db->get();
		if ($haveArtist->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function getCategoryBySubCategoryID($subcategory_id)
	{
		$this->db->where('subcategory_id', $subcategory_id);
		$this->db->join('mrk_subcategory', 'mrk_subcategory.subcategory_category = mrk_category.category_id');
		$result = $this->db->get('mrk_category');
		return $result;
	}

	public function getSubCategoryByID($subcategory_id)
	{
		$this->db->where('subcategory_id', $subcategory_id);
		$result = $this->db->get('mrk_subcategory');
		return $result;
	}

	public function getSubCategoryBySlug($subcategory_slug)
	{
		$this->db->where('subcategory_slug', $subcategory_slug);
		$result = $this->db->get('mrk_subcategory');
		return $result;
	}

	public function getSubCategoryByCategoryID($category_id)
	{
		$this->db->where('subcategory_category', $category_id);
		$result = $this->db->get('mrk_subcategory');
		return $result;
	}

	public function getProductBySlug($product_slug)
	{
		$this->db->where('product_slug', $product_slug);
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductByCode($product_code)
	{
		$this->db->where('product_code', $product_code);
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductByID($product_id)
	{
		$this->db->where('product_id', $product_id);
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductsBySubcategory($product_subcategory_id)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_subcategory', $product_subcategory_id);
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function getProductsBySubcategoryAvailable($product_subcategory_id)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_subcategory', $product_subcategory_id);
		$this->db->where('product_status', 1);
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductsInCategory($category_id)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('category_id', $category_id);
		$this->db->join('mrk_subcategory', 'mrk_product.product_subcategory = mrk_subcategory.subcategory_id');
		$this->db->join('mrk_category', 'mrk_subcategory.subcategory_category = mrk_category.category_id');
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductsInCategoryAvailable($category_id)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('category_id', $category_id);
		$this->db->where('product_status', 1);
		$this->db->join('mrk_subcategory', 'mrk_product.product_subcategory = mrk_subcategory.subcategory_id');
		$this->db->join('mrk_category', 'mrk_subcategory.subcategory_category = mrk_category.category_id');
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getProductsWithCategories()
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->join('mrk_subcategory', 'mrk_product.product_subcategory = mrk_subcategory.subcategory_id');
		$this->db->join('mrk_category', 'mrk_subcategory.subcategory_category = mrk_category.category_id');
		$result = $this->db->get('mrk_product');
		return $result;
	}

	public function getImageSupport($product_id)
	{
		$this->db->where('product_id', $product_id);
		$result = $this->db->get('mrk_product_img_support');
		return $result;
	}

	public function getSizeSupport($product_id)
	{
		$this->db->where('product_id', $product_id);
		$result = $this->db->get('mrk_product_size');
		return $result;
	}

	public function getShippingSupport($product_id)
	{
		$this->db->where('product_id', $product_id);
		$result = $this->db->get('mrk_product_shipping');
		return $result;
	}

	public function getDetailsSupport($product_id)
	{
		$this->db->where('product_id', $product_id);
		$result = $this->db->get('mrk_product_details');
		return $result;
	}

	public function getProductsCompletedDataByQuery($query)
	{
		$this->db->order_by('product_exposition', 'desc');
		$this->db->where('product_status', 1);
		$this->db->like('product_name', $query);
		$this->db->or_like('product_desc', $query);
		$this->db->or_like('product_keyword', $query);
		$this->db->or_like('artist_lastname', $query);
		$this->db->join('artist_information', 'mrk_product.product_author = artist_information.artist_id');
		$this->db->join('mrk_subcategory', 'mrk_product.product_subcategory = mrk_subcategory.subcategory_id');
		$this->db->join('mrk_category', 'mrk_subcategory.subcategory_category = mrk_category.category_id');
		$result = $this->db->get('mrk_product');
		return $result;
	}

	// Artist
	public function getArtists()
	{
		$result = $this->db->get('artist_information');
		return $result;
	}
	public function getArtistInformation($artist_id)
	{
		$this->db->where('artist_id', $artist_id);
		$result = $this->db->get('artist_information');
		return $result;
	}
	public function getWorksByArtistID($product_author)
	{
		$this->db->where('product_author', $product_author);
		$this->db->join('artist_information', 'mrk_product.product_author = artist_information.user_id');
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function getArtistBySlug($artist_slug)
	{
		$this->db->where('artist_slug', $artist_slug);
		$this->db->join('users', 'users.id = artist_information.user_id');
		$result = $this->db->get('artist_information');
		return $result;
	}
	public function getArtistByUserID($user_id)
	{
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('artist_information');
		return $result;
	}
	public function getProductsByArtist($product_author)
	{
		$this->db->where('product_author', $product_author);
		$this->db->limit(6);
		$result = $this->db->get('mrk_product');
		return $result;
	}
	public function countWorksByArtist($artist_id)
	{
		$this->db->where('product_author', $artist_id);
		$this->db->from('mrk_product');
		$result = $this->db->count_all_results();
		return $result;
	}
	public function countProductsByArtistStatus($product_author, $product_status)
	{
		$this->db->where('product_author', $product_author);
		$this->db->where('product_status', $product_status);
		$this->db->from('mrk_product');
		$result = $this->db->count_all_results();
		return $result;
	}
	// Buys & Sales
	public function getBuysByUser($user_buyer)
	{
		$this->db->where('user_buyer', $user_buyer);
		$this->db->join('mrk_product', 'mrk_product.product_id = mrk_sale_register.product_id');
		$this->db->join('users', 'users.id = mrk_sale_register.user_owner');
		$result = $this->db->get('mrk_sale_register');
		return $result;
	}
	public function getSalesByUserByState($user_owner, $state)
	{
		$this->db->where('user_owner', $user_owner);
		$this->db->where('sale_status', $state);
		$this->db->join('mrk_product', 'mrk_product.product_id = mrk_sale_register.product_id');
		$this->db->join('users', 'users.id = mrk_sale_register.user_buyer');
		$result = $this->db->get('mrk_sale_register');
		return $result;
	}
	// Favorites
	public function getUserFavoriteArtist($faver_user)
	{
		$this->db->where('faver_user', $faver_user);
		$this->db->join('artist_information', 'mrk_fav_artist_register.artist_id = artist_information.artist_id');
		$result = $this->db->get('mrk_fav_artist_register');
		return $result;
	}
	public function getUserFavoriteProducts($faver_user)
	{
		$this->db->where('faver_user', $faver_user);
		$this->db->join('mrk_product', 'mrk_fav_prod_register.product_id = mrk_product.product_id');
		$this->db->join('artist_information', 'mrk_product.product_author = artist_information.artist_id');
		$result = $this->db->get('mrk_fav_prod_register');
		return $result;
	}
	public function countFavsByProd($product_id)
	{
		$this->db->where('product_id', $product_id);
		$this->db->from('mrk_fav_prod_register');
		$result = $this->db->count_all_results();
		return $result;
	}
	public function countFavsByArtist($artist_id)
	{
		$this->db->where('artist_id', $artist_id);
		$this->db->from('mrk_fav_artist_register');
		$result = $this->db->count_all_results();
		return $result;
	}
	// Questions
	public function countQuestionsByArtistStatus($user_id, $question_status)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('question_status', $question_status);
		$this->db->from('mkt_question');
		$result = $this->db->count_all_results();
		return $result;
	}
	public function getQuestionsByProduct($product_id)
	{
		$this->db->where('product_id', $product_id);
		$result = $this->db->get('mkt_question');
		return $result;
	}
	public function getQuestionsByQuestionID($question_id)
	{
		$this->db->where('question_id', $question_id);
		$result = $this->db->get('mkt_question');
		return $result;
	}
	public function getQuestionsByQuestionIDWUserNfo($question_id)
	{
		$this->db->where('question_id', $question_id);
		$this->db->join('artist_information', 'mkt_question.user_id = artist_information.user_id');
		$this->db->join('mrk_product', 'mkt_question.product_id = mrk_product.product_id');
		$result = $this->db->get('mkt_question');
		return $result;
	}
	public function getQuestionsByUser($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->join('mrk_product', 'mkt_question.product_id = mrk_product.product_id');
		$this->db->join('users', 'mkt_question.question_author = users.id');
		$result = $this->db->get('mkt_question');
		return $result;
	}
	public function getQuestionsMadeItByUser($question_author)
	{
		$this->db->where('question_author', $question_author);
		$this->db->join('mrk_product', 'mkt_question.product_id = mrk_product.product_id');
		$this->db->join('users', 'mkt_question.question_author = users.id');
		$result = $this->db->get('mkt_question');
		return $result;
	}
	public function getSaleTermsByProdID($product_id)
	{
		$this->db->where('product_id', $product_id);
		$this->db->order_by('initial_date', 'desc');
		$this->db->where('status', 1);
		$this->db->limit(1);
		$result = $this->db->get('mrk_product_sale_terms');
		return $result;
	}
	public function getPaymentMethods()
	{
		$result = $this->db->get('mrk_pay_method');
		return $result;
	}
	public function getPaymentMethodByID($payment_id)
	{
		$this->db->where('pay_method_id', $payment_id);
		$result = $this->db->get('mrk_pay_method');
		return $result;
	}
	// Credit Operations
	public function getNumberOfCreditsByUserID($user_id)
	{
		$this->db->where('mrk_credit_operation_user_id', $user_id);
		$this->db->where('mrk_credit_operation_type', 1);
		$this->db->select_sum('mrk_credit_operation_qty');
		$positive_credits = $this->db->get('mrk_credit_operation')->result()[0]->mrk_credit_operation_qty;

		$this->db->where('mrk_credit_operation_user_id', $user_id);
		$this->db->where('mrk_credit_operation_type', 2);
		$this->db->select_sum('mrk_credit_operation_qty');
		$negative_credits = $this->db->get('mrk_credit_operation')->result()[0]->mrk_credit_operation_qty;

		$result = $positive_credits - $negative_credits;

		return $result;
	}

	public function getStoreConfiguration()
	{
		$this->db->where('mrk_configuration_id', 1);
		$result = $this->db->get('mrk_configuration');
		return $result;
	}

	public function getCreditPlans()
	{
		$result = $this->db->get('mrk_point_plans');
		return $result;
	}

	public function getPointPlanDetails($credit_plan_detail)
	{
		$this->db->where('mrk_point_plans_id', $credit_plan_detail);
		$result = $this->db->get('mrk_point_plans');
		return $result;
	}

	public function getCreditOperationsByUserID($user_id)
	{
		$this->db->order_by('mrk_credit_operation_date', 'desc');
		$this->db->where('mrk_credit_operation_user_id', $user_id);
		$result = $this->db->get('mrk_credit_operation');
		return $result;
	}

	public function countTotalAdvisesLoad()
	{
		$this->db->from('mrk_product');
		$result = $this->db->count_all_results();
		return $result;
	}

	public function countTotalCommercialContacts()
	{
		$this->db->from('user_message');
		$result = $this->db->count_all_results();
		return $result;
	}

	public function countAllValuesFromAdvisesLoad()
	{
		$this->db->where('product_status', 1);
		$this->db->select_sum('product_price');
		$resultPartial = $this->db->get('mrk_product');
		$result = number_format($resultPartial->result()[0]->product_price);
		return $result;
	}

	public function getAnswersToQuestion($question_id)
	{
		$this->db->where('question_id', $question_id);
		$this->db->join('mrk_product', 'mrk_product.product_id = mkt_question_answer.product_id');
		$result = $this->db->get('mkt_question_answer');
		return $result;
	}

	public function getContactUserByContactID($contact_id)
	{
		$this->db->where('message_id', $contact_id);
		$this->db->join('mrk_product', 'mrk_product.product_id = user_message.message_prod_id');
		$result = $this->db->get('user_message');
		return $result;
	}

	public function getCompleteRecipesByUserID($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->join('site_recipe_entry_register_mp', 'site_recipe_entry_register_mp.plane_code = site_recipe_entry_register.code');
		$this->db->join('mrk_credit_operation', 'mrk_credit_operation.mrk_credit_operation_operation_plane_code = site_recipe_entry_register.code');
		$this->db->join('users', 'users.id = site_recipe_entry_register.user_id');
		$result = $this->db->get('site_recipe_entry_register');
		return $result;
	}

	public function getCompleteRecipesByRecipeID($recipe_id)
	{
		$this->db->where('site_recipe_id', $recipe_id);
		$this->db->join('site_recipe_entry_register_mp', 'site_recipe_entry_register_mp.plane_code = site_recipe_entry_register.code');
		$this->db->join('mrk_credit_operation', 'mrk_credit_operation.mrk_credit_operation_operation_plane_code = site_recipe_entry_register.code');
		$this->db->join('users', 'users.id = site_recipe_entry_register.user_id');
		$this->db->join('mrk_point_plans', 'mrk_point_plans.mrk_point_plans_id = site_recipe_entry_register.plan_id');
		$result = $this->db->get('site_recipe_entry_register');
		return $result;
	}

}
