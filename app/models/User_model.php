<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
	// User Information
	public function getUserInformation($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->join('artist_information', 'users.id = artist_information.user_id');
		$result = $this->db->get('users');
		return $result;
	}
	public function getUserInformationSimple($user_id)
	{
		$this->db->where('id', $user_id);
		$result = $this->db->get('users');
		return $result;
	}

	public function getBuyerUserInformation($user_id)
	{
		$this->db->where('id', $user_id);
		$result = $this->db->get('users');
		return $result;
	}

	public function checkIfUserHaveArtist($user_id)
	{
		$this->db->from('artist_information');
		$this->db->where('user_id', $user_id);
		$haveArtist = $this->db->get();
		if ($haveArtist->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function insertUpdateInCounterByProduct($product_id)
	{
		$ip_addres = $this->input->ip_address();
		$this->db->where('product_id', $product_id);
		$this->db->where('ip', $ip_addres);
		$result = $this->db->get('mrk_product_counter');
		if ($result->num_rows() > 0){
			// Work with Counter Date
			$counter_date = $result->result()[0]->date;
			$parsed_counter_date = strtotime($counter_date);
			$parsed_and_plus_date = strtotime('+1 days', $parsed_counter_date);
			$actual_date = date('Y-m-d H:i:s');
			$parsed_actual_date = strtotime($actual_date);
			if ($parsed_actual_date > $parsed_and_plus_date) {
				$product_owner = getProductOwnerByProdID($product_id);
				$save_counter_by_prod = array(
					'product_id' => $product_id,
					'ip' => $ip_addres,
					'product_owner' => $product_owner
				);
				$this->db->insert('mrk_product_counter', $save_counter_by_prod);
			}
		}else{
			// Prepare Terms Collection
			$product_owner = getProductOwnerByProdID($product_id);
			$save_counter_by_prod = array(
				'product_id' => $product_id,
				'ip' => $ip_addres,
				'product_owner' => $product_owner
			);
			$this->db->insert('mrk_product_counter', $save_counter_by_prod);
		}
	}

	public function countAllVisitsByUserID($user_id)
	{
		$this->db->where('product_owner', $user_id);
		$this->db->group_by('value');
		$this->db->select_sum('value');
		$result = $this->db->get('mrk_product_counter');
		$result = $result->result()[0]->value;
		return $result;
	}

	public function countVisitedProdsBySumByUserID($user_id)
	{
		$this->db->where('product_owner', $user_id);
		$this->db->group_by('product_id');
		$this->db->limit(5);
		$this->db->select('product_id, product_owner, value, date, ip');
		$this->db->select_sum('value');
		$result = $this->db->get('mrk_product_counter');
		$graphConstructor = [];
		$total_sum_of_counts = 0;
		foreach ($result->result() as $counts) {
			$total_sum_of_counts = $total_sum_of_counts + $counts->value;
		}
		foreach ($result->result() as $counts) {
			$percent = ($counts->value * 100)/$total_sum_of_counts;
			// echo 'el producto ' . $counts->product_id . ' vale ' . $percent . '%';

			$count_collection = array(
				'y' => $percent,
				'label' => getProductNameByID($counts->product_id),
				'value' => $counts->value
			);
			array_push($graphConstructor, $count_collection);

		}
		$json_values = json_encode($graphConstructor);
		return $json_values;
	}

	public function countVisitedProdsBySumByUserIDByDate($user_id)
	{
		$this->db->where('product_owner', $user_id);
		$this->db->group_by('MONTH(date), YEAR(date)');
		$this->db->select('product_id, product_owner, value, date, ip');
		$this->db->select_sum('value');
		$result = $this->db->get('mrk_product_counter');
		$graphConstructor = [];
		$total_sum_of_counts = 0;
		foreach ($result->result() as $counts) {
			$total_sum_of_counts = $total_sum_of_counts + $counts->value;
		}
		foreach ($result->result() as $counts) {
			$percent = ($counts->value * 100)/$total_sum_of_counts;
			$d = date("Y-m-d", strtotime($counts->date));
			$count_collection = array(
				'date' => $d,
				'value' => $counts->value
			);
			array_push($graphConstructor, $count_collection);
		}
		$json_values = json_encode($graphConstructor);
		return $json_values;
	}

	public function countConsultProdsBySumByUser($user_id)
	{
		$this->db->where('message_receptor', $user_id);
		$this->db->group_by('message_prod_id');
		$this->db->join('mrk_product', 'mrk_product.product_id = user_message.message_prod_id');
		$this->db->limit(5);
		$this->db->select('product_name, message_date, value, product_exposition');
		$this->db->select_sum('value');
		$result = $this->db->get('user_message');
		$graphConstructor = [];
		foreach ($result->result() as $counts) {
			$d = date("Y-m-d", strtotime($counts->message_date));
			$count_collection = array(
				'date' => $d,
				'qty' => $counts->value,
				'prod_exposition' => $counts->product_exposition,
				'prod_name' => $counts->product_name
			);
			array_push($graphConstructor, $count_collection);
		}
		$json_values = json_encode($graphConstructor);
		return $json_values;
	}

	public function getMessagesNotReadForUser($user_id)
	{
		$this->db->where('message_receptor', $user_id);
		$this->db->where('message_read', 0);
		$result = $this->db->count_all_results('user_message');
		return $result;
	}

	public function getMessagesForUser($user_id)
	{
		$this->db->where('message_receptor', $user_id);
		$this->db->group_by('message_author');
		$this->db->join('mrk_product', 'mrk_product.product_id = user_message.message_prod_id');
		$result = $this->db->get('user_message');
		return $result;
	}

	// Mail Notification Methods
	public function getMailsNotification($user_id)
	{
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('users_notification_setting');
		return $result;
	}

	public function checkIfUserHaveNotificationSetted($user_id)
	{
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('users_notification_setting');
		if ($result->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function getMailNotificationSettings($user_id)
	{
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('users_notification_setting');
		return $result->result()[0];
	}

}
