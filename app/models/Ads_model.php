<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ads_model extends CI_Model
{

  // Ads Methods
  public function getAdsAvailables()
  {
    $this->db->order_by('mkt_ads_relevance', 'desc');
    $this->db->order_by('mkt_ads_id', 'RANDOM');
    $this->db->limit(3);
    $this->db->where('mkt_status', 1);
    $result = $this->db->get('mkt_ads');
    return $result;
  }

  public function getAdsByAdsCode($ads_code)
  {
    $this->db->where('mkt_ads_adscode', $ads_code);
    $result = $this->db->get('mkt_ads');
    return $result;
  }

  public function countAdsByPublicistStatus($ads_author, $ads_status)
  {
    $this->db->where('mkt_ads_user_author', $ads_author);
    $this->db->where('mkt_status', $ads_status);
    $this->db->from('mkt_ads');
    $result = $this->db->count_all_results();
    return $result;
  }

  public function getAdsByUserByState($user_owner, $state)
	{
		$this->db->where('mkt_ads_user_author', $user_owner);
		$this->db->where('mkt_status', $state);
		$this->db->join('users', 'users.id = mkt_ads.mkt_ads_user_author');
		$result = $this->db->get('mkt_ads');
		return $result;
	}

  public function getAdviseByUserID($user_id)
	{
		$this->db->where('mkt_ads_user_author', $user_id);
		$this->db->join('users', 'mkt_ads.mkt_ads_user_author = users.id');
		$result = $this->db->get('mkt_ads');
		return $result;
	}

  public function checkIfUserOwnAds($user_id, $ads_code)
  {
    $this->db->where('mkt_ads_user_author', $user_id);
    $this->db->where('mkt_ads_adscode', $ads_code);
		$this->db->join('users', 'mkt_ads.mkt_ads_user_author = users.id');
		$result = $this->db->get('mkt_ads');
    if ($result->num_rows() != 0) {
      return true;
    }else{
      return false;
    }
  }

  // Clics Operations Methods
  public function countAllClicByAdsAuthor($ads_author)
  {
    $this->db->where('mkt_ads_click_count_ads_author', $ads_author);
    $this->db->from('mkt_ads_click_count');
    $result = $this->db->count_all_results();
    return $result;
  }
  public function countAllClicsByAdsAuthorByActualMonth($ads_author)
  {
    $actualMonth = date('m');
    $actualYear = date('Y');
    $this->db->group_by('MONTH(' . $actualMonth . '), YEAR(' . $actualYear . ')');
    $this->db->where('mkt_ads_click_count_ads_author', $ads_author);
    $this->db->from('mkt_ads_click_count');
    $result = $this->db->count_all_results();
    return $result;
  }
  public function getCountClicsDataFromAdsByAdsCodePieGraph($ads_code)
  {
    $this->db->where('mkt_ads_adscode', $ads_code);
		$result = $this->db->get('mkt_ads');
		$info_crude = $result->result()[0];
    $burned_clics = countAllClicsByAdsCode($ads_code);
    $crude_info_array_clicked = array(
      'title' => 'Clics Realizados',
      'value' => $burned_clics
    );
    $crude_info_array_total = array(
      'title' => 'Clics Restantes',
      'value' => $info_crude->mkt_ads_clickpointer - $burned_clics
    );
    $pie_graph_info = array();
    array_push($pie_graph_info, $crude_info_array_clicked);
    array_push($pie_graph_info, $crude_info_array_total);
		$json_object = json_encode($pie_graph_info);
    return $json_object;
  }
  public function getCountClicHistoricalByAdsCode($ads_code)
  {
    $this->db->where('mkt_ads_click_count_ads_code', $ads_code);
    $this->db->order_by('mkt_ads_click_count_date', 'asc');
    $this->db->from('mkt_ads_click_count');
    $result = $this->db->get();
    $stat_graph_info = array();
    foreach ($result->result() as $print_info) {
      $parsed_date = date('Y-m-d', strtotime($print_info->mkt_ads_click_count_date));
      $position = array_search($parsed_date, array_column($stat_graph_info, 'date'));
      if($position !== false) {
      }else{
        $crude_info_array_total = array(
          'date' => $parsed_date,
          'value' => getClicsSumByDateAndAdsCode($ads_code, $print_info->mkt_ads_click_count_date)
        );
        array_push($stat_graph_info, $crude_info_array_total);
      }
    }
    $stat_graph_info_prejson = array_values($stat_graph_info);
    $json_object = json_encode($stat_graph_info_prejson);
    return $json_object;
  }
  // Clics Operations Methods End

  // Prints Operations Methods
  public function countAllPrintsByAdsAuthor($ads_author)
  {
    $this->db->where('mkt_ads_user_author', $ads_author);
    $this->db->join('mkt_ads', 'mkt_ads_print_record.mkt_ads_print_record_ads_code = mkt_ads.mkt_ads_adscode');
    $this->db->from('mkt_ads_print_record');
    $result = $this->db->count_all_results();
    return $result;
  }
  public function countAllPrintsByAdsAuthorByActualMonth($ads_author)
  {
    $actualMonth = date('m');
    $actualYear = date('Y');
    $this->db->group_by('MONTH(' . $actualMonth . '), YEAR(' . $actualYear . ')');
    $this->db->where('mkt_ads_user_author', $ads_author);
    $this->db->join('mkt_ads', 'mkt_ads_print_record.mkt_ads_print_record_ads_code = mkt_ads.mkt_ads_adscode');
    $this->db->from('mkt_ads_print_record');
    $result = $this->db->count_all_results();
    return $result;
  }
  public function getCountPrintsDataFromAdsByAdsCodePieGraph($ads_code)
  {
    $this->db->where('mkt_ads_adscode', $ads_code);
		$result = $this->db->get('mkt_ads');
		$info_crude = $result->result()[0];
    $burned_prints = countAllPrintsByAdsCode($ads_code);
    $crude_info_array_printed = array(
      'title' => 'Impresiones Realizadas',
      'value' => $burned_prints
    );
    $crude_info_array_total = array(
      'title' => 'Impresiones Restantes',
      'value' => $info_crude->mkt_ads_print_initial - $burned_prints
    );
    $pie_graph_info = array();
    array_push($pie_graph_info, $crude_info_array_printed);
    array_push($pie_graph_info, $crude_info_array_total);
		$json_object = json_encode($pie_graph_info);
    return $json_object;
  }
  public function getCountPrintHistoricalByAdsCode($ads_code)
  {
    $this->db->where('mkt_ads_print_record_ads_code', $ads_code);
    $this->db->order_by('mkt_ads_print_record_date', 'asc');
    $this->db->from('mkt_ads_print_record');
    $result = $this->db->get();
    $stat_graph_info = array();
    foreach ($result->result() as $print_info) {
      $parsed_date = date('Y-m-d', strtotime($print_info->mkt_ads_print_record_date));
      $position = array_search($parsed_date, array_column($stat_graph_info, 'date'));
      if($position !== false) {
      }else{
        $crude_info_array_total = array(
          'date' => $parsed_date,
          'value' => getPrintSumByDateAndAdsCode($ads_code, $print_info->mkt_ads_print_record_date)
        );
        array_push($stat_graph_info, $crude_info_array_total);
      }
    }
    $stat_graph_info_prejson = array_values($stat_graph_info);
    $json_object = json_encode($stat_graph_info_prejson);
    return $json_object;
  }
  // Prints Operations Methods End

  public function getAdsByCode($ads_code)
	{
		$this->db->where('mkt_ads_adscode', $ads_code);
		$result = $this->db->get('mkt_ads');
		return $result;
	}

  public function getAdsForPremiumProducts()
	{
		$this->db->where('mrk_product_premium_ads_id', 1);
		$result = $this->db->get('mrk_product_premium_ads_stats');
		return $result;
	}

  public function getAdsPlansAll()
	{
		$result = $this->db->get('mkt_ads_plans');
		return $result;
	}
}
