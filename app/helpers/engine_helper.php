<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Core Engine for Grupo Dahra
// Created by Grupo Dahra
// All Rights Reserved.
// 2018

function checkIfProdIsFavedByUserID($user_id, $prod_id)
{
  $ci =& get_instance();
  $ci->db->where('faver_user', $user_id);
  $ci->db->where('product_id', $prod_id);
  $ci->db->from('mrk_fav_prod_register');
  $result = $ci->db->get();
  if ($result->num_rows() != 0) {
    return TRUE;
  }else{
    return FALSE;
  }
}
function calculateAge($birthDate)
{
  $birthDate = explode("/", $birthDate);
  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[0], $birthDate[2]))) > date("md")
    ? ((date("Y") - $birthDate[2]) - 1)
    : (date("Y") - $birthDate[2]));
  return $age;
}
function getMyID()
{
    $ci =& get_instance();
    $user = $ci->ion_auth->user()->row();
    if(isset($user->id)) {
        return $user->id;
    }else{
    	return null;
    }
}
function questionHaveAnswer($question_id)
{
    $ci =& get_instance();
    $ci->db->where('question_id', $question_id);
    $ci->db->from('mkt_question_answer');
    $result = $ci->db->count_all_results();
    return $result;
}
function answersDetails($question_id)
{
  $ci =& get_instance();
  $ci->db->from('mkt_question_answer');
  $ci->db->where('question_id', $question_id);
  $result = $ci->db->get();
  return $result;
}

function chatHaveAnswer($message_id)
{
    $ci =& get_instance();
    $ci->db->where('message_original_thread', $message_id);
    $ci->db->from('user_message');
    $result = $ci->db->count_all_results();
    return $result;
}
function chatDetails($message_id)
{
  $ci =& get_instance();
  $ci->db->where('message_original_thread', $message_id);
	$ci->db->order_by('message_date', 'ASC');
  $result = $ci->db->get('user_message');
  return $result;
}

function insertPrint($adsCode)
{
  $ci =& get_instance();
  // Get IP Information
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
      $ip = $_SERVER['REMOTE_ADDR'];
  }
  // Prepare Terms Collection
  $dataPrintRecord = array(
    'mkt_ads_print_record_date' => $date = date('Y-m-d'),
    'mkt_ads_print_record_ip' => $ip,
    'mkt_ads_print_record_ads_code' => $adsCode
  );
  $ci->db->insert('mkt_ads_print_record', $dataPrintRecord);
}
function getPrintSumByDateAndAdsCode($ads_code, $print_date)
{
  $ci = get_instance();
  $ci->db->from('mkt_ads_print_record');
  $ci->db->where('mkt_ads_print_record_ads_code', $ads_code);
  $ci->db->where('mkt_ads_print_record_date', $print_date);
  $result = $ci->db->count_all_results();
  return $result;
}
function getClicsSumByDateAndAdsCode($ads_code, $clic_date)
{
  $ci = get_instance();
  $ci->db->from('mkt_ads_click_count');
  $ci->db->where('mkt_ads_click_count_ads_code', $ads_code);
  $ci->db->where('mkt_ads_click_count_date', $clic_date);
  $result = $ci->db->count_all_results();
  return $result;
}

function getUserDetails($user_id)
{
  $ci =& get_instance();
  $ci->db->where('id', $user_id);
  $result = $ci->db->get('users');
  return $result->result()[0];
}

function checkIFHaveArtistHelper()
{
  $user_id = getMyID();
  $ci = get_instance();
  $ci->load->model('User_model');
  $result = $ci->User_model->checkIfUserHaveArtist($user_id);
  return $result;
}

function getExpirationDate($product_id)
{
  $ci = get_instance();
  $ci->db->from('mrk_product_sale_terms');
  $ci->db->where('product_id', $product_id);
  $ci->db->where('status', 1);
  $result = $ci->db->get();
  if ($result->num_rows() != 0) {
    $unix_time = strtotime($result->result()[0]->expiration_date);
    return gmdate('d/m/Y', $unix_time);
  }else{
    return 0;
  }
}
function getProductOwnerByProdID($product_id)
{
  $ci = get_instance();
  $ci->db->from('mrk_product');
  $ci->db->where('product_id', $product_id);
  $result = $ci->db->get();
  return $result->result()[0]->product_author;
}

function getProductNameByID($product_id)
{
  $ci = get_instance();
  $ci->db->from('mrk_product');
  $ci->db->where('product_id', $product_id);
  $result = $ci->db->get();
  return $result->result()[0]->product_name;
}

function getSocialMedia()
{
  $ci = get_instance();
  $ci->db->from('site_contact');
  $ci->db->where('site_contact_id', 1);
  $result = $ci->db->get();
  return $result;
}

function countVisitsInProds($product_id)
{
  $ci = get_instance();
  $ci->db->from('mrk_product_counter');
  $ci->db->where('product_id', $product_id);
  $result = $ci->db->count_all_results();
  return $result;
}

function countAllPrintsByAdsAuthorAndMinusCurrent($ads_author, $ads_code) {
  $ci =& get_instance();
  $ci->db->from('mkt_ads');
  $ci->db->where('mkt_ads_adscode', $ads_code);
  $result = $ci->db->get();
  $actualPrintCount = $result->result()[0]->mkt_ads_print_initial;
  $ci->db->where('mkt_ads_user_author', $ads_author);
  $ci->db->join('mkt_ads', 'mkt_ads_print_record.mkt_ads_print_record_ads_code = mkt_ads.mkt_ads_adscode');
  $ci->db->from('mkt_ads_print_record');
  $actualPrintSpend = $ci->db->count_all_results();
  return $actualPrintCount - $actualPrintSpend;
}

function countAllClicsByAdsAuthorAndMinusCurrent($ads_author, $ads_code) {
  $ci =& get_instance();
  $ci->db->from('mkt_ads');
  $ci->db->where('mkt_ads_adscode', $ads_code);
  $result = $ci->db->get();
  $actualClicCount = $result->result()[0]->mkt_ads_clickpointer;
  $ci->db->where('mkt_ads_click_count_ads_author', $ads_author);
  $ci->db->join('mkt_ads', 'mkt_ads_click_count.mkt_ads_click_count_ads_code = mkt_ads.mkt_ads_adscode');
  $ci->db->from('mkt_ads_click_count');
  $actualClicSpend = $ci->db->count_all_results();
  return $actualClicCount - $actualClicSpend;
}

function countAllClicsByAdsCode($ads_code)
{
  $ci =& get_instance();
  $ci->db->where('mkt_ads_click_count_ads_code', $ads_code);
  $ci->db->from('mkt_ads_click_count');
  $actualClicSpend = $ci->db->count_all_results();
  return $actualClicSpend;
}

function countAllPrintsByAdsCode($ads_code)
{
  $ci =& get_instance();
  $ci->db->where('mkt_ads_print_record_ads_code', $ads_code);
  $ci->db->from('mkt_ads_print_record');
  $actualPritnsSpend = $ci->db->count_all_results();
  return $actualPritnsSpend;
}

function generateID()
{
  $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  $random_string_length = 15;
  $finalID = '';
  $max = strlen($characters) - 1;
  for ($i = 0; $i < $random_string_length; $i++) {
    $finalID .= $characters[mt_rand(0, $max)];
  }
  return $finalID;
}

function getValueOfCreditOperation($credit_operation_id)
{
  $ci =& get_instance();
  $ci->db->where('mrk_credit_operation_operation_id', $credit_operation_id);
  $ci->db->join('site_recipe_entry_register', 'site_recipe_entry_register.code = mrk_credit_operation.mrk_credit_operation_operation_plane_code');
  $ci->db->from('mrk_credit_operation');
  $result = $ci->db->get();
  if ($result->num_rows() > 0)
  {
    $number = $result->result()[0]->amount;
    $value_formatted = number_format((float)$number, 2, '.', '');
    return $value_formatted;
  }
}
function getRecipeIDByReferenceCode($reference_code)
{
  $ci =& get_instance();
  $ci->db->where('mrk_credit_operation_operation_plane_code', $reference_code);
  $ci->db->join('site_recipe_entry_register', 'site_recipe_entry_register.code = mrk_credit_operation.mrk_credit_operation_operation_plane_code');
  $ci->db->from('mrk_credit_operation');
  $result = $ci->db->get();
  if ($result->num_rows() > 0)
  {
    return $result->result()[0]->site_recipe_id;
  }
}
function callAPI($method, $url, $data){
   $curl = curl_init();
   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKEY: 111111111111111111111',
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}
function uploadFile($advise_code, $file, $name, $path, $type, $ext, $deletable) {
  $ci =& get_instance();

  // Check if File Path Exist
  $commonPath = './assets/uploads/' . $type . '/';
  $completePath = $commonPath . $advise_code . '/' . $path;

  if (!file_exists($commonPath . $advise_code)) {
    mkdir($completePath, 0777, true);
  }

  if ($deletable == true) {
    if (file_exists($completePath . '/' . $name . $ext)) {
      unlink($completePath . '/' . $name . $ext);
    }
  }

  // Config Upload Engine
  $config['file_name'] = $name;
  $config['upload_path'] = $commonPath . $advise_code . '/pictures';
  $config['allowed_types'] = 'gif|jpg|png|doc|txt';
  $config['max_size'] = 1024 * 8;
  $config['encrypt_name'] = FALSE;
  // Config Upload Engine End

  // Do Upload
  $ci->load->library('upload', $config);
  $ci->upload->do_upload($file);
}

// Get Mail Configuration
function getMailConfiguration()
{
    $ci =& get_instance();
    $ci->db->from('smtp_mail_conf');
    $ci->db->where('smtp_conf_id', 1);
    $malRend = $ci->db->get();
    $mlCnfVal = null;
    if ($malRend->num_rows() > 0)
    {
        foreach ($malRend->result() as $mlCnf)
        {
            $mlCnfVal = array(
                'smtp_host' => $mlCnf->smtp_host,
                'smtp_port' => $mlCnf->smtp_port,
                'smtp_timeout' => $mlCnf->smtp_timeout,
                'smtp_user' => $mlCnf->smtp_user,
                'smtp_pass' => $mlCnf->smtp_pass,
                'smtp_charset' => $mlCnf->smtp_charset,
                'smtp_newline' => $mlCnf->smtp_newline,
                'smtp_mailtype' => $mlCnf->smtp_mailtype,
                'smtp_validation' => $mlCnf->smtp_validation
            );
        }
        return $mlCnfVal;
    }else{
        return null;
    }
}
