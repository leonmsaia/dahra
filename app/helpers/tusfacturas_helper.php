<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Helper Library of Tus Facturas API for Grupo Dahra
// Created by Grupo Dahra
// All Rights Reserved.
// 2018

function getSystemEstate()
{
  $ci =& get_instance();
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  // Api Call Credentials End

  $arrayName = array(
    'usertoken' => $UserToken,
    'apikey' => $APIkey,
    'apitoken' => $APIToken
  );
  $send_request_test = json_encode($arrayName);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/app/api/v2/estado_servicios/alertas', json_encode($arrayName));
  $response = json_decode($get_data, true);
  print_r($response);
}

function getMeditionUnits()
{
  $ci =& get_instance();
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  // Api Call Credentials End

  $medition_units_collection = array(
    'usertoken' => $UserToken,
    'apikey' => $APIkey,
    'apitoken' => $APIToken
  );
  $send_request_test = json_encode($medition_units_collection);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/app/api/v2/tablas_referencia/unidades_medida', json_encode($medition_units_collection));
  $response = json_decode($get_data, true);
  print_r($response);
}

function getCountryList()
{
  $ci =& get_instance();
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  // Api Call Credentials End

  $get_country_list_collection = array(
    'usertoken' => $UserToken,
    'apikey' => $APIkey,
    'apitoken' => $APIToken
  );
  $send_request_test = json_encode($get_country_list_collection);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/app/api/v2/tablas_referencia/paises', json_encode($get_country_list_collection));
  $response = json_decode($get_data, true);
  print_r($response);
}

function getAfipDolarValue()
{
  $ci =& get_instance();
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  // Api Call Credentials End

  $afip_dollar_value_collection = array(
    'usertoken' => $UserToken,
    'apikey' => $APIkey,
    'apitoken' => $APIToken
  );
  $send_request_test = json_encode($afip_dollar_value_collection);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/app/api/v2/tablas_referencia/cotizacion', json_encode($afip_dollar_value_collection));
  $response = json_decode($get_data, true);
  print_r($response);
}

function getMonthValueParalelByProvinceID($province_id)
{
  if ($province_id == 2) {
    return 1;
  }else{
    $ci =& get_instance();
    $ci->db->where('id', $province_id);
    $ci->db->from('loc_provincia');
    $result = $ci->db->get();
    if ($result->num_rows() != 0) {
      $result_comparative_val = $result->result()[0];
      $province_db_name = strtoupper($result->result()[0]->nombre);
      $province_list = array(
        'BUENOS AIRES' => 2,
        'CATAMARCA' => 3,
        'CHACO' => 4,
        'CHUBUT' => 5,
        'CIUDAD AUTONOMA DE BUENOS AIRES' => 1,
        'CORDOBA' => 6,
        'CORRIENTES' => 7,
        'ENTRE RIOS' => 8,
        'FORMOSA' => 9,
        'JUJUY' => 10,
        'LA PAMPA' => 11,
        'LA RIOJA' => 12,
        'MENDOZA' => 13,
        'MISIONES' => 14,
        'NEUQUEN' => 15,
        'Otro' => 25,
        'RIO NEGRO' => 16,
        'SALTA' => 17,
        'SAN JUAN' => 18,
        'SAN LUIS' => 19,
        'SANTA CRUZ' => 20,
        'SANTA FE' => 21,
        'SANTIAGO DEL ESTERO' => 22,
        'TIERRA DEL FUEGO' => 23,
        'TUCUMAN' => 24,
        '-' => 26,
      );
      foreach ($province_list as $key => $id) {
        if ($province_db_name == $key) {
          $province_afip_val = $id;
        }
      }
      return $province_afip_val;
    }else{
      return false;
    }
  }
}

function getTaxPercentByUserDNI($user_dni_number, $user_dni_type)
{
  $ci =& get_instance();
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  // Api Call Credentials End

  $tax_percent_per_user_collection = array(
    'usertoken' => $UserToken,
    'apikey' => $APIkey,
    'apitoken' => $APIToken,
    'cliente' => array(
      'documento_nro' => $user_dni_number,
      'documento_tipo' => $user_dni_type
    )
  );
  $send_request_test = json_encode($tax_percent_per_user_collection);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/app/api/v2/tablas_referencia/cotizacion', json_encode($tax_percent_per_user_collection));
  $response = json_decode($get_data, true);
  print_r($response);
}

function getLastBillNumeration()
{
  $ci =& get_instance();
  $fiscalDataCrude = $ci->Site_model->getSiteTaxConfig();
  $fiscalData =  $fiscalDataCrude->result()[0];
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  // Api Call Credentials End

  $last_bill_numeration = array(
    'usertoken' => $UserToken,
    'apikey' => $APIkey,
    'apitoken' => $APIToken,
    'comprobante' => array(
      'tipo' => $fiscalData->billing_type,
      'operacion' => $fiscalData->operation,
      'punto_venta' => $fiscalData->sale_point
    )
  );
  $send_request_test = json_encode($last_bill_numeration);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/app/api/v2/facturacion/numeracion', json_encode($last_bill_numeration));
  $response = json_decode($get_data, true);
  return $response['comprobante']['numero'];
}

function generateBillingTypeC($payment_billing_collection)
{
  $ci =& get_instance();
  // Api Call Credentials
  $APIkey =	$ci->config->item('tusfacturas_APIkey');
  $APIToken	= $ci->config->item('tusfacturas_APIToken');
  $UserToken = $ci->config->item('tusfacturas_UserToken');
  $ci->load->model('Site_model');
  $fiscalDataCrude = $ci->Site_model->getSiteTaxConfig();
  $fiscalData =  $fiscalDataCrude->result()[0];
  // Api Call Credentials End
  $generate_billing_c = array(
    'apitoken' => $UserToken,
    'cliente' => array(
      'documento_tipo' => 'DNI',
      'condicion_iva' => 'CF',
      'domicilio' => $payment_billing_collection->street_and_number,
      'condicion_pago' => 0,
      'documento_nro' => $payment_billing_collection->dni,
      'razon_social' => $payment_billing_collection->first_name . ' ' . $payment_billing_collection->last_name,
      'provincia' => getMonthValueParalelByProvinceID($payment_billing_collection->provincia),
      // 'email' => $payment_billing_collection['email'],
      'email' => 'leonmsaia@gmail.com',
      'envia_por_mail' => 'S'
    ),
    'apikey' => $APIkey,
    'comprobante' => array(
      'rubro' => $fiscalData->entry_type,
      'percepciones_iva' => $fiscalData->tax_perception,
      'tipo' => $fiscalData->billing_type,
      'numero' => getLastBillNumeration(),
      'percepciones_iibb' => $fiscalData->perception_iibb,
      'bonificacion' => $fiscalData->bonus,
      'operacion' => $fiscalData->operation,
      'detalle' => array(
        array(
          'cantidad' => 1,
          'producto' => array(
            'descripcion' => 'Plan: ' . $payment_billing_collection->mrk_point_plans_title . '. ' . $payment_billing_collection->mrk_point_plans_qty . ' Tokens.',
            'codigo' => $payment_billing_collection->code,
            'lista_precios' => $fiscalData->price_list,
            'leyenda' => '',
            'unidad_bulto' => $fiscalData->unity,
            'alicuota' => $fiscalData->tax_cuote,
            'precio_unitario_sin_iva' => number_format((float)$payment_billing_collection->amount, 2, '.', '')
          )
        )
      ),
      'fecha' => date('d/m/Y', strtotime($payment_billing_collection->mrk_point_plans_date)),
      'rubro_grupo_contable' => $fiscalData->tax_group,
      'total' => number_format((float)$payment_billing_collection->amount, 2, '.', ''),
      'cotizacion' => 1,
      'moneda' => $fiscalData->currency,
      'punto_venta' => $fiscalData->sale_point,
      'impuestos_internos' => $fiscalData->internal_tax
    ),
    'usertoken' => $UserToken
  );
  $send_request_test = json_encode($generate_billing_c, JSON_PRETTY_PRINT);
  $get_data = callAPI('POST', 'https://www.tusfacturas.com.ar/api/v2/facturacion/nuevo', json_encode($generate_billing_c));
  $response = json_decode($get_data, true);
}
