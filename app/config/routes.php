<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Basic System Pages
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Basic User Pages
$route['login'] = "User/login";
$route['register'] = "User/register";
$route['register/artist'] = "User/register_artist";
$route['recover'] = "User/recover";

// Dashboard User Pages
$route['dashboard'] = "Dashboard/dashboard";
$route['dashboard/subscription'] = "Dashboard/subscription";
$route['dashboard/subscription/history'] = "Dashboard/subscription_history";
$route['dashboard/view_recipe/(:any)'] = "Dashboard/recipe_gen/$1";
$route['dashboard/stats'] = "Dashboard/stats";
$route['dashboard/buys'] = "Dashboard/buys";
$route['dashboard/buys/favorites'] = "Dashboard/buys_favorites";
$route['dashboard/buys/questions'] = "Dashboard/buys_questions";
$route['dashboard/sales'] = "Dashboard/sales";
$route['dashboard/sales/publications'] = "Dashboard/sales_publications";
$route['dashboard/sales/questions'] = "Dashboard/sales_questions";
$route['my_profile'] = "User/my_profile";
$route['dashboard/messages'] = "User/my_messages";

// User Configuration Pages
$route['dashboard/configuration/personal_data'] = "Dashboard/personal_data";
$route['dashboard/configuration/security_data'] = "Dashboard/security_data";
$route['dashboard/configuration/my_profile'] = "Dashboard/my_profile";
$route['dashboard/configuration/mails_notifications'] = "Dashboard/mails_notifications";

// Internal Pages
$route['contact'] = "Internal/contact";
$route['about'] = "Internal/about";
$route['terms'] = "Internal/terms";
$route['privacity'] = "Internal/privacity";
$route['atention'] = "Internal/atention";
$route['services'] = "Internal/services";
$route['advises'] = "Internal/advises";

// Publish Process Pages
$route['marketplace/post'] = "Dashboard/create_publications";
$route['marketplace/post/second_step/(:any)'] = "Dashboard/create_new_second_step/$1";
$route['marketplace/post/payment/(:any)'] = "Dashboard/product_publication_payment/$1";
$route['marketplace/post/republish/(:any)'] = "Dashboard/product_publication_payment/$1";
$route['marketplace/post/edit/(:any)'] = "Dashboard/edit_publications/$1";
$route['marketplace/post/terms'] = "Marketplace/post_terms";
$route['marketplace/post/shipping'] = "Marketplace/post_shipping";
$route['user/buy_credits'] = "Marketplace/creditMarket";
$route['user/payment_process/success'] = "Marketplace/payment_process_success";
$route['user/payment_process/failure'] = "Marketplace/payment_process_failure";
$route['user/payment_process/pending'] = "Marketplace/payment_process_pending";

// Marketplace Pages
$route['marketplace/category/(:any)'] = "Marketplace/Category/$1";
$route['marketplace/subcategory/(:any)'] = "Marketplace/Subcategory/$1";
$route['marketplace/product/(:any)'] = "Marketplace/advise/$1";
$route['marketplace/search/(:any)'] = "Marketplace/Product/$1";
$route['marketplace/tag/(:any)'] = "Marketplace/Product/$1";

// Artist Pages
$route['artist/(:any)'] = "Marketplace/Artist/$1";
$route['artist/(:any)/store'] = "Marketplace/Artist_Store/$1";
$route['artist/advertising'] = "Marketplace/Artist_Advertising_Me";

// Ads Pages
$route['ads/(:any)'] = "Ads/adsRedirect/$1";
$route['publicist/register'] = "Ads/register";
$route['publicist/dashboard'] = "Ads/dashboard";
$route['publicist/subscription'] = "Ads/subscription";
$route['publicist/subscription/history'] = "Ads/subscription_history";
$route['publicist/buy_credits'] = "Ads/creditMarket";
$route['publicist/ads'] = "Ads/ads";
$route['publicist/stats'] = "Ads/stats";
$route['publicist/stats/(:any)'] = "Ads/ads_stats_detail/$1";
$route['publicist/configuration/personal_data'] = "Ads/personal_data";
$route['publicist/configuration/security_data'] = "Ads/security_data";

// Ads Publications Pages
$route['ads/publication/create_ads'] = "Ads/create_ads";
$route['ads/publication/create_ads_second_step/(:any)'] = "Ads/create_new_second_step/$1";
$route['ads/publication/edit_ads/(:any)'] = "Ads/edit_ads/$1";
$route['ads/publication/edit_basic_ads'] = "Ads/edit_basic_ads";
$route['ads/publication/payment_publication/(:any)'] = "Ads/ads_publication_payment/$1";
$route['ads/publication/republish/(:any)'] = "Ads/ads_publication_payment/$1";
$route['ads/publication/pay_process'] = "Ads/save_ads_terms/";

// Ads Payment Pages
$route['ads/payment_process/success'] = "Ads/payment_process_success";
$route['ads/payment_process/failure'] = "Ads/payment_process_failure";
$route['ads/payment_process/pending'] = "Ads/payment_process_pending";
